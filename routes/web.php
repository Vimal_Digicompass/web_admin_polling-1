<?php

use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\KYC\PendingKYCController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Useractivity\UpgradeUserController;
use App\Http\Controllers\KYC\KYCController;
use App\Http\Controllers\KYC\RejectedKYCController;
use App\Http\Controllers\KYC\ApprovalKYCController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\User\TemplateController;
use App\Http\Controllers\User\RoleController;
use App\Http\Controllers\User\PermissionController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Post\RejectedPostController;
use App\Http\Controllers\Post\ApprovedPostController;
use App\Http\Controllers\Post\PostController;
use App\Http\Controllers\Payment\WalletRechargeFailedController;
use App\Http\Controllers\Payment\CommonerPackageExpiryController;
use App\Http\Controllers\Payment\PaymentPendingController;
use App\Http\Controllers\Useractivity\NotPostController;
use App\Http\Controllers\Useractivity\NotEnquiryPostController;
use App\Http\Controllers\Useractivity\NotPostServiceReqController;
use App\Http\Controllers\Useractivity\MarkedFavouriteController;
use App\Http\Controllers\Useractivity\InactiveUserController;
use App\Http\Controllers\Useractivity\LeadsNotViewedController;
use App\Http\Controllers\User\PerformanceTestController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
// Dashboard
Route::post('/getdashboarddata', [DashboardController::class, 'getdashboarddata'])->name('getdashboarddata');
Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});
//indexpage
Route::get('/', [LoginController::class, 'index'])->name('/');
// Route::get('home', [LoginController::class, 'index'])->name('home');

//get User info
Route::post('getuserdetail', [UserController::class, 'getuserdetail'])->name('getuserdetail');
Route::post('teamleadgetuserdetail', [UserController::class, 'teamleadgetuserdetail'])->name('teamleadgetuserdetail');

//logout
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

//update user status
Route::post('updatecallstatus', [Controller::class, 'updatecallstatus'])->name('updatecallstatus');
Route::post('getchangelog', [Controller::class, 'getchangelog'])->name('getchangelog');
Route::post('getdetailview', [Controller::class, 'getdetailview'])->name('getdetailview');
Route::post('sendnotification', [Controller::class, 'sendnotification'])->name('sendnotification');


//KYC related routes
Route::group(
    ['prefix' => 'kyc'],
    function () {
        Route::get('getkycrejectionstatus',[KYCController::class, 'getkycrejectionstatus'])->name('getkycrejectionstatus');
        Route::post('getkycdocumentstatus', [KYCController::class, 'getkycdocumentstatus'])->name('getkycdocumentstatus');
        Route::post('getkycapprovestatus', [KYCController::class, 'getkycapprovestatus'])->name('getkycapprovestatus');
        Route::get('pending/{id}', [PendingKYCController::class, 'index'])->name('pending');
        Route::get('approve/{id}', [ApprovalKYCController::class, 'index'])->name('aprove');
        Route::get('rejected/{id}', [RejectedKYCController::class, 'index'])->name('reject');
        Route::post('getkycapprovallist', [ApprovalKYCController::class, 'getkycapprovallist'])->name('getkycapprovallist');
        Route::post('getkycpendinglist', [PendingKYCController::class, 'getkycpendinglist'])->name('getkycpendinglist');
        Route::post('getkycrejectedlist', [RejectedKYCController::class, 'getkycrejectedlist'])->name('getkycrejectedlist');
        Route::post('pendingexcelexport', [PendingKYCController::class, 'excelexport'])->name('pendingexcelexport');
        Route::post('approvalexcelexport', [ApprovalKYCController::class, 'excelexport'])->name('approveexcelexport');
        Route::post('rejectedexcelexport', [RejectedKYCController::class, 'excelexport'])->name('rejectexcelexport');
        Route::post('updatekycstatus', [KYCController::class, 'updatekycstatus'])->name('updatekycstatus');
        Route::post('updatekycdocument', [KYCController::class, 'updatekycdocument'])->name('updatekycdocument');
        Route::post('getkycdocument', [KYCController::class, 'getkycdocument'])->name('getkycdocument');
        Route::post('approverejectkycdoc', [KYCController::class, 'approverejectkycdoc'])->name('approverejectkycdoc');
        Route::post('docdownload', [KYCController::class, 'docdownload'])->name('docdownload');
        Route::get('download/{documenttype}/{id}/{filetype}', [KYCController::class, 'download'])->name('download');
        Route::post('getrejectstatus', [KYCController::class, 'getrejectstatus'])->name('getrejectstatus');
        Route::post('userstatusupdate', [KYCController::class, 'statusupdate'])->name('userstatusupdate');
        Route::post('getKYCStatusInfo', [KYCController::class, 'getKYCStatusInfo'])->name('getKYCStatusInfo');
    }
);

//Payment related related routes
Route::group(
    ['prefix' => 'payment'],
    function () {
        Route::get('rechargefailed/{id}', [WalletRechargeFailedController::class, 'index'])->name('rechargefailed');
        Route::post('getwalletrechargefailedlist', [WalletRechargeFailedController::class, 'getwalletrechargefailedlist'])->name('getwalletrechargefailedlist');
        Route::post('walletrechargefailedexport', [WalletRechargeFailedController::class, 'walletrechargefailedexport'])->name('walletrechargefailedexport');
        Route::post('gettoptransaction', [WalletRechargeFailedController::class, 'gettoptransaction'])->name('gettoptransaction');
        Route::get('packageexpiry/{id}', [CommonerPackageExpiryController::class, 'index'])->name('packageexpiry');
        Route::post('getwalletexpirylist', [CommonerPackageExpiryController::class, 'getwalletexpirylist'])->name('getwalletexpirylist');
        Route::post('walletexpiryexport', [CommonerPackageExpiryController::class, 'walletexpiryexport'])->name('walletexpiryexport');
        Route::get('paymentpending/{id}', [PaymentPendingController::class, 'index'])->name('paymentpending');
        Route::post('getnotmakepaymentlist', [PaymentPendingController::class, 'getnotmakepaymentlist'])->name('getnotmakepaymentlist');
        Route::post('notmakepaymentexport', [PaymentPendingController::class, 'notmakepaymentexport'])->name('notmakepaymentexport');
    }
);

//Useractivity related related routes
Route::group(
    ['prefix' => 'useractivity'],
    function () {

        Route::get('upgradeuser/{id}', [UpgradeUserController::class, 'index'])->name('upgradeuser');
        Route::post('upgradeuserexport', [UpgradeUserController::class, 'excelexport'])->name('excelexport');
        Route::post('getupgradeuserlist', [UpgradeUserController::class, 'getupgradeuserlist'])->name('getupgradeuserlist');
        Route::get('notenquirypost/{id}', [NotEnquiryPostController::class, 'index'])->name('notenquirypost');
        Route::post('getnotenquirepostlist', [NotEnquiryPostController::class, 'getnotenquirepostlist'])->name('getnotenquirepostlist');
        Route::post('nepexport', [NotEnquiryPostController::class, 'nepexport'])->name('nepexport');
        Route::post('npsexport', [NotPostServiceReqController::class, 'npsexport'])->name('npsexport');
        Route::post('inactiveexport', [InactiveUserController::class, 'inactiveexport'])->name('inactiveexport');
        Route::get('notpost/{id}', [NotPostController::class, 'index'])->name('notpost');
        Route::get('notpostservicereq/{id}', [NotPostServiceReqController::class, 'index'])->name('notpostservicereq');
        Route::post('getnotpostservicereqlist', [NotPostServiceReqController::class, 'getnotpostservicereqlist'])->name('getnotpostservicereqlist');
        Route::post('getnotpostuserlist', [NotPostController::class, 'getnotpostuserlist'])->name('getnotpostuserlist');
        Route::post('notpostexport', [NotPostController::class, 'notpostexport'])->name('notpostexport');
        Route::post('getinactiveuserlist', [InactiveUserController::class, 'getinactiveuserlist'])->name('getinactiveuserlist');
        Route::get('markedfavourite/{id}', [MarkedFavouriteController::class, 'index'])->name('markedfavourite');
        Route::get('inactiveuser/{id}', [InactiveUserController::class, 'index'])->name('inactiveuser');
        Route::get('leadsnotviewed/{id}', [LeadsNotViewedController::class, 'index'])->name('leadsnotviewed');
        Route::post('getservicesubcategory', [Controller::class, 'getservicesubcategory'])->name('getservicesubcategory');
        Route::post('getpropertycategory', [Controller::class, 'getpropertycategory'])->name('getpropertycategory');
        Route::post('getleadsnotviewedlist', [LeadsNotViewedController::class, 'getleadsnotviewedlist'])->name('getleadsnotviewedlist');
        Route::post('leadsnotviewexport', [LeadsNotViewedController::class, 'leadsnotviewexport'])->name('leadsnotviewexport');
        Route::post('getmarkedfavouritelist', [MarkedFavouriteController::class, 'getmarkedfavouritelist'])->name('getmarkedfavouritelist');
        Route::post('markedfavouriteexport', [MarkedFavouriteController::class, 'markedfavouriteexport'])->name('markedfavouriteexport');
    }
);

//Post related routes
Route::group(
    ['prefix' => 'post'],
    function () {
        Route::get('rejected/{id}', [RejectedPostController::class, 'index'])->name('postrejected');
        Route::get('approved/{id}', [ApprovedPostController::class, 'index'])->name('postapprove');
        Route::post('getpostlist', [PostController::class, 'getpostlist'])->name('getpostlist');
        Route::post('updatepoststatus', [PostController::class, 'updatepoststatus'])->name('updatepoststatus');
        Route::post('publishstatusupdate', [PostController::class, 'publishstatusupdate'])->name('publishstatusupdate');
        Route::post('postexport', [PostController::class, 'postexport'])->name('postexport');
        Route::Post('deleteimage', [PostController::class, 'deleteimage'])->name('deleteimage');
        Route::Post('getpoststatus', [PostController::class, 'getpoststatus'])->name('getpoststatus');
        Route::Post('editpost', [PostController::class, 'editpost'])->name('editpost');
        Route::get('edit/{type}/{id}/{criteriaId}', [PostController::class, 'editview'])->name('editview');
        Route::Post('editflat', [PostController::class, 'editflat'])->name('editflat');
        Route::get('unit/flat/{id}', [PostController::class, 'unitandflat'])->name('unitandflat');
        Route::Post('flatdetails', [PostController::class, 'flatdetails'])->name('flatdetails');
        Route::Post('getrejectionstatus', [PostController::class, 'getrejectionstatus'])->name('getrejectionstatus');
        Route::Post('getpostapprovestatus', [PostController::class, 'getpostapprovestatus'])->name('getpostapprovestatus');
    }
);


Route::get('notification', [UserController::class, 'notification'])->name('notification');
Route::get('serviceperformancetest', [PerformanceTestController::class, 'serviceperformancetest'])->name('serviceperformancetest');
Route::get('propertyperformancetest', [PerformanceTestController::class, 'propertyperformancetest'])->name('propertyperformancetest');

//User creation related routes
Route::group(
    ['prefix' => 'users'],
    function () {
        Route::get('notificationajaxcall', [UserController::class, 'notificationajaxcall'])->name('notificationajaxcall');
        Route::get('getnotificationlist', [UserController::class, 'getnotificationlist'])->name('getnotificationlist');
        Route::get('lists', [UserController::class, 'index'])->name('lists');
        Route::get('roles', [RoleController::class, 'index'])->name('roles');
        Route::get('permissions', [PermissionController::class, 'index'])->name('permission');
        Route::get('permissions/{id}', [PermissionController::class, 'index'])->name('permissionwithid');
        Route::post('rolecreate', [RoleController::class, 'create'])->name('rolecreate');
        Route::post('roledelete', [RoleController::class, 'delete'])->name('roledelete');
        Route::post('getstate', [UserController::class, 'getstate'])->name('getstate');
        Route::post('usercreate', [UserController::class, 'create'])->name('usercreate');
        Route::post('statusupdate', [UserController::class, 'statusupdate'])->name('statusupdate');
        Route::post('deleteprofilepic', [UserController::class, 'deleteprofilepic'])->name('deleteprofilepic');
        Route::post('profileupdate', [UserController::class, 'profileupdate'])->name('profileupdate');
        Route::post('getrolebasedpermission', [PermissionController::class, 'getrolebasedpermission'])->name('getrolebasedpermission');
        Route::post('updaterolebasedpermission', [PermissionController::class, 'updaterolebasedpermission'])->name('updaterolebasedpermission');
        Route::post('getteamlead', [UserController::class, 'getteamlead'])->name('getteamlead');
        Route::get('assigntopoll', [Controller::class, 'assigntopoll'])->name('assigntopoll');
        Route::post('getuser', [Controller::class, 'getuser'])->name('getuser');
        Route::post('getpermission', [Controller::class, 'getpermission'])->name('getpermission');
        Route::post('reassignlogic', [Controller::class, 'reassignlogic'])->name('reassignlogic');
        Route::get('/schedules', [TemplateController::class, 'criteriaSchedules']);
        Route::post('/storeandedit', [TemplateController::class, 'storeOrEdit'])->name('storeandedit');
        Route::post('/getScheduleDetail', [TemplateController::class, 'getScheduleDetail'])->name('getScheduleDetail');
        Route::post('enduserupdate', [UserController::class, 'enduserupdate'])->name('enduserupdate');
    }
);

//Authentication related routes
Route::group(['prefix' => 'admin'], function () {
    Route::get('forgotpassword', [ForgotPasswordController::class, 'index'])->name('forgotpassword');
    Route::get('changepassword', [ResetPasswordController::class, 'index'])->name('changepassword');
    Route::post('changepasswordupdate', [ResetPasswordController::class, 'changepasswordupdate'])->name('changepasswordupdate');
    Route::post('sendresetlink', [ForgotPasswordController::class, 'sendresetlink'])->name('sendresetlink');
    Route::get('updatepassword/{id}', [ForgotPasswordController::class, 'updatepassword'])->name('updatepassword/{id}');
    Route::post('resetpassword/', [ForgotPasswordController::class, 'resetpassword'])->name('resetpassword');
    Route::post('updateonlinestatus/', [LoginController::class, 'updateonlinestatus'])->name('updateonlinestatus');
});
