<nav class="navbar">
    <a href="#" class="sidebar-toggler">
        <i data-feather="menu"></i>
    </a>
    <div class="navbar-content">

        <ul class="navbar-nav">

            <li class="nav-item dropdown nav-notifications" id="nav-notifications">

            </li>


            <li class="nav-item dropdown nav-profile">
                <a class="nav-link dropdown-toggle" href="#" id="profileDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    @php

                        $path = Auth::user()->image_name;
                        $data = getFileURLPublicPath($path);
                        if ($data != '') {
                            $imageurl = $base64url = $data;
                        } else {
                            $base64url = 'https://via.placeholder.com/80x80';
                            $imageurl = '';
                        }
                    @endphp
                    <img src="{{ $base64url }}" style="vertical-align:none !important" alt="profile">
                </a>
                <div class="dropdown-menu" aria-labelledby="profileDropdown">
                    <div class="dropdown-header d-flex flex-column align-items-center">
                        <div class="figure mb-3">
                            <img src="{{ $base64url }}" alt="">
                        </div>
                        <div class="info text-center">
                            <p class="name font-weight-bold mb-0">{{ Auth::user()->username }}</p>
                            <p class="email text-muted mb-3">{{ Auth::user()->roles[0]['role_title'] }}</p>
                        </div>
                        <div class="info text-center onlineselect" style="margin-bottom:10px;">
                            <label class="form-check-label" for="flexSwitchCheckChecked">Online</label>
                            <input id="flexSwitchCheckChecked" data-size="xs" data-width="50" data-height="25"
                                type="checkbox" {{ Auth::user()->is_online == 1 ? 'checked' : '' }}
                                data-toggle="toggle" data-onstyle="success">

                        </div>
                    </div>
                    <div class="dropdown-body">
                        <ul class="profile-nav p-0 pt-3">

                            <li class="nav-item">
                                <a id="profileupdate" href="javascript:void(0)" class="nav-link">
                                    <i data-feather="user"></i>
                                    <span>Profile</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/admin/changepassword" class="nav-link">
                                    <i data-feather="edit"></i>
                                    <span>Change Password</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="javascript::void(0)" id="logout" class="nav-link">
                                    <i data-feather="log-out"></i>
                                    <span>Log Out</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>

<div class="modal bd-example-modal-lg fade" id="profileupdatemodal" tabindex="-1" role="dialog"
    aria-labelledby="profileupdateLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="profileform" class="forms-sample">
                <input type="hidden" name="profile_user_id" id="profile_user_id" value="{{ Auth::user()->user_id }}">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Update Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 grid-margin">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label>First Name:<span style="color:red">*</span></label>
                                            <input class="form-control mb-4 mb-md-0" id="profilefirstname"
                                                value="{{ Auth::user()->first_name }}" name="firstname" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>Last Name:<span style="color:red">*</span></label>
                                            <input class="form-control" id="profilelastname"
                                                value="{{ Auth::user()->last_name }}" name="lastname" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label>Email Address:<span style="color:red">*</span></label>

                                            <input id="profileuseremail" name="useremail"
                                                value="{{ Auth::user()->email }}" class="form-control mb-4 mb-md-0"
                                                type="email" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>Photo:</label>
                                            <input class="form-control" type="file" id="profilefiledata"
                                                name="filedata" />
                                            <img src="{{ $imageurl }}">
                                        </div>
                                    </div>
                                    <div class="form-group mb-3">
                                        <span id="error" style="font-size:14px;color:red"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" id="profilesubmit" type="submit">Save</button>
                   <input class="btn btn-outline-secondary" data-dismiss="modal" type="reset" value="Cancel">

                </div>
            </form>
        </div>
    </div>
</div>
