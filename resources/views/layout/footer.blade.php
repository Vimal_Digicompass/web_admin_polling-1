<footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
  <p class="text-muted text-center text-md-left">1SqFt © {{ date('Y') }}. All rights reserved</p>
</footer>
