<nav class="sidebar">
    <div class="sidebar-header">
        <a href="#" class="sidebar-brand">
            <img src="/assets/images/1sqft_logo.png" title="1SqFt" alt="1SqFt" style="">
            <span class="logo-default">1SqFt</span>
        </a>
        <div class="sidebar-toggler not-active">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="sidebar-body">
        <ul class="nav">
            <li class="nav-item  {{ active_class(['dashboard']) }}">
                <a href="{{ url('dashboard') }}" class="nav-link">
                   <i class="fas fa-home"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            @if (Auth::user()->role_type == Config::get('constants.ROLE_TYPE.SUPER_ADMIN'))
                <li class="nav-item {{ active_class(['users/*']) }}">
                    <a class="nav-link" data-toggle="collapse" href="#users" role="button"
                        aria-expanded="{{ is_active_route(['users/*']) }}" aria-controls="users">
                      <i class="fas fa-tasks"></i>
                        <span>Management</span>

                    </a>
                    <div class="collapse {{ show_class(['users/*']) }}" id="users">
                        <ul class="nav sub-menu">
                            <li class="nav-item">
                                <a href="{{ url('/users/lists') }}"
                                    class="nav-link {{ active_class(['users/lists']) }}"><i class="fas fa-user"></i>&nbsp;&nbsp;&nbsp;&nbsp;Users</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/users/roles') }}"
                                    class="nav-link {{ active_class(['users/roles']) }}"><i class="fas fa-project-diagram"></i>&nbsp;&nbsp;&nbsp;&nbsp;Roles</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/users/permissions/*') }}"
                                    class="nav-link {{ active_class(['users/permissions/*']) }}"><i class="fas fa-user-lock"></i>&nbsp;&nbsp;&nbsp;&nbsp;Permissions</a>
                            </li>
                            <li class="nav-item">
							<a href="{{ url('/users/schedules') }}"
								class="nav-link {{ active_class(['users/schedules']) }}"><i class="fas fa-calendar-alt"></i>&nbsp;&nbsp;&nbsp;&nbsp;Schedules</a>
						    </li>
                            <li class="nav-item">
                                <a href="{{ url('/users/assigntopoll') }}"
                                    class="nav-link {{ active_class(['users/assigntopoll']) }}"><i class="fas fa-poll"></i>&nbsp;&nbsp;&nbsp;&nbsp;Send to Poll</a>

                            </li>
                        </ul>
                    </div>
                </li>
            @else
                @foreach ($PermissionDetail as $key => $value)
                    @php $activeclass = $value['permission_path'].'/*'; @endphp
                    <li class="nav-item {{ active_class($activeclass) }} ">
                        <a href="{{ url($value['permission_path']) }}/{{ Auth::user()->user_id }}"
                            class="nav-link"><i class="{{ $value['icon'] }}"></i>
                            <span>{{ $value['permission_name'] }}</span></a>
                    </li>
                @endforeach
                @if (Auth::user()->role_type == Config::get('constants.ROLE_TYPE.TEAM_LEAD') ||
                Auth::user()->role_type == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM'))
                    <li class="nav-item {{ active_class('users/assigntopoll') }} ">
                        <a href="{{ url('/users/assigntopoll') }}" class="nav-link"><i class="fas fa-poll"></i>
                            <span>Send to Poll</span></a>
                    </li>
                @else
                <li class="nav-item  {{ active_class(['notification']) }}">
                <a href="{{ url('/notification') }}" class="nav-link">
                    <i class="far fa-bell"></i>
                    <span>Notification</span>
                </a>
            </li>
                @endif

            @endif

        </ul>


</nav>
