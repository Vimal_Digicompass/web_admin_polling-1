@if ($scenario == "Dasboard")
  <h4 class="mb-3 mb-md-0">Welcome to {{ $scenario }}&nbsp;<i data-toggle="popover"  data-content="{{ $content }}" style="width:16px;height:25px;cursor: pointer;" class="link-icon"
                            data-feather="help-circle"></i></h4>
@else
<h4 class="mb-3 mb-md-0">{{ $scenario }}&nbsp;<i data-toggle="popover" data-content="{{ $content }}"
        style="width:16px;height:25px;cursor: pointer;" class="link-icon" data-feather="help-circle"></i></h4>
@endif
