  <div class="col-sm-3">
      <div class="form-group">
          <label class="control-label">State</label>

          <select id="state" name="state" class="js-example-basic-multiple w-100">
              <option value="">Select</option>
              @foreach ($state as $key => $value)
                  <option value="{{ $value['id'] }}">{{ $value['title']  }}</option>
              @endforeach
          </select>
      </div>
  </div><!-- Col -->
  <div class="col-sm-3">
      <div class="form-group">
          <label class="control-label">City</label>
          <select id="city" name="city" class="js-example-basic-multiple w-100">
              <option value="">Select</option>
          </select>
      </div>
  </div><!-- Col -->
