@foreach ($getData as $key => $value)


        <div class="row">

            <div class="col-md-12" style="text-align:end">
                 @if ( Auth::user()->role_type != Config::get('constants.ROLE_TYPE.CRM_USER'))

                    @if ($is_approved == 0)
                        <button type="button" id="{{ $post_id }}_1_S" class="approverejectpost btn btn-primary">
                            Approve
                        </button>&nbsp;&nbsp;
                        <button type="button" id="{{ $post_id }}_2_S" class="approverejectpost btn btn-danger">
                            Reject
                        </button>
                    @elseif($is_approved == 1)

                        <span class="badge badge-success" style="font-size:14px;"> Approved</span> &nbsp;&nbsp;
                        <button type="button" id="{{ $post_id }}_2_S" class="approverejectpost btn btn-danger">
                            Reject</span>
                        </button>
                    @elseif ($is_approved == 2)
                        <button type="button" id="{{ $post_id }}_1_S" class="approverejectpost btn btn-primary">
                            Approve
                        </button>&nbsp;&nbsp;
                        <span class="badge badge-danger" style="font-size:14px;"> Rejected</span>

                    @else
                    @endif

            @else
            @endif

            </div>
        </div>
         <form enctype="multipart/form-data" id="serviceeditform">
              @csrf
        <div class="row" style="margin-top:20px">
            <div class="col-md-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Category:<span style="color:red">*</span></label>
                                    <input type="hidden" id="postid" name="postid" value={{ $value['id'] }}>
                                    <input type="hidden" id="posttype" name="posttype" value="S">
                                    @foreach ($ServiceCategory as $key => $catvalue)
                                        @if ($value['service_type_id'] == $catvalue['id'])
                                            <p style="font-size: 14px;">
                                                {{ $catvalue['title'] }}
                                            </p>
                                        @else


                                        @endif

                                    @endforeach


                                </div>
                            </div><!-- Col -->
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"> Sub Category<span style="color:red;">*</span></label>

                                    @php
                                        $subcategory = explode(',', $value['sub_category_id']);
                                    @endphp
                                    @foreach ($ServiceSubCategory as $key => $subvalue)

                                        @if (in_array($subvalue['id'], $subcategory))
                                            <p style="font-size: 14px;">
                                                {{ $subvalue['title'] }}
                                            </p>

                                        @else

                                        @endif

                                    @endforeach


                                </div>
                            </div><!-- Col -->
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Short Description<span
                                            style="color:red;">*</span></label>
                                    <input value="{{ $value['short_description'] }}" name="short_description"
                                        id="short_description" class="form-control mb-4 mb-md-0" type="text" />
                                </div>
                            </div><!-- Col -->
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Benifits/Usage</label>
                                    <input id="benefits" name="benefits" class="form-control mb-4 mb-md-0"
                                        value="{{ $value['benefits'] }}" type="text" />
                                </div>
                            </div><!-- Col -->
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Warranty</label>
                                    <input id="warranty" name="warranty" class="form-control mb-4 mb-md-0"
                                        value="{{ $value['warranty'] }}" type="text" />
                                </div>
                            </div><!-- Col -->
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Offer/Discount </label>
                                    <input id="offers" name="offers" class="form-control mb-4 mb-md-0"
                                        value="{{ $value['offers'] }}" type="text" />
                                </div>
                            </div><!-- Col -->
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Delivery Time </label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Days </label>
                                            <input name="delivery" value="{{ $value['delivery'] }}" id="delivery"
                                                class="form-control mb-4 mb-md-0" type="number" maxlength="365"
                                                minlength="1" />
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Hours </label>
                                            <select id="hours" name="hours" class="js-example-basic-multiple w-100">
                                                @if ($value['hours'] == 0)
                                                    @php $value['hours'] == ""; @endphp
                                                @else
                                                @endif
                                                <option value="">
                                                    select
                                                </option>
                                                @for ($i = 0; $i < 24; $i++)
                                                    @if ($i == $value['hours']) <option selected
                                                    value="{{ $i }}">
                                                    {{ $i }}
                                                    </option>
                                                @else
                                                    <option value="{{ $i }}">
                                                    {{ $i }}
                                                    </option> @endif
                                                @endfor
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div><!-- Col -->

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"> Payment method<span
                                            style="color:red;">*</span></label>
                                    <select required id="payment_mode_id" name="payment_mode_id[]" multiple
                                        class="js-example-basic-multiple w-100">
                                        @php
                                            $paymenttype = explode(',', $value['payment_mode_id']);
                                        @endphp
                                        @foreach ($PaymentMode as $key => $paymentvalue)
                                            @if (in_array($key, $paymenttype))
                                                <option selected value="{{ $key }}">
                                                    {{ $paymentvalue }}
                                                </option>
                                            @else
                                                <option value="{{ $key }}">
                                                    {{ $paymentvalue }}
                                                </option>
                                            @endif

                                        @endforeach

                                    </select>
                                </div>
                            </div><!-- Col -->


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Created Date</label>
                                    <p style="font-size:14px;">
                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value['created_at'], 'UTC')->setTimezone('Asia/Calcutta')->format('d-m-Y H:i A') }}
                                    </p>
                                </div>
                            </div><!-- Col -->
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">State</label>
                                    <p style="font-size:14px;">{{ $value['state'] }}</p>
                                </div>
                            </div><!-- Col -->

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">City</label>
                                    <p style="font-size:14px;">{{ $value['city'] }}</p>
                                </div>
                            </div><!-- Col -->
                            <div class="col-sm-4">
                                <div class="form-group autoheight_area">
                                    <label class="control-label">Area<span style="color:red">*</span>
                                    </label>
                                    <select multiple required class="js-example-basic-multiple w-100" id="area_id"
                                        name="area_id[]">
                                        <option value="">select</option>
                                        @php
                                            $areaRecord = \DB::connection('mysql_1')
                                                ->table('service_areas')
                                                ->where('service_id', $value['id'])
                                                ->get();
                                            $areaSelected = $areaRecord->pluck('area_id')->toArray();
                                        @endphp
                                        @foreach ($AreaMaster as $key => $areavalue)
                                            @if (in_array($areavalue['id'], $areaSelected))
                                                <option selected value="{{ $areavalue['id'] }}">
                                                    {{ $areavalue['title'] }}</option>
                                            @else
                                                <option value="{{ $areavalue['id'] }}">
                                                    {{ $areavalue['title'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div><!-- Col -->

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Main Photo</label>
                                    <input accept="image/*" name="main_image_name" id="main_image_name"
                                        class="form-control mb-4 mb-md-0" type="file" />
                                    @php
                                        if ($value['main_image_name'] != null || $value['main_image_name'] != '') {
                                            $path = getFileURLPublicPathPost($value['main_image_name'], $PostType, $value['id'], 'image');
                                        } else {
                                            $path = '';
                                        }
                                    @endphp
                                    <div class="row" id="appendmainimage">
                                        @if ($path != '')
                                            <div id="mainimagediv">
                                                <a class="gallery-image" href="{{ $path }}">
                                                    <img src={{ $path }} width="130px" height="130px"
                                                        title="1SqFt" alt="1SqFt" style=""></a><a
                                                    href="javascript:void(0)" class="removeimage" imageextension="image"
                                                    posttype="{{ $PostType }}" imagetype="mainimage"
                                                    postid="{{ $value['id'] }}"
                                                    imagename="{{ $value['main_image_name'] }}"
                                                    imageid="{{ $value['id'] }}"><i style="width:15px;height:15px"
                                                        data-feather="trash-2"></i></a>
                                            </div>
                                        @else
                                        @endif
                                    </div>
                                </div>
                            </div><!-- Col -->

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Other Photos<span style="color:red;"> [Max
                                            7-Images]</span></label>
                                    <input name="otherimage[]" id="otherimage" accept="image/*" multiple
                                        class="form-control mb-4 mb-md-0" type="file" />
                                    @php $count = 0; @endphp
                                </div>
                                <div class="row" id="appendotherimage">
                                    @foreach ($value['getimages'] as $key => $imagevalue)
                                        @php $path = getFileURLPublicPathPost($imagevalue['image_name'], $PostType, $value['id'], 'image') @endphp
                                        @if ($path != '')
                                        @php $count++; @endphp
                                            <div cclass="col-sm-6"
                                                id="{{ $PostType . '_' . $imagevalue['id'] . '_otherimage' }}">
                                                <a class="gallery-image" href="{{ $path }}">
                                                    <img src="{{ $path }}" width="130px" height="130px"
                                                        title="1SqFt" alt="1SqFt" style=""></a>
                                                <a href="javascript:void(0)" class="removeimage" imageextension="image"
                                                    postid="{{ $value['id'] }}" posttype="{{ $PostType }}"
                                                    imagetype="otherimage"
                                                    imagename="{{ $imagevalue['image_name'] }}"
                                                    imageid="{{ $imagevalue['id'] }}"><i
                                                        style="width:15px;height:15px" data-feather="trash-2"></i></a>
                                                &nbsp;
                                            </div>
                                        @else
                                        @endif
                                    @endforeach
                                </div>
                                    <input type="hidden" id="totalotherimage" value={{ $count }}>
                            </div><!-- Col -->
                            <div class="col-sm-12" style="text-align:center">
                                <div class="form-group">
                                    <button class="btn btn-danger" id="postsubmit">UPDATE</button>
                                    <input onClick="window.location.reload();" class="btn btn-secondary" type="reset" value="CANCEL">
                                </div>
                            </div><!-- Col -->

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
@endforeach
