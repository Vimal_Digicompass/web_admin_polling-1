 <div class="modal bd-example-modal-lg fade" id="exampleModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">

             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">KYC Document</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12 grid-margin">

                         <div class="card">
                             <div class="card-body">
                                 <div class="row">
                                     <div id="username_type" style="font-size:14px;float:left" class="col-md-6">
                                     </div>
                                 </div>
                                 <div class="table-responsive pt-3">
                                     <table class="table table-bordered">
                                         <thead>
                                             <tr>
                                                 <th>
                                                     Type
                                                 </th>
                                                 <th>
                                                     Number
                                                 </th>
                                                 <th>
                                                     Front File
                                                 </th>
                                                 <th>
                                                     Back File
                                                 </th>
                                                 <th>
                                                     Action
                                                 </th>
                                             </tr>
                                         </thead>
                                         <tbody id="documentbody">

                                         </tbody>
                                     </table>
                                 </div>

                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="modal-footer">

         </div>
     </div>
 </div>
 </div>

 <div class="modal bd-example-modal-lg fade" id="uploaddocumentmodal" tabindex="-1" role="dialog"
     aria-labelledby="uploaddocumentmodalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">

         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="uploaddocumentmodalLabel">Upload Document</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12 grid-margin">

                         <div class="card">
                             <div class="card-body">
                                 <form id="documentuploadform" enctype="multipart/form-data">
                                     <div class="row">

                                         <div class="col-sm-6">
                                             <div class="form-group">
                                                 <label class="control-label"> Category<span
                                                         style="color:red;">*</span></label>
                                                 <input type="hidden" id="documentuser_id" name="user_id">
                                                 <input type="hidden" id="documentuser_type" name="documentuser_type">
                                                 @php $role_type = Auth::user()->role_type; @endphp
                                                 @if ($role_type == 3)
                                                     <select required id="selectcategory" name="selectcategory"
                                                         class="js-example-basic-multiple w-100">
                                                     @else
                                                         <select required id="selectcategory" disabled
                                                             name="selectcategory"
                                                             class="js-example-basic-multiple w-100">
                                                 @endif
                                                 <option value="">Select
                                                     </option>
                                                 @foreach ($UserCategory as $key => $value)
                                                     <option value="{{ $value['id'] }}">{{ $value['title'] }}
                                                     </option>
                                                 @endforeach
                                                 </select>

                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-6">
                                             <div class="form-group">
                                                 <label class="control-label"> Document type<span
                                                         style="color:red;">*</span></label>
                                                 <select required id="selectdocuments" name="selectdocuments"
                                                     class="js-example-basic-multiple w-100">
                                                     <option value="">Select</option>
                                                     @foreach ($KYCDocumentList as $key => $value)
                                                         <option value="{{ $value['id'] }}">{{ $value['title'] }}
                                                         </option>

                                                     @endforeach

                                                 </select>
                                             </div>
                                         </div><!-- Col -->

                                         <div id="documentnumberdiv" class="col-sm-6">
                                             <div class="form-group">
                                                 <label class="control-label"> Document No<span
                                                         style="color:red;">*</span></label>
                                                 <input required class="documentnumber form-control mb-4 mb-md-0"
                                                     value="" name="documentnumber" id="documentnumber" type="text" />
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-6">
                                             <div class="form-group">
                                                 <label class="control-label"> Front File<span
                                                         style="color:red;">*</span></label>
                                                 <input

                                                     accept="application/pdf,image/*" required
                                                     class="frontfile form-control mb-4 mb-md-0" value=""
                                                     name="frontfile" id="" type="file" /><br>
                                                 <span id="errormessage" style="font-size:14px;color:red"></span><br>
                                               <a class="gallery-image" style="display:none" id="gallery-image" >
                                                 <img id="frontimage" alt="" width="100" height="100" /></a>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-6">
                                             <div class="form-group">
                                                 <label class="control-label"> Back File</label>
                                                 <input

                                                     accept="application/pdf,image/*"
                                                     class="backfile form-control mb-4 mb-md-0" value="" name="backfile"
                                                     id="" type="file" /><br>
                                                <a class="gallery-image" id="gallery-image1" style="display:none" > <img id="backimage" alt="" width="100" height="100" /></a>
                                             </div>
                                         </div><!-- Col -->
                                     </div>
                                     <button id="documentuploadsubmit" class="btn btn-danger"
                                         type="submit">Update</button>
                                     <input class="btn btn-secondary" data-dismiss="modal" type="reset" value="CANCEL">
                                 </form>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>

             <div class="modal-footer">

             </div>
             </form>
         </div>
     </div>
 </div>


 <div class="modal bd-example-modal-lg fade" id="detailviewmodal" tabindex="-1" role="dialog"
     aria-labelledby="detailviewmodalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <form>
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="detailviewmodalLabel">Detail View</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                 <div class="modal-body">
                     <div class="row">
                         <div class="col-md-12 grid-margin">

                             <div class="card">
                                 <div class="card-body">
                                     <div class="row">

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> First
                                                     Name:</label>
                                                 <p id="detailviewfirstname" style="font-size:14px;"></p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> Last
                                                     Name:</label>
                                                 <p id="detailviewlastname" style="font-size:14px;"></p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">Email
                                                     Address:</label>
                                                 <p id="detailviewemail" style="font-size:14px;"></p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">Mobile
                                                     Number:</label>
                                                 <p id="detailviewmobilenumber" style="font-size:14px;"></p>
                                             </div>
                                         </div><!-- Col -->

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Company:</label>
                                                 <p id="detailviewcompany" style="font-size:14px;"></p>
                                             </div>
                                         </div><!-- Col -->

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">User
                                                     type:</label>
                                                 <p id="detailviewusertype" style="font-size:14px;"></p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> User
                                                     category:</label>
                                                 <p id="detailviewcategory" style="font-size:14px;"></p>
                                             </div>
                                         </div><!-- Col -->


                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;"
                                                     class="control-label">Address:</label>
                                                 <p id="detailviewaddress" style="font-size:14px;"></p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Pincode:</label>
                                                 <p id="detailviewpincode" style="font-size:14px;"></p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">City:</label>
                                                 <p id="detailviewcity" style="font-size:14px;"></p>
                                             </div>
                                         </div><!-- Col -->
                                     </div>

                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>

                 <div class="modal-footer">

                 </div>
         </form>
     </div>
 </div>
 </div>

 <div class=" modal right modalkyc fade" id="editviewmodal" tabindex="-1" role="dialog"
     aria-labelledby="editviewmodalLabel" aria-hidden="true">
     <div class="modal-dialog " role="document">

         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="editviewmodalLabel">Status</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12 grid-margin">
                         <div class="card">
                             <div class="card-body">
                                 <ul class="nav nav-tabs nav-tabs-line" id="lineTab" role="tablist">
                                     @php $getpermissionData = getPermissionAndAccess($criteriaId,Auth::user()->role_id) @endphp

                                     @if ($getpermissionData[0]['is_approve'] != 0)
                                         <li class="nav-item">
                                             @if ($criteriaId == 4 || $criteriaId == 5)
                                                 <a class="nav-link active" id="kyc-status-tab" data-toggle="tab"
                                                     href="#kyc-status" role="tab" aria-controls="kyc-status"
                                                     aria-selected="true">POST STATUS</a>
                                             @else
                                                 <a class="nav-link active" id="kyc-status-tab" data-toggle="tab"
                                                     href="#kyc-status" role="tab" aria-controls="kyc-status"
                                                     aria-selected="true">KYC STATUS</a>
                                             @endif
                                         </li>
                                         @if ($getpermissionData[0]['is_callstatus'] != 0)
                                             <li class="nav-item">
                                                 <a class="nav-link" id="call-status-tab" data-toggle="tab"
                                                     href="#call-status" role="tab" aria-controls="call-status"
                                                     aria-selected="false">CALL
                                                     STATUS</a>
                                             </li>
                                             <li class="nav-item">
                                                 <a class="nav-link" id="changelog-tab" data-toggle="tab"
                                                     href="#changelog" role="tab" aria-controls="changelog"
                                                     aria-selected="false">HISTORY</a>
                                             </li>
                                         @else
                                             <li class="nav-item">
                                                 <a class="nav-link" id="changelog-tab" data-toggle="tab"
                                                     href="#changelog" role="tab" aria-controls="changelog"
                                                     aria-selected="false">HISTORY</a>
                                             </li>
                                         @endif


                                     @else
                                         @if ($getpermissionData[0]['is_callstatus'] != 0)
                                             <li class="nav-item">
                                                 <a class="nav-link active" id="call-status-tab" data-toggle="tab"
                                                     href="#call-status" role="tab" aria-controls="call-status"
                                                     aria-selected="false">CALL
                                                     STATUS</a>
                                             </li>
                                             <li class="nav-item">
                                                 <a class="nav-link" id="changelog-tab" data-toggle="tab"
                                                     href="#changelog" role="tab" aria-controls="changelog"
                                                     aria-selected="false">HISTORY</a>
                                             </li>
                                         @else
                                             <li class="nav-item">
                                                 <a class="nav-link active" id="changelog-tab" data-toggle="tab"
                                                     href="#changelog" role="tab" aria-controls="changelog"
                                                     aria-selected="false">HISTORY</a>
                                             </li>
                                         @endif
                                     @endif

                                 </ul>
                                 <div class="tab-content mt-3" id="lineTabContent">
                                     @if ($getpermissionData[0]['is_approve'] != 0)
                                         <div class=" tab-pane fade show active" id="kyc-status" role="tabpanel"
                                             aria-labelledby="kyc-status-tab">
                                             @if ($criteriaId == 4 || $criteriaId == 5)
                                                 <form id="poststatusform">
                                                 @else
                                                     <form id="kycstatusform">
                                             @endif
                                             <div id="kycstatusrow" class="row">
                                                 <div class="col-sm-12">
                                                     <div class="form-group">
                                                         <label class="control-label"> STATUS<span
                                                                 style="color:red;">*</span></label>
                                                         <input type="hidden" id="post_id" name="post_id">
                                                         <input type="hidden" id="kyc_user_id" name="user_id">
                                                         @if ($criteriaId == 4 || $criteriaId == 5)
                                                             <select required id="poststatusaction" name="poststatus"
                                                                 class="js-example-basic-multiple w-100">
                                                             @else
                                                                 <select required id="kycstatusaction" name="kycstatus"
                                                                     class="js-example-basic-multiple w-100">

                                                         @endif
                                                         <option value="">Select</option>
                                                         @foreach ($KYCStatus as $key => $value)
                                                             @if ($value['id'] == 1 || $value['id'] == 2)
                                                                 <option value="{{ $value['id'] }}">
                                                                     {{ $value['title'] }}</option>
                                                             @else
                                                             @endif
                                                         @endforeach
                                                         </select>
                                                     </div>
                                                 </div><!-- Col -->
                                                 @if ($criteriaId == 4 || $criteriaId == 5)
                                                     <div class="col-sm-12">
                                                         <div id="poststatusactionreasondiv" style="display:none;"
                                                             class="form-group">
                                                             <select required id="poststatusactionreason"
                                                                 name="poststatusactionreason"
                                                                 class="js-example-basic-multiple w-100">
                                                                 <option value="">Select</option>

                                                             </select>
                                                         </div>
                                                     </div>
                                                 @else
                                                     <div id="rejectedcheckbox" style="display:none"
                                                         class=" rejectedcheckbox col-sm-12">
                                                     </div>
                                                     <span id="checkboxerror" style="font-size:14px;color:red"></span>

                                                 @endif

                                             </div>
                                             <button class="btn btn-danger" value="UPDATE"
                                                 id="kycstatussubmit">UPDATE</button>
                                             {{-- <input class="btn btn-danger" type="submit" value="UPDATE"> --}}
                                             <input class="btn btn-secondary" data-dismiss="modal" type="reset"
                                                 value="CANCEL">
                                             </form>
                                         </div>
                                         @if ($getpermissionData[0]['is_callstatus'] != 0)
                                             <div class="tab-pane fade" id="call-status" role="tabpanel"
                                                 aria-labelledby="call-status-tab">
                                                 <form id="callstatusform">
                                                     <div class="row">

                                                         <div class="col-sm-6">
                                                             <div class="form-group">
                                                                 <input type="hidden" id="user_id" name="user_id">
                                                                 <label class="control-label"> CALLING STATUS<span
                                                                         style="color:red;">*</span></label>
                                                                 <select required name="callingstatus"
                                                                     id="callingstatus"
                                                                     class="js-example-basic-multiple w-100">
                                                                     <option value="">Status</option>
                                                                     @foreach ($callStatus as $key => $value)
                                                                         @if ($value['title'] != 'All')
                                                                             <option value="{{ $value['id'] }}">
                                                                                 {{ $value['title'] }}</option>
                                                                         @else
                                                                         @endif
                                                                     @endforeach

                                                                 </select>
                                                             </div>
                                                         </div><!-- Col -->
                                                         <div id="followdatediv"  class="col-sm-6">
                                                             <div class="form-group">
                                                                 <label class="control-label">FOLLOW-UP DATE</label>
                                                                 <input id="followdate" name="followdate"
                                                                     class="form-control mb-4 mb-md-0 followdate"
                                                                     type="text" />
                                                             </div>
                                                         </div><!-- Col -->

                                                         <div class="col-sm-12">
                                                             <div class="form-group">
                                                                 <label class="control-label"> REMARKS</label>
                                                                 <textarea class="form-control" name="tinymce"
                                                                     id="callremark" rows="10"></textarea>
                                                             </div>
                                                         </div><!-- Col -->
                                                     </div>
                                                     <button class="btn btn-danger" id="callstatussubmit"
                                                         value="UPDATE">UPDATE</button>
                                                     <input class="btn btn-secondary" data-dismiss="modal" type="reset"
                                                         value="CANCEL">
                                                 </form>
                                             </div>
                                             <div class="tab-pane fade" id="changelog" role="tabpanel"
                                                 aria-labelledby="changelog-tab">
                                             </div>
                                         @else
                                             <input type="hidden" id="user_id" name="user_id">
                                             <div class="tab-pane fade" id="changelog" role="tabpanel"
                                                 aria-labelledby="changelog-tab">
                                             </div>
                                         @endif
                                     @else
                                         @if ($getpermissionData[0]['is_callstatus'] != 0)
                                             <div class="tab-pane fade show active" id="call-status" role="tabpanel"
                                                 aria-labelledby="call-status-tab">
                                                 <form id="callstatusform">
                                                     
                                                     <div class="row">

                                                         <div class="col-sm-6">
                                                             <div class="form-group">
                                                                 <input type="hidden" id="user_id" name="user_id">
                                                                 <label class="control-label"> CALLING STATUS<span
                                                                         style="color:red;">*</span></label>
                                                                 <select required name="callingstatus"
                                                                     id="callingstatus"
                                                                     class="js-example-basic-multiple w-100">
                                                                     <option value="">Status</option>
                                                                     @foreach ($callStatus as $key => $value)
                                                                         @if ($value['title'] != 'All')
                                                                             <option value="{{ $value['id'] }}">
                                                                                 {{ $value['title'] }}</option>
                                                                         @else
                                                                         @endif
                                                                     @endforeach

                                                                 </select>
                                                             </div>
                                                         </div><!-- Col -->
                                                         <div id="followdatediv" class="col-sm-6">
                                                             <div class="form-group">
                                                                 <label class="control-label">FOLLOW-UP DATE</label>
                                                                 <input id="followdate" name="followdate"
                                                                     class="form-control mb-4 mb-md-0 followdate"
                                                                     type="text" />
                                                             </div>
                                                         </div><!-- Col -->

                                                         <div class="col-sm-12">
                                                             <div class="form-group">
                                                                 <label class="control-label"> REMARKS</label>
                                                                 <textarea class="form-control" name="tinymce"
                                                                     id="callremark" rows="10"></textarea>
                                                             </div>
                                                         </div><!-- Col -->
                                                     </div>
                                                     <button class="btn btn-danger" id="callstatussubmit"
                                                         value="UPDATE">UPDATE</button>
                                                     <input class="btn btn-secondary" data-dismiss="modal" type="reset"
                                                         value="CANCEL">
                                                 </form>
                                             </div>
                                             <div class="tab-pane fade" id="changelog" role="tabpanel"
                                                 aria-labelledby="changelog-tab">
                                             </div>
                                         @else
                                             <input type="hidden" id="user_id" name="user_id">
                                             <div class="tab-pane fade show active" id="changelog" role="tabpanel"
                                                 aria-labelledby="changelog-tab">
                                             </div>
                                         @endif
                                     @endif


                                 </div>
                             </div>
                         </div>
                     </div>

                 </div>
             </div>

             {{-- <div class="modal-footer">

                    </div> --}}
             </form>
         </div>
     </div>
 </div>


 <div class="modal bd-example-modal-lg fade" id="viewdoc" tabindex="-1" role="dialog" aria-labelledby="viewdocLabel"
     aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">

         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="viewdocLabel">View KYC Document</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12 grid-margin">

                         <div class="card">
                             <div class="card-body">
                                 <div class="row">
                                     <div class="col-md-4">
                                         <span style="font-size:14px;font-weight:bold">Document No:</span> <span class="badge badge-info" id="documentnodetailview" style="font-weight:bold;font-size:14px;"></span>
                                     </div>

                                 </div><br>
                                 <center>
                                     <img alt="No document found" id="viewimage" width="600" height="600">
                                 </center>

                             </div>
                         </div>
                     </div>
                 </div>
             </div>

             <div class="modal-footer">

             </div>
             </form>
         </div>
     </div>
 </div>


 <div class="modal bd-example-modal-lg fade" id="editpropertystatus" tabindex="-1" role="dialog"
     aria-labelledby="editpropertystatusLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">

         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="editpropertystatusLabel">Change Property Status</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12 grid-margin">

                         <div class="card">
                             <div class="card-body">
                                 <div class="row">
                                     <div class="col-sm-6">
                                         <div class="form-group">
                                             <input type="hidden" id="propertyupdateid" name="propertyupdateid">
                                             <label class="control-label"> Select STATUS<span
                                                     style="color:red;">*</span></label>
                                             <select required name="propertystatusupdate" id="propertystatusupdate"
                                                 class="js-example-basic-multiple w-100">
                                                 <option value="">Status</option>

                                             </select>
                                         </div>
                                     </div>

                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button class="btn btn-danger" type="button" id="updatepropertystatusBtn">UPDATE</button>
             </div>
         </div>
     </div>
 </div>


 <div class="modal bd-example-modal-lg fade" id="sendnotification" tabindex="-1" role="dialog"
     aria-labelledby="sendnotificationLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <form id="sendnotifyform">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="sendnotificationLabel">Send Notification</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                 <div class="modal-body">
                     <div class="row">
                         <div class="col-md-12 grid-margin">

                             <div class="card">
                                 <div class="card-body">
                                     <div class="row">
                                         <div class="col-sm-12">
                                             <div class="notifydiv form-group">
                                                 <label class="control-label"> Content<span
                                                         style="color:red">*</span></label>
                                                 <textarea class="form-control" name="notificationbody"
                                                     id="notificationbody" rows="10"></textarea>
                                             </div>
                                         </div>

                                     </div>

                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>

                 <div class="modal-footer">
                     <button class="btn btn-danger" id="sendnotificationBtn">Send Notification</button>
                     <input class="btn btn-secondary" data-dismiss="modal" type="reset" value="CANCEL">
                 </div>
             </div>
         </form>
     </div>
 </div>
