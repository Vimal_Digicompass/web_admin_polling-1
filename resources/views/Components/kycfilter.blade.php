  @if ($scenario == 'Pending' || $scenario == 'Rejected')
          <div class="row">
              @include('Components.statecityfilter')
              <div class="col-sm-3">
                  <div class="form-group">
                      <label class="control-label">User Type</label>
                      <select id="usertype" name="usertype" class="js-example-basic-multiple w-100">
                          <option value="">Select</option>
                          @foreach ($userType as $key => $value)
                              <option value="{{ $value['titlevalue'] }}">{{ $value['title'] }}</option>
                          @endforeach

                      </select>
                  </div>
              </div><!-- Col -->
              <div class="col-sm-3">
                  <div class="form-group">
                      <label class="control-label">User Category</label>
                      <select id="searchusercategory" class="js-example-basic-multiple w-100">
                          <option value="">select</option>
                          @foreach ($UserCategory as $key => $value)
                              <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                          @endforeach
                      </select>
                  </div>
              </div><!-- Col -->
              <div class="col-sm-3">
                  <div class="form-group">
                      @if ($scenario == 'Pending' || $scenario == "Rejected")
                          <label class="control-label">Date of Registration</label>

                      @else
                      @endif
                      <input type="text" name="kycdate" id='kycdate' class=" filterdate form-control">
                  </div>
              </div><!-- Col -->
              @include('Components.searchfilter')

          </div><!-- Row -->
  @elseif($scenario == "Upgrade to Business User")
          <div class="row">
              <div class="col-sm-3">
                  <div class="form-group">
                      <label class="control-label">Upgrading To</label>
                      <select id="usertype" name="usertype" class="js-example-basic-multiple w-100">
                          <option value="">Select</option>
                          @foreach ($userType as $key => $value)
                              <option value="{{ $value['titlevalue'] }}">{{ $value['title'] }}</option>
                          @endforeach

                      </select>
                  </div>
              </div><!-- Col -->
              <div class="col-sm-3">
                  <div class="form-group">
                      <label class="control-label">Upgrading Category</label>
                      <select id="searchusercategory" class="js-example-basic-multiple w-100">
                          <option value="">select</option>
                          @foreach ($UserCategory as $key => $value)
                              <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                          @endforeach
                      </select>
                  </div>
              </div><!-- Col -->
              @include('Components.statecityfilter')

              @include('Components.searchfilter')
          </div>
  @else
          <div class="row">

              <div class="col-sm-3">
                  <div class="form-group">
                      <label class="control-label">Date of Registration</label>
                      <input type="text" name="kycdate" id='kycdate' class="filterdate form-control">
                  </div>
              </div><!-- Col -->
              <div class="col-sm-3">
                  <div class="form-group">
                      <label class="control-label">User Type</label>
                      <select id="usertype" name="usertype" class="js-example-basic-multiple w-100">
                          <option value="">Select</option>
                          @foreach ($userType as $key => $value)
                              <option value="{{ $value['titlevalue'] }}">{{ $value['title'] }}</option>
                          @endforeach

                      </select>
                  </div>
              </div><!-- Col -->
              @include('Components.statecityfilter')

              @include('Components.searchfilter')
  @endif



  </div><!-- Row -->
