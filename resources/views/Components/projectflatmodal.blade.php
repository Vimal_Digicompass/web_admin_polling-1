@foreach ($getData as $key => $value)
    <div class="modal-dialog modal-lg" role="document">
        <form id="flateditform">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="serviceeditviewmodalLabel">Edit Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 grid-margin">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @if ($projectcategory == 1)
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Select BHK:<span
                                                            style="color:red">*</span></label>
                                                    <input type="hidden" name="postid" id="postid"
                                                        value="{{ $value['project_id'] }}">
                                                    <input type="hidden" id="flatid" name="flatid"
                                                        value={{ $value['id'] }}>


                                                    @foreach ($BHKMaster as $key => $bhkvalue)
                                                        @if ($value['bhk_id'] == $bhkvalue['id'])
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                    <input checked type="radio" class="form-check-input"
                                                                        name="bhk" id="bhk"
                                                                        value="{{ $bhkvalue['id'] }}">
                                                                    {{ $bhkvalue['title'] }}
                                                                    <i class="input-frame"></i></label>
                                                            </div>
                                                        @else
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                    <input type="radio" class="form-check-input"
                                                                        name="bhk" id="bhk"
                                                                        value="{{ $bhkvalue['id'] }}">
                                                                    {{ $bhkvalue['title'] }}
                                                                    <i class="input-frame"></i></label>
                                                            </div>
                                                        @endif

                                                    @endforeach
                                                </div>
                                            </div><!-- Col -->
                                        @else
                                        @endif
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="hidden" name="postid" id="postid"
                                                    value="{{ $value['project_id'] }}">
                                                <input type="hidden" id="flatid" name="flatid"
                                                    value={{ $value['id'] }}>
                                                <label class="control-label"> Built-up area in Sq.feet<span
                                                        style="color:red;">*</span></label>
                                                <input required value="{{ $value['builtup_area'] }}"
                                                    name="builtuparea" id="builtuparea" min='1' minlength='3'
                                                    maxlength='7' class="form-control mb-4 mb-md-0" type="text" />
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label"> Carpet Area in Sq.feet<span
                                                        style="color:red;">*</span></label>
                                                <input required value="{{ $value['carpet_area'] }}" name="carpetarea"
                                                    min='100' minlength='3' maxlength='7' id="carpetarea"
                                                    class="form-control mb-4 mb-md-0" type="text" />
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Furnished Type</label>
                                                <p style="font-size:14px;">{{ $value['Furniture_Title'] }}
                                                <p>

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Loans offered by Banks<span
                                                    style="color:red">*</span></label><br>
                                            @php
                                                $explodeBankId = explode(',', $value['loan_offered_bank_ids']);
                                                $bankoffers = \DB::connection('mysql_1')
                                                    ->table('loan_offered_by_banks_master')
                                                    ->whereIn('id', $explodeBankId)
                                                    ->get();
                                                $bankoffer = $bankoffers->pluck('title')->toArray();
                                                $bnkoffer = json_decode(json_encode($bankoffers), true);
                                                $banknames = implode(',', $bankoffer);
                                                
                                            @endphp
                                            @if (Auth::user()->role_type == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM'))
                                                @foreach ($bnkoffer as $key => $bankoffervalud)
                                                    <div class="form-check form-check-inline">
                                                        <label class="form-check-label">
                                                            <input required checked type="checkbox"
                                                                name="loan_offered_bank_ids[]"
                                                                value="{{ $bankoffervalud['id'] }}"
                                                                class="form-check-input loan_offered_bank_ids">
                                                            {{ $bankoffervalud['title'] }}
                                                            <i class="input-frame"></i></label>
                                                    </div>
                                                @endforeach
                                            @else
                                                <p style="font-size:14px;">{{ $banknames }}</p>
                                            @endif
                                        </div><!-- Col -->
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Floor No<span
                                                        style="color:red;">*</span></label>
                                                <select class="js-example-basic-multiple w-100" id="floor" name="floor">
                                                    @foreach ($FloorMaster as $key => $fvalue)
                                                        @if ($value['floor_id'] == $fvalue['id'])
                                                            <option selected value="{{ $fvalue['id'] }}">
                                                                {{ $fvalue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $fvalue['id'] }}">
                                                                {{ $fvalue['title'] }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Transaction Type<span
                                                        style="color:red;">*</span></label>
                                                <select class="js-example-basic-multiple w-100" id="transactiontype"
                                                    name="transactiontype">
                                                    @foreach ($TransactionType as $key => $tvalue)
                                                        @if ($value['transaction_type_id'] == $tvalue['id'])
                                                            <option selected value="{{ $tvalue['id'] }}">
                                                                {{ $tvalue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $tvalue['id'] }}">
                                                                {{ $tvalue['title'] }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        @if ($value['transaction_type_id'] == 2)
                                            <div id="propertyage" class="col-sm-6">
                                            @else
                                                <div id="propertyage" style="display:none" class="col-sm-6">
                                        @endif

                                        <div class="form-group">
                                            <label class="control-label">
                                                If Resale, Property Age:<span style="color:red">*</span></label>
                                            <input required name="flat_age" id="flat_age"
                                                value="{{ $value['flat_age'] }}" class="form-control mb-4 mb-md-0"
                                                type="text" />

                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Price <span style="color:red;">*</span></label>
                                            <input id="price" name="price" class="form-control mb-4 mb-md-0"
                                                value="{{ $value['price'] }}" type="text" />
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Possesstion Date <span
                                                    style="color:red;">*</span></label>
                                            <input name="possessiondate" id="possessiondate"
                                                value="{{ \Carbon\Carbon::parse($value['possession_date'])->format('d-m-Y') }}"
                                                class="form-control mb-4 mb-md-0" type="text" />
                                        </div>
                                    </div><!-- Col -->
                                    @if ($projectcategory == 2)
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Ideal for business?<span
                                                        style="color:red;">*</span></label>
                                                <select multiple class="js-example-basic-multiple w-100"
                                                    id="idealbusiness" name="idealbusiness[]">
                                                    @php
                                                        $business = \DB::connection('mysql_1')
                                                            ->table('project_flat_ideal_for_business')
                                                            ->where('flat_id', $value['id'])
                                                            ->get();
                                                        $business = $business->pluck('ideal_business_id')->toArray();

                                                    @endphp
                                                    @foreach ($IdealBusniess as $key => $bvalue)
                                                        @if (in_array($bvalue['id'], $business))
                                                            <option selected value="{{ $bvalue['id'] }}">
                                                                {{ $bvalue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $bvalue['id'] }}">
                                                                {{ $bvalue['title'] }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                    @else
                                    @endif
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Floor Plan <span
                                                    style="color:red;">*</span></label>
                                            <input accept="image/*" name="floor_plan_file_name"
                                                id="floor_plan_file_name" class="form-control mb-4 mb-md-0"
                                                type="file" />
                                            @php
                                                if ($value['floor_plan_file_name'] != null || $value['floor_plan_file_name'] != '') {
                                                    $path = getFileURLPublicPathPost($value['floor_plan_file_name'], 'PR', $value['project_id'], 'flats/' . $value['id'] . '');
                                                } else {
                                                    $path = '';
                                                }
                                            @endphp
                                            <div class="row" id="appendflatimage">
                                                @if ($path != '')
                                                    <div id="flatimagediv">
                                                        <a class="gallery-image" href="{{ $path }}"> <img
                                                                src={{ $path }} width="130px" height="130px"
                                                                title="1SqFt" alt="1SqFt" style=""></a><a
                                                            href="javascript:void(0)" class="removeimage"
                                                            imageextension="image" posttype="PR" imagetype="flatimage"
                                                            postid="{{ $value['project_id'] }}"
                                                            imagename="{{ $value['floor_plan_file_name'] }}"
                                                            imageid="{{ $value['id'] }}"><i
                                                                style="width:15px;height:15px"
                                                                data-feather="trash-2"></i></a>
                                                    </div>
                                                @else
                                                @endif
                                            </div>
                                        </div>
                                    </div><!-- Col -->


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-danger" id="postsubmit">UPDATE</button>
                <input class="btn btn-secondary" data-dismiss="modal" type="reset" value="CANCEL">
            </div>
    </div>

    </form>
    </div>
@endforeach
