 @foreach ($getData as $key => $value)

             <div class="row">

            <div class="col-md-12" style="text-align:end">
                 @if (Auth::user()->role_type != Config::get('constants.ROLE_TYPE.CRM_USER'))

                    @if ($is_approved == 0)
                        <button type="button" id="{{ $post_id }}_1_P" class="approverejectpost btn btn-primary">
                            Approve
                        </button>&nbsp;&nbsp;
                        <button type="button" id="{{ $post_id }}_2_P" class="approverejectpost btn btn-danger">
                            Reject
                        </button>
                    @elseif($is_approved == 1)

                        <span class="badge badge-success" style="font-size:14px;"> Approved</span> &nbsp;&nbsp;
                        <button type="button" id="{{ $post_id }}_2_P" class="approverejectpost btn btn-danger">
                            Reject</span>
                        </button>
                    @elseif ($is_approved == 2)
                        <button type="button" id="{{ $post_id }}_1_P" class="approverejectpost btn btn-primary">
                            Approve
                        </button>&nbsp;&nbsp;
                        <span class="badge badge-danger" style="font-size:14px;"> Rejected</span>

                    @else
                    @endif

            @else
            @endif

            </div>
        </div>
          <form enctype="multipart/form-data" id="propertyeditform">
              @csrf
             <input type="hidden" name="postid" id="postid" value="{{ $value['id'] }}">
                     <div class="row" style="margin-top:12px;">
                         <div class="col-md-12 grid-margin">
                             <div style="margin-top: 8px;" class="card">
                                 <div class="card-body">

                                     <div class="row">

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> Property
                                                     Type:</label>
                                                 <p style="font-size:14px;">{{ $value['propertytype'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> Property
                                                     Category:</label>
                                                 <p style="font-size:14px;">{{ $value['propertycategory'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         @if ($value['property_sub_category_id'] != 0)
                                         @php $count = count($PropertySubCategory); @endphp
                                            @if($count != 0)
                                                <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Property
                                                         Sub Category:<span style="color:red">*</span></label>
                                                     <select required class="js-example-basic-multiple w-100"
                                                         id="property_sub_category_id" name="property_sub_category_id">
                                                         <option value="">select</option>
                                                         @foreach ($PropertySubCategory as $key => $psvalue)
                                                             @if ($value['property_sub_category_id'] == $psvalue['id'])
                                                                 <option selected value="{{ $psvalue['id'] }}">
                                                                     {{ $psvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $psvalue['id'] }}">
                                                                     {{ $psvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                            @else

                                            @endif
                                         @else
                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 1)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :<span style="color:red;">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="bhk_id"
                                                         name="bhk_id">
                                                         <option value="">select</option>
                                                         @foreach ($BHKMaster as $key => $bhkvalue)
                                                             @if ($value['bhk_id'] == $bhkvalue['id'])
                                                                 <option selected value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <select class="js-example-basic-multiple w-100" id="bathroom_id"
                                                         name="bathroom_id">
                                                         <option value="">select</option>
                                                         @foreach ($BathroomMaster as $key => $bathvalue)
                                                             @if ($value['bathroom_id'] == $bathvalue['id'])
                                                                 <option selected value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <select class="js-example-basic-multiple w-100" id="balcony_id"
                                                         name="balcony_id">
                                                         <option value="">select</option>
                                                         @foreach ($BalconyMaster as $key => $balconyvalue)
                                                             @if ($value['balcony_id'] == $balconyvalue['id'])
                                                                 <option selected value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:<span style="color: red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="tenant_type_id" name="tenant_type_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                         $tenant = \DB::connection('mysql_1')
                                                                 ->table('property_tenant_type')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $tenanttype = $tenant->pluck('tenant_type_id')->toArray();

                                                         @endphp
                                                         @foreach ($TenantMaster as $key => $ptvalue)
                                                             @if (in_array($ptvalue['id'], $tenanttype))
                                                                 <option selected value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="furnish_type_id" name="furnish_type_id">
                                                         <option value="">select</option>
                                                         @foreach ($FurnishMaster as $key => $Furvalue)
                                                             @if ($value['furnish_type_id'] == $Furvalue['id'])
                                                                 <option selected value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Floor:<span
                                                             style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="floor_id"
                                                         name="floor_id">
                                                         <option value="">select</option>
                                                         @foreach ($FloorMaster as $key => $floorvalue)
                                                             @if ($value['floor_id'] == $floorvalue['id'])
                                                                 <option selected value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>

                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                         @endif <input name="plot_area" id="plot_area"
                                                             value="{{ $value['plot_area'] }}"
                                                             class="form-control mb-4 mb-md-0" type="number">


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     <select class="js-example-basic-multiple w-100" id="area_unit"
                                                         name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>



                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 7)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :<span style="color:red">*</span></label>
                                                     <select disabled class="js-example-basic-multiple w-100"
                                                         id="bhk_id" name="bhk_id">
                                                         <option value="">select</option>
                                                         @foreach ($BHKMaster as $key => $bhkvalue)
                                                             @if ($value['bhk_id'] == $bhkvalue['id'])
                                                                 <option selected value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <select class="js-example-basic-multiple w-100" id="bathroom_id"
                                                         name="bathroom_id">
                                                         <option value="">select</option>
                                                         @foreach ($BathroomMaster as $key => $bathvalue)
                                                             @if ($value['bathroom_id'] == $bathvalue['id'])
                                                                 <option selected value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <select class="js-example-basic-multiple w-100" id="balcony_id"
                                                         name="balcony_id">
                                                         <option value="">select</option>
                                                         @foreach ($BalconyMaster as $key => $balconyvalue)
                                                             @if ($value['balcony_id'] == $balconyvalue['id'])
                                                                 <option selected value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:<span style="color: red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="tenant_type_id" name="tenant_type_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                         $tenant = \DB::connection('mysql_1')
                                                                 ->table('property_tenant_type')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $tenanttype = $tenant->pluck('tenant_type_id')->toArray();

                                                         @endphp
                                                         @foreach ($TenantMaster as $key => $ptvalue)
                                                             @if (in_array($ptvalue['id'], $tenanttype))
                                                                 <option selected value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="furnish_type_id" name="furnish_type_id">
                                                         <option value="">select</option>
                                                         @foreach ($FurnishMaster as $key => $Furvalue)

                                                             @if ($value['furnish_type_id'] == $Furvalue['id'])
                                                                 <option selected value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Floor:<span
                                                             style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="floor_id"
                                                         name="floor_id">
                                                         <option value="">select</option>
                                                         @foreach ($FloorMaster as $key => $floorvalue)
                                                             @if ($value['floor_id'] == $floorvalue['id'])
                                                                 <option selected value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     <select class="js-example-basic-multiple w-100" id="area_unit"
                                                         name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>

                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 6)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="bhk_id"
                                                         name="bhk_id">
                                                         <option value="">select</option>
                                                         @foreach ($BHKMaster as $key => $bhkvalue)
                                                             @if ($value['bhk_id'] == $bhkvalue['id'])
                                                                 <option selected value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <select class="js-example-basic-multiple w-100" id="bathroom_id"
                                                         name="bathroom_id">
                                                         <option value="">select</option>
                                                         @foreach ($BathroomMaster as $key => $bathvalue)
                                                             @if ($value['bathroom_id'] == $bathvalue['id'])
                                                                 <option selected value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <select class="js-example-basic-multiple w-100" id="balcony_id"
                                                         name="balcony_id">
                                                         <option value="">select</option>
                                                         @foreach ($BalconyMaster as $key => $balconyvalue)
                                                             @if ($value['balcony_id'] == $balconyvalue['id'])
                                                                 <option selected value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:<span style="color: red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="tenant_type_id" name="tenant_type_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                         $tenant = \DB::connection('mysql_1')
                                                                 ->table('property_tenant_type')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $tenanttype = $tenant->pluck('tenant_type_id')->toArray();

                                                         @endphp
                                                         @foreach ($TenantMaster as $key => $ptvalue)
                                                             @if (in_array($ptvalue['id'], $tenanttype))
                                                                 <option selected value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="furnish_type_id" name="furnish_type_id">
                                                         <option value="">select</option>
                                                         @foreach ($FurnishMaster as $key => $Furvalue)
                                                             @if ($value['furnish_type_id'] == $Furvalue['id'])
                                                                 <option selected value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Floor:<span
                                                             style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="floor_id"
                                                         name="floor_id">
                                                         <option value="">select</option>
                                                         @foreach ($FloorMaster as $key => $floorvalue)
                                                             @if ($value['floor_id'] == $floorvalue['id'])
                                                                 <option selected value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 2)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="bhk_id"
                                                         name="bhk_id">
                                                         <option value="">select</option>
                                                         @foreach ($BHKMaster as $key => $bhkvalue)
                                                             @if ($value['bhk_id'] == $bhkvalue['id'])
                                                                 <option selected value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <select class="js-example-basic-multiple w-100" id="bathroom_id"
                                                         name="bathroom_id">
                                                         <option value="">select</option>
                                                         @foreach ($BathroomMaster as $key => $bathvalue)
                                                             @if ($value['bathroom_id'] == $bathvalue['id'])
                                                                 <option selected value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <select class="js-example-basic-multiple w-100" id="balcony_id"
                                                         name="balcony_id">
                                                         <option value="">select</option>
                                                         @foreach ($BalconyMaster as $key => $balconyvalue)
                                                             @if ($value['balcony_id'] == $balconyvalue['id'])
                                                                 <option selected value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:<span style="color: red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="tenant_type_id" name="tenant_type_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                         $tenant = \DB::connection('mysql_1')
                                                                 ->table('property_tenant_type')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $tenanttype = $tenant->pluck('tenant_type_id')->toArray();

                                                         @endphp
                                                         @foreach ($TenantMaster as $key => $ptvalue)
                                                             @if (in_array($ptvalue['id'], $tenanttype))
                                                                 <option selected value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="furnish_type_id" name="furnish_type_id">
                                                         <option value="">select</option>
                                                         @foreach ($FurnishMaster as $key => $Furvalue)

                                                             @if ($value['furnish_type_id'] == $Furvalue['id'])
                                                                 <option selected value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Floor:<span
                                                             style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="floor_id"
                                                         name="floor_id">
                                                         <option value="">select</option>
                                                         @foreach ($FloorMaster as $key => $floorvalue)
                                                             @if ($value['floor_id'] == $floorvalue['id'])
                                                                 <option selected value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     <select class="js-example-basic-multiple w-100" id="area_unit"
                                                         name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div>
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif


                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 3)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="bhk_id"
                                                         name="bhk_id">
                                                         <option value="">select</option>
                                                         @foreach ($BHKMaster as $key => $bhkvalue)
                                                             @if ($value['bhk_id'] == $bhkvalue['id'])
                                                                 <option selected value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <select class="js-example-basic-multiple w-100" id="bathroom_id"
                                                         name="bathroom_id">
                                                         <option value="">select</option>
                                                         @foreach ($BathroomMaster as $key => $bathvalue)
                                                             @if ($value['bathroom_id'] == $bathvalue['id'])
                                                                 <option selected value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <select class="js-example-basic-multiple w-100" id="balcony_id"
                                                         name="balcony_id">
                                                         <option value="">select</option>
                                                         @foreach ($BalconyMaster as $key => $balconyvalue)
                                                             @if ($value['balcony_id'] == $balconyvalue['id'])
                                                                 <option selected value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:<span style="color: red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="tenant_type_id" name="tenant_type_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                         $tenant = \DB::connection('mysql_1')
                                                                 ->table('property_tenant_type')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $tenanttype = $tenant->pluck('tenant_type_id')->toArray();

                                                         @endphp
                                                         @foreach ($TenantMaster as $key => $ptvalue)
                                                             @if (in_array($ptvalue['id'], $tenanttype))
                                                                 <option selected value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="furnish_type_id" name="furnish_type_id">
                                                         <option value="">select</option>
                                                         @foreach ($FurnishMaster as $key => $Furvalue)

                                                             @if ($value['furnish_type_id'] == $Furvalue['id'])
                                                                 <option selected value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Floor:<span
                                                             style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="floor_id"
                                                         name="floor_id">
                                                         <option value="">select</option>
                                                         @foreach ($FloorMaster as $key => $floorvalue)
                                                             @if ($value['floor_id'] == $floorvalue['id'])
                                                                 <option selected value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     <select class="js-example-basic-multiple w-100" id="area_unit"
                                                         name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet </label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 4)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="bhk_id"
                                                         name="bhk_id">
                                                         <option value="">select</option>
                                                         @foreach ($BHKMaster as $key => $bhkvalue)
                                                             @if ($value['bhk_id'] == $bhkvalue['id'])
                                                                 <option selected value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <select class="js-example-basic-multiple w-100" id="bathroom_id"
                                                         name="bathroom_id">
                                                         <option value="">select</option>
                                                         @foreach ($BathroomMaster as $key => $bathvalue)
                                                             @if ($value['bathroom_id'] == $bathvalue['id'])
                                                                 <option selected value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <select class="js-example-basic-multiple w-100" id="balcony_id"
                                                         name="balcony_id">
                                                         <option value="">select</option>
                                                         @foreach ($BalconyMaster as $key => $balconyvalue)
                                                             @if ($value['balcony_id'] == $balconyvalue['id'])
                                                                 <option selected value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:<span style="color: red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="tenant_type_id" name="tenant_type_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                         $tenant = \DB::connection('mysql_1')
                                                                 ->table('property_tenant_type')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $tenanttype = $tenant->pluck('tenant_type_id')->toArray();

                                                         @endphp
                                                         @foreach ($TenantMaster as $key => $ptvalue)
                                                             @if (in_array($ptvalue['id'], $tenanttype))
                                                                 <option selected value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="furnish_type_id" name="furnish_type_id">
                                                         <option value="">select</option>
                                                         @foreach ($FurnishMaster as $key => $Furvalue)

                                                             @if ($value['furnish_type_id'] == $Furvalue['id'])
                                                                 <option selected value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->



                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     <select class="js-example-basic-multiple w-100" id="area_unit"
                                                         name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif


                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 5)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->




                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:<span style="color: red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="tenant_type_id" name="tenant_type_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                         $tenant = \DB::connection('mysql_1')
                                                                 ->table('property_tenant_type')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $tenanttype = $tenant->pluck('tenant_type_id')->toArray();

                                                         @endphp
                                                         @foreach ($TenantMaster as $key => $ptvalue)
                                                             @if (in_array($ptvalue['id'], $tenanttype))
                                                                 <option selected value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:<span style="color: red">*</span></label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input required name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:<span style="color: red">*</span></label>
                                                     <select required class="js-example-basic-multiple w-100"
                                                         id="area_unit" name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif



                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 9)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>

                                                     <select class="js-example-basic-multiple w-100"
                                                         id="personal_washroom" name="personal_washroom">
                                                         <option value="">select</option>
                                                         @foreach ($WashRoom as $key => $wvalue)
                                                             @if ($value['personal_washroom'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>

                                                     <select class="js-example-basic-multiple w-100"
                                                         id="cafeteria_area_id" name="cafeteria_area_id">
                                                         <option value="">select</option>
                                                         @foreach ($PantryRoom as $key => $cvalue)
                                                             @if ($value['cafeteria_area_id'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:<span style="color:red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="ideal_business_id" name="ideal_business_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                             $busi = \DB::connection('mysql_1')
                                                                 ->table('property_ideal_for_business')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $busitype = $busi->pluck('ideal_business_id')->toArray();

                                                         @endphp
                                                         @foreach ($IdealBusiness as $key => $busivalue)
                                                             @if (in_array($busivalue['id'], $busitype))
                                                                 <option selected value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     <select class="js-example-basic-multiple w-100" id="area_unit"
                                                         name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif
                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 12)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="personal_washroom" name="personal_washroom">
                                                         <option value="">select</option>
                                                         @foreach ($WashRoom as $key => $wvalue)
                                                             @if ($value['personal_washroom'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="cafeteria_area_id" name="cafeteria_area_id">
                                                         <option value="">select</option>
                                                         @foreach ($PantryRoom as $key => $cvalue)
                                                             @if ($value['cafeteria_area_id'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:<span style="color:red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="ideal_business_id" name="ideal_business_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                             $busi = \DB::connection('mysql_1')
                                                                 ->table('property_ideal_for_business')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $busitype = $busi->pluck('ideal_business_id')->toArray();

                                                         @endphp
                                                         @foreach ($IdealBusiness as $key => $busivalue)
                                                             @if (in_array($busivalue['id'], $busitype))
                                                                 <option selected value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:<span style="color: red">*</span></label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input required name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:<span style="color: red">*</span></label>
                                                     <select required class="js-example-basic-multiple w-100"
                                                         id="area_unit" name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->




                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 16)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="personal_washroom" name="personal_washroom">
                                                         <option value="">select</option>
                                                         @foreach ($WashRoom as $key => $wvalue)
                                                             @if ($value['personal_washroom'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="cafeteria_area_id" name="cafeteria_area_id">
                                                         <option value="">select</option>
                                                         @foreach ($PantryRoom as $key => $cvalue)
                                                             @if ($value['cafeteria_area_id'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:<span style="color:red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="ideal_business_id" name="ideal_business_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                             $busi = \DB::connection('mysql_1')
                                                                 ->table('property_ideal_for_business')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $busitype = $busi->pluck('ideal_business_id')->toArray();

                                                         @endphp
                                                         @foreach ($IdealBusiness as $key => $busivalue)
                                                             @if (in_array($busivalue['id'], $busitype))
                                                                 <option selected value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     <select class="js-example-basic-multiple w-100" id="area_unit"
                                                         name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 15)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="personal_washroom" name="personal_washroom">
                                                         <option value="">select</option>
                                                         @foreach ($WashRoom as $key => $wvalue)
                                                             @if ($value['personal_washroom'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="cafeteria_area_id" name="cafeteria_area_id">
                                                         <option value="">select</option>
                                                         @foreach ($PantryRoom as $key => $cvalue)
                                                             @if ($value['cafeteria_area_id'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:<span style="color:red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="ideal_business_id" name="ideal_business_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                             $busi = \DB::connection('mysql_1')
                                                                 ->table('property_ideal_for_business')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $busitype = $busi->pluck('ideal_business_id')->toArray();

                                                         @endphp
                                                         @foreach ($IdealBusiness as $key => $busivalue)
                                                             @if (in_array($busivalue['id'], $busitype))
                                                                 <option selected value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     <select class="js-example-basic-multiple w-100" id="area_unit"
                                                         name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 14)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="personal_washroom" name="personal_washroom">
                                                         <option value="">select</option>
                                                         @foreach ($WashRoom as $key => $wvalue)
                                                             @if ($value['personal_washroom'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="cafeteria_area_id" name="cafeteria_area_id">
                                                         <option value="">select</option>
                                                         @foreach ($PantryRoom as $key => $cvalue)
                                                             @if ($value['cafeteria_area_id'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:<span style="color:red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="ideal_business_id" name="ideal_business_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                             $busi = \DB::connection('mysql_1')
                                                                 ->table('property_ideal_for_business')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $busitype = $busi->pluck('ideal_business_id')->toArray();

                                                         @endphp
                                                         @foreach ($IdealBusiness as $key => $busivalue)
                                                             @if (in_array($busivalue['id'], $busitype))
                                                                 <option selected value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:<span style="color:red">*</span></label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input required name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:<span style="color:red">*</span></label>
                                                     <select required class="js-example-basic-multiple w-100"
                                                         id="area_unit" name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 13)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="personal_washroom" name="personal_washroom">
                                                         <option value="">select</option>
                                                         @foreach ($WashRoom as $key => $wvalue)
                                                             @if ($value['personal_washroom'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="cafeteria_area_id" name="cafeteria_area_id">
                                                         <option value="">select</option>
                                                         @foreach ($PantryRoom as $key => $cvalue)
                                                             @if ($value['cafeteria_area_id'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:<span style="color:red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="ideal_business_id" name="ideal_business_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                             $busi = \DB::connection('mysql_1')
                                                                 ->table('property_ideal_for_business')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $busitype = $busi->pluck('ideal_business_id')->toArray();

                                                         @endphp
                                                         @foreach ($IdealBusiness as $key => $busivalue)
                                                             @if (in_array($busivalue['id'], $busitype))
                                                                 <option selected value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     <select class="js-example-basic-multiple w-100" id="area_unit"
                                                         name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 11)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="personal_washroom" name="personal_washroom">
                                                         <option value="">select</option>
                                                         @foreach ($WashRoom as $key => $wvalue)
                                                             @if ($value['personal_washroom'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="cafeteria_area_id" name="cafeteria_area_id">
                                                         <option value="">select</option>
                                                         @foreach ($PantryRoom as $key => $cvalue)
                                                             @if ($value['cafeteria_area_id'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:<span style="color:red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="ideal_business_id" name="ideal_business_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                             $busi = \DB::connection('mysql_1')
                                                                 ->table('property_ideal_for_business')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $busitype = $busi->pluck('ideal_business_id')->toArray();

                                                         @endphp
                                                         @foreach ($IdealBusiness as $key => $busivalue)
                                                             @if (in_array($busivalue['id'], $busitype))
                                                                 <option selected value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     <select class="js-example-basic-multiple w-100" id="area_unit"
                                                         name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 10)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="personal_washroom" name="personal_washroom">
                                                         <option value="">select</option>
                                                         @foreach ($WashRoom as $key => $wvalue)
                                                             @if ($value['personal_washroom'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="cafeteria_area_id" name="cafeteria_area_id">
                                                         <option value="">select</option>
                                                         @foreach ($PantryRoom as $key => $cvalue)
                                                             @if ($value['cafeteria_area_id'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:<span style="color:red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="ideal_business_id" name="ideal_business_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                             $busi = \DB::connection('mysql_1')
                                                                 ->table('property_ideal_for_business')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $busitype = $busi->pluck('ideal_business_id')->toArray();

                                                         @endphp
                                                         @foreach ($IdealBusiness as $key => $busivalue)
                                                             @if (in_array($busivalue['id'], $busitype))
                                                                 <option selected value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     <select class="js-example-basic-multiple w-100" id="area_unit"
                                                         name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 8)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="personal_washroom" name="personal_washroom">
                                                         <option value="">select</option>
                                                         @foreach ($WashRoom as $key => $wvalue)
                                                             @if ($value['personal_washroom'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $wvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="cafeteria_area_id" name="cafeteria_area_id">
                                                         <option value="">select</option>
                                                         @foreach ($PantryRoom as $key => $cvalue)
                                                             @if ($value['cafeteria_area_id'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $cvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:<span style="color:red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="ideal_business_id" name="ideal_business_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                             $busi = \DB::connection('mysql_1')
                                                                 ->table('property_ideal_for_business')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $busitype = $busi->pluck('ideal_business_id')->toArray();

                                                         @endphp
                                                         @foreach ($IdealBusiness as $key => $busivalue)
                                                             @if (in_array($busivalue['id'], $busitype))
                                                                 <option selected value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $busivalue['id'] }}">
                                                                     {{ $busivalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     <select class="js-example-basic-multiple w-100" id="area_unit"
                                                         name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 4 )

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="bhk_id"
                                                         name="bhk_id">
                                                         <option value="">select</option>
                                                         @foreach ($BHKMaster as $key => $bhkvalue)
                                                             @if ($value['bhk_id'] == $bhkvalue['id'])
                                                                 <option selected value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->




                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:<span style="color: red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="tenant_type_id" name="tenant_type_id[]">
                                                         <option value="">select</option>
                                                         @php
                                                         $tenant = \DB::connection('mysql_1')
                                                                 ->table('property_tenant_type')
                                                                 ->where('property_id', $value['id'])
                                                                 ->get();
                                                             $tenanttype = $tenant->pluck('tenant_type_id')->toArray();

                                                         @endphp
                                                         @foreach ($TenantMaster as $key => $ptvalue)
                                                             @if (in_array($ptvalue['id'], $tenanttype))
                                                                 <option selected value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $ptvalue['id'] }}">
                                                                     {{ $ptvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="furnish_type_id" name="furnish_type_id">
                                                         <option value="">select</option>
                                                         @foreach ($FurnishMaster as $key => $Furvalue)

                                                             @if ($value['furnish_type_id'] == $Furvalue['id'])
                                                                 <option selected value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Width of
                                                         road facing the plot(in meters):<span
                                                             style="color:red">*</span></label>
                                                     <input name="width_of_road" id="width_of_road"
                                                         value="{{ $value['width_of_road'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />

                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Floor:<span
                                                             style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="floor_id"
                                                         name="floor_id">
                                                         <option value="">select</option>
                                                         @foreach ($FloorMaster as $key => $floorvalue)
                                                             @if ($value['floor_id'] == $floorvalue['id'])
                                                                 <option selected value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month):<span
                                                             style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     @if ($value['plot_area'] == 0)
                                                         @php $value['plot_area'] = ""; @endphp

                                                     @else

                                                     @endif
                                                     <input name="plot_area" id="plot_area"
                                                         value="{{ $value['plot_area'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     <select class="js-example-basic-multiple w-100" id="area_unit"
                                                         name="area_unit">
                                                         <option value="">select</option>
                                                         @foreach ($AreaUnit as $key => $areavalue)
                                                             @if ($value['area_unit'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $areavalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet <span style="color:red">*</span></label>
                                                     <input name="carpet_area" id="carpet_area"
                                                         value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">Year</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_year"
                                                         name="expected_duration_of_year">
                                                         <option value="" selected>Select Year</option>
                                                         @for ($i = 0; $i <= 10; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_year'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                     <p style="font-size:14px;">Month</p>
                                                     <select
                                                         class="form-control js-example-basic-multiple w-100 text-left "
                                                         id="expected_duration_of_month"
                                                         name="expected_duration_of_month">
                                                         <option value="" selected>Select Month</option>
                                                         @for ($i = 0; $i <= 11; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['expected_duration_of_month'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 5 )


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> PG/Hostel
                                                         Name :
                                                         <span style="color:red">*</span></label>
                                                     <input name="PG_or_hostel_name" id="PG_or_hostel_name"
                                                         value="{{ $value['PG_or_hostel_name'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Gender
                                                         :<span style="color:red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="PG_type" name="PG_type[]">
                                                         <option value="">select</option>
                                                         @php
                                                             $PG_type = explode(',', $value['PG_type']);
                                                         @endphp
                                                         @foreach ($Gender as $gendertype)
                                                             @if (in_array($gendertype, $PG_type))
                                                                 <option selected value="{{ $gendertype }}">
                                                                     {{ $gendertype }}</option>
                                                             @else
                                                                 <option value="{{ $gendertype }}">
                                                                     {{ $gendertype }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>


                                             </div>

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:<span style="color: red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="PG_preferred_tenant" name="PG_preferred_tenant[]">
                                                         <option value="">select</option>
                                                         @php
                                                             $PG_preferred_tenant = explode(',', $value['PG_preferred_tenant']);
                                                         @endphp
                                                         @foreach ($PreferPGTenantType as $pfttype)
                                                             @if (in_array($pfttype, $PG_preferred_tenant))
                                                                 <option selected value="{{ $pfttype }}">
                                                                     {{ $pfttype }}</option>
                                                             @else
                                                                 <option value="{{ $pfttype }}">
                                                                     {{ $pfttype }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Room
                                                         Type:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="room_type"
                                                         name="room_type">
                                                         <option value="">select</option>
                                                         @foreach ($PGRoomType as $key => $roomvalue)
                                                             @if ($value['room_type'] == $roomvalue))
                                                                 <option selected value="{{ $roomvalue }}">
                                                                     {{ $roomvalue }}</option>
                                                             @else
                                                                 <option value="{{ $roomvalue }}">
                                                                     {{ $roomvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Attached
                                                         Bathrooms:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="attach_bathroom" name="attach_bathroom">
                                                         <option value="">select</option>
                                                         @foreach ($AttachBathroom as $key => $pvalue)
                                                             @if ($value['attach_bathroom'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Food:<span
                                                             style="color: red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="food_facility" name="food_facility[]">
                                                         <option value="">select</option>
                                                         @php
                                                             $food_facility = explode(',', $value['food_facility']);
                                                         @endphp
                                                         @foreach ($Food as $foovalue)
                                                             @if (in_array($foovalue, $food_facility))
                                                                 <option selected value="{{ $foovalue }}">
                                                                     {{ $foovalue }}</option>
                                                             @else
                                                                 <option value="{{ $foovalue }}">
                                                                     {{ $foovalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Parking:</label>
                                                     <select class="js-example-basic-multiple w-100" id="parking"
                                                         name="parking">
                                                         <option value="">select</option>
                                                         @foreach ($Parking as $key => $parkingvalue)
                                                             @if ($value['parking'] == $parkingvalue))
                                                                 <option selected value="{{ $parkingvalue }}">
                                                                     {{ $parkingvalue }}</option>
                                                             @else
                                                                 <option value="{{ $parkingvalue }}">
                                                                     {{ $parkingvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Gated
                                                         Closing Time:<span style="color:red">*</span></label>
                                                     <input name="gate_closing_time" id="gate_closing_time"
                                                         value="{{ $value['gate_closing_time'] }}"
                                                         class="form-control mb-4 mb-md-0 timepicker" type="text" />

                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Room
                                                         Amenities:</label>
                                                     @php
                                                         $room_amenities = explode(',', $value['room_amenities']);
                                                     @endphp
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="room_amenities" name="room_amenities[]">
                                                         <option value="">select</option>
                                                         @foreach ($RoomAmenities as $rmvalue)
                                                             @if (in_array($rmvalue, $room_amenities))
                                                                 <option selected value="{{ $rmvalue }}">
                                                                     {{ $rmvalue }}</option>
                                                             @else
                                                                 <option value="{{ $rmvalue }}">
                                                                     {{ $rmvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Common Amenities:</label>
                                                     @php
                                                         $common_amenities = explode(',', $value['common_amenities']);
                                                     @endphp
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="common_amenities" name="common_amenities[]">
                                                         <option value="">select</option>
                                                         @foreach ($CommonAmenities as $cmvalue)
                                                             @if (in_array($cmvalue, $common_amenities))
                                                                 <option selected value="{{ $cmvalue }}">
                                                                     {{ $cmvalue }}</option>
                                                             @else
                                                                 <option value="{{ $cmvalue }}">
                                                                     {{ $cmvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">PG
                                                         Rules:<span style="color:red">*</span></label>
                                                     @php
                                                         $PG_rules = explode(',', $value['PG_rules']);
                                                     @endphp
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="PG_rules" name="PG_rules[]">
                                                         <option value="">select</option>
                                                         @foreach ($PGRules as $pgrulesvalue)
                                                             @if (in_array($pgrulesvalue, $PG_rules))
                                                                 <option selected value="{{ $pgrulesvalue }}">
                                                                     {{ $pgrulesvalue }}</option>
                                                             @else
                                                                 <option value="{{ $pgrulesvalue }}">
                                                                     {{ $pgrulesvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>

                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges:<span style="color:red">*</span></label>
                                                     <input name="maintenance_charges" id="maintenance_charges"
                                                         value="{{ $value['maintenance_charges'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif


                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 6 )

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Flatmate
                                                         Property Type :<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="flatmates_property_type" name="flatmates_property_type">
                                                         <option value="">select</option>
                                                         @foreach ($FlatProperty as $key => $fptvalue)
                                                             @if ($value['flatmates_property_type'] == $fptvalue)
                                                                 <option selected value="{{ $fptvalue }}">
                                                                     {{ $fptvalue }}</option>
                                                             @else
                                                                 <option value="{{ $fptvalue }}">
                                                                     {{ $fptvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Tenant
                                                         Type:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="flatmates_tenant_type" name="flatmates_tenant_type">
                                                         <option value="">select</option>
                                                         @foreach ($FlatTenantType as $key => $fttype)
                                                             @if ($value['flatmates_tenant_type'] == $fttype)
                                                                 <option selected value="{{ $fttype }}">
                                                                     {{ $fttype }}</option>
                                                             @else
                                                                 <option value="{{ $fttype }}">
                                                                     {{ $fttype }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:<span style="color: red">*</span></label>
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="flatmates_preferred_tenant_type"
                                                         name="flatmates_preferred_tenant_type[]">
                                                         <option value="">select</option>
                                                         @php
                                                             $flatmates_preferred_tenant_type = explode(',', $value['flatmates_preferred_tenant_type']);
                                                         @endphp
                                                         @foreach ($PreferFlatTenantType as $pfttype)
                                                             @if (in_array($pfttype, $flatmates_preferred_tenant_type))
                                                                 <option selected value="{{ $pfttype }}">
                                                                     {{ $pfttype }}</option>
                                                             @else
                                                                 <option value="{{ $pfttype }}">
                                                                     {{ $pfttype }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Room
                                                         Type:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="flatmates_room_type" name="flatmates_room_type">
                                                         <option value="">select</option>
                                                         @foreach ($RoomType as $key => $roomvalue)
                                                             @if ($value['flatmates_room_type'] == $roomvalue)

                                                                 <option selected value="{{ $roomvalue }}">
                                                                     {{ $roomvalue }}</option>
                                                             @else
                                                                 <option value="{{ $roomvalue }}">
                                                                     {{ $roomvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="furnish_type_id" name="furnish_type_id">
                                                         <option value="">select</option>
                                                         @foreach ($FurnishMaster as $key => $Furvalue)

                                                             @if ($value['furnish_type_id'] == $Furvalue['id'])
                                                                 <option selected value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <select class="js-example-basic-multiple w-100" id="bathroom_id"
                                                         name="bathroom_id">
                                                         <option value="">select</option>
                                                         @foreach ($BathroomMaster as $key => $bathvalue)
                                                             @if ($value['bathroom_id'] == $bathvalue['id'])
                                                                 <option selected value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Parking:</label>
                                                     <select class="js-example-basic-multiple w-100" id="parking"
                                                         name="parking">
                                                         <option value="">select</option>
                                                         @foreach ($Parking as $key => $parkingvalue)
                                                             @if ($value['parking'] == $parkingvalue))
                                                                 <option selected value="{{ $parkingvalue }}">
                                                                     {{ $parkingvalue }}</option>
                                                             @else
                                                                 <option value="{{ $parkingvalue }}">
                                                                     {{ $parkingvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Non Veg
                                                         Allowed: </label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="flatmates_non_veg_allowed"
                                                         name="flatmates_non_veg_allowed">
                                                         <option value="">select</option>
                                                         @foreach ($NonVegAllow as $key => $nvvalue)
                                                             @if ($value['flatmates_non_veg_allowed'] == $nvvalue)
                                                                 )
                                                                 <option selected value="{{ $nvvalue }}">
                                                                     {{ $nvvalue }}</option>
                                                             @else
                                                                 <option value="{{ $nvvalue }}">
                                                                     {{ $nvvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Gated
                                                         Security:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="flatmates_gated_security" name="flatmates_gated_security">

                                                         <option value="">select</option>
                                                         @foreach ($GatedSecurity as $key => $gatedvalue)
                                                             @if ($value['flatmates_gated_security'] == $gatedvalue)
                                                                 )
                                                                 <option selected value="{{ $gatedvalue }}">
                                                                     {{ $gatedvalue }}</option>
                                                             @else
                                                                 <option value="{{ $gatedvalue }}">
                                                                     {{ $gatedvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Room
                                                         Amenities:</label>
                                                     @php
                                                         $room_amenities = explode(',', $value['room_amenities']);
                                                     @endphp
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="room_amenities" name="room_amenities[]">
                                                         <option value="">select</option>
                                                         @foreach ($RoomAmenities as $rmvalue)
                                                             @if (in_array($rmvalue, $room_amenities))
                                                                 <option selected value="{{ $rmvalue }}">
                                                                     {{ $rmvalue }}</option>
                                                             @else
                                                                 <option value="{{ $rmvalue }}">
                                                                     {{ $rmvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Common Amenities:</label>
                                                     @php
                                                         $common_amenities = explode(',', $value['common_amenities']);
                                                     @endphp
                                                     <select multiple class="js-example-basic-multiple w-100"
                                                         id="common_amenities" name="common_amenities[]">
                                                         <option value="">select</option>
                                                         @foreach ($CommonAmenities as $cmvalue)
                                                             @if (in_array($cmvalue, $common_amenities))
                                                                 <option selected value="{{ $cmvalue }}">
                                                                     {{ $cmvalue }}</option>
                                                             @else
                                                                 <option value="{{ $cmvalue }}">
                                                                     {{ $cmvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Floor:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="flatmates_floor" name="flatmates_floor">
                                                         <option value="">select</option>
                                                         @for ($i = 1; $i <= 200; $i++)
                                                             <option value="{{ $i }}"
                                                                 {{ $i == $value['flatmates_floor'] ? 'selected' : '' }}>
                                                                 {{ $i }}</option>
                                                         @endfor

                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Possession Date:<span style="color:red">*</span></label>
                                                     <p style="font-size:14px;">
                                                         <input name="possession_date" id="possession_date"
                                                             value="{{ \Carbon\Carbon::parse($value['possession_date'])->format('d-m-Y') }}"
                                                             class="form-control mb-4 mb-md-0" type="text" />

                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:<span style="color:red">*</span></label>
                                                     <input name="monthly_rent" id="monthly_rent"
                                                         value="{{ $value['monthly_rent'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:<span style="color:red">*</span></label>
                                                     <input name="security_deposit" id="security_deposit"
                                                         value="{{ $value['security_deposit'] }}"
                                                         class="form-control mb-4 mb-md-0" type="number" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet:<span style="color:red">*</span></label>
                                                     <input name="builtup_area" id="builtup_area"
                                                         value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                         class="form-control mb-4 mb-md-0" type="number"
                                                         minlength='3' />
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="negotiable"
                                                         name="negotiable">
                                                         <option value="">select</option>
                                                         @foreach ($PriceNegotiable as $key => $pvalue)
                                                             @if ($value['negotiable'] == $key)
                                                                 <option selected value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @else
                                                                 <option value="{{ $key }}">
                                                                     {{ $pvalue }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Description:</label>
                                                     <input name="description" id="description"
                                                         value="{{ $value['description'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif



                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 1)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builder
                                                         Name:</label>
                                                     <input id="builder_name" name="builder_name"
                                                         class="form-control mb-4 mb-md-0"
                                                         value="{{ $value['builder_name'] }}" type="text" />
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="bhk_id"
                                                         name="bhk_id">
                                                         <option value="">select</option>
                                                         @foreach ($BHKMaster as $key => $bhkvalue)
                                                             @if ($value['bhk_id'] == $bhkvalue['id'])
                                                                 <option selected value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bhkvalue['id'] }}">
                                                                     {{ $bhkvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <select class="js-example-basic-multiple w-100" id="bathroom_id"
                                                         name="bathroom_id">
                                                         <option value="">select</option>
                                                         @foreach ($BathroomMaster as $key => $bathvalue)
                                                             @if ($value['bathroom_id'] == $bathvalue['id'])
                                                                 <option selected value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $bathvalue['id'] }}">
                                                                     {{ $bathvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <select class="js-example-basic-multiple w-100" id="balcony_id"
                                                         name="balcony_id">
                                                         <option value="">select</option>
                                                         @foreach ($BalconyMaster as $key => $balconyvalue)
                                                             @if ($value['balcony_id'] == $balconyvalue['id'])
                                                                 <option selected value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $balconyvalue['id'] }}">
                                                                     {{ $balconyvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:<span style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="furnish_type_id" name="furnish_type_id">
                                                         <option value="">select</option>
                                                         @foreach ($FurnishMaster as $key => $Furvalue)

                                                             @if ($value['furnish_type_id'] == $Furvalue['id'])
                                                                 <option selected value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $Furvalue['id'] }}">
                                                                     {{ $Furvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Floor:<span
                                                             style="color:red">*</span></label>
                                                     <select class="js-example-basic-multiple w-100" id="floor_id"
                                                         name="floor_id">
                                                         <option value="">select</option>
                                                         @foreach ($FloorMaster as $key => $floorvalue)
                                                             @if ($value['floor_id'] == $floorvalue['id'])
                                                                 <option selected value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $floorvalue['id'] }}">
                                                                     {{ $floorvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>
                                                     <input name="available_from" id="available_from"
                                                         value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <select class="js-example-basic-multiple w-100"
                                                         id="transaction_type_id" name="transaction_type_id">
                                                         <option value="">select</option>
                                                         @foreach ($TransactionType as $key => $ttvalue)
                                                             @if ($value['transaction_type_id'] == $ttvalue['id'])
                                                                 <option selected value="{{ $ttvalue['id'] }}">
                                                                     {{ $ttvalue['title'] }}</option>
                                                             @else
                                                                 <option value="{{ $ttvalue['id'] }}">
                                                                     {{ $ttvalue['title'] }}</option>
                                                             @endif
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div><!-- Col -->
                                             @if ($value['transaction_type_id'] == 2)
                                                 <div id="propertyage" class="col-sm-4">
                                                 @else
                                                     <div id="propertyage" style="display:none" class="col-sm-4">
                                             @endif

                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     If Resale, Property Age:</label>
                                                 <input name="property_age" id="property_age"
                                                     value="{{ $value['property_age'] }}"
                                                     class="form-control mb-4 mb-md-0" type="text" />

                                             </div>
                                     </div><!-- Col -->

                                     <div class="col-sm-4">
                                         <div class="form-group">
                                             <label style="font-weight: bold;" class="control-label">Price:<span
                                                     style="color:red">*</span></label>
                                             <input name="price" id="price" maxlength=10 minlength=4
                                                 value="{{ $value['price'] }}" class="form-control mb-4 mb-md-0"
                                                 type="number" />
                                         </div>
                                     </div><!-- Col -->
                                     <div class="col-sm-4">
                                         <div class="form-group">
                                             <label style="font-weight: bold;" class="control-label">
                                                 Advance:</label>
                                             @if ($value['token_advance'] == 0)
                                                 @php
                                                     $value['token_advance'] = '';
                                                 @endphp
                                             @else
                                             @endif
                                             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                                                 value="{{ $value['token_advance'] }}"
                                                 class="form-control mb-4 mb-md-0" type="number" />
                                         </div>
                                     </div><!-- Col -->
                                     <div class="col-sm-4">
                                         <div class="form-group">
                                             <label style="font-weight: bold;" class="control-label">
                                                 Construction Status:</label>
                                             <select class="js-example-basic-multiple w-100" id="construction_type_id"
                                                 name="construction_type_id">
                                                 <option value="">select</option>
                                                 @foreach ($ConstructionStatus as $key => $csvalue)
                                                     @if ($value['construction_type_id'] == $csvalue['id'])
                                                         <option selected value="{{ $csvalue['id'] }}">
                                                             {{ $csvalue['title'] }}</option>
                                                     @else
                                                         <option value="{{ $csvalue['id'] }}">
                                                             {{ $csvalue['title'] }}</option>
                                                     @endif
                                                 @endforeach
                                             </select>

                                         </div>
                                     </div><!-- Col -->
                                     <div class="col-sm-4">
                                         <div class="form-group">
                                             <label style="font-weight: bold;" class="control-label">Plot
                                                 Area:</label>
                                             @if ($value['plot_area'] == 0)
                                                 @php $value['plot_area'] = ""; @endphp

                                             @else

                                             @endif
                                             <input name="plot_area" id="plot_area"
                                                 value="{{ $value['plot_area'] }}" class="form-control mb-4 mb-md-0"
                                                 type="number" />


                                         </div>
                                     </div><!-- Col -->
                                     <div class="col-sm-4">
                                         <div class="form-group">
                                             <label style="font-weight:  bold;" class="control-label">Area
                                                 Unit:</label>
                                             <select class="js-example-basic-multiple w-100" id="area_unit"
                                                 name="area_unit">
                                                 <option value="">select</option>
                                                 @foreach ($AreaUnit as $key => $areavalue)
                                                     @if ($value['area_unit'] == $key)
                                                         <option selected value="{{ $key }}">
                                                             {{ $areavalue }}</option>
                                                     @else
                                                         <option value="{{ $key }}">
                                                             {{ $areavalue }}</option>
                                                     @endif
                                                 @endforeach
                                             </select>
                                         </div>
                                     </div><!-- Col -->

                                     <div class="col-sm-4">
                                         <div class="form-group">
                                             <label style="font-weight: bold;" class="control-label"> Builtup
                                                 Area in Sq.feet:<span style="color:red">*</span></label>
                                             <input name="builtup_area" id="builtup_area"
                                                 value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                                                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
                                         </div>
                                     </div><!-- Col -->
                                     <div class="col-sm-4">
                                         <div class="form-group">
                                             <label style="font-weight: bold;" class="control-label"> Carpet
                                                 Area in Sq.feet <span style="color:red">*</span></label>
                                             <input name="carpet_area" id="carpet_area"
                                                 value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                                                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
                                         </div>
                                     </div><!-- Col -->

                                     <div class="col-sm-4">
                                         <div class="form-group">
                                             <label style="font-weight: bold;" class="control-label">
                                                 Negotiable:<span style="color:red">*</span></label>
                                             <select class="js-example-basic-multiple w-100" id="negotiable"
                                                 name="negotiable">
                                                 <option value="">select</option>
                                                 @foreach ($PriceNegotiable as $key => $pvalue)
                                                     @if ($value['negotiable'] == $key)
                                                         <option selected value="{{ $key }}">
                                                             {{ $pvalue }}</option>
                                                     @else
                                                         <option value="{{ $key }}">
                                                             {{ $pvalue }}</option>
                                                     @endif
                                                 @endforeach
                                             </select>

                                         </div>
                                     </div><!-- Col -->
                                     <div class="col-sm-4">
                                         <div class="form-group">
                                             <label style="font-weight: bold;" class="control-label">
                                                 Property Description:</label>
                                             <input name="description" id="description"
                                                 value="{{ $value['description'] }}"
                                                 class="form-control mb-4 mb-md-0" type="text" />
                                         </div>
                                     </div><!-- Col -->
                                 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 2)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> BHK
                 :<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="bhk_id" name="bhk_id">
                 <option value="">select</option>
                 @foreach ($BHKMaster as $key => $bhkvalue)
                     @if ($value['bhk_id'] == $bhkvalue['id'])
                         <option selected value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @else
                         <option value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Bathrooms:</label>
             <select class="js-example-basic-multiple w-100" id="bathroom_id" name="bathroom_id">
                 <option value="">select</option>
                 @foreach ($BathroomMaster as $key => $bathvalue)
                     @if ($value['bathroom_id'] == $bathvalue['id'])
                         <option selected value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @else
                         <option value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Balcony:</label>
             <select class="js-example-basic-multiple w-100" id="balcony_id" name="balcony_id">
                 <option value="">select</option>
                 @foreach ($BalconyMaster as $key => $balconyvalue)
                     @if ($value['balcony_id'] == $balconyvalue['id'])
                         <option selected value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @else
                         <option value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 3)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> BHK
                 :<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="bhk_id" name="bhk_id">
                 <option value="">select</option>
                 @foreach ($BHKMaster as $key => $bhkvalue)
                     @if ($value['bhk_id'] == $bhkvalue['id'])
                         <option selected value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @else
                         <option value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Bathrooms:</label>
             <select class="js-example-basic-multiple w-100" id="bathroom_id" name="bathroom_id">
                 <option value="">select</option>
                 @foreach ($BathroomMaster as $key => $bathvalue)
                     @if ($value['bathroom_id'] == $bathvalue['id'])
                         <option selected value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @else
                         <option value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Balcony:</label>
             <select class="js-example-basic-multiple w-100" id="balcony_id" name="balcony_id">
                 <option value="">select</option>
                 @foreach ($BalconyMaster as $key => $balconyvalue)
                     @if ($value['balcony_id'] == $balconyvalue['id'])
                         <option selected value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @else
                         <option value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet </label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 4)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> BHK
                 :<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="bhk_id" name="bhk_id">
                 <option value="">select</option>
                 @foreach ($BHKMaster as $key => $bhkvalue)
                     @if ($value['bhk_id'] == $bhkvalue['id'])
                         <option selected value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @else
                         <option value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Bathrooms:</label>
             <select class="js-example-basic-multiple w-100" id="bathroom_id" name="bathroom_id">
                 <option value="">select</option>
                 @foreach ($BathroomMaster as $key => $bathvalue)
                     @if ($value['bathroom_id'] == $bathvalue['id'])
                         <option selected value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @else
                         <option value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Balcony:</label>
             <select class="js-example-basic-multiple w-100" id="balcony_id" name="balcony_id">
                 <option value="">select</option>
                 @foreach ($BalconyMaster as $key => $balconyvalue)
                     @if ($value['balcony_id'] == $balconyvalue['id'])
                         <option selected value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @else
                         <option value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1' maxlength='7'
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 5)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:<span style="color: red">*</span></label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input required name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:<span style="color: red">*</span></label>
             <select required class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->



     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 6)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> BHK
                 :<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="bhk_id" name="bhk_id">
                 <option value="">select</option>
                 @foreach ($BHKMaster as $key => $bhkvalue)
                     @if ($value['bhk_id'] == $bhkvalue['id'])
                         <option selected value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @else
                         <option value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Bathrooms:</label>
             <select class="js-example-basic-multiple w-100" id="bathroom_id" name="bathroom_id">
                 <option value="">select</option>
                 @foreach ($BathroomMaster as $key => $bathvalue)
                     @if ($value['bathroom_id'] == $bathvalue['id'])
                         <option selected value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @else
                         <option value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Balcony:</label>
             <select class="js-example-basic-multiple w-100" id="balcony_id" name="balcony_id">
                 <option value="">select</option>
                 @foreach ($BalconyMaster as $key => $balconyvalue)
                     @if ($value['balcony_id'] == $balconyvalue['id'])
                         <option selected value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @else
                         <option value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 7)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> BHK
                 :<span style="color:red">*</span></label>
             <select disabled class="js-example-basic-multiple w-100" id="bhk_id" name="bhk_id">
                 <option value="">select</option>
                 @foreach ($BHKMaster as $key => $bhkvalue)
                     @if ($value['bhk_id'] == $bhkvalue['id'])
                         <option selected value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @else
                         <option value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Bathrooms:</label>
             <select class="js-example-basic-multiple w-100" id="bathroom_id" name="bathroom_id">
                 <option value="">select</option>
                 @foreach ($BathroomMaster as $key => $bathvalue)
                     @if ($value['bathroom_id'] == $bathvalue['id'])
                         <option selected value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @else
                         <option value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Balcony:</label>
             <select class="js-example-basic-multiple w-100" id="balcony_id" name="balcony_id">
                 <option value="">select</option>
                 @foreach ($BalconyMaster as $key => $balconyvalue)
                     @if ($value['balcony_id'] == $balconyvalue['id'])
                         <option selected value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @else
                         <option value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>

                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif


 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 8)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Personal
                 Washroom:</label>
             <select class="js-example-basic-multiple w-100" id="personal_washroom" name="personal_washroom">
                 <option value="">select</option>
                 @foreach ($WashRoom as $key => $wvalue)
                     @if ($value['personal_washroom'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Pantry/Cafeteria:</label>
             <select class="js-example-basic-multiple w-100" id="cafeteria_area_id" name="cafeteria_area_id">
                 <option value="">select</option>
                 @foreach ($PantryRoom as $key => $cvalue)
                     @if ($value['cafeteria_area_id'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 9)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Personal
                 Washroom:</label>
             <select class="js-example-basic-multiple w-100" id="personal_washroom" name="personal_washroom">
                 <option value="">select</option>
                 @foreach ($WashRoom as $key => $wvalue)
                     @if ($value['personal_washroom'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Pantry/Cafeteria:</label>
             <select class="js-example-basic-multiple w-100" id="cafeteria_area_id" name="cafeteria_area_id">
                 <option value="">select</option>
                 @foreach ($PantryRoom as $key => $cvalue)
                     @if ($value['cafeteria_area_id'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 10)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Personal
                 Washroom:</label>
             <select class="js-example-basic-multiple w-100" id="personal_washroom" name="personal_washroom">
                 <option value="">select</option>
                 @foreach ($WashRoom as $key => $wvalue)
                     @if ($value['personal_washroom'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Pantry/Cafeteria:</label>
             <select class="js-example-basic-multiple w-100" id="cafeteria_area_id" name="cafeteria_area_id">
                 <option value="">select</option>
                 @foreach ($PantryRoom as $key => $cvalue)
                     @if ($value['cafeteria_area_id'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>

         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 11)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Personal
                 Washroom:</label>
             <select class="js-example-basic-multiple w-100" id="personal_washroom" name="personal_washroom">
                 <option value="">select</option>
                 @foreach ($WashRoom as $key => $wvalue)
                     @if ($value['personal_washroom'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Pantry/Cafeteria:</label>
             <select class="js-example-basic-multiple w-100" id="cafeteria_area_id" name="cafeteria_area_id">
                 <option value="">select</option>
                 @foreach ($PantryRoom as $key => $cvalue)
                     @if ($value['cafeteria_area_id'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 12)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Personal
                 Washroom:</label>
             <select class="js-example-basic-multiple w-100" id="personal_washroom" name="personal_washroom">
                 <option value="">select</option>
                 @foreach ($WashRoom as $key => $wvalue)
                     @if ($value['personal_washroom'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Pantry/Cafeteria:</label>
             <select class="js-example-basic-multiple w-100" id="cafeteria_area_id" name="cafeteria_area_id">
                 <option value="">select</option>
                 @foreach ($PantryRoom as $key => $cvalue)
                     @if ($value['cafeteria_area_id'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:<span style="color: red">*</span></label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input required name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:<span style="color: red">*</span></label>
             <select required class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->



     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 13)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Personal
                 Washroom:</label>
             <select class="js-example-basic-multiple w-100" id="personal_washroom" name="personal_washroom">
                 <option value="">select</option>
                 @foreach ($WashRoom as $key => $wvalue)
                     @if ($value['personal_washroom'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Pantry/Cafeteria:</label>
             <select class="js-example-basic-multiple w-100" id="cafeteria_area_id" name="cafeteria_area_id">
                 <option value="">select</option>
                 @foreach ($PantryRoom as $key => $cvalue)
                     @if ($value['cafeteria_area_id'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 14)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Personal
                 Washroom:</label>
             <select class="js-example-basic-multiple w-100" id="personal_washroom" name="personal_washroom">
                 <option value="">select</option>
                 @foreach ($WashRoom as $key => $wvalue)
                     @if ($value['personal_washroom'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Pantry/Cafeteria:</label>
             <select class="js-example-basic-multiple w-100" id="cafeteria_area_id" name="cafeteria_area_id">
                 <option value="">select</option>
                 @foreach ($PantryRoom as $key => $pcvalue)
                     @if ($value['cafeteria_area_id'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pcvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pcvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();
                     // $busitype=[]
                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:<span style="color: red">*</span></label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input required name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:<span style="color: red">*</span></label>
             <select required class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 15)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Personal
                 Washroom:</label>
             <select class="js-example-basic-multiple w-100" id="personal_washroom" name="personal_washroom">
                 <option value="">select</option>
                 @foreach ($WashRoom as $key => $wvalue)
                     @if ($value['personal_washroom'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Pantry/Cafeteria:</label>
             <select class="js-example-basic-multiple w-100" id="cafeteria_area_id" name="cafeteria_area_id">
                 <option value="">select</option>
                 @foreach ($PantryRoom as $key => $cvalue)
                     @if ($value['cafeteria_area_id'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 16)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Personal
                 Washroom:</label>
             <select class="js-example-basic-multiple w-100" id="personal_washroom" name="personal_washroom">
                 <option value="">select</option>
                 @foreach ($WashRoom as $key => $wvalue)
                     @if ($value['personal_washroom'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $wvalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Pantry/Cafeteria:</label>
             <select class="js-example-basic-multiple w-100" id="cafeteria_area_id" name="cafeteria_area_id">
                 <option value="">select</option>
                 @foreach ($PantryRoom as $key => $cvalue)
                     @if ($value['cafeteria_area_id'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $cvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100" id="transaction_type_id" name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     @if ($value['transaction_type_id'] == 2)
         <div id="propertyage" class="col-sm-4">
         @else
             <div id="propertyage" style="display:none" class="col-sm-4">

     @endif
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label">
             If Resale, Property Age:</label>
         <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
             class="form-control mb-4 mb-md-0" type="number" />
     </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif


 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 3 )
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Number of
                 Open Sides:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="no_open_side_id" name="no_open_side_id">
                 <option value="">select</option>
                 @foreach ($Opensite as $key => $opensitevalue)
                     @if ($value['no_open_side_id'] == $opensitevalue['id'])
                         <option selected value="{{ $opensitevalue['id'] }}">
                             {{ $opensitevalue['title'] }}</option>
                     @else
                         <option value="{{ $opensitevalue['id'] }}">
                             {{ $opensitevalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Width of
                 road facing the plot(in meters):<span style="color:red">*</span></label>
             <input name="width_of_road" id="width_of_road" value="{{ $value['width_of_road'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Boundary
                 Wall Made:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="boundary_wall_made" name="boundary_wall_made">
                 <option value="">select</option>
                 @foreach ($BoundaryWall as $key => $bwvalue)
                     @if ($value['boundary_wall_made'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $bwvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $bwvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:<span style="color: red">*</span></label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input required name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:<span style="color: red">*</span></label>
             <select required class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Corner
                 Plot:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="corner_plot" name="corner_plot">
                 <option value="">select</option>
                 @foreach ($BoundaryWall as $key => $bwvalue)
                     @if ($value['corner_plot'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $bwvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $bwvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->




     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->




     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif


 @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 4 )
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> BHK
                 :<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="bhk_id" name="bhk_id">
                 <option value="">select</option>
                 @foreach ($BHKMaster as $key => $bhkvalue)
                     @if ($value['bhk_id'] == $bhkvalue['id'])
                         <option selected value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @else
                         <option value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Number of
                 Open Sides:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="no_open_side_id" name="no_open_side_id">
                 <option value="">select</option>
                 @foreach ($Opensite as $key => $opensitevalue)
                     @if ($value['no_open_side_id'] == $opensitevalue['id'])
                         <option selected value="{{ $opensitevalue['id'] }}">
                             {{ $opensitevalue['title'] }}</option>
                     @else
                         <option value="{{ $opensitevalue['id'] }}">
                             {{ $opensitevalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Width of
                 road facing the plot(in meters):<span style="color:red">*</span></label>
             <input name="width_of_road" id="width_of_road" value="{{ $value['width_of_road'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Maintenance Charges (per month):<span style="color:red">*</span></label>
             <input name="maintenance_charges" id="maintenance_charges" value="{{ $value['maintenance_charges'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Transaction Type:</label>
             <select class="js-example-basic-multiple w-100 transaction_type_id" id="transaction_type_id"
                 name="transaction_type_id">
                 <option value="">select</option>
                 @foreach ($TransactionType as $key => $ttvalue)
                     @if ($value['transaction_type_id'] == $ttvalue['id'])
                         <option selected value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @else
                         <option value="{{ $ttvalue['id'] }}">
                             {{ $ttvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div id="propertyage" style="display:none" id="propertyage" style="display:none" class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 If Resale, Property Age:</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->




     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif




 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 1)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> BHK
                 :<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="bhk_id" name="bhk_id">
                 <option value="">select</option>
                 @foreach ($BHKMaster as $key => $bhkvalue)
                     @if ($value['bhk_id'] == $bhkvalue['id'])
                         <option selected value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @else
                         <option value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Bathrooms:</label>
             <select class="js-example-basic-multiple w-100" id="bathroom_id" name="bathroom_id">
                 <option value="">select</option>
                 @foreach ($BathroomMaster as $key => $bathvalue)
                     @if ($value['bathroom_id'] == $bathvalue['id'])
                         <option selected value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @else
                         <option value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Balcony:</label>
             <select class="js-example-basic-multiple w-100" id="balcony_id" name="balcony_id">
                 <option value="">select</option>
                 @foreach ($BalconyMaster as $key => $balconyvalue)
                     @if ($value['balcony_id'] == $balconyvalue['id'])
                         <option selected value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @else
                         <option value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->



     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 2)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> BHK
                 :<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="bhk_id" name="bhk_id">
                 <option value="">select</option>
                 @foreach ($BHKMaster as $key => $bhkvalue)
                     @if ($value['bhk_id'] == $bhkvalue['id'])
                         <option selected value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @else
                         <option value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Bathrooms:</label>
             <select class="js-example-basic-multiple w-100" id="bathroom_id" name="bathroom_id">
                 <option value="">select</option>
                 @foreach ($BathroomMaster as $key => $bathvalue)
                     @if ($value['bathroom_id'] == $bathvalue['id'])
                         <option selected value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @else
                         <option value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Balcony:</label>
             <select class="js-example-basic-multiple w-100" id="balcony_id" name="balcony_id">
                 <option value="">select</option>
                 @foreach ($BalconyMaster as $key => $balconyvalue)
                     @if ($value['balcony_id'] == $balconyvalue['id'])
                         <option selected value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @else
                         <option value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->



     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 3)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> BHK
                 :<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="bhk_id" name="bhk_id">
                 <option value="">select</option>
                 @foreach ($BHKMaster as $key => $bhkvalue)
                     @if ($value['bhk_id'] == $bhkvalue['id'])
                         <option selected value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @else
                         <option value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Bathrooms:</label>
             <select class="js-example-basic-multiple w-100" id="bathroom_id" name="bathroom_id">
                 <option value="">select</option>
                 @foreach ($BathroomMaster as $key => $bathvalue)
                     @if ($value['bathroom_id'] == $bathvalue['id'])
                         <option selected value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @else
                         <option value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Balcony:</label>
             <select class="js-example-basic-multiple w-100" id="balcony_id" name="balcony_id">
                 <option value="">select</option>
                 @foreach ($BalconyMaster as $key => $balconyvalue)
                     @if ($value['balcony_id'] == $balconyvalue['id'])
                         <option selected value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @else
                         <option value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->



     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet </label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 4)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> BHK
                 :<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="bhk_id" name="bhk_id">
                 <option value="">select</option>
                 @foreach ($BHKMaster as $key => $bhkvalue)
                     @if ($value['bhk_id'] == $bhkvalue['id'])
                         <option selected value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @else
                         <option value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Bathrooms:</label>
             <select class="js-example-basic-multiple w-100" id="bathroom_id" name="bathroom_id">
                 <option value="">select</option>
                 @foreach ($BathroomMaster as $key => $bathvalue)
                     @if ($value['bathroom_id'] == $bathvalue['id'])
                         <option selected value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @else
                         <option value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Balcony:</label>
             <select class="js-example-basic-multiple w-100" id="balcony_id" name="balcony_id">
                 <option value="">select</option>
                 @foreach ($BalconyMaster as $key => $balconyvalue)
                     @if ($value['balcony_id'] == $balconyvalue['id'])
                         <option selected value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @else
                         <option value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->



     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif


 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 5)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->



     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:<span style="color: red">*</span></label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input required name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:<span style="color: red">*</span></label>
             <select required class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 6)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> BHK
                 :<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="bhk_id" name="bhk_id">
                 <option value="">select</option>
                 @foreach ($BHKMaster as $key => $bhkvalue)
                     @if ($value['bhk_id'] == $bhkvalue['id'])
                         <option selected value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @else
                         <option value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Bathrooms:</label>
             <select class="js-example-basic-multiple w-100" id="bathroom_id" name="bathroom_id">
                 <option value="">select</option>
                 @foreach ($BathroomMaster as $key => $bathvalue)
                     @if ($value['bathroom_id'] == $bathvalue['id'])
                         <option selected value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @else
                         <option value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Balcony:</label>
             <select class="js-example-basic-multiple w-100" id="balcony_id" name="balcony_id">
                 <option value="">select</option>
                 @foreach ($BalconyMaster as $key => $balconyvalue)
                     @if ($value['balcony_id'] == $balconyvalue['id'])
                         <option selected value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @else
                         <option value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->



     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 7)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> BHK
                 :<span style="color:red">*</span></label>
             <select disabled class="js-example-basic-multiple w-100" id="bhk_id" name="bhk_id">
                 <option value="">select</option>
                 @foreach ($BHKMaster as $key => $bhkvalue)
                     @if ($value['bhk_id'] == $bhkvalue['id'])
                         <option selected value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @else
                         <option value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Bathrooms:</label>
             <select class="js-example-basic-multiple w-100" id="bathroom_id" name="bathroom_id">
                 <option value="">select</option>
                 @foreach ($BathroomMaster as $key => $bathvalue)
                     @if ($value['bathroom_id'] == $bathvalue['id'])
                         <option selected value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @else
                         <option value="{{ $bathvalue['id'] }}">
                             {{ $bathvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Balcony:</label>
             <select class="js-example-basic-multiple w-100" id="balcony_id" name="balcony_id">
                 <option value="">select</option>
                 @foreach ($BalconyMaster as $key => $balconyvalue)
                     @if ($value['balcony_id'] == $balconyvalue['id'])
                         <option selected value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @else
                         <option value="{{ $balconyvalue['id'] }}">
                             {{ $balconyvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->



     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 8)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 9)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 10)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 11)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 12)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:<span style="color: red">*</span></label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input required name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:<span style="color: red">*</span></label>
             <select required class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 13)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 14)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:<span style="color: red">*</span></label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input required name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:<span style="color: red">*</span></label>
             <select required class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 15)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 16)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builder
                 Name:</label>
             <input id="builder_name" name="builder_name" class="form-control mb-4 mb-md-0"
                 value="{{ $value['builder_name'] }}" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Ideal for
                 Business?:<span style="color:red">*</span></label>
             <select multiple class="js-example-basic-multiple w-100" id="ideal_business_id" name="ideal_business_id[]">
                 <option value="">select</option>
                 @php
                     $busi = \DB::connection('mysql_1')
                         ->table('property_ideal_for_business')
                         ->where('property_id', $value['id'])
                         ->get();
                     $busitype = $busi->pluck('ideal_business_id')->toArray();

                 @endphp
                 @foreach ($IdealBusiness as $key => $busivalue)
                     @if (in_array($busivalue['id'], $busitype))
                         <option selected value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @else
                         <option value="{{ $busivalue['id'] }}">
                             {{ $busivalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age(in Years):</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Construction Status:</label>
             <select class="js-example-basic-multiple w-100" id="construction_type_id" name="construction_type_id">
                 <option value="">select</option>
                 @foreach ($ConstructionStatus as $key => $csvalue)
                     @if ($value['construction_type_id'] == $csvalue['id'])
                         <option selected value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @else
                         <option value="{{ $csvalue['id'] }}">
                             {{ $csvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 3 )
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Number of
                 Open Sides:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="no_open_side_id" name="no_open_side_id">
                 <option value="">select</option>
                 @foreach ($Opensite as $key => $opensitevalue)
                     @if ($value['no_open_side_id'] == $opensitevalue['id'])
                         <option selected value="{{ $opensitevalue['id'] }}">
                             {{ $opensitevalue['title'] }}</option>
                     @else
                         <option value="{{ $opensitevalue['id'] }}">
                             {{ $opensitevalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Width of
                 road facing the plot(in meters):<span style="color:red">*</span></label>
             <input name="width_of_road" id="width_of_road" value="{{ $value['width_of_road'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Boundary
                 Wall Made:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="boundary_wall_made" name="boundary_wall_made">
                 <option value="">select</option>
                 @foreach ($BoundaryWall as $key => $bwvalue)
                     @if ($value['boundary_wall_made'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $bwvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $bwvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:<span style="color: red">*</span></label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input required name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:<span style="color: red">*</span></label>
             <select required class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Corner
                 Plot:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="corner_plot" name="corner_plot">
                 <option value="">select</option>
                 @foreach ($BoundaryWall as $key => $bwvalue)
                     @if ($value['corner_plot'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $bwvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $bwvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->


     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 4)
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> BHK
                 :<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="bhk_id" name="bhk_id">
                 <option value="">select</option>
                 @foreach ($BHKMaster as $key => $bhkvalue)
                     @if ($value['bhk_id'] == $bhkvalue['id'])
                         <option selected value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @else
                         <option value="{{ $bhkvalue['id'] }}">
                             {{ $bhkvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Furnish
                 Type:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="furnish_type_id" name="furnish_type_id">
                 <option value="">select</option>
                 @foreach ($FurnishMaster as $key => $Furvalue)

                     @if ($value['furnish_type_id'] == $Furvalue['id'])
                         <option selected value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @else
                         <option value="{{ $Furvalue['id'] }}">
                             {{ $Furvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Number of
                 Open Sides:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="no_open_side_id" name="no_open_side_id">
                 <option value="">select</option>
                 @foreach ($Opensite as $key => $opensitevalue)
                     @if ($value['no_open_side_id'] == $opensitevalue['id'])
                         <option selected value="{{ $opensitevalue['id'] }}">
                             {{ $opensitevalue['title'] }}</option>
                     @else
                         <option value="{{ $opensitevalue['id'] }}">
                             {{ $opensitevalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Width of
                 road facing the plot(in meters):<span style="color:red">*</span></label>
             <input name="width_of_road" id="width_of_road" value="{{ $value['width_of_road'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Floor:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="floor_id" name="floor_id">
                 <option value="">select</option>
                 @foreach ($FloorMaster as $key => $floorvalue)
                     @if ($value['floor_id'] == $floorvalue['id'])
                         <option selected value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @else
                         <option value="{{ $floorvalue['id'] }}">
                             {{ $floorvalue['title'] }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Available From:<span style="color:red">*</span></label>
             <input name="available_from" id="available_from"
                 value="{{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->



     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Age:</label>
             <input name="property_age" id="property_age" value="{{ $value['property_age'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Price:<span style="color:red">*</span></label>
             <input name="price" id="price" maxlength=10 minlength=4 value="{{ $value['price'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <label style="font-weight: bold;" class="control-label">Advance:</label>
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">Plot
                 Area:</label>
             @if ($value['plot_area'] == 0)
                 @php $value['plot_area'] = ""; @endphp

             @else

             @endif
             <input name="plot_area" id="plot_area" value="{{ $value['plot_area'] }}"
                 class="form-control mb-4 mb-md-0" type="number" />


         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight:  bold;" class="control-label">Area
                 Unit:</label>
             <select class="js-example-basic-multiple w-100" id="area_unit" name="area_unit">
                 <option value="">select</option>
                 @foreach ($AreaUnit as $key => $areavalue)
                     @if ($value['area_unit'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $areavalue }}</option>
                     @endif
                 @endforeach
             </select>
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Builtup
                 Area in Sq.feet:<span style="color:red">*</span></label>
             <input name="builtup_area" id="builtup_area" value="{{ $value['builtup_area'] }}" min='1'
                 maxlength='7' class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label"> Carpet
                 Area in Sq.feet <span style="color:red">*</span></label>
             <input name="carpet_area" id="carpet_area" value="{{ $value['carpet_area'] }}" min="100" maxlength="7"
                 class="form-control mb-4 mb-md-0" type="number" minlength='3' />
         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Advance:</label>
             @if ($value['token_advance'] == 0)
                 @php
                     $value['token_advance'] = '';
                 @endphp
             @else
             @endif
             <input name="token_advance" id="token_advance" maxlength=10 minlength=3
                 value="{{ $value['token_advance'] }}" class="form-control mb-4 mb-md-0" type="number" />
         </div>
     </div><!-- Col -->

     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Negotiable:<span style="color:red">*</span></label>
             <select class="js-example-basic-multiple w-100" id="negotiable" name="negotiable">
                 <option value="">select</option>
                 @foreach ($PriceNegotiable as $key => $pvalue)
                     @if ($value['negotiable'] == $key)
                         <option selected value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @else
                         <option value="{{ $key }}">
                             {{ $pvalue }}</option>
                     @endif
                 @endforeach
             </select>

         </div>
     </div><!-- Col -->
     <div class="col-sm-4">
         <div class="form-group">
             <label style="font-weight: bold;" class="control-label">
                 Property Description:</label>
             <input name="description" id="description" value="{{ $value['description'] }}"
                 class="form-control mb-4 mb-md-0" type="text" />
         </div>
     </div><!-- Col -->
 @else

 @endif

 <div class="col-sm-4">
     <div class="form-group">
         <label style="font-weight: bold;" class="control-label"> Created
             Date:</label>
         <p style="font-size:14px;">
             {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value['created_at'], 'UTC')->setTimezone('Asia/Calcutta')->format('d-m-Y H:i A') }}
         </p>
     </div>
 </div><!-- Col -->
 </div>
 </div>
 </div>
 <div class="card" style="margin-top: 15px;">
     <div class="card-header">Location Detail:</div>
     <div class="card-body">

         <div class="row">
             <div class="col-sm-4">
                 <div class="form-group">
                     <label style="font-weight: bold;" class="control-label">
                         Property Address:</label>
                     @if ($value['address'] == '')
                         <input id="address" name="address" class=" form-control mb-4 mb-md-0"
                             value="{{ $value['address'] }}" type="text" />
                     @else
                         <p style="font-size:14px;">{{ $value['address'] }}</p>
                     @endif


                 </div>
             </div><!-- Col -->
             <div class="col-sm-4">
                 <div class="form-group">
                     <label style="font-weight: bold;" class="control-label"> State:</label>
                     <p style="font-size:14px;">{{ $value['state'] }}</p>
                 </div>
             </div><!-- Col -->
             <div class="col-sm-4">
                 <div class="form-group">
                     <label style="font-weight: bold;" class="control-label">City:</label>
                     <p style="font-size:14px;">{{ $value['city'] }}</p>
                 </div>
             </div><!-- Col -->

             <div class="col-sm-4">
                 <div class="form-group">
                     <label style="font-weight: bold;" class="control-label">
                         Area:</label>

                     <p style="font-size:14px;">{{ $value['area'] }}</p>


                 </div>
             </div><!-- Col -->
             <div class="col-sm-4">
                 <div class="form-group">
                     <label style="font-weight: bold;" class="control-label">
                         Pincode:</label>

                     <p style="font-size:14px;">{{ $value['pincode'] }}</p>


                 </div>
             </div><!-- Col -->


             <div class="col-sm-4">
                 <div class="form-group">
                     <label class="control-label">Main Photo</label>
                     <input accept="image/*" name="main_image_name" id="main_image_name"
                         class="form-control mb-4 mb-md-0" type="file" />
                     @php
                         if ($value['main_image_name'] != null || $value['main_image_name'] != '') {
                             $path = getFileURLPublicPathPost($value['main_image_name'], $PostType, $value['id'], 'image');
                         } else {
                             $path = '';
                         }
                     @endphp
                     <div class="row" id="appendmainimage">
                         @if ($path != '')
                             <div id="mainimagediv">
                                 <a class="gallery-image" href="{{ $path }}">
                                     <img src={{ $path }} width="130px" height="130px" title="1SqFt"
                                         alt="1SqFt" style=""></a><a href="javascript:void(0)" class=" removeimage"
                                     imageextension="image" posttype="{{ $PostType }}" imagetype="mainimage"
                                     postid="{{ $value['id'] }}" imagename="{{ $value['main_image_name'] }}"
                                     imageid="{{ $value['id'] }}"><i style="width:15px;height:15px"
                                         data-feather="trash-2"></i></a>
                             </div>
                         @else
                         @endif
                     </div>
                 </div>
             </div><!-- Col -->
             <div class="col-sm-8">
                 <div class="form-group">
                     <label class="control-label">Other Photos [Max
                         7-Images]</span></label>
                     <input name="otherimage[]" id="otherimage" accept="image/*" multiple
                         class="form-control mb-4 mb-md-0" type="file" />
                     @php $count = count($value['getimages']); @endphp
                     <input type="hidden" id="totalotherimage" value={{ $count }}>
                 </div>
                 <div class="row" id="appendotherimage">
                     @foreach ($value['getimages'] as $key => $imagevalue)
                         @php $path = getFileURLPublicPathPost($imagevalue['image_name'], $PostType, $value['id'], 'image') @endphp
                         @if ($path != '')
                             <div class="col-sm-6" id="{{ $PostType . '_' . $imagevalue['id'] . '_otherimage' }}">
                                 <a class="gallery-image" href="{{ $path }}">
                                     <img src="{{ $path }}" width="130px" height="130px" title="1SqFt"
                                         alt="1SqFt" style=""></a>
                                 <a href="javascript:void(0)" class="removeimage" imageextension="image"
                                     postid="{{ $value['id'] }}" posttype="{{ $PostType }}"
                                     imagetype="otherimage" imagename="{{ $imagevalue['image_name'] }}"
                                     imageid="{{ $imagevalue['id'] }}"><i style="width:15px;height:15px"
                                         data-feather="trash-2"></i></a>
                                 &nbsp;
                             </div>
                         @else

                         @endif
                     @endforeach
                 </div>

             </div><!-- Col -->
             <div class="col-sm-12" style="text-align: center">
                 <div class="form-group">
                    <button class="btn btn-danger" id="postsubmit">UPDATE</button>
                    <input onClick="window.location.reload();" class="btn btn-secondary" data-dismiss="modal" type="reset" value="CANCEL">
                 </div>
             </div><!-- Col -->
         </div>
     </div>
 </div>
 </div>
 </div>
 </div>
 </form>
 @endforeach
