    <div class="row">
        @if ($scenario == 'Approval')
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Posted Date</label>
                    <input type="text" name="fromdate" id='fromdate' class="filterdate form-control">
                </div>
            </div><!-- Col -->
        @elseif($scenario == "Rejected")
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Rejected Date</label>
                    <input type="text" name="fromdate" id='fromdate' class="filterdate form-control">
                </div>
            </div><!-- Col -->
        @else
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Date</label>
                    <input type="text" name="postdate" id='postdate' class="filterdate form-control">
                </div>
            </div><!-- Col -->
        @endif

        @include('Components.statecityfilter')
        <div class="col-sm-3">
            <div class="form-group">
                <label class="control-label">Post Type</label>
                <select id="posttype" name="posttype" class="js-example-basic-multiple w-100">
                    @foreach ($PostType as $key => $value)
                        @if ($value['value'] == 'P')
                            <option selected value="{{ $value['value'] }}">{{ $value['title'] }}</option>
                        @else
                            <option value="{{ $value['value'] }}">{{ $value['title'] }}</option>
                        @endif
                    @endforeach

                </select>
            </div>
        </div><!-- Col -->


        <div  class="col-sm-3 property">
            <div class="form-group">
                <label class="control-label">Property Type</label>
                <select id="propertytype" name="propertytype" class="js-example-basic-multiple w-100">
                    <option value="">Select</option>
                    @foreach ($PropertyType as $key => $value)
                        <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                    @endforeach

                </select>
            </div>
        </div><!-- Col -->
        <div id="propertycategoryediv"  class="col-sm-3 property">
            <div class="form-group">
                <label class="control-label">Property Category</label>
                <select id="propertycategory" name="propertycategory" class="js-example-basic-multiple w-100">
                    <option value="">select</option>

                </select>
            </div>
        </div><!-- Col -->
        @if ($scenario == 'Leads Not Viewed')

            <div style="display:none" class="col-sm-3 property">
                <div class="form-group">
                    <label class="control-label">User Type</label>
                    <select id="usertype" name="usertype" class="js-example-basic-multiple w-100">
                        <option value="">select</option>
                        @foreach ($userType as $key => $value)
                            <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                        @endforeach

                    </select>
                </div>
            </div><!-- Col -->
        @else
        @endif
        <div id="projectcategoryediv" style="display:none" class="col-sm-3 project">
            <div class="form-group">
                <label class="control-label">Project Category</label>
                <select id="projectcategory" name="projectcategory" class="js-example-basic-multiple w-100">
                    <option selected value="0">All</option>
                    @foreach ($ProjectCategory as $key => $value)
                        @if ($value['title'] == 'Residential' || $value['title'] == 'Commercial')
                            <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div><!-- Col -->
        <div id="servicecategoryediv" style="display:none" class="col-sm-3 service">
            <div class="form-group">
                <label class="control-label">Service Category</label>
                <select id="Servicecategory" name="servicecategory" class="js-example-basic-multiple w-100">
                    <option value="">Select</option>
                    @foreach ($ServiceCategory as $key => $value)
                        <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                    @endforeach

                </select>
            </div>
        </div><!-- Col -->
        <div id="servicesubcategoryediv" style="display:none" class="service col-sm-3">
            <div class="form-group">
                <label class="control-label">Service Sub Category</label>
                <select id="servicesubcategory" name="servicesubcategory" class="js-example-basic-multiple w-100">
                    <option value="">Select</option>
                </select>
            </div>
        </div><!-- Col -->
        @include('Components.searchfilter')
    </div><!-- Row -->
