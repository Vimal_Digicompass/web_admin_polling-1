          <div class="row">
              <div class="col-sm-3">
                  <div class="form-group">
                      <label class="control-label">State</label>
                      <select class="js-example-basic-multiple w-100">
                          <option value="Tamil Nadu">Tamil Nadu</option>
                          <option value="Maharashtra">Maharashtra</option>
                      </select>
                  </div>
              </div><!-- Col -->
              <div class="col-sm-3">
                  <div class="form-group">
                      <label class="control-label">City</label>
                      <select class="js-example-basic-multiple w-100">
                          <option value="chennai">Chennai</option>
                          <option value="mumbai">Mumbai</option>
                      </select>
                  </div>
              </div><!-- Col -->
              @if ($scenario == 'Wallet Recharge Failed' || $scenario == 'Payment Pending')
                  <div class="col-sm-3">
                      <div class="form-group">
                          <label class="control-label">User Type</label>
                          <select class="js-example-basic-multiple w-100">
                              <option value="Builder">Builder</option>
                              <option value="Broker/Agent">Broker/Agent</option>
                              <option value="Service Provider">Service Provider</option>

                          </select>
                      </div>
                  </div><!-- Col -->
              @else
              @endif
              @if ($scenario == 'Wallet Recharge Failed')

                  <div class="col-sm-3">
                      <div class="form-group">
                          <label class="control-label">Package Name</label>
                          <select id="pacakgename" name="packagename" class="js-example-basic-multiple w-100">
                             <option value="">select</option>
                            @foreach ($WalletMaster as $key => $value)
                                  <option value="{{ $value['id'] }}">{{ $value['package_name'] }}</option>
                              @endforeach
                          </select>
                      </div>
                  </div><!-- Col -->
              @else
              @endif
              @if ($scenario == 'Commoner Package Expiry')

                  <div class="col-sm-3">
                      <div class="form-group">
                          <label class="control-label">No of days Package Epiry</label>
                          <select class="js-example-basic-multiple w-100">
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                          </select>
                      </div>
                  </div><!-- Col -->
              @else
              @endif
              <div class="col-sm-3">
                  <button style="margin-top: 30px;
                padding: 10px;" type="button" data-tooltip="tooltip" data-placement="top" title="Click to search"
                      class="btn btn-danger submit">Search</button>
                  <button style="margin-top: 30px;
                padding: 10px;" type="button" data-tooltip="tooltip" data-placement="top" title="Export"
                      class="btn btn-primary submit">Export to Excel</button>
              </div>
          </div><!-- Row -->
