@foreach ($getData as $key => $value)


        <div class="row">
            <div class="col-md-6" style="text-align:initial;">
                @php $flatcount = count($value['getflats']);@endphp
                @if ($projectcategory == 1)
                    @if ($flatcount == 0)
                        <a href="javascript:void(0)" class="pull-right btn btn-danger">
                            Flats({{ $flatcount }})</a>

                    @else
                        <a href="/post/unit/flat/{{ $value['id'] }}" target="_blank"
                            class="pull-right btn btn-danger">
                            Flats({{ $flatcount }})</a>

                    @endif
                @else
                    @if ($flatcount == 0)
                        <a href="javascript:void(0)" class="pull-right btn btn-danger">
                            Units({{ $flatcount }})</a>
                    @else
                        <a href="/post/unit/flat/{{ $value['id'] }}" target="_blank"
                            class="pull-right btn btn-danger">
                            Unit({{ $flatcount }})</a>
                    @endif
                @endif

            </div>
            <div class="col-md-6" style="text-align:end">
                 @if (Auth::user()->role_type != Config::get('constants.ROLE_TYPE.CRM_USER'))
                    @if ($is_approved == 0)
                        <button type="button" id="{{ $post_id }}_1_PR" class="approverejectpost btn btn-primary">
                            Approve
                        </button>&nbsp;&nbsp;
                        <button type="button" id="{{ $post_id }}_2_PR" class="approverejectpost btn btn-danger">
                            Reject
                        </button>
                    @elseif($is_approved == 1)
                        <span class="badge badge-success" style="font-size:14px;"> Approved</span> &nbsp;&nbsp;
                        <button type="button" id="{{ $post_id }}_2_PR" class="approverejectpost btn btn-danger">
                            Reject</span>
                        </button>
                    @elseif ($is_approved == 2)
                        <button type="button" id="{{ $post_id }}_1_PR" class="approverejectpost btn btn-primary">
                            Approve
                        </button>&nbsp;&nbsp;
                        <span class="badge badge-danger" style="font-size:14px;"> Rejected</span>

                    @else
                    @endif
            @else
            @endif

            </div>
        </div>
         <form enctype="multipart/form-data" id="projectform">
              @csrf
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12 grid-margin ">
                <div class="card">
                    <div class="card-body">

                        <div id="wizards">
                            <section>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Project Category:</label>
                                            <p style="font-size:14px;">{{ $value['projectcategory'] }}
                                            </p>
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Project RERA ID <span
                                                    style="color:red;">*</span></label>
                                            <input type="hidden" name="postid" id="postid" value="{{ $value['id'] }}">
                                            <input id="projectreraid" name="projectreraid"
                                                class="form-control mb-4 mb-md-0"
                                                value="{{ $value['project_rera_id'] }}" type="text" />
                                        </div>
                                    </div><!-- Col -->

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Project Name <span
                                                    style="color:red;">*</span></label>
                                            <input id="projectname" name="projectname" class="form-control mb-4 mb-md-0"
                                                value="{{ $value['project_name'] }}" type="text" />
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Project Area (in acre) <span
                                                    style="color:red;">*</span></label>
                                            <input id="projectarea" name="projectarea" class="form-control mb-4 mb-md-0"
                                                value="{{ $value['project_area'] }}" type="number" />
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Total no of units <span
                                                    style="color:red;">*</span></label>
                                            <input id="totalnoofunits" name="totalnoofunits"
                                                class="form-control mb-4 mb-md-0"
                                                value="{{ $value['total_no_of_units'] }}" type="number" />
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Project Description <span
                                                    style="color:red;">*</span></label>
                                            <input id="description" name="description" class="form-control mb-4 mb-md-0"
                                                value="{{ $value['description'] }}" type="text" />
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Available no of units <span
                                                    style="color:red;">*</span></label>
                                            <input id="availablenoofunits" name="availablenoofunits"
                                                class="form-control mb-4 mb-md-0"
                                                value="{{ $value['available_no_of_units'] }}" type="number" />
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Launch Date <span
                                                    style="color:red;">*</span></label>
                                            <input id="launchdate" name="launchdate"
                                                class="followdate form-control mb-4 mb-md-0"
                                                value="{{ \Carbon\Carbon::parse($value['launch_date'])->format('d-m-Y') }}"
                                                type="text" />
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label"> Construction Status <span
                                                    style="color:red;">*</span></label>
                                            <select id="constructionstatus" name="constructionstatus"
                                                class="js-example-basic-multiple w-100">
                                                @foreach ($ConstructionStatus as $key => $ConValue)
                                                    @if ($value['construction_status_id'] == $ConValue['id'])
                                                        <option selected value="{{ $ConValue['id'] }}">
                                                            {{ $ConValue['title'] }}</option>
                                                    @else
                                                        <option value="{{ $ConValue['id'] }}">
                                                            {{ $ConValue['title'] }}</option>
                                                    @endif

                                                @endforeach

                                            </select>
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Area Unit<span
                                                    style="color:red;">*</span></label>
                                            <select id="areaid" name="areaid" class="js-example-basic-multiple w-100">
                                                @foreach ($AreaUnit as $key => $AreaValue)
                                                    @if ($value['area_unit_id'] == $key)
                                                        <option selected value="{{ $key }}">
                                                            {{ $AreaValue }}</option>
                                                    @else
                                                        <option value="{{ $key }}">
                                                            {{ $AreaValue }}</option>
                                                    @endif

                                                @endforeach

                                            </select>
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label"> Total
                                                Floors:<span style="color: red">*</span></label>
                                            <input required id="total_floors" name="total_floors"
                                                class="followdate form-control mb-4 mb-md-0"
                                                value="{{ $value['total_floors'] }}" type="number" />
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Price Negotiable<span
                                                    style="color:red;">*</span></label>
                                            <select id="pricenegotiable" name="pricenegotiable"
                                                class="js-example-basic-multiple w-100">
                                                @foreach ($PriceNegotiable as $key => $negotiableValue)
                                                    @if ($value['price_negotiable'] == $key)
                                                        <option selected value="{{ $key }}">
                                                            {{ $negotiableValue }}</option>
                                                    @else
                                                        <option value="{{ $key }}">
                                                            {{ $negotiableValue }}</option>
                                                    @endif

                                                @endforeach

                                            </select>
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Approval Authority<span
                                                    style="color:red;">*</span></label>
                                            <select id="approvalauthority" name="approvalauthority"
                                                class="js-example-basic-multiple w-100">
                                                @foreach ($ApprovalAuthority as $key => $AppValue)
                                                    @if ($value['approval_authority'] == $AppValue['id'])
                                                        <option selected value="{{ $AppValue['id'] }}">
                                                            {{ $AppValue['title'] }}</option>
                                                    @else
                                                        <option value="{{ $AppValue['id'] }}">
                                                            {{ $AppValue['title'] }}</option>
                                                    @endif

                                                @endforeach


                                            </select>
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label"> Created
                                                Date:</label>
                                            <p style="font-size:14px;">
                                                {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value['created_at'], 'UTC')->setTimezone('Asia/Calcutta')->format('d-m-Y H:i A') }}
                                            </p>
                                        </div>
                                    </div><!-- Col -->
                                </div>
                            </section>
                            <h4>Location</h4>
                            <section>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Address </label>
                                            @if ($value['address'] == '')
                                                <input  id="address" name="address"
                                                    class=" form-control mb-4 mb-md-0"
                                                    value="{{ $value['address'] }}" type="text" />
                                            @else
                                                <p style="font-size:14px;">{{ $value['address'] }}</p>
                                            @endif



                                        </div>
                                    </div><!-- Col -->


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">State</label>
                                            <p style="font-size:14px;">{{ $value['state'] }}</p>
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">City</label>
                                            <p style="font-size:14px;">{{ $value['city'] }}</p>
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Area</label>
                                            <p style="font-size:14px;">{{ $value['area'] }}</p>
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Pincode</label>
                                            <p style="font-size:14px;">{{ $value['pincode'] }}</p>
                                        </div>
                                    </div><!-- Col -->
                                </div>

                            </section>
                            <h4>Upload</h4>
                            <section>
                                <br>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Main Photo</label>
                                            <input accept="image/*" name="main_image_name" id="main_image_name"
                                                class="form-control mb-4 mb-md-0" type="file" />
                                            @php
                                                if ($value['main_image_name'] != null || $value['main_image_name'] != '') {
                                                    $path = getFileURLPublicPathPost($value['main_image_name'], $PostType, $value['id'], 'image');
                                                } else {
                                                    $path = '';
                                                }
                                            @endphp
                                            <div class="row" id="appendmainimage">
                                                @if ($path != '')
                                                    <div id="mainimagediv">
                                                        <a class="gallery-image" href="{{ $path }}">
                                                            <img src={{ $path }} width="130px" height="130px"
                                                                title="1SqFt" alt="1SqFt" style=""></a><a
                                                            href="javascript:void(0)" class="removeimage"
                                                            imageextension="image" posttype="{{ $PostType }}"
                                                            imagetype="mainimage" postid="{{ $value['id'] }}"
                                                            imagename="{{ $value['main_image_name'] }}"
                                                            imageid="{{ $value['id'] }}"><i
                                                                style="width:15px;height:15px"
                                                                data-feather="trash-2"></i></a>
                                                    </div>
                                                @else
                                                @endif
                                            </div>
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Completion Certificate</label>
                                            <input accept="image/*" id="certificate" name="certificate"
                                                class="form-control mb-4 mb-md-0" type="file" />
                                            @php
                                                if ($value['completion_certificate_image_name'] != null || $value['completion_certificate_image_name'] != '') {
                                                    $path = getFileURLPublicPathPost($value['completion_certificate_image_name'], $PostType, $value['id'], 'image');
                                                } else {
                                                    $path = '';
                                                }
                                            @endphp
                                            <div class="row" id="appendcertificateimage">
                                                @if ($path != '')
                                                    <div id="certificatediv">
                                                        <a class="gallery-image" href="{{ $path }}">
                                                            <img src={{ $path }} width="130px" height="130px"
                                                                title="1SqFt" alt="1SqFt" style=""></a><a
                                                            href="javascript:void(0)" class="removeimage"
                                                            imageextension="image" posttype="{{ $PostType }}"
                                                            imagetype="certificateimage" postid="{{ $value['id'] }}"
                                                            imagename="{{ $value['completion_certificate_image_name'] }}"
                                                            imageid="{{ $value['id'] }}"><i
                                                                style="width:15px;height:15px"
                                                                data-feather="trash-2"></i></a>
                                                    </div>
                                                @else
                                                @endif
                                            </div>
                                        </div>
                                    </div><!-- Col -->

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Other Photos<span style="color:red;"> [Max
                                                    7-Images]</span></label>
                                            <input name="otherimage[]" id="otherimage" accept="image/*" multiple
                                                class="form-control mb-4 mb-md-0" type="file" />
                                            @php $count = count($value['getimages']); @endphp
                                            <input type="hidden" id="totalotherimage" value={{ $count }}>
                                        </div>
                                        <div class="row" id="appendotherimage">
                                            @foreach ($value['getimages'] as $key => $imagevalue)
                                                @php $path = getFileURLPublicPathPost($imagevalue['image_name'], $PostType, $value['id'], 'image') @endphp
                                                @if ($path != '')
                                                    <div class="col-sm-6"
                                                        id="{{ $PostType . '_' . $imagevalue['id'] . '_otherimage' }}">
                                                        <a class="gallery-image" href="{{ $path }}"><img
                                                                src="{{ $path }}" width="130px" height="130px"
                                                                title="1SqFt" alt="1SqFt" style=""></a>
                                                        <a href="javascript:void(0)" class="removeimage"
                                                            imageextension="image" postid="{{ $value['id'] }}"
                                                            posttype="{{ $PostType }}" imagetype="otherimage"
                                                            imagename="{{ $imagevalue['image_name'] }}"
                                                            imageid="{{ $imagevalue['id'] }}"><i
                                                                style="width:15px;height:15px"
                                                                data-feather="trash-2"></i></a>
                                                        &nbsp;
                                                    </div>
                                                @else

                                                @endif
                                            @endforeach
                                        </div>
                                    </div><!-- Col -->
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Video</label>
                                            <input name="video_image_name" id="video_image_name"
                                                accept="video/mp4,video/x-m4v,video/*" class="form-control mb-4 mb-md-0"
                                                type="file" />
                                            @php
                                                if ($value['video_file_name'] != null || $value['video_file_name'] != '') {
                                                    $path = getFileURLPublicPathPost($value['video_file_name'], $PostType, $value['id'], 'video');
                                                } else {
                                                    $path = '';
                                                }
                                            @endphp
                                            @if ($path != '')
                                                <div id="videodiv">
                                                    <video width="320" height="240" controls>
                                                        <source src="{{ $path }}" type="video/mp4">
                                                        <source src="movie.ogg" type="video/ogg">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                    <a href="javascript:void(0)" class="removeimage"
                                                        imageextension="video" posttype="{{ $PostType }}"
                                                        imagetype="video" postid="{{ $value['id'] }}"
                                                        imagename="{{ $value['video_file_name'] }}"
                                                        imageid="{{ $value['id'] }}"><i
                                                            style="width:15px;height:15px"
                                                            data-feather="trash-2"></i></a>
                                                </div>
                                            @else
                                            @endif
                                        </div>
                                    </div><!-- Col -->
                                </div>

                            </section>

                            @if ($projectcategory == 1)
                                <h4>Flooring</h4>
                                <section>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Master Bedroom <span
                                                        style="color:red;">*</span></label>
                                                <select id="masterbedroom" name="masterbedroom"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($MasterBedroom as $key => $MBedvalue)
                                                        @if ($value['master_bedroom_Id'] == $MBedvalue['id'])
                                                            <option selected value="{{ $MBedvalue['id'] }}">
                                                                {{ $MBedvalue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $MBedvalue['id'] }}">
                                                                {{ $MBedvalue['title'] }}</option>
                                                        @endif
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Balcony<span
                                                        style="color:red;">*</span></label>
                                                <select id="balcony" name="balcony"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($MasterBedroom as $key => $Balconyvalue)
                                                        @if ($value['balcony_id'] == $Balconyvalue['id'])
                                                            <option selected value="{{ $Balconyvalue['id'] }}">
                                                                {{ $Balconyvalue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $Balconyvalue['id'] }}">
                                                                {{ $Balconyvalue['title'] }}</option>
                                                        @endif

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Kitchen <span
                                                        style="color:red;">*</span></label>
                                                <select id="floorkitchen" name="floorkitech"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($FloorKitchenMaster as $key => $FKitchenvalue)
                                                        @if ($value['floor_kitchen_id'] == $FKitchenvalue['id'])
                                                            <option selected value="{{ $FKitchenvalue['id'] }}">
                                                                {{ $FKitchenvalue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $FKitchenvalue['id'] }}">
                                                                {{ $FKitchenvalue['title'] }}</option>
                                                        @endif

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Other bedroom<span
                                                        style="color:red;">*</span></label>
                                                <select id="otherbedroomo" name="otherbedroom"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($OtherBedroom as $key => $OBedvalue)
                                                        @if ($OBedvalue['id'] == $value['other_bedroom_id'])
                                                            <option selected value="{{ $OBedvalue['id'] }}">
                                                                {{ $OBedvalue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $OBedvalue['id'] }}">
                                                                {{ $OBedvalue['title'] }}</option>

                                                        @endif

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Living/Dining<span
                                                        style="color:red;">*</span></label>
                                                <select id="living" name="living"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($LivingMaster as $key => $Livingvalue)
                                                        @if ($Livingvalue['id'] == $value['living_or_dining_id'])
                                                            <option selected value="{{ $Livingvalue['id'] }}">
                                                                {{ $Livingvalue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $Livingvalue['id'] }}">
                                                                {{ $Livingvalue['title'] }}</option>

                                                        @endif

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Toilet<span
                                                        style="color:red;">*</span></label>
                                                <select id="Ftoilet" name="Ftoilet"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($FloorToiletMaster as $key => $Toiletvalue)
                                                        @if ($Toiletvalue['id'] == $value['floor_toilet_id'])
                                                            <option selected value="{{ $Toiletvalue['id'] }}">
                                                                {{ $Toiletvalue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $Toiletvalue['id'] }}">
                                                                {{ $Toiletvalue['title'] }}</option>

                                                        @endif

                                                    @endforeach


                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                    </div>
                                </section>

                                <h4>Fittings</h4>
                                <section>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Main Door<span
                                                        style="color:red;">*</span></label>
                                                <select name="mdoor" id="mdoor" class="js-example-basic-multiple w-100">
                                                    @foreach ($MDoorMaster as $key => $MDoorValue)
                                                        @if ($MDoorValue['id'] == $value['main_door_id'])
                                                            <option selected value="{{ $MDoorValue['id'] }}">
                                                                {{ $MDoorValue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $MDoorValue['id'] }}">
                                                                {{ $MDoorValue['title'] }}</option>
                                                        @endif

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div><!-- Col -->

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Kitchen <span
                                                        style="color:red;">*</span></label>
                                                <select id="fitkitchen" name="fitkitchen"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($FittingKitchenMaster as $key => $FitKitValue)
                                                        @if ($FitKitValue['id'] == $value['fitting_kitchen_id'])
                                                            <option selected value="{{ $FitKitValue['id'] }}">
                                                                {{ $FitKitValue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $FitKitValue['id'] }}">
                                                                {{ $FitKitValue['title'] }}</option>
                                                        @endif

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Toilet<span
                                                        style="color:red;">*</span></label>
                                                <select id="fittoilet" name="fittoilet"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($FittingToiletMaster as $key => $FitToiletValue)
                                                        @if ($value['fitting_toilet_id'] == $FitToiletValue['id'])
                                                            <option selected value="{{ $FitToiletValue['id'] }}">
                                                                {{ $FitToiletValue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $FitToiletValue['id'] }}">
                                                                {{ $FitToiletValue['title'] }}</option>
                                                        @endif

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Windows<span
                                                        style="color:red;">*</span></label>
                                                <select id="window" name="window"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($WindowMaster as $key => $winValue)
                                                        @if ($value['windows_id'] == $winValue['id'])
                                                            <option selected value="{{ $winValue['id'] }}">
                                                                {{ $winValue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $winValue['id'] }}">
                                                                {{ $winValue['title'] }}</option>
                                                        @endif

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Internal Door <span
                                                        style="color:red;">*</span></label>
                                                <select id="idoor" name="idoor" class="js-example-basic-multiple w-100">
                                                    @foreach ($IDoorMaster as $key => $IDoorValue)
                                                        @if ($IDoorValue['id'] == $value['internal_door_id'])
                                                            <option selected value="{{ $IDoorValue['id'] }}">
                                                                {{ $IDoorValue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $IDoorValue['id'] }}">
                                                                {{ $IDoorValue['title'] }}</option>
                                                        @endif

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Electical<span
                                                        style="color:red;">*</span></label>
                                                <select name="electrical" id="electrical"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($ElectricalMaster as $key => $Evalue)
                                                        @if ($Evalue['id'] == $value['electrical_id'])
                                                            <option selected value="{{ $Evalue['id'] }}">
                                                                {{ $Evalue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $Evalue['id'] }}">
                                                                {{ $Evalue['title'] }}</option>
                                                        @endif

                                                    @endforeach


                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                    </div>
                                </section>
                                <h4>Walls</h4>

                                <section>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Kitchen <span
                                                        style="color:red;">*</span></label>
                                                <select id="wkitchen" name="wkitchen"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($WallKitchenMaster as $key => $WValue)
                                                        @if ($WValue['id'] == $value['wall_kitchen_id'])
                                                            <option selected value="{{ $WValue['id'] }}">
                                                                {{ $WValue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $WValue['id'] }}">
                                                                {{ $WValue['title'] }}</option>
                                                        @endif

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Toilet<span
                                                        style="color:red;">*</span></label>
                                                <select id="wtoilet" name="wtoilet"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($WallToiletMaster as $key => $WToiletValue)
                                                        @if ($WToiletValue['id'] == $value['wall_toilet_id'])
                                                            <option selected value="{{ $WToiletValue['id'] }}">
                                                                {{ $WToiletValue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $WToiletValue['id'] }}">
                                                                {{ $WToiletValue['title'] }}</option>

                                                        @endif

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div><!-- Col -->


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Interior <span
                                                        style="color:red;">*</span></label>
                                                <select id="interior" name="interior"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($InteriorMaster as $key => $Ivalue)
                                                        @if ($value['interior_id'] == $Ivalue['id'])
                                                            <option selected value="{{ $Ivalue['id'] }}">
                                                                {{ $Ivalue['title'] }}</option>
                                                        @else
                                                            <option value="{{ $Ivalue['id'] }}">
                                                                {{ $Ivalue['title'] }}</option>
                                                        @endif

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div><!-- Col -->

                                    </div>
                                </section>
                            @else
                            @endif
                            <h4>Amenities</h4>
                            <section>
                                <br>
                                <div class="row">

                                    @foreach ($Amenities as $key => $amenValue)
                                        @if (in_array($amenValue['id'], $amenityId))
                                            <div class="col-sm-3">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input checked type="checkbox" name="documenttype[]"
                                                            value="{{ $amenValue['id'] }}"
                                                            class="form-check-input documenttype">
                                                        {{ $amenValue['title'] }}
                                                        <i class="input-frame"></i></label>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-sm-3">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" name="documenttype[]"
                                                            value="{{ $amenValue['id'] }}"
                                                            class="form-check-input documenttype">
                                                        <i class="input-frame"></i>
                                                        {{ $amenValue['title'] }}
                                                    </label>
                                                </div>
                                            </div>
                                        @endif

                                    @endforeach

                                </div>
                                <div class="col-sm-12" style='text-align: center;'>
                                    <div class="form-group">
                                        <button class="btn btn-danger" id="postsubmit">UPDATE</button>
                                        <input onClick="window.location.reload();" class="btn btn-secondary" data-dismiss="modal" type="reset"
                                            value="CANCEL">
                                    </div>
                                </div><!-- Col -->
                            </section>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </form>
@endforeach
