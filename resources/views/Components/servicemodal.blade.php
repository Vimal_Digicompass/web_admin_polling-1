 @foreach ($getData as $key => $value)

     <div class="modal-dialog modal-lg" role="document">
         <form>
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="servicedetailviewmodal">Detail View</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                 <div class="modal-body">
                     <div class="row">
                         <div class="col-md-12 grid-margin">
                             <div class="card">
                                 <div class="card-header">User Info:</div>
                                 <div class="card-body">

                                     <div class="row">
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> First
                                                     Name:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['firstname'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> Last
                                                     Name:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['lastname'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">Email
                                                     Address:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['email'] }}</p>
                                             </div>
                                         </div><!-- Col -->

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Company:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['company_name'] }}</p>
                                             </div>
                                         </div><!-- Col -->

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">User
                                                     type:</label>
                                                 @if ($value['user']['user_type'] == 'C')
                                                     <p style="font-size:14px;">Commoners</p>
                                                 @elseif($value['user']['user_type'] == "BR")
                                                     <p style="font-size:14px;">Brokers</p>
                                                 @elseif($value['user']['user_type'] == "B")
                                                     <p style="font-size:14px;">Builder</p>
                                                 @elseif($value['user']['user_type'] == "CT")
                                                     <p style="font-size:14px;">Service Provider</p>
                                                 @else
                                                 @endif

                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> User
                                                     category:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['categoryname'] }}</p>
                                             </div>
                                         </div><!-- Col -->


                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;"
                                                     class="control-label">Address:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['address'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Pincode:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['pincode'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">City:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['city'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> State:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['state'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                     </div>
                                 </div>
                             </div>
                             <div style="margin-top: 15px;" class="card">
                                 <div class="card-header">Leads Detail:</div>
                                 <div class="card-body">
                                     <div class="row">
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Total No of Leads:</label>
                                                 <p style="font-size:14px;">{{ $totalleads }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Total No of Leads Viewed:</label>
                                                 <p style="font-size:14px;">{{ $totalviewleads }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Total No of Leads not viewed:</label>
                                                 <p style="font-size:14px;">{{ $totalunviewleads }}</p>
                                             </div>
                                         </div><!-- Col -->
                                     </div>
                                 </div>
                             </div>
                             <div style="margin-top: 15px;" class="card">
                                 <div class="card-header">Service Detail:</div>
                                 <div class="card-body">

                                     <div class="row">


                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Category:</label>
                                                 <p style="font-size:14px;">{{ $value['servicecategory'] }}</p>
                                             </div>
                                         </div><!-- Col -->

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> Sub
                                                     Category:</label>
                                                 <p style="font-size:14px;">{{ $value['servicesubcategory'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">Short
                                                     Description:</label>
                                                 <p style="font-size:14px;">{{ $value['short_description'] }}</p>
                                             </div>
                                         </div><!-- Col -->

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Benifits/Usage:</label>
                                                 <p style="font-size:14px;">{{ $value['benefits'] }}</p>
                                             </div>
                                         </div><!-- Col -->


                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;"
                                                     class="control-label">Warranty:</label>
                                                 <p style="font-size:14px;">{{ $value['warranty'] }}</p>
                                             </div>
                                         </div><!-- Col -->


                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;"
                                                     class="control-label">Offer/Discounts:</label>
                                                 <p style="font-size:14px;">{{ $value['offers'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Delivery Time:</label>
                                                 @if ($value['hours'] == 0)
                                                     @php $value['hours'] == ""; @endphp
                                                 @else
                                                 @endif
                                                 <p style="font-size:14px;">{{ $value['delivery'] }} days
                                                     {{ $value['hours'] }} hours</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">Created
                                                     Date:</label>
                                                 <p style="font-size:14px;">
                                                     {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value['created_at'], 'UTC')->setTimezone('Asia/Calcutta')->format('d-m-Y H:i A') }}
                                                 </p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">

                                                 @php
                                                     $paymenttext = '';
                                                     $paymenttype = explode(',', $value['payment_mode_id']);
                                                 @endphp

                                                 <label style="font-weight: bold;" class="control-label"> Payment
                                                     Method:</label>
                                                 @foreach ($paymenttype as $type)
                                                     @if ($type == 1)
                                                         <p style="font-size:14px;">By cash</p>
                                                     @elseif($type == 2)
                                                         <p style="font-size:14px;">By DD</p>
                                                     @elseif($type == 3)
                                                         <p style="font-size:14px;">Online Payment</p>
                                                     @elseif($type == 4)
                                                         <p style="font-size:14px;">By Cheque</p>
                                                     @else
                                                     @endif
                                                 @endforeach

                                             </div>
                                         </div><!-- Col -->

                                     </div>
                                 </div>
                             </div>
                             <div class="card" style="margin-top: 15px;">
                                 <div class="card-header">Location Detail:</div>
                                 <div class="card-body">

                                     <div class="row">

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> State:</label>
                                                 <p style="font-size:14px;">{{ $value['state'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">City:</label>
                                                 <p style="font-size:14px;">{{ $value['city'] }}</p>
                                             </div>
                                         </div><!-- Col -->

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Area:</label>
                                                 @php
                                                     $value['getarea'] = $value['getarea']->unique(function ($item) {
                                                         return $item['id'];
                                                     });
                                                 @endphp
                                                 @foreach ($value['getarea'] as $key => $areavalue)
                                                     <p style="font-size:14px;">{{ $areavalue['title'] }}</p>
                                                 @endforeach

                                             </div>
                                         </div><!-- Col -->

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">Main
                                                     Photo:</label><br>
                                                 @php $path = getFileURLPublicPathPost($value['main_image_name'], $PostType, $value['id'], 'image') @endphp
                                                 @if ($path != '')
                                                     <a class="gallery-image" href="{{ $path }}">
                                                         <img src={{ $path }} width="150px" height="150px"
                                                             title="1SqFt" alt="1SqFt" style=""></a>
                                                 @else
                                                 @endif
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Other Photo:</label><br>
                                                 <div class="row">
                                                     @foreach ($value['getimages'] as $key => $imagevalue)
                                                         @php $path = getFileURLPublicPathPost($imagevalue['image_name'], $PostType, $value['id'], 'image') @endphp
                                                         @if ($path != '')
                                                             <div class="col-sm-6" style="margin-top:10px">
                                                                 <a class="gallery-image" href="{{ $path }}">
                                                                     <img src="{{ $path }}" width="150px"
                                                                         height="150px" title="1SqFt" alt="1SqFt"
                                                                         style="">
                                                                 </a>
                                                             </div>
                                                         @else
                                                         @endif
                                                     @endforeach
                                                 </div>


                                             </div>
                                         </div><!-- Col -->

                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">

             </div>

         </form>
     </div>

 @endforeach
