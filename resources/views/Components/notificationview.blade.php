  @php $notifyCount = count($notificationData); @endphp
  <a class="nav-link dropdown-toggle" href="#" id="notificationDropdown" role="button" data-toggle="dropdown"
      aria-haspopup="true" aria-expanded="false">
      <i class="far fa-bell"></i>
      <div class="indicator">
          @if ($notificationDataNew != 0)
              <span style="" class="headerBadgeColor1 badge">
                  {{ $notificationDataNew }}
              </span>
          @else
          @endif
      </div>
  </a>
  @if ($notifyCount != 0)
      <div class="dropdown-menu" aria-labelledby="notificationDropdown">
          <div class="dropdown-header d-flex align-items-center justify-content-between">
              @if ($notificationDataNew != 0)
                  <p class="mb-0 font-weight-medium">{{ $notificationDataNew }} New Notifications
                  </p>
              @else
              @endif
          </div>

          <div class="dropdown-body">
              @if ($notifyCount < 5)
                  @for ($i = 0; $i < $notifyCount; $i++) <a href="{{$notificationData[$i]['criteriapath'][0]['permission_path']  }}/{{ Auth::user()->user_id }}" class="dropdown-item">
                                        <div class="icon">
                                            <i data-feather="user-plus"></i>
                                        </div>
                                        <div class="content">

                                            <p>{{ $notificationData[$i]['description'] }}&nbsp;&nbsp;<span
                                                    class="badge badge-info">{{ Carbon\Carbon::parse($notificationData[$i]['created_at'])->format('d-m-Y H:i:s') }}</span>
                                            </p>
                                        </div>
                                    </a> @endfor @else @for ($i = 0; $i < 5; $i++)
                      <a href="{{$notificationData[$i]['criteriapath'][0]['permission_path']  }}/{{ Auth::user()->user_id }}" class="dropdown-item">
                          <div class="icon">
                              <i data-feather="user-plus"></i>
                          </div>
                          <div class="content">
                               <p>{{ $notificationData[$i]['description'] }}&nbsp;&nbsp;<span
                                                    class="badge badge-info">{{ Carbon\Carbon::parse($notificationData[$i]['created_at'])->format('d-m-Y H:i:s') }}</span>
                                            </p>
                          </div>
                      </a>

              @endfor
  @endif
  </div>
  <div class="dropdown-footer d-flex align-items-center justify-content-center">
      <a href="/notification">View all</a>
  </div>

  </div>
@else
  @endif
