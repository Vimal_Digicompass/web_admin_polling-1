@foreach ($getData as $key=>$value )
<div class="modal-dialog modal-lg" role="document">
        <form>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="projectresidentialdetailviewmodalLabel">Detail View</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 grid-margin">

                            <div class="card">
                                <div class="card-header">User Info:</div>
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> First
                                                    Name:</label>
                                                <p style="font-size:14px;">{{$value['user']['firstname']  }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Last
                                                    Name:</label>
                                                <p style="font-size:14px;">{{$value['user']['lastname']  }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">Email
                                                    Address:</label>
                                                <p style="font-size:14px;">{{ $value['user']['email'] }}</p>
                                            </div>
                                        </div><!-- Col -->

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Company:</label>
                                                <p style="font-size:14px;">{{ $value['user']['company_name'] }}</p>
                                            </div>
                                        </div><!-- Col -->

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">User
                                                    type:</label>
                                                    @if ($value['user']['user_type'] == "C")
                                                    <p style="font-size:14px;">Commoners</p>
                                                    @elseif($value['user']['user_type'] == "BR")
                                                     <p style="font-size:14px;">Brokers</p>
                                                      @elseif($value['user']['user_type'] == "B")
                                                     <p style="font-size:14px;">Builder</p>
                                                      @elseif($value['user']['user_type'] == "CT")
                                                     <p style="font-size:14px;">Service Provider</p>
                                                     @else
                                                    @endif

                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> User
                                                    category:</label>
                                                <p style="font-size:14px;">{{ $value['user']['categoryname'] }}</p>
                                            </div>
                                        </div><!-- Col -->


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">Address:</label>
                                                <p style="font-size:14px;">{{$value['user']['address']}}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Pincode:</label>
                                                <p style="font-size:14px;">{{ $value['user']['pincode'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">City:</label>
                                                <p style="font-size:14px;">{{ $value['user']['city'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> State:</label>
                                                <p style="font-size:14px;">{{ $value['user']['state'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                    </div>
                                </div>
                            </div>
                            <div style="margin-top: 15px;" class="card">
                                <div class="card-header">Leads Detail:</div>
                                <div class="card-body">
                                    <div class="row">
                                           <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Total No of Leads:</label>
                                                <p style="font-size:14px;">{{ $totalleads }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Total No of Leads Viewed:</label>
                                                <p style="font-size:14px;">{{ $totalviewleads }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Total No of Leads not viewed:</label>
                                                <p style="font-size:14px;">{{ $totalunviewleads }}</p>
                                            </div>
                                        </div><!-- Col -->
                                    </div>
                                </div>
                            </div>
                            <div style="margin-top: 15px;" class="card">
                                <div class="card-header">Project Detail:</div>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Project
                                                    Category:</label>
                                                <p style="font-size:14px;">{{ $value['projectcategory'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">Project RERA
                                                    ID:</label>
                                                <p style="font-size:14px;">{{ $value['project_rera_id'] }}</p>
                                            </div>
                                        </div><!-- Col -->

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Project Name:</label>
                                                <p style="font-size:14px;">{{ $value['project_name'] }}</p>
                                            </div>
                                        </div><!-- Col -->

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">Project Area (in
                                                    acre):</label>
                                                <p style="font-size:14px;">{{ $value['project_area'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">Total no of
                                                    units:</label>
                                                <p style="font-size:14px;">{{ $value['total_no_of_units'] }}</p>
                                            </div>
                                        </div><!-- Col -->


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">Project
                                                    Description:</label>
                                                <p style="font-size:14px;">{{ $value['description'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Available no of units:</label>
                                                <p style="font-size:14px;">{{ $value['available_no_of_units'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">Launch
                                                    Date:</label>
                                                <p style="font-size:14px;">{{ \Carbon\Carbon::parse($value['launch_date'])->format('d-m-Y') }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Created
                                                    Date:</label>

                                                <p style="font-size:14px;"> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value['created_at'],'UTC')
                                                        ->setTimezone('Asia/Calcutta')
                                                        ->format('d-m-Y H:i A')}}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Construction
                                                    Status:</label>
                                                <p style="font-size:14px;">{{ $value['constructionstatus'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Area
                                                    Unit:</label>
                                                     @if ($value['area_unit_id'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit_id'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                    @elseif ($value['area_unit_id'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit_id'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit_id'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit_id'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit_id'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else
                                                    @endif

                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Total
                                                    Floors:</label>
                                                <p style="font-size:14px;">{{ $value['total_floors'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Address:</label>
                                                <p style="font-size:14px;">{{ $value['address'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> State:</label>
                                                <p style="font-size:14px;">{{ $value['state'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> City:</label>
                                                <p style="font-size:14px;">{{ $value['city'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Area:</label>
                                                <p style="font-size:14px;">{{ $value['area'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Pincode:</label>
                                                <p style="font-size:14px;">{{ $value['pincode'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">Price
                                                    Negotiable:</label>
                                                <p style="font-size:14px;">{{ $value['price_negotiable'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Approval
                                                    Authority:</label>
                                                <p style="font-size:14px;">{{ $value['approvalauthority'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">Main
                                                    Photo:</label><br>
                                                @php $path = getFileURLPublicPathPost($value['main_image_name'], $PostType, $value['id'], 'image') @endphp
                                                    @if($path !="")
                                                    <a class="gallery-image" href="{{ $path }}">
                                                <img src={{  $path }} width="150px" height="150px"
                                                    title="1SqFt" alt="1SqFt" style=""></a>
                                                    @else
                                                    @endif
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">Completion
                                                    Certificate:</label><br>
                                                    @php $path = getFileURLPublicPathPost($value['completion_certificate_image_name'], $PostType, $value['id'], 'image') @endphp
                                                    @if($path !="")
                                                    <a class="gallery-image" href="{{ $path }}">
                                                <img src={{  $path }} width="150px" height="150px"
                                                    title="1SqFt" alt="1SqFt" style=""></a>
                                                    @else
                                                    @endif

                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;"
                                                    class="control-label">Video:</label><br>
                                                     @php $path = getFileURLPublicPathPost($value['video_file_name'], $PostType, $value['id'], 'video') @endphp
                                                    @if($path !="")
                                                <video width="320" height="240" controls>
                                                            <source src="{{ $path }}" type="video/mp4">
                                                            <source src="movie.ogg" type="video/ogg">
                                                            Your browser does not support the video tag.
                                                            </video>
                                                            @else
                                                            @endif
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Other Photo:</label><br>
                                                <div class="row">
                                                    @foreach ($value['getimages'] as $key=>$imagevalue )
                                                    @php $path = getFileURLPublicPathPost($imagevalue['image_name'], $PostType, $value['id'], 'image') @endphp
                                                    @if($path != "")
                                                    <div class="col-sm-6" style="margin-top:10px">
                                                        <a class="gallery-image" href="{{ $path }}">
                                                        <img src="{{ $path }}" width="150px" height="150px"
                                                            title="1SqFt" alt="1SqFt" style=""></a></div>
                                                    @else
                                                    @endif
                                                    @endforeach
                                                </div>


                                            </div>
                                        </div><!-- Col -->
                                    </div>
                                </div>
                            </div>
                            @if($projectcategory == 1)
                            <div class="card" style="margin-top: 15px;">
                                <div class="card-header">Project Detail - Flooring:</div>
                                <div class="card-body">

                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Master:</label>
                                                <p style="font-size:14px;">{{ $value['bedroommaster'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Balcony:</label>
                                                <p style="font-size:14px;">{{ $value['balconymaster'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Kitchen:</label>
                                                <p style="font-size:14px;">{{ $value['kitchenmaster'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Other
                                                    bedroom:</label>
                                                <p style="font-size:14px;">{{ $value['otherbedroom'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Living/Dining:</label>
                                                <p style="font-size:14px;">{{ $value['diningmaster'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Toilet:</label>
                                                <p style="font-size:14px;">{{ $value['toiletmaster'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                    </div>
                                </div>
                            </div>
                            <div class="card" style="margin-top: 15px;">
                                <div class="card-header">Project Detail - Fittings:</div>
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Internal
                                                    Door:</label>
                                                <p style="font-size:14px;">{{ $value['internaldoor'] }}</p>
                                            </div>
                                        </div><!-- Col -->

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Kitchen:</label>
                                                <p style="font-size:14px;">{{ $value['fittingkitchen'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Toilet:</label>
                                                <p style="font-size:14px;">{{ $value['fittingtoilet'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Windows:</label>
                                                <p style="font-size:14px;">{{ $value['windowmaster'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Main
                                                    Door:</label>
                                                <p style="font-size:14px;">{{ $value['maindoor'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Electrical:</label>
                                                <p style="font-size:14px;">{{ $value['electricalmaster'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                    </div>
                                </div>
                            </div>
                            <div class="card" style="margin-top: 15px;">
                                <div class="card-header">Project Detail - Walls:</div>
                                <div class="card-body">

                                    <div class="row">


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Kitchen:</label>
                                                <p style="font-size:14px;">{{ $value['wallkitchen'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Toilet:</label>
                                                <p style="font-size:14px;">{{ $value['walltoilet'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">
                                                    Interior:</label>
                                                <p style="font-size:14px;">{{ $value['interiormaster'] }}</p>
                                            </div>
                                        </div><!-- Col -->

                                    </div>
                                </div>
                            </div>
                            @else
                            @endif
                            <div class="card" style="margin-top: 15px;">
                                <div class="card-header">Amentities:</div>
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            @foreach ($value['getamenities'] as $key=>$amenityvalue )
                                                 <div class="form-check form-check-inline">
                                                <label class="form-check-label">

                                                    {{ $amenityvalue['title'] }}
                                                </label>
                                            </div>
                                            @endforeach


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card" style="margin-top: 15px;">
                                @if($projectcategory == 1)
                                <div class="card-header">Flats:</div>
                                @else
                                <div class="card-header">Units:</div>

                                @endif

                                    @foreach ($value['getflats'] as $key=>$flatvalue )
                                    <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                  @php $path = getFileURLPublicPathPost($flatvalue['floor_plan_file_name'], $PostType, $value['id'], 'flats/'.$flatvalue['id'].'') @endphp
                                                    @if($path != "")
                                                    <div class="col-sm-6" style="margin-top:10px">
                                                        <a class="gallery-image" href="{{ $path }}">
                                                        <img src="{{ $path }}" width="150px" height="150px"
                                                            title="1SqFt" alt="1SqFt" style=""></div></a>
                                                    @else
                                                    @endif

                                            </div>
                                        </div><!-- Col -->
                                         @if ($projectcategory == 1)
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> BHK:</label>
                                                <p style="font-size:14px;">{{ $flatvalue['bhk'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        @else
                                        @endif
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Furnish
                                                    Type:</label>
                                                <p style="font-size:14px;">{{ $flatvalue['Furniture_Titile'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Floor
                                                    No:</label>
                                                <p style="font-size:14px;">{{$flatvalue['Floor_Title']  }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Carpet
                                                    area:</label>
                                                <p style="font-size:14px;">{{ $flatvalue['carpet_area'] }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label">Transaction Type</label>

                                                    @foreach ($TransactionType as $key => $tvalue)
                                                        @if ($flatvalue['transaction_type_id'] == $tvalue['id'])
                                                            <p style="font-size:14px;"> {{ $tvalue['title'] }}</p>
                                                        @else

                                                        @endif
                                                    @endforeach

                                            </div>
                                        </div><!-- Col -->
                                         @if ($flatvalue['transaction_type_id'] == 2)
                                             <div id="propertyage"  class="col-sm-4">
                                             @else
                                             <div id="propertyage" style="display:none" class="col-sm-4">
                                             @endif

                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         If Resale, Property Age:</label>
                                                     <input disabled name="flat_age" id="flat_age"
                                                         value="{{ $flatvalue['flat_age'] }}"
                                                         class="form-control mb-4 mb-md-0" type="text" />

                                                 </div>
                                             </div><!-- Col -->

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Price:</label>
                                                <p style="font-size:14px;">₹ {{ ConvertNoToWord($flatvalue['price']) }}</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Loan offered by
                                                    Banks:</label>
                                                    @php $bankoffer=\DB::connection('mysql_1')->table('loan_offered_by_banks_master')
                                                    ->whereIn('id' ,explode(',',$flatvalue['loan_offered_bank_ids']))
                                                    ->get();
                                                        $bankoffer = $bankoffer->pluck('title')->toArray();
                                                        $banknames = implode(',',$bankoffer);

                                                    @endphp
                                                <p style="font-size:14px;">{{ $banknames }}</p>
                                            </div>
                                        </div><!-- Col -->
                                         @if ($projectcategory == 2)
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label">Ideal for business?</label>

                                                        @php $business = \DB::connection('mysql_1')
                                                                ->table('project_flat_ideal_for_business')
                                                                ->where('flat_id', $flatvalue['id'])
                                                                ->get();
                                                            $business = $business->pluck('ideal_business_id')->toArray();

                                                        @endphp
                                                        @foreach ($IdealBusniess as $key => $bvalue)
                                                            @if (in_array($bvalue['id'], $business))
                                                                   <p style="font-size:14px;"> {{ $bvalue['title'] }}</p>
                                                            @else

                                                            @endif
                                                        @endforeach
                                                </div>
                                            </div><!-- Col -->
                                        @else
                                        @endif
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label style="font-weight: bold;" class="control-label"> Possession
                                                    Date:</label>
                                                <p style="font-size:14px;">{{ \Carbon\Carbon::parse($flatvalue['possession_date'])->format('d-m-Y') }}</p>
                                            </div>
                                        </div><!-- Col -->
                                    </div>
                                    </div>
                                    <hr>
                                    @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </form>
    </div>
@endforeach
