 @foreach ($getData as $key => $value)
     <div class="modal-dialog modal-lg" role="document">
         <form>
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="propertydetailviewmodalLabel">Detail View</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                 <div class="modal-body">
                     <div class="row">
                         <div class="col-md-12 grid-margin">

                             <div class="card">
                                 <div class="card-header">User Info:</div>
                                 <div class="card-body">

                                     <div class="row">
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> First
                                                     Name:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['firstname'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> Last
                                                     Name:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['lastname'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">Email
                                                     Address:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['email'] }}</p>
                                             </div>
                                         </div><!-- Col -->

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Company:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['company_name'] }}</p>
                                             </div>
                                         </div><!-- Col -->

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">User
                                                     type:</label>

                                                 @if ($value['user']['user_type'] == 'C')
                                                     <p style="font-size:14px;">Commoners</p>
                                                 @elseif($value['user']['user_type'] == "BR")
                                                     <p style="font-size:14px;">Brokers</p>
                                                 @elseif($value['user']['user_type'] == "B")
                                                     <p style="font-size:14px;">Builder</p>
                                                 @elseif($value['user']['user_type'] == "CT")
                                                     <p style="font-size:14px;">Service Provider</p>
                                                 @else

                                                 @endif

                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> User
                                                     category:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['categoryname'] }}</p>
                                             </div>
                                         </div><!-- Col -->


                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;"
                                                     class="control-label">Address:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['address'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Pincode:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['pincode'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">City:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['city'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> State:</label>
                                                 <p style="font-size:14px;">{{ $value['user']['state'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                     </div>
                                 </div>
                             </div>
                             <div style="margin-top: 15px;" class="card">
                                 <div class="card-header">Leads Detail:</div>
                                 <div class="card-body">
                                     <div class="row">
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Total No of Leads:</label>
                                                 <p style="font-size:14px;">{{ $totalleads }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Total No of Leads Viewed:</label>
                                                 <p style="font-size:14px;">{{ $totalviewleads }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Total No of Leads not viewed:</label>
                                                 <p style="font-size:14px;">{{ $totalunviewleads }}</p>
                                             </div>
                                         </div><!-- Col -->
                                     </div>
                                 </div>
                             </div>
                             <div style="margin-top: 15px;" class="card">
                                 <div class="card-header">Property Detail:</div>
                                 <div class="card-body">

                                     <div class="row">

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> Property
                                                     Type:</label>
                                                 <p style="font-size:14px;">{{ $value['propertytype'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> Property
                                                     Category:</label>
                                                 <p style="font-size:14px;">{{ $value['propertycategory'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         @if ($value['property_sub_category_id'] != 0)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Property
                                                         Sub Category:</label>
                                                     <p style="font-size:14px;">{{ $value['propertysubcategory'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else
                                         @endif
                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 1)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:</label>
                                                     @foreach ($value['gettenant'] as $key => $tenantvalue)
                                                         <p style="font-size:14px;">{{ $tenantvalue['title'] }}</p>
                                                     @endforeach

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 7)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:</label>
                                                     @foreach ($value['gettenant'] as $key => $tenantvalue)
                                                         <p style="font-size:14px;">{{ $tenantvalue['title'] }}</p>
                                                     @endforeach

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->



                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                      <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 6)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:</label>
                                                     @foreach ($value['gettenant'] as $key => $tenantvalue)
                                                         <p style="font-size:14px;">{{ $tenantvalue['title'] }}</p>
                                                     @endforeach

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 2)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:</label>
                                                     @foreach ($value['gettenant'] as $key => $tenantvalue)
                                                         <p style="font-size:14px;">{{ $tenantvalue['title'] }}</p>
                                                     @endforeach

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                      <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div>
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif


                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 3)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:</label>
                                                     @foreach ($value['gettenant'] as $key => $tenantvalue)
                                                         <p style="font-size:14px;">{{ $tenantvalue['title'] }}</p>
                                                     @endforeach

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                      <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 4)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:</label>
                                                     @foreach ($value['gettenant'] as $key => $tenantvalue)
                                                         <p style="font-size:14px;">{{ $tenantvalue['title'] }}</p>
                                                     @endforeach

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->



                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                      <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif


                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 5)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->




                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:</label>
                                                     @foreach ($value['gettenant'] as $key => $tenantvalue)
                                                         <p style="font-size:14px;">{{ $tenantvalue['title'] }}</p>
                                                     @endforeach

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif



                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 9)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                      <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif
                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 12)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->




                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                      <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 16)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 15)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 14)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 13)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 11)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                      <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 10)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 8)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>

                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                     <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 4 && $value['property_sub_category_id'] == 0)

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->




                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:</label>
                                                     @foreach ($value['gettenant'] as $key => $tenantvalue)
                                                         <p style="font-size:14px;">{{ $tenantvalue['title'] }}</p>
                                                     @endforeach

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Width of
                                                         road facing the plot(in meters)*:</label>
                                                     <p style="font-size:14px;">{{ $value['width_of_road'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Expected duration of stay:</label>
                                                      <p style="font-size:14px;">year -
                                                         {{ $value['expected_duration_of_year'] }}</p>
                                                     <p style="font-size:14px;">Month -
                                                         {{ $value['expected_duration_of_month'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 5 && $value['property_sub_category_id'] == 0)


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> PG/Hostel
                                                         Name :
                                                     </label>
                                                     <p style="font-size:14px">
                                                         {{ $value['PG_or_hostel_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Gender
                                                         :</label>
                                                     @php
                                                         $PG_type = explode(',', $value['PG_type']);
                                                     @endphp
                                                     @foreach ($Gender as $gendertype)
                                                         @if (in_array($gendertype, $PG_type))
                                                             <p style="font-size:14px">
                                                                 {{ $gendertype }}</p>
                                                         @else

                                                         @endif
                                                     @endforeach


                                                 </div>
                                             </div>

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:</label>
                                                     @php
                                                         $PG_preferred_tenant = explode(',', $value['PG_preferred_tenant']);
                                                     @endphp
                                                     @foreach ($PreferPGTenantType as $pfttype)
                                                         @if (in_array($pfttype, $PG_preferred_tenant))
                                                             <p style="font-size:14px">
                                                                 {{ $pfttype }}</p>

                                                         @else

                                                         @endif
                                                     @endforeach


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Room
                                                         Type:</label>
                                                     @foreach ($PGRoomType as $key => $roomvalue)
                                                         @if ($value['room_type'] == $roomvalue)
                                                             <p style="font-size:14px">
                                                                 {{ $roomvalue }}</p>
                                                         @else
                                                         @endif
                                                     @endforeach

                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Attached
                                                         Bathrooms:</label>
                                                     @foreach ($AttachBathroom as $key => $pvalue)
                                                         @if ($value['attach_bathroom'] == $key)
                                                             <p style="font-size:14px">
                                                                 {{ $pvalue }}</p>
                                                         @else

                                                         @endif
                                                     @endforeach

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Food:</label>
                                                     @php
                                                         $food_facility = explode(',', $value['food_facility']);
                                                     @endphp
                                                     @foreach ($Food as $foovalue)
                                                         @if (in_array($foovalue, $food_facility))
                                                             <p style="font-size:14px">
                                                                 {{ $foovalue }}</p>
                                                         @else

                                                         @endif
                                                     @endforeach


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Parking:</label>
                                                     @foreach ($Parking as $key => $parkingvalue)
                                                         @if ($value['parking'] == $parkingvalue))
                                                             <p style="font-size:14px;">
                                                                 {{ $parkingvalue }}</p>
                                                         @else

                                                         @endif
                                                     @endforeach

                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Gated
                                                         Closing Time:</label>
                                                     <p style="font-size:14px">{{ $value['gate_closing_time'] }}</p>

                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Room
                                                         Amenities:</label>
                                                     @php
                                                         $room_amenities = explode(',', $value['room_amenities']);
                                                     @endphp
                                                     @foreach ($RoomAmenities as $rmvalue)
                                                         @if (in_array($rmvalue, $room_amenities))
                                                             <p style="font-size:14px">{{ $rmvalue }}</p>
                                                         @else

                                                         @endif
                                                     @endforeach


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Common Amenities:</label>
                                                     @php
                                                         $common_amenities = explode(',', $value['common_amenities']);
                                                     @endphp

                                                     @foreach ($CommonAmenities as $cmvalue)
                                                         @if (in_array($cmvalue, $common_amenities))
                                                             <p style="font-size:14px">{{ $cmvalue }}</p>
                                                         @else

                                                         @endif
                                                     @endforeach


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">PG
                                                         Rules:<span style="color:red">*</span></label>
                                                     @php
                                                         $PG_rules = explode(',', $value['PG_rules']);
                                                     @endphp
                                                     @foreach ($PGRules as $pgrulesvalue)
                                                         @if (in_array($pgrulesvalue, $PG_rules))
                                                             <p style="font-size:14px">
                                                                 {{ $pgrulesvalue }}</p>
                                                         @else

                                                         @endif
                                                     @endforeach

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:<span style="color:red">*</span></label>


                                                     <p style="font-size:14px">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>

                                                     <p style="font-size:14px">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>

                                                     <p style="font-size:14px">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges:<span style="color:red">*</span></label>

                                                     <p style="font-size:14px">{{ $value['maintenance_charges'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @foreach ($PriceNegotiable as $key => $pvalue)
                                                         @if ($value['negotiable'] == $key)
                                                             <p style="font-size:14px">{{ $pvalue }}</p>

                                                         @else

                                                         @endif
                                                     @endforeach

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px">
                                                         {{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif


                                         @if ($value['property_type_id'] == 3 && $value['property_category_id'] == 6 && $value['property_sub_category_id'] == 0)

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Flatmate
                                                         Property Type :</label>
                                                     <p style="font-size:14px;">
                                                         {{ $value['flatmates_property_type'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Tenant
                                                         Type:</label>

                                                     <p style="font-size:14px;">
                                                         {{ $value['flatmates_tenant_type'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Preferred
                                                         Tenant:</label>

                                                     <p style="font-size:14px;">
                                                         {{ $value['flatmates_preferred_tenant_type'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Room
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['flatmates_room_type'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Parking:</label>
                                                     <p style="font-size:14px;">{{ $value['parking'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Non Veg
                                                         Allowed:</label>
                                                     <p style="font-size:14px;">
                                                         {{ $value['flatmates_non_veg_allowed'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Gated
                                                         Security:</label>
                                                     <p style="font-size:14px;">
                                                         {{ $value['flatmates_gated_security'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Room
                                                         Amenities:</label>
                                                     <p style="font-size:14px;">{{ $value['room_amenities'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Common Amenities:</label>
                                                     <p style="font-size:14px;">{{ $value['common_amenities'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['flatmates_floor'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Possession Date:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['possession_date'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Monthly
                                                         Rent:</label>
                                                     <p style="font-size:14px;">{{ $value['monthly_rent'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Security Deposit:</label>
                                                     <p style="font-size:14px;">{{ $value['security_deposit'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif



                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 1)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 2)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 3)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 4)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 5)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->



                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 6)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 7)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif


                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 8)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 9)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 10)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 11)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 12)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->



                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 13)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else


                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 14)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 15)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 16)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Personal
                                                         Washroom:</label>
                                                     @if ($value['personal_washroom'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;"
                                                         class="control-label">Pantry/Cafeteria:</label>
                                                     @if ($value['cafeteria_area_id'] == 1)
                                                         <p style="font-size:14px;">Dry</p>
                                                     @elseif($value['cafeteria_area_id'] == 2)
                                                         <p style="font-size:14px;">Wet</p>
                                                     @else
                                                         <p style="font-size:14px;">Not Available</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif


                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 3 && $value['property_sub_category_id'] == 0)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Number of
                                                         Open Site:</label>
                                                     <p style="font-size:14px;">{{ $value['opensite'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Width of
                                                         road facing the plot(in meters)*:</label>
                                                     <p style="font-size:14px;">{{ $value['width_of_road'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Boundary
                                                         Wall Made:</label>
                                                     @if ($value['boundary_wall_made'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">NO</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Corner
                                                         Plot:</label>
                                                     @if ($value['corner_plot'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">NO</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->




                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->




                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif


                                         @if ($value['property_type_id'] == 2 && $value['property_category_id'] == 4 && $value['property_sub_category_id'] == 0)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Number of
                                                         Open Site:</label>
                                                     <p style="font-size:14px;">{{ $value['opensite'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Width of
                                                         road facing the plot(in meters)*:</label>
                                                     <p style="font-size:14px;">{{ $value['width_of_road'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Maintenance Charges (per month)*:</label>
                                                     <p style="font-size:14px;">{{ $value['maintenance_charges'] }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Transaction Type:</label>
                                                     <p style="font-size:14px;">{{ $value['transactiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->




                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif




                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 1)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->



                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 2)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->



                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 3)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->



                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 4)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->



                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif


                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 5)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->



                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 6)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->



                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 1 && $value['property_sub_category_id'] == 7)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Bathrooms:</label>
                                                     <p style="font-size:14px;">{{ $value['bathroomtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Balcony:</label>
                                                     <p style="font-size:14px;">{{ $value['balconytitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 8)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 9)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 10)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 11)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 12)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 13)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 14)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 15)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 2 && $value['property_sub_category_id'] == 16)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Bulider
                                                         Name:</label>
                                                     <p style="font-size:14px;">{{ $value['builder_name'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Ideal for
                                                         Business?:</label>
                                                     @foreach ($value['getideal'] as $key => $idealvalue)
                                                         <p style="font-size:14px;">{{ $idealvalue['title'] }}</p>
                                                     @endforeach
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Construction Status:</label>
                                                     <p style="font-size:14px;">{{ $value['constructiontype'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 3 && $value['property_sub_category_id'] == 0)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Number of
                                                         Open Site:</label>
                                                     <p style="font-size:14px;">{{ $value['opensite'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Width of
                                                         road facing the plot(in meters)*:</label>
                                                     <p style="font-size:14px;">{{ $value['width_of_road'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Boundary
                                                         Wall Made:</label>
                                                     @if ($value['boundary_wall_made'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">NO</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Corner
                                                         Plot:</label>
                                                     @if ($value['corner_plot'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">NO</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->


                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         @if ($value['property_type_id'] == 4 && $value['property_category_id'] == 4 && $value['property_sub_category_id'] == 0)
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> BHK
                                                         :</label>
                                                     <p style="font-size:14px;">{{ $value['bhktitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Furnish
                                                         Type:</label>
                                                     <p style="font-size:14px;">{{ $value['furnishtitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Number of
                                                         Open Site:</label>
                                                     <p style="font-size:14px;">{{ $value['opensite'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Width of
                                                         road facing the plot(in meters)*:</label>
                                                     <p style="font-size:14px;">{{ $value['width_of_road'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Floor:</label>
                                                     <p style="font-size:14px;">{{ $value['floortitle'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Available From:</label>
                                                     <p style="font-size:14px;">
                                                         {{ \Carbon\Carbon::parse($value['available_from'])->format('d-m-Y') }}
                                                     </p>
                                                 </div>
                                             </div><!-- Col -->



                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Property Age:</label>
                                                     <p style="font-size:14px;">{{ $value['property_age'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Price:</label>
                                                     <p style="font-size:14px;">
                                                         ₹ {{ ConvertNoToWord($value['price']) }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;"
                                                         class="control-label">Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">Plot
                                                         Area:</label>
                                                     <p style="font-size:14px;">{{ $value['plot_area'] }}</p>


                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight:  bold;" class="control-label">Area
                                                         Unit:</label>
                                                     @if ($value['area_unit'] == 1)
                                                         <p style="font-size:14px;">sq. ft</p>
                                                     @elseif ($value['area_unit'] == 2)
                                                         <p style="font-size:14px;">sq. yrd</p>
                                                     @elseif ($value['area_unit'] == 3)
                                                         <p style="font-size:14px;">sq. m</p>
                                                     @elseif ($value['area_unit'] == 4)
                                                         <p style="font-size:14px;">acre</p>
                                                     @elseif ($value['area_unit'] == 5)
                                                         <p style="font-size:14px;">hectare</p>
                                                     @elseif ($value['area_unit'] == 6)
                                                         <p style="font-size:14px;">ground</p>
                                                          @elseif ($value['area_unit'] == 7)
                                                         <p style="font-size:14px;">cent</p>
                                                     @else

                                                     @endif
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Builtup
                                                         Area in Sq.feet*:</label>
                                                     <p style="font-size:14px;">{{ $value['builtup_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label"> Carpet
                                                         Area in Sq.feet*</label>
                                                     <p style="font-size:14px;">{{ $value['carpet_area'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Advance:</label>
                                                     <p style="font-size:14px;">{{ $value['token_advance'] }}</p>
                                                 </div>
                                             </div><!-- Col -->

                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Negotiable:</label>
                                                     @if ($value['negotiable'] == 1)
                                                         <p style="font-size:14px;">Yes</p>
                                                     @else
                                                         <p style="font-size:14px;">No</p>

                                                     @endif

                                                 </div>
                                             </div><!-- Col -->
                                             <div class="col-sm-4">
                                                 <div class="form-group">
                                                     <label style="font-weight: bold;" class="control-label">
                                                         Description:</label>
                                                     <p style="font-size:14px;">{{ $value['description'] }}</p>
                                                 </div>
                                             </div><!-- Col -->
                                         @else

                                         @endif

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> Created
                                                     Date:</label>
                                                 <p style="font-size:14px;">
                                                     {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value['created_at'],'UTC')
                                                        ->setTimezone('Asia/Calcutta')
                                                        ->format('d-m-Y H:i A')}}
                                                 </p>
                                             </div>
                                         </div><!-- Col -->
                                     </div>
                                 </div>
                             </div>
                             <div class="card" style="margin-top: 15px;">
                                 <div class="card-header">Location Detail:</div>
                                 <div class="card-body">

                                     <div class="row">

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label"> State:</label>
                                                 <p style="font-size:14px;">{{ $value['state'] }}</p>
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">City:</label>
                                                 <p style="font-size:14px;">{{ $value['city'] }}</p>
                                             </div>
                                         </div><!-- Col -->

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Area:</label>

                                                 <p style="font-size:14px;">{{ $value['area'] }}</p>


                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Pincode:</label>

                                                 <p style="font-size:14px;">{{ $value['pincode'] }}</p>


                                             </div>
                                         </div><!-- Col -->

                                         <div class="col-sm-4">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">Main
                                                     Photo:</label><br>
                                                 @php $path = getFileURLPublicPathPost($value['main_image_name'], $PostType, $value['id'], 'image') @endphp
                                                 @if ($path != '')
                                                 <a class="gallery-image" href="{{ $path }}">
                                                     <img src={{ $path }} width="150px" height="150px"
                                                         title="1SqFt" alt="1SqFt" style=""></a>
                                                 @else

                                                 @endif
                                             </div>
                                         </div><!-- Col -->
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                                 <label style="font-weight: bold;" class="control-label">
                                                     Other Photo:</label><br>
                                                 <div class="row">
                                                     @foreach ($value['getimages'] as $key => $imagevalue)
                                                         @php $path = getFileURLPublicPathPost($imagevalue['image_name'], $PostType, $value['id'], 'image') @endphp
                                                         @if ($path != '')
                                                             <div class="col-sm-6" style="margin-top:10px">
                                                                <a class="gallery-image" href="{{ $path }}">
                                                                 <img src="{{ $path }}" width="150px"
                                                                     height="150px" title="1SqFt" alt="1SqFt" style=""></a>
                                                             </div>
                                                         @else

                                                         @endif
                                                     @endforeach
                                                 </div>


                                             </div>
                                         </div><!-- Col -->

                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">

             </div>

         </form>
     </div>
 @endforeach
