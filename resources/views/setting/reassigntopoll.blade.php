@extends('layout.master')
@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            @include('Components.heading')
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard"> <i style="width:13px;height:12px" class="link-icon"
                            data-feather="home"></i>&nbsp;Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a class="active" href="#">{{ $scenario }}</a>
                </li>
            </ol>
        </nav>
    </div>

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form id="reassignform">
                        <div class="row">
                            @if (Auth::user()->role_type == 1)
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">User<span style="color:red">*</span></label>
                                        <select id="user" name="user" class="js-example-basic-multiple w-100">
                                            <option value="">select</option>
                                            @foreach ($getuser as $key => $value)
                                                <option value="{{ $value['user_id'] }}">{{ $value['username'] }}
                                                </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div><!-- Col -->
                            @else
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Team Lead<span style="color:red">*</span></label>
                                        <select id="teamlead" name="teamlead" class="js-example-basic-multiple w-100">
                                            <option value="">Select</option>
                                            @foreach ($getteamlead as $key => $value)
                                                <option value="{{ $value['user_id'] }}">{{ $value['username'] }}
                                                </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div><!-- Col -->
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">User<span style="color:red">*</span></label>
                                        <select id="user" name="user" class="js-example-basic-multiple w-100">
                                            <option value="">select</option>

                                        </select>
                                    </div>
                                </div><!-- Col -->
                            @endif


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Permission List<span style="color:red">*</span></label>
                                    <select multiple id="permission" name="permission[]"
                                        class="js-example-basic-multiple w-100">
                                        <option value="">select</option>

                                    </select>
                                </div>
                            </div><!-- Col -->
                            <div class="col-sm-3">
                                
                                <button id="sendtopoll" style="margin-top: 30px;
                                        padding: 10px;" type="submit" data-tooltip="tooltip" data-placement="top"
                                    title="Move" class="btn btn-danger submit">Move to Polling</button>
                            </div>

                        </div><!-- Row -->
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('plugin-scripts')

    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>


@endpush

@push('custom-scripts')
    <script src="{{ asset('assets/js/form-validation.js') }}"></script>
    <script src="{{ asset('assets/js/useractivity/assigntopoll.js') }}"></script>

@endpush
