@push('plugin-styles')
    <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">

@endpush
@extends('layout.master')
@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            @include('Components.heading')
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard"> <i style="width:13px;height:12px" class="link-icon"
                            data-feather="home"></i>&nbsp;Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a class="active" href="#">{{ $scenario }}</a>
                </li>
            </ol>
        </nav>
    </div>
    <form id="form_schedules_filter">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label">Criterion</label>
                    <select name="criteria_id" class="js-example-basic-multiple w-100">
                        <option value="-1">All</option>
                        @foreach ($data['criteria'] as $key => $value)
                            <option value="{{ $value['criteria_id'] }}">{{ $value['criteria_name'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div><!-- Col -->


            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label">Rule name</label>
                    <select name="rule_name" class="js-example-basic-multiple w-100">
                        <option value="-1">All</option>
                        @foreach ($data['rule_names'] as $value)
                            @if (strtoupper(env('APP_ENV')) == strtoupper('production'))
                                @if (strpos($value, 'prod-') !== false)
                                    <option value="{{ $value }}">{{ $value }}</option>
                                @else
                                @endif
                            @else
                                <option value="{{ $value }}">{{ $value }}</option>
                            @endif

                        @endforeach
                    </select>
                </div>
            </div><!-- Col -->
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label">City</label>
                    <select name="city_id" class="js-example-basic-multiple w-100">
                        <option value="-1">All</option>
                        @foreach ($data['cities'] as $key => $value)
                            <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div><!-- Col -->
        </div><!-- Row -->
        <button class="btn btn-primary" id="submit-btn">Submit</button>
        <button class="btn btn-secondary" id="add-btn">Add</button>
    </form>
    <div style="margin-top:30px;padding:0px" class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table style="width:100%" id="table_criteria_schedule" class="table  table-hover">
                        <thead>
                            <tr>
                                <th>Criteria</th>
                                <th>City</th>
                                <th>Schedule rule</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="table_criteria_schedule_body">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal bd-example-modal-lg fade" id="editSchedule" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form id="form_edit_schedule" class="forms-sample">
                    <input type="hidden" id="id" name="id">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Schedule</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 grid-margin">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <div class="time-select col-md-8 ">
                                                <label class="control-label">Scheduled rule name: <span
                                                        style="color:red">*</span></label><br>
                                                <select id="select_schedule_time" name="rule_name"
                                                    class="js-example-basic-multiple w-100">
                                                    @foreach ($data['rule_names'] as $value)
                                                        @if (strtoupper(env('APP_ENV')) == strtoupper('production'))
                                                            @if (strpos($value, 'prod-') !== false)
                                                                <option value="{{ $value }}">{{ $value }}
                                                                </option>
                                                            @else
                                                            @endif
                                                        @else
                                                            <option value="{{ $value }}">{{ $value }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        @csrf
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@push('plugin-scripts')
    <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

    <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>

@endpush

@push('custom-scripts')
    <script src="{{ asset('assets/js/data-table.js') }}"></script>
    <script src="{{ asset('assets/js/useractivity/schedules.js') }}"></script>
    <script src="{{ asset('assets/js/sweet-alert.js') }}"></script>

@endpush
