@push('plugin-styles')
    <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">

@endpush
@extends('layout.master')
@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            @include('Components.heading')
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard"> <i style="width:13px;height:12px" class="link-icon"
                            data-feather="home"></i>&nbsp;Dashboard</a></li>
                <li class="breadcrumb-item " aria-current="page"><a href="/users/lists">Users</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a class="active" href="#">{{ $scenario }}</a>
                </li>
            </ol>
        </nav>
    </div>

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
               <div class="card-head mt-2">
                        <div class="p-3 btn-group" role="group" aria-label="Third group">
                            <button type="button" data-tooltip="tooltip" id="addrole" data-placement="top" title="Create Role" class="btn btn-danger submit">ADD Role <i class="fas fa-user-plus"></i></button>
                        </div>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table id="roleTable" class="table  table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>No of Users</th>
                                    <th>Role type</th>
                                    <th>Created Date</th>
                                    <th>Updated Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="tbody">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal bd-example-modal-lg fade" id="rolemodal" tabindex="-1" role="dialog" aria-labelledby="rolemodalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form id="roleform" class="forms-sample">
                    <div class="modal-header">
                        <h5 class="modal-title" id="roletext">Add Role</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 grid-margin">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label>Role Title:<span style="color:red">*</span></label>
                                                <input type="hidden" class="form-control mb-4 mb-md-0" value=0 id="role_id"
                                                    name="role_id" />
                                                <input class="form-control mb-4 mb-md-0" required id="role_title"
                                                    name="role_title" />
                                            </div>
                                            <div class="col-md-6">
                                                <label>Role Type:<span style="color:red">*</span></label>
                                                <select id="roletype" required name="roletype"
                                                    class="js-example-basic-multiple w-100">
                                                    <option value="">Select Roletype</option>
                                                    @foreach ($role_type as $roletype)
                                                        <option value={{ $roletype->role_type_id }}>
                                                            {{ $roletype->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group mb-3">
                                            <span id="error" style="font-size:14px;color:red"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" id="rolesubmit">SAVE</button>
                        <input class="btn btn-secondary" data-dismiss="modal" type="reset" value="CANCEL">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('plugin-scripts')
    <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

    <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>

@endpush

@push('custom-scripts')
    <script src="{{ asset('assets/js/data-table.js') }}"></script>
    <script src="{{ asset('assets/js/sweet-alert.js') }}"></script>

@endpush
