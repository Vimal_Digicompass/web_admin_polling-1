<table>

    <tbody>
        <tr>
            <td style="text-align:center;font-size:15px;font-weight:bold;" colspan="9">{{ $text }}</td>
        </tr>

        <tr>
            <td style="font-size:14px;font-weight:bold;" colspan="9">From Date : {{ $fromdate }} and To
                Date: {{ $todate }}</td>
        </tr>

    </tbody>
</table><br>
<table id="customers" style="width:100%">
    <thead>
        <tr>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                Name</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                User Type</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                User Category</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                Mobile Number</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                Date of Registration</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                State</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                City</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                Area</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                User Status</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                KYC Status</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                REJECTED REASON</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                Calling Status</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                Follow-up Date</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                CALL Remarks</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                Assigned To</th>


                
        </tr>
    </thead>
    <tbody>
        @foreach ($kyc as $key => $info)
            <tr>
                <td style="border: 1px solid black;">{{ $info['Name'] }}</td>
                <td style="border: 1px solid black;">{{ $info['UserType'] }}</td>
                <td style="border: 1px solid black;">{{ $info['UserCategory'] }}</td>
                <td style="border: 1px solid black;">{{ $info['MobileNumber'] }}</td>
                <td style="border: 1px solid black;">{{ $info['DateofRegistration'] }}</td>
                <td style="border: 1px solid black;">{{ $info['State'] }}</td>
                <td style="border: 1px solid black;">{{ $info['City'] }}</td>
                <td style="border: 1px solid black;">{{ $info['Area'] }}</td>
                <td style="border: 1px solid black;">{{ $info['UserStatus'] }}</td>
                <td style="border: 1px solid black;">{{ $info['KYCStatus'] }}</td>
                <td style="border: 1px solid black;">{{ $info['RejectedReason'] }}</td>
                <td style="border: 1px solid black;">{{ $info['CallingStatus'] }}</td>
                <td style="border: 1px solid black;">{{ $info['FollowupDate'] }}</td>
                <td style="border: 1px solid black;">{{ strip_tags($info['CallRemarks']) }}</td>
                <td style="border: 1px solid black;">{{ $info['assignedTo'] }}</td>
            </tr>
        @endforeach
    </tbody>
