@extends('layout.master')

@push('plugin-styles')
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/plugins/simplemde/simplemde.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/scenariocommon.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">

@endpush

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            @include('Components.heading')

        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard"> <i style="width:13px;height:12px" class="link-icon"
                            data-feather="home"></i>&nbsp;Dashboard</a></li>
                <li class="breadcrumb-item"><a href="#"> KYC</a></li>
                <li class="breadcrumb-item " aria-current="page"><a class="active" href="#">{{ $scenario }}</a></li>
            </ol>
        </nav>
    </div>
    @include('Components.kycfilter')

    <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                @php $getpermissionData = getPermissionAndAccess($criteriaId,Auth::user()->role_id) @endphp

                {{-- @include('Components.sendnotification') --}}
                <div class="card-body">
                    <span style="display: none"
                        id="getnotifydata">{{ $getpermissionData[0]['is_bulkknotificationaccess'] }}</span>
                    <div class="table-responsive">
                        <table id="kycapproval" style="width:100%" class="table  table-hover">
                            <thead>
                                <tr>
                                    @if ($getpermissionData[0]['is_bulkknotificationaccess'] != 0)
                                        <th><button style="border: none; background: transparent; font-size: 14px;"
                                                name="CheckAllButton" id="CheckAllButton">
                                                <i class="fa fa-square-o" aria-hidden="true"></i>
                                            </button></th>
                                        @else
                                        @endif
                                        <th>Name</th>
                                        <th>User Type</th>
                                        <th>User Category</th>
                                        <th>Mobile Number</th>
                                        <th>Date of Registration</th>
                                        <th>State</th>
                                        <th>City</th>
                                        <th>Area</th>
                                        <th>Document</th>
                                        <th>KYC Status</th>
                                        <th>REJECTED REASON</th>
                                        <th>ASSIGNED TO</th>
                                        <th>Calling Status</th>
                                        <th>Follow-up Date</th>
                                        <th>CALL Remarks</th>
                                        <th>Action</th>

                                </tr>
                            </thead>
                            <tbody id="tbody">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('Components.commonmodals')

@endsection

@push('plugin-scripts')

    <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('assets/plugins/simplemde/simplemde.min.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>


@endpush

@push('custom-scripts')
    <script src="{{ asset('assets/js/useractivity/kyc/approvalkyc.js') }}"></script>
    <script src="{{ asset('assets/js/useractivity/common.js') }}"></script>
    <script src="{{ asset('assets/js/datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/data-table.js') }}"></script>
    <script src="{{ asset('assets/js/simplemde.js') }}"></script>

@endpush
