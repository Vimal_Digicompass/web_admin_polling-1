@extends('layout.master')

@push('plugin-styles')
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link href="{{ asset('css/scenariocommon.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link href="{{ asset('assets/plugins/simplemde/simplemde.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/jquery-steps/jquery.steps.css') }}" rel="stylesheet" />

@endpush

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            @include('Components.heading')
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard"> <i style="width:13px;height:12px" class="link-icon"
                            data-feather="home"></i>&nbsp;Dashboard</a></li>
                <li class="breadcrumb-item"><a href="#"> Payment</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a class="active" href="#">{{ $scenario }}</a>
                </li>
            </ol>
        </nav>
    </div>
    @include('Components.filter')
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                @php $getpermissionData = getPermissionAndAccess($criteriaId,Auth::user()->role_id) @endphp

                <div class="card-body">
                    <span style="display: none"
                        id="getnotifydata">{{ $getpermissionData[0]['is_bulkknotificationaccess'] }}</span>
                    <div class="table-responsive">
                        <table id="walletexpiry" style="width:100%" class="table table-hover">
                            <thead>
                                <tr>
                                    @if ($getpermissionData[0]['is_bulkknotificationaccess'] != 0)
                                        <th class="no-sort"> <button
                                                style="border: none; background: transparent; right:7px;position:relative;font-size: 18px;"
                                                name="CheckAllButton" id="CheckAllButton">
                                                <i class="fa fa-square-o" aria-hidden="true"></i>
                                            </button></th>
                                             @else
                                             @endif
                                        <th>Name</th>
                                        <th>User Type</th>
                                        <th>Mobile Number</th>
                                        <th>State</th>
                                        <th>City</th>
                                        <th>Package Name</th>
                                        <th>Days to Expiry</th>
                                        <th>Calling Status</th>
                                        <th>Follow-up Date</th>
                                        <th>CALL Remarks</th>
                                        <th>Assigned To</th>
                                        <th>Action</th>

                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('Components.commonmodals')

    <div class="modal bd-example-modal-lg fade" id="walletexpirydetailview" tabindex="-1" role="dialog"
        aria-labelledby="walletexpirydetailviewLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="walletexpirydetailviewLabel">Detail View</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 grid-margin">
                                <div class="card">
                                    <div class="card-header">Package Info:</div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label">Package
                                                        Name:</label>
                                                    <p id="detailviewpackagename1" style="font-size:14px;"></p>
                                                </div>
                                            </div><!-- Col -->
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label">Package
                                                        Validity:</label>
                                                    <p id="detailviewpackagevalidity1" style="font-size:14px;"></p>
                                                </div>
                                            </div><!-- Col -->

                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label"> Days to Expiry:</label>
                                                    <p id="daystoexpiry1" style="font-size:14px;"></p>
                                                </div>
                                            </div><!-- Col -->

                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top:15px;" class="card">
                                    <div class="card-header">User Info:</div>
                                    <div class="card-body">
                                        <div class="row">



                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label"> First
                                                        Name:</label>
                                                    <p id="detailviewfirstname1" style="font-size:14px;"></p>
                                                </div>
                                            </div><!-- Col -->
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label"> Last
                                                        Name:</label>
                                                    <p id="detailviewlastname1" style="font-size:14px;"></p>
                                                </div>
                                            </div><!-- Col -->
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label">Email
                                                        Address:</label>
                                                    <p id="detailviewemail1" style="font-size:14px;"></p>
                                                </div>
                                            </div><!-- Col -->
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label">
                                                        Company:</label>
                                                    <p id="detailviewcompany1" style="font-size:14px;"></p>
                                                </div>
                                            </div><!-- Col -->

                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label">User
                                                        type:</label>
                                                    <p id="detailviewusertype1" style="font-size:14px;"></p>
                                                </div>
                                            </div><!-- Col -->
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label">Mobile
                                                        Number:</label>
                                                    <p id="detailviewmobilenumber1" style="font-size:14px;"></p>
                                                </div>
                                            </div><!-- Col -->
                                        </div>
                                        <div class="row">

                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label">Address:</label>
                                                    <p id="detailviewaddress1" style="font-size:14px;"></p>
                                                </div>
                                            </div><!-- Col -->
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label">
                                                        Pincode:</label>
                                                    <p id="detailviewpincode1" style="font-size:14px;"></p>
                                                </div>
                                            </div><!-- Col -->
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label">City:</label>
                                                    <p id="detailviewcity1" style="font-size:14px;"></p>
                                                </div>
                                            </div><!-- Col -->
                                        </div>
                                        <div class="row">


                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label style="font-weight: bold;" class="control-label"> State:</label>
                                                    <p id="detailviewstate1" style="font-size:14px;"></p>
                                                </div>
                                            </div><!-- Col -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">

                    </div>
            </form>
        </div>
    </div>
    </div>


    <div class="modal bd-example-modal-lg fade" id="transactionmodal" tabindex="-1" role="dialog"
        aria-labelledby="transactionmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="transactionmodalLabel">Transaction List</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 grid-margin">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive pt-3">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Package Name
                                                        </th>
                                                        <th>
                                                            Package Value
                                                        </th>
                                                        <th>
                                                            Transaction Status
                                                        </th>
                                                        <th>
                                                            Transaction Date
                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody id="toptransactiontablebody">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">

                    </div>
            </form>
        </div>
    </div>
    </div>
@endsection

@push('plugin-scripts')

    <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script src="{{ asset('assets/plugins/jquery-steps/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/simplemde/simplemde.min.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>


@endpush

@push('custom-scripts')
    <script src="{{ asset('assets/js/datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/data-table.js') }}"></script>
    <script src="{{ asset('assets/js/wizard.js') }}"></script>
    <script src="{{ asset('assets/js/useractivity/common.js') }}"></script>
    <script src="{{ asset('assets/js/useractivity/walletexpiry/walletexpiry.js') }}"></script>
@endpush
