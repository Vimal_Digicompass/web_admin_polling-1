@extends('layout.master')

@push('plugin-styles')
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link href="{{ asset('css/scenariocommon.css') }}" rel="stylesheet" />

@endpush

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">{{ $scenario }}</h4>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard"> <i style="width:13px;height:12px" class="link-icon"
                            data-feather="home"></i>&nbsp;Dashboard</a></li>
                <li class="breadcrumb-item"><a href="#"> Post</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a class="active" href="#">{{ $scenario }}</a>
                </li>
            </ol>
        </nav>
    </div>
    <div class="row ">
        <div class="col-md-12 col-sm-12">
            <div class="card " style="font-size:14px;">
                @foreach ($getData as $key => $value)


                    <div class="card-body mt-2">

                        <div class="row">
                            <div class=" col-md-5 col-sm-6">
                                <div class="preview-pic tab-content">
                                    <div class="tab-pane active" id="pic-1">
                                        @php
                                            if ($value['floor_plan_file_name'] != null || $value['floor_plan_file_name'] != '') {
                                                $path = getFileURLPublicPathPost($value['floor_plan_file_name'], 'PR', $value['project_id'], 'flats/' . $value['id'] . '');
                                            } else {
                                                $path = '';
                                            }
                                        @endphp
                                        @if ($path != '')
                                            <div class="col-sm-6" style="margin-top:10px">
                                                <a class="gallery-image" href="{{ $path }}">
                                                    <img src="{{ $path }}" width="200px" height="200px"
                                                        title="1SqFt" alt="1SqFt" style=""></a>
                                            </div>
                                        @else
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class=" col-md-8 col-sm-6" style="margin-top:10px">
                                <h4 class="title"> {{ $value['title'] }}</h4>
                                <div class="row">
                                    <div class="col-md-4 col-md-4 border-right">
                                        <label class="pro_type">Furnish Type</label><br>
                                        <label class="pro_details">{{ $value['Furnish_Title'] }}</label>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="pro_type">Floor No</label><br>
                                        <label class="pro_details">{{ $value['Floor_Title'] }}</label>
                                    </div>
                                    <div class="col-md-4 col-md-4 border-left">
                                        <label class="pro_type">Carpet Area</label><br>
                                        <label class="pro_details">{{ $value['carpet_area'] }}</label>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-4 col-md-4 border-right">
                                        <label class="pro_type">Price</label><br>
                                        <label class="pro_details">₹ {{ ConvertNoToWord($value['price']) }}</label>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="pro_type">Loan offered by Banks</label><br>
                                        @php $bankoffer = \DB::connection('mysql_1')
                                                ->table('loan_offered_by_banks_master')
                                                ->whereIn('id', explode(',', $value['loan_offered_bank_ids']))
                                                ->get();
                                            $bankoffer = $bankoffer->pluck('title')->toArray();
                                            $banknames = implode(',', $bankoffer);

                                        @endphp
                                        <p style="font-size:14px;">{{ $banknames }}</p>
                                    </div>
                                    <div class="col-md-4 col-md-4 border-left">
                                        <label class="pro_type">Possession Date</label><br>
                                        <label
                                            class="pro_details">{{ \Carbon\Carbon::parse($value['possession_date'])->format('d-m-Y') }}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-6">
                                <div class="pull-right">
                                    <a href="javascript:void(0)" id="{{ $value['project_id'] }}_{{ $value['id'] }}"
                                        class="flatview btn blue btn-primary ">Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
    </div>
    @include('Components.postmodal')


@endsection

@push('plugin-scripts')

    <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>


@endpush

@push('custom-scripts')
    <script src="{{ asset('assets/js/datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/data-table.js') }}"></script>
    <script src="{{ asset('assets/js/useractivity/common.js') }}"></script>

    <script src="{{ asset('assets/js/useractivity/post/post.js') }}"></script>

@endpush
