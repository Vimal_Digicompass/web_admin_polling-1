@extends('layout.master')

@push('plugin-styles')
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/plugins/simplemde/simplemde.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/scenariocommon.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">

@endpush

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            @include('Components.heading')

        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard"> <i style="width:13px;height:12px" class="link-icon"
                            data-feather="home"></i>&nbsp;Dashboard</a></li>
                <li class="breadcrumb-item"><a href="#"> UserActivity</a></li>
                <li class="breadcrumb-item " aria-current="page"><a class="active" href="#">{{ $scenario }}</a></li>
            </ol>
        </nav>
    </div>
    @include('Components.kycfilter')

    <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                @php $getpermissionData = getPermissionAndAccess($criteriaId,Auth::user()->role_id) @endphp

                <div class="card-body">
                    <span style="display: none"
                        id="getnotifydata">{{ $getpermissionData[0]['is_bulkknotificationaccess'] }}</span>
                    <div class="table-responsive">
                        <table id="upgradeuser" style="width:100%" class="table table-hover">
                            <thead>
                                <tr>
                                    @if ($getpermissionData[0]['is_bulkknotificationaccess'] != 0)
                                        <th><button style="border: none; background: transparent; font-size: 14px;"
                                                name="CheckAllButton" id="CheckAllButton">
                                                <i class="fa fa-square-o" aria-hidden="true"></i>
                                            </button></th>
                                        @else
                                        @endif
                                        <th>Name</th>
                                        <th>User Type</th>
                                        <th>User Category</th>
                                        <th>Mobile Number</th>
                                        <th>Date of Registration</th>
                                        <th>Date of Upgrade</th>
                                        <th>State</th>
                                        <th>City</th>
                                        <th>Area</th>
                                        <th>Document</th>
                                        <th>KYC Status</th>
                                        <th>REJECTED REASON</th>
                                        <th>Calling Status</th>
                                        <th>Follow-up Date</th>
                                        <th>CALL Remarks</th>
                                        <th>Assigned To</th>
                                        <th>Action</th>

                                </tr>
                            </thead>
                            <tbody id="tbody">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal bd-example-modal-lg fade" id="userprofileeditmodal" tabindex="-1" role="dialog"
        aria-labelledby="userprofileeditmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">


                <div class="modal-header">
                    <h5 class="modal-title" id="usertext">Edit Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form name="profile_form" id="profile_form">
                    <!-- Modal body -->
                    <input type="hidden" name="user_id" id="user_idd" value=>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">First name<span style="color:red">*</span></label>
                                    <input type="text" id="firstname" name="firstname" class="form-control" required
                                        placeholder="Enter your name" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Last name<span style="color:red">*</span></label>
                                    <input type="text" id="lastname" name="lastname" class="form-control" required
                                        placeholder="Enter your last name" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Mobile No<span style="color:red">*</span></label>
                                    <input type="number" maxlength=10 name="mobile_number" id="mobile_number"
                                        class="form-control" required placeholder="Enter your mobile no" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Email Id<span style="color:red">*</span></label>
                                    <input type="email" name="email" id="email" class="form-control" required
                                        placeholder="Enter your email" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Whats App No</label>
                                    <input type="number" name="whatsapp_number" id="whatsapp_number" class="form-control"
                                        placeholder="Enter your Whatsapp no" />
                                    <label><input type="checkbox" name="whatsapp_sameno" id="whatsapp_mobileno" value="" />
                                        Whatsapp No is same as your mobile no</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">User Type<span style="color:red">*</span></label>
                                    <select id="user_type" name="user_type" class="js-example-basic-multiple w-100">
                                        <option value="">select</option>
                                        @foreach ($userType as $key => $value)
                                            <option value="{{ $value['titlevalue'] }}">{{ $value['title'] }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Category<span style="color:red">*</span></label>
                                    <select id="category_id" name="category_id" class="js-example-basic-multiple w-100">
                                        <option value="">select</option>
                                        @foreach ($UserCategory as $key => $value)
                                            <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Company<span style="color:red">*</span></label>
                                    <input type="text" name="company_name" id="company_name" class="form-control" required
                                        placeholder="Enter company" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Website</label>
                                    <input type="url" name="website_url" id="website_url" class="form-control"
                                        placeholder="Enter website" />
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Address</label>
                                    <textarea class="form-control" name="address" id="address"
                                        placeholder="Enter address"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">State<span style="color:red">*</span></label>
                                    <select id="states" name="state" class="js-example-basic-multiple w-100">
                                        <option value="">Select</option>
                                        @foreach ($state as $key => $value)
                                            <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">City<span style="color:red">*</span></label>
                                    <select name="city_id" id="city_id"
                                        class=" js-example-basic-multiple w-100 form-control">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Pincode<span style="color:red">*</span></label>
                                    <input type="number" maxlength="6" name="pincode" id="pincode" class="form-control"
                                        required placeholder="Enter pincode" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" id="profileupdatesubmit" class="btn btn-danger btn-sm">Submit</button>
                        <input class="btn btn-secondary" data-dismiss="modal" type="reset" value="CANCEL">

                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('Components.commonmodals')

@endsection

@push('plugin-scripts')

    <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('assets/plugins/simplemde/simplemde.min.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>


@endpush

@push('custom-scripts')
    <script src="{{ asset('assets/js/useractivity/upgradeuser/upgradeuser.js') }}"></script>
    <script src="{{ asset('assets/js/useractivity/common.js') }}"></script>
    <script src="{{ asset('assets/js/datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/data-table.js') }}"></script>
    <script src="{{ asset('assets/js/simplemde.js') }}"></script>

@endpush
