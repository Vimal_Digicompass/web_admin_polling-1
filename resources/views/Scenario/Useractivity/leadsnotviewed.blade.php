@extends('layout.master')

@push('plugin-styles')
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link href="{{ asset('css/scenariocommon.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link href="{{ asset('assets/plugins/simplemde/simplemde.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/plugins/jquery-steps/jquery.steps.css') }}" rel="stylesheet" />
@endpush
@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            @include('Components.heading')
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard"> <i style="width:13px;height:12px" class="link-icon"
                            data-feather="home"></i>&nbsp;Dashboard</a></li>
                <li class="breadcrumb-item"><a href="#"> Post</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a class="active" href="#">{{ $scenario }}</a>
                </li>
            </ol>
        </nav>
    </div>
    @include('Components.postfilter')
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                @php $getpermissionData = getPermissionAndAccess($criteriaId,Auth::user()->role_id) @endphp

                <div class="card-body">
                    @php $role_type = Auth::user()->role_type; @endphp
                    @if ($role_type == Config::get('constants.ROLE_TYPE.CRM_USER')|| $role_type == Config::get('constants.ROLE_TYPE.SUB_ADMIN'))
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label style="font-weight: bold;" class="control-label">
                                        Property Count:</label>
                                    <p style="font-size:14px;">Waiting - {{ $propertywaitingcount }}</p>
                                    <p style="font-size:14px;">Processed - {{ $propertyprocessedcount }}</p>
                                </div>
                            </div><!-- Col -->
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label style="font-weight: bold;" class="control-label">
                                        Project Count:</label>

                                    <p style="font-size:14px;">Waiting - {{ $projectwaitingcount }}</p>
                                    <p style="font-size:14px;">Processed - {{ $projectprocessedcount }}</p>
                                </div>
                            </div><!-- Col -->
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label style="font-weight: bold;" class="control-label">
                                        Service Count:</label>
                                    <p style="font-size:14px;">Waiting - {{ $servicewaitingcount }}</p>
                                    <p style="font-size:14px;">Processed - {{ $serviceprocessedcount }}</p>
                                </div>
                            </div><!-- Col -->
                        </div>
                    @else
                    @endif
                    <span style="display: none"
                        id="getnotifydata">{{ $getpermissionData[0]['is_bulkknotificationaccess'] }}</span>

                    <div class="table-responsive">
                        <table id="leadsnotviewed" style="width:100%" class="table table-hover">
                            <thead>
                                <tr>
                                    @if ($getpermissionData[0]['is_bulkknotificationaccess'] != 0)
                                        <th class="no-sort"> <button
                                                style="border: none; background: transparent; right:7px;position:relative;font-size: 18px;"
                                                name="CheckAllButton" id="CheckAllButton">
                                                <i class="fa fa-square-o" aria-hidden="true"></i>
                                            </button></th>
                                            @else
                                            @endif
                                        <th>Name</th>
                                        <th>User Type</th>
                                        <th id="type">Project Category</th>
                                        <th>Mobile Number</th>
                                        <th>State</th>
                                        <th>City</th>
                                        <th>Total no of Leads</th>
                                        <th>Total No Of Unviewed Leads</th>
                                        <th>Calling Status</th>
                                        <th>Follow-up Date</th>
                                        <th>CALL Remarks</th>
                                        <th>Assigned To</th>
                                        <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('Components.postmodal')
    @include('Components.commonmodals')

@endsection

@push('plugin-scripts')
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script src="{{ asset('assets/plugins/jquery-steps/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/simplemde/simplemde.min.js') }}"></script>


@endpush

@push('custom-scripts')
    <script src="{{ asset('assets/js/datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/data-table.js') }}"></script>
    <script src="{{ asset('assets/js/wizard.js') }}"></script>
    <script src="{{ asset('assets/js/simplemde.js') }}"></script>
    <script src="{{ asset('assets/js/useractivity/common.js') }}"></script>
    <script src="{{ asset('assets/js/useractivity/leadsnotviewed/leadsnotviewed.js') }}"></script>
@endpush
