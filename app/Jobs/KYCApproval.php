<?php

declare(strict_types=1);

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Traits\HasTransactionUpdatesTrait;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use App\Models\UserActivity\KYCApproval\KYCApprovalArcheive;
use App\Models\UserActivity\KYCApproval\KYCApprovalPoll;
use Exception;

class KYCApproval implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use HasTransactionUpdatesTrait;

    public $tries = 3; // retry option if job fails

    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
            $data
    ) {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::info("Job Initialized");
            Log::info($this->data);
            $KYCApprovalJobInfo = $this->data;
            $user_id = $KYCApprovalJobInfo['user_id'];
            $city_id = $KYCApprovalJobInfo['city_id'];
            KYCApprovalArcheive::create(
            ['city_id' => $city_id,
             'user_id' => $user_id,
             'status' =>Config::get('constants.ADMIN_POLLING_FLOW.SENT_TO_POLL')
             ]);
            KYCApprovalPoll::create(
            ['city_id' => $city_id,
             'user_id' => $user_id
             ]);
            Log::info("Job Done");
        } catch (Exception $exc) {
            $this->fail($exc);
            Log::error($exc);
            Log::error($this->fail($exc));
        } catch (\Throwable $th) {
            $this->fail($th);
            Log::error($th);
            Log::error($this->fail($th));
        }
    }

    /**
     * Handle a job failure.
     *
     * @param  MailJobException  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $this->transactionFailed($exception);
    }
}
