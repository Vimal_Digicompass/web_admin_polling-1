<?php

namespace App\Exports;

use stdClass;
use App\Models\Master\CallStatus;

use App\Models\Master\ServiceCategory;
use App\Models\Master\ProjectCategory;
use App\Models\Master\PropertyCategory;
use App\Models\SqFt\ServiceRequestLeads;
use App\Models\SqFt\ProjectRequestLeads;
use App\Models\SqFt\PropertyRequestLeads;
use App\Models\Master\State;
use App\Models\Master\City;
use App\User as CRMUser;

use App\Models\LeadRequest;
use App\Models\Master\UserType;
use App\Models\Master\UserCategory;
use App\Models\UserActivity\LeadsNotViewed\LeadsNotViewedAssign;
use Maatwebsite\Excel\Concerns\Exportable;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class LeadsNotViewedExport implements FromView
{
    use Exportable;
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct($getCustomerData, $request)
    {
        $this->getCustomerData = $getCustomerData;
        $this->request = $request;
    }

    public function view(): View
    {
        $getCustomerData = $this->getCustomerData;
        $request = $this->request;
        $fromdate = $request['fromdate'];
        $fromdate = Carbon::createFromFormat('Y-m-d', $fromdate)
            ->format('d-m-Y');
        $todate = $request['todate'];
        $todate = Carbon::createFromFormat('Y-m-d', $todate)
            ->format('d-m-Y');
        $Arr = [];

        $text = "";
        $PostType = $request['posttype'];
        foreach ($getCustomerData as $key => $value) {
            $name = $value['user']->firstname . ' ' . $value['user']->lastname;
            $mobilenumber = $value['user']->mobile_number;
            $usertype = UserType::where('titlevalue', $value['user']->user_type)->first();
            $usertypetitle = $usertype['title'];
            if ($PostType == "PR") {
                $text = "Project Reports";
                $totalLeadsCount = LeadRequest::where('post_id', $value['id'])->where('post_type','PR')->count();
                $totalNotViewCount = LeadRequest::where('post_id', $value['id'])->where('post_type','PR')
                    ->where('is_viewed', true)
                    ->count();
                $category = ProjectCategory::where('id', $value['category_id'])->first();
            } else if ($PostType == "P") {
                $text = "Property Reports";
                $totalLeadsCount = LeadRequest::where('post_id', $value['id'])->where('post_type', 'P')->count();
                $totalNotViewCount = LeadRequest::where('post_id', $value['id'])->where('post_type', 'P')
                    ->where('is_viewed', true)
                    ->count();
                $category = PropertyCategory::where('id', $value['property_category_id'])->first();
            } else {
                $text = "Service Reports";
                $totalLeadsCount = LeadRequest::where('post_id', $value['id'])->where('post_type', 'S')->count();
                $totalNotViewCount = LeadRequest::where('post_id', $value['id'])->where('post_type', 'S')
                    ->where('is_viewed', true)
                    ->count();
                $category = ServiceCategory::where('id', $value['service_type_id'])->first();
            }
            if ($category) {
                $postcategory =  $category['title'];
            } else {
                 $postcategory = "";
            }
            $category = UserCategory::where('id', $value['user']->category_id)->first();
            if ($category) {
                $category = $category['title'];
            } else {
                $category = "";
            }
            $city = City::where('id', $value['city_id'])->first();
            if ($city) {
                $city_title = $city['title'];
                $stateid = $city['state_id'];

                $statetitle = State::where('id', $stateid)->first();
                if ($statetitle) {
                    $state =  $statetitle['title'];
                } else {
                    $state =  "";
                }
            } else {
                $city_title = "";
                $state  = "";
            }

            $getData =    LeadsNotViewedAssign::where('post_id', $value['id'])
                ->where('post_type', $request['posttype'])
                ->first();
            $callstatus = $getData['searchcallstatus'];
            $crm_user_id = $getData['crm_user_id'];
            $crmData = CRMUser::where('user_id', $crm_user_id)->first();
            $assignedTo =  $crmData['username'];
            if ($callstatus != 0) {
                $getcallstatus = CallStatus::where('id', $callstatus)->first();
                $callstatus = $getcallstatus['title'];
            } else {
                $callstatus = "";
            }
            $kycRejectStatus = $value['kyc_rejcted_reason'];

            if ($getData['followup_date'] != NULL) {
                $followdate =  Carbon::createFromFormat('Y-m-d', $getData['followup_date'])
                    ->setTimezone('Asia/Calcutta')
                    ->format('d-m-Y');
            } else {

                $followdate =  '';
            }

            if ($getData) {
                $remarks =  $getData['remarks'];
            } else {
                $remarks =  '';
            }
            array_push($Arr, [
                'Name' => $name,
                'UserType' => $usertypetitle,
                'PostCategory'=> $postcategory,
                'UserCategory' => $category,
                'State' => $state,
                'totalLeadsCount'=> $totalLeadsCount,
                'City' => $city_title,
                'totalNotViewCount'=> $totalNotViewCount,
                'RejectedReason' => $kycRejectStatus,
                'CallingStatus' => $callstatus,
                'FollowupDate' => $followdate,
                'CallRemarks' => $remarks,
                'mobilenumber'=>$mobilenumber,
                'assignedTo'=> $assignedTo

            ]);
        }

        return view('Exports.leadsnotviewed', [
            'leadsnotviewed' => $Arr,
            'fromdate' => $fromdate,
            'todate' => $todate,
            'text' => $text,
            'PostType' => $PostType
        ]);
    }
}
