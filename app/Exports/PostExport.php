<?php

namespace App\Exports;

use stdClass;
use App\Models\Master\CallStatus;
use Illuminate\Support\Facades\Config;
use App\Models\UserActivity\PostRejected\PostRejectedAssign;
use App\Models\Master\ServiceCategory;
use App\Models\Master\ProjectCategory;
use App\Models\Master\PropertyCategory;
use App\Models\Master\PostRejectedStatus;
use App\Models\SqFt\Service;
use App\Models\SqFt\Project;
use App\User as CRMUser;

use App\Models\SqFt\Property;
use App\Models\Master\State;
use App\Models\Master\City;
use App\Models\Master\UserType;
use App\Models\Master\UserCategory;
use App\Models\UserActivity\PostApproval\PostApprovalAssign;
use Maatwebsite\Excel\Concerns\Exportable;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PostExport implements FromView
{
    use Exportable;
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct($getPostData, $request)
    {
        $this->getPostData = $getPostData;
        $this->request = $request;
    }

    public function view(): View
    {
        $getPostData = $this->getPostData;
        $request = $this->request;
        $fromdate = $request['fromdate'];
        $fromdate = Carbon::createFromFormat('Y-m-d', $fromdate)
            ->format('d-m-Y');
        $todate = $request['todate'];
        $todate = Carbon::createFromFormat('Y-m-d', $todate)
            ->format('d-m-Y');
        $Arr = [];
        $PostType = $request['posttype'];
        $criteriaId = $request['criteriaId'];
        $text = "";
        if ($PostType == "PR") {
            $text = "Project Reports";
        } else if ($PostType == "P") {
            $text = "Property Reports";
        } else {
            $text = "Service Reports";
        }
        foreach ($getPostData as $key => $value) {
            $name = $value['user']->firstname . ' ' . $value['user']->lastname;
            $usertype = UserType::where('titlevalue', $value['user']->user_type)->first();

            $usertypetitle = $usertype['title'];
            if ($PostType == "PR") {
                $category = ProjectCategory::where('id', $value['category_id'])->first();
            } else if ($PostType == "P") {
                $category = PropertyCategory::where('id', $value['property_category_id'])->first();
            } else {
                $category = ServiceCategory::where('id', $value['service_type_id'])->first();
            }
            if ($category) {
                $postcategory =  $category['title'];
            } else {
                $postcategory = "";
            }
            $mobilenumber = $value['user']->mobile_number;
            $city = City::where('id', $value['city_id'])->first();
            if ($city) {
                $city_title = $city['title'];
                $stateid = $city['state_id'];

                $statetitle = State::where('id', $stateid)->first();
                if ($statetitle) {
                    $state =  $statetitle['title'];
                } else {
                    $state =  "";
                }
            } else {
                $city_title = "";
                $state  = "";
            }
            if ($criteriaId == Config::get('constants.CRITERION.POST_APPROVAL')) {
                $getData = PostApprovalAssign::query();
            } else {
                $getData  = PostRejectedAssign::query();
            }
            $getData =   $getData->where('post_id', $value['id'])
                ->where('post_type', $request['posttype'])
                ->first();
            $crm_user_id = $getData['crm_user_id'];
            $crmData = CRMUser::where('user_id', $crm_user_id)->first();
            $assignedTo =  $crmData['username'];
            $callstatus = $getData['searchcallstatus'];
            if ($callstatus != 0) {
                $getcallstatus = CallStatus::where('id', $callstatus)->first();
                $callstatus = $getcallstatus['title'];
            } else {
                $callstatus = "";
            }
            // $kycRejectStatus = $value['kyc_rejcted_reason'];

            if ($getData['followup_date'] != NULL) {
                $followdate =  Carbon::createFromFormat('Y-m-d', $getData['followup_date'])
                    ->setTimezone('Asia/Calcutta')
                    ->format('d-m-Y');
            } else {

                $followdate =  '';
            }

            if ($getData) {
                $remarks =  $getData['remarks'];
            } else {
                $remarks =  '';
            }

            if ($PostType == "PR") {
                $postedDate = Project::where('id', $value['id'])->first();
                if ($postedDate) {
                    $postedData =  Carbon::parse($postedDate['posted_date'])->format('d-m-Y');
                } else {
                    $postedData = "";
                }
                $reject_message = $postedDate['reject_message'];

                $is_approved = $postedDate['is_project_approved'];
            } else if ($PostType == "P") {
                $postedDate = Property::where('id', $value['id'])->first();
                if ($postedDate) {
                    $postedData =  Carbon::parse($postedDate['created_at'])->format('d-m-Y');
                } else {
                    $postedData = "";
                }
                $reject_message = $postedDate['reject_message'];

                $is_approved = $postedDate['is_property_approved'];
            } else {

                $postedDate = Service::where('id', $value['id'])->first();
                $reject_message = $postedDate['reject_message'];
                $is_approved = $postedDate['is_service_approved'];
                if ($postedDate) {
                    $postedData =  Carbon::parse($postedDate['created_at'])->format('d-m-Y');
                } else {
                    $postedData = "";
                }
            }

            if ($is_approved == 0) {
                $poststatus = "Waiting";
            } else if ($is_approved == 1) {
                $poststatus = "Approved";
            } else if ($is_approved == 2) {
                $poststatus = "Rejected";
            } else {
                $poststatus = "";
            }
            $publishdate = $value['published_date'];
            if ($publishdate) {
                $publishdate =  Carbon::parse($postedDate['published_date'])->format('d-m-Y');
            }
            $is_published = $value['is_published'];
            if ($is_published == 1) {
                $publishstatus = "ACTIVE";
            } else {
                $publishstatus = "In-ACTIVE";
            }
            $rejecttitle = PostRejectedStatus::where('id', $reject_message)->first();
            if ($rejecttitle) {
                $reject_message = $rejecttitle['reason'];
            }else{
                $reject_message = "";
            }
            array_push($Arr, [
                'Name' => $name,
                'UserType' => $usertypetitle,
                'PostCategory' => $postcategory,
                'MobileNumber' => $mobilenumber,
                'State' => $state,
                'City' => $city_title,
                'RejectedReason' => $reject_message,
                'CallingStatus' => $callstatus,
                'FollowupDate' => $followdate,
                'CallRemarks' => $remarks,
                'postedData' => $postedData,
                'publishstatus' => $publishstatus,
                "poststatus" => $poststatus,
                "publishdate" => $publishdate,
                'assignedTo'=> $assignedTo
            ]);
        }

        return view('Exports.post', [
            'post' => $Arr,
            'fromdate' => $fromdate,
            'todate' => $todate,
            'text' => $text,
            'PostType' => $PostType
        ]);
    }
}
