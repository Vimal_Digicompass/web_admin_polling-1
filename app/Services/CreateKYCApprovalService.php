<?php

declare(strict_types=1);

namespace App\Services;

use App\Traits\HasKYCApprovalDispatchTrait;
use App\Traits\HasKYCApproval;
use App\Traits\AdminPollsLoggerTrait;
use Illuminate\Support\Facades\Config;

class CreateKYCApprovalService
{
    use AdminPollsLoggerTrait,
        HasKYCApprovalDispatchTrait,
        HasKYCApproval;

    public function __construct(
        int $cityId,
        $arrayOfAdminPollsCriteria
    ) {
        $this->cityId = $cityId;
        $this->pollTimingId = $arrayOfAdminPollsCriteria;
    }
    public function __invoke()
    {
        $this->invoker();
    }
}
