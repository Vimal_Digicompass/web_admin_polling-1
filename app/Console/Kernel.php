<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        Log::error(env('SCHEDULE_QUEUE_TYPE'));
        if (env('SCHEDULE_QUEUE_TYPE') == "CONSUMER") {
            //Work the jobs in queue for 58 seconds on every minute.
            $schedule->command('queue:work', ["--max-time" => "58"])->everyMinute();
        } else  {
            //Schedule watch service
            Log::info(env('SCHEDULE_QUEUE_TYPE'));
          
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
