<?php
namespace App\Traits;
use App\Models\Master\Amenities;
use App\Models\Master\State;
use Carbon\Carbon;
/**
 * Class ProjectAttributeTrait
 *
 * @author  Gokul Trident
 */
trait ProjectAttributeTrait
{


    public function getElasticSearchCreateIndexParametres()
    {
        $projectId = $this->id;
        $userId = $this->user_id;

        $userFullName = $this->user ? $this->user->fullname : 'NULL';

        $ProjectReraId = $this->project_rera_id;
        $ProjectName = $this->project_name;
        $ProjectArea = $this->project_area;
        $description = $this->description;

        $category = $this->projectCategory ? $this->projectCategory->title : 'NULL';

        $totalFloors = $this->total_floors ? $this->total_floors : 0;

        $TotalNoOfUnits = $this->total_no_of_units ? $this->total_no_of_units : 0;
        $availableNoOfUnits = $this->available_no_of_units ? $this->available_no_of_units : 0;
        $launchDate = $this->launch_date ? $this->launch_date : null;
        $constructionStatusTitle = $this->constructionStatus ? $this->constructionStatus->title : 'NULL';
        $stateId = $this->projectCity->state_id;
        $state = State::where('id', $stateId)->first();
        if ($state) {
            $stateTitle = $state['title'];
        } else {
            $stateTitle = "NULL";
        }
        $city = $this->projectCity ? $this->projectCity->title : 'NULL';

        $address = $this->address;
        $pincode = $this->pincode;
        $priceNegotiable = $this->price_negotiable;
        $approvalAuthority = $this->approval_authority;

        $mainImageName = $this->main_image_name ? $this->main_image_name : 'NULL';
        $completionCertificateImageName = $this->completion_certificate_image_name ? $this->completion_certificate_image_name : 'NULL';

        //Floorring
        //--------------------------------------------------------------------------------------------------------------------
        $masterBedroomTitle = $this->masterBedroom ? $this->masterBedroom->title : 'NULL';
        $balconyTitle = $this->balcony ? $this->balcony->title : 'NULL';
        $floorKitchenTitle = $this->floorKitchen ? $this->floorKitchen->title : 'NULL';
        $otherBedroomTitle = $this->otherBedroom ? $this->otherBedroom->title : 'NULL';
        $livingOrDiningTitle = $this->livingOrDining ? $this->livingOrDining->title : 'NULL';
        $floorToiletTitle = $this->floorToilet ? $this->floorToilet->title : 'NULL';
        //--------------------------------------------------------------------------------------------------------------------

        //Fittings
        //--------------------------------------------------------------------------------------------------------------------
        $internalDoorTitle = $this->internalDoor ? $this->internalDoor->title : 'NULL';
        $fittingKitchenTitle = $this->fittingKitchen ? $this->fittingKitchen->title : 'NULL';
        $fittingToiletTitle = $this->fittingToilet ? $this->fittingToilet->title : 'NULL';
        $windowsTitle = $this->windows ? $this->windows->title : 'NULL';
        $mainDoorTitle = $this->mainDoor ? $this->mainDoor->title : 'NULL';
        $electricalTitle = $this->electrical ? $this->electrical->title : 'NULL';
        //--------------------------------------------------------------------------------------------------------------------

        //Wall
        //--------------------------------------------------------------------------------------------------------------------
        $wallKitchenTitle = $this->wallKitchen ? $this->wallKitchen->title : 'NULL';
        $wallToiletTitle = $this->wallToilet ? $this->wallToilet->title : 'NULL';
        $interiorTitle = $this->interior ? $this->interior->title : 'NULL';
        //--------------------------------------------------------------------------------------------------------------------


        $amenityTitles = [];
        $amenities = $this->amenities;
        if (count($amenities)) {
            $amenityIds = $amenities ? $amenities->pluck('amenity_id')->all() : [];
            $DBAmenities = Amenities::whereIn('id', $amenityIds)->get();
            if (count($DBAmenities))
                $amenityTitles = $DBAmenities->pluck('title')->all();
        }

        //Get Area
        $areaTitle = $this->projectArea ? $this->projectArea->title : 'NULL';

        $createdDate = $this->created_at ? Carbon::parse($this->created_at)->format('Y-m-d') : NULL;
        $updatedDate = $this->updated_at ? Carbon::parse($this->updated_at)->format('Y-m-d') : NULL;
        $publishedDate = $this->published_date ? Carbon::parse($this->published_date)->format('Y-m-d') : NULL;

        $isPublished = $this->is_published ? 1 : 0;
        $isApproved = $this->is_project_approved  ? 1 : 0;

        return $params = [
            'index' => env('ElasticSearchProjectIndex'),
            'type' => env('ElasticSearchProjectType'),
            'id' => $this->id,
            'body' => [
                'ProjectId' => $projectId,
                'UserId' => $userId,
                'OwnerName' => $userFullName,
                'Category' => $category,
                'City' => $city,
                'State' => $stateTitle,
                'Address' => $address,
                'ProjectReraId' => $ProjectReraId,
                'ProjectName' => $ProjectName,
                'ProjectArea' => $ProjectArea,
                'Description' => $description,

                'TotalFloors' => $totalFloors,
                'TotalNoOfUnits' => $TotalNoOfUnits,
                'AvailableNoOfUnits' => $availableNoOfUnits,
                'LaunchDate' => $launchDate,
                'ConstructionStatus' => $constructionStatusTitle,

                'Area' => $areaTitle,
                'Pincode' => $pincode,
                'PriceNegotiable' => $priceNegotiable,
                'ApprovalAuthority' => $approvalAuthority,
                'MainImageName' => $mainImageName,
                'CompletionCertificateImageName' => $completionCertificateImageName,

                'MasterBedroom' => $masterBedroomTitle,
                'Balcony' => $balconyTitle,
                'FloorKitchen' => $floorKitchenTitle,
                'OtherBedroom' => $otherBedroomTitle,
                'LivingOrDining' => $livingOrDiningTitle,
                'FloorToilet' => $floorToiletTitle,

                'InternalDoor' => $internalDoorTitle,
                'FittingKitchen' => $fittingKitchenTitle,
                'FittingToilet' => $fittingToiletTitle,
                'Windows' => $windowsTitle,
                'MainDoor' => $mainDoorTitle,
                'Electrical' => $electricalTitle,
                'WallToilet' => $wallToiletTitle,
                'Interior' => $interiorTitle,

                'Amenities' => $amenityTitles,

                'CreatedDate' => $createdDate,
                'UpdatedDate' => $updatedDate,
                'PublishedDate' => $publishedDate,

                'IsApproved' => $isApproved,
                'IsPublished' => $isPublished
            ]
        ];
    }

    public function getElasticSearchGetIndexParametres()
    {
        return $params = [
            'index' => env('ElasticSearchProjectIndex'),
            'type' => env('ElasticSearchProjectType'),
            'id' => $this->id
        ];
    }

    public function getElasticSearchUpdateApproveStatusParametres()
    {
        $isPublished = $this->is_published ? 1 : 0;
        $isApproved = $this->is_project_approved ? 1 : 0;

        return $params = [
            'index' => env('ElasticSearchProjectIndex'),
            'type' => env('ElasticSearchProjectType'),
            'id' => $this->id,
            'body' => [
                'doc' => [
                    'IsApproved' => $isApproved, 'IsPublished' => $isPublished
                ]
            ]
        ];
    }

    public function getESUpdateApproveByStatus($isApproved)
    {
        return $params = [
            'index' => env('ElasticSearchProjectIndex'),
            'type' => env('ElasticSearchProjectType'),
            'id' => $this->id,
            'body' => [
                'doc' => [
                    'IsApproved' => $isApproved
                ]
            ]
        ];
    }



    public function getElasticSearchUpdatePublishStatusParametres()
    {
        $isPublished = $this->is_published ? 1 : 0;
        return $params = [
            'index' => env('ElasticSearchProjectIndex'),
            'type' => env('ElasticSearchProjectType'),
            'id' => $this->id,
            'body' => [
                'doc' => [
                    'IsPublished' => $isPublished
                ]
            ]
        ];
    }
}
