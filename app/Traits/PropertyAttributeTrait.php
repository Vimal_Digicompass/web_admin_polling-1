<?php

namespace App\Traits;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Models\Master\TenantMaster;
use App\Models\Master\IdealBusniess;
use Carbon\Carbon;
use App\Models\Master\State;
use App\Models\Master\Zipcodes;
use App\Models\Master\Area;


/**
 * Class PropertyAttributeTrait
 *
 * @author  Gokultrident
 */
trait PropertyAttributeTrait
{


    public function getElasticSearchCreateIndexParametres()
    {
        $propertyId = $this->id;
        $propertyUserId = $this->user_id;
        $propertyTitle = $this->title;
        $propertyUserFullName = $this->user ? $this->user->fullname : 'NULL';
        $propertyType = $this->propertyType ? $this->propertyType->title : 'NULL';
        $propertyBuilderName  = $this->builder_name ? $this->builder_name : 'NULL';

        $propertyCategory = $this->propertyCategory ? $this->propertyCategory->title : 'NULL';
        $propertySubCategory = $this->propertySubCategory ? $this->propertySubCategory->title : 'NULL';

        $balcony = $this->propertyBalcony ? $this->propertyBalcony->title : 'NULL';
        $bathroom = $this->propertyBathroom ? $this->propertyBathroom->title : 'NULL';

        $propertyBHK = $this->propertyBHK ? $this->propertyBHK->title : 'NULL';

        $propertyDescription = $this->description ? $this->description : 'NULL';

        $propertyCity = $this->propertyCity ? $this->propertyCity->title : 'NULL';
        $propertyAddress  = $this->address ? $this->address : 'NULL';
        $propertyPincode  = $this->pincode ? $this->pincode : 0;
        $propertyPrice = $this->price ? (float)($this->price) : 0;

        $propertyMainImageName = $this->main_image_name;
        $isPropertyPublished = $this->is_published ? 1 : 0;
        $isPropertyApproved = $this->is_property_approved ? 1 : 0;

        $tenantTypeTitles = [];
        $tenantTypes = $this->tenantTypes;
        if (count($tenantTypes)) {
            $tenantTypeIds = $tenantTypes ? $tenantTypes->pluck('tenant_type_id')->all() : [];
            if (count($tenantTypeIds)) {
                $tenantTypesData = TenantMaster::whereIn('id', $tenantTypeIds)->get();
                if ($tenantTypesData != null && count($tenantTypesData))
                    $tenantTypeTitles = $tenantTypesData->pluck('title')->all();
            }
        }

        $idealForBusinessTitles = [];
        $idealForBusiness = $this->idealForBusiness;
        if (count($idealForBusiness)) {
            $idealBusinessIds = $idealForBusiness ? $idealForBusiness->pluck('ideal_business_id')->all() : [];
            if (count($idealBusinessIds)) {
                $idealBusinessData = IdealBusniess::whereIn('id', $idealBusinessIds)->get();
                if ($idealBusinessData != null && count($idealBusinessData))
                    $idealForBusinessTitles = $idealBusinessData->pluck('title')->all();
            }
        }

        $personalWashroom = $this->personal_washroom ? 'Yes' : 'No';
        $cafeteriaArea = $this->CafeteriaArea ? $this->CafeteriaArea : '';
        $furnishType = $this->propertyFurnishType ? $this->propertyFurnishType->title : '';
        $floor = $this->propertyFloor ? $this->propertyFloor->title : '';

        $availableFrom = $this->available_from ? $this->available_from : null;
        $monthlyRent = $this->monthly_rent ? (float)($this->monthly_rent) : 0;
        $maintenanceCharges = $this->maintenance_charges ? (float)($this->maintenance_charges) : 0;
        $securityDeposit = $this->security_deposit ? (float)($this->security_deposit) : 0;
        $builtupArea = $this->builtup_area ? (float)($this->builtup_area) : 0;
        $carpetArea = $this->carpet_area ? (float)($this->carpet_area) : 0;
        $negotiable = $this->negotiable ? 'Yes' : 'No';
        $expectedDurationOfStay = $this->expected_duration_of_stay ? $this->expected_duration_of_stay : 0;
        $noOfOpenSite = $this->noOfOpenSite ? (float)($this->noOfOpenSite->title) : 0;
        $widthOfRoad = $this->width_of_road ? (float)($this->width_of_road) : 0;
        $boundaryWallMade = $this->boundary_wall_made ? 'Yes' : 'No';

        $constructionType =  $this->constructionType ? $this->constructionType->title : 'NULL';
        $transactionType =  $this->transactionType ? $this->transactionType->title : 'NULL';
        $plotArea =  $this->plot_area ? $this->plot_area : 0;

        $areaUnit =  $this->areaUnit ? $this->areaUnit->title : 'NULL';
        $cornerPlot = $this->corner_plot ? 'Yes' : 'No';
        $priceNegotiable = $this->negotiable ? 'Yes' : 'No';
        $tokenAdvance = $this->token_advance ? (float)($this->token_advance) : 0;
        $propertyAge =  $this->property_age ? (float)($this->property_age) : 0;

        $propertyStatus = $this->propertyStatus ? $this->propertyStatus->title : 'NULL';

        $PGHostelName = $this->PG_or_hostel_name ? $this->PG_or_hostel_name : 'NULL';
        $preferredTenant = $this->PG_preferred_tenant ? explode(',', $this->PG_preferred_tenant) : [];
        $PGType = $this->PG_type ? explode(',', $this->PG_type) : [];
        $PGRoomType = $this->room_type ? $this->PGroomType() : '';
        $attachBathroom = $this->attach_bathroom;
        $foodFacility = $this->food_facility ? explode(',', $this->food_facility) : [];
        $parking = $this->parking ? explode(',', $this->parking) : [];
        $roomAmenities = $this->room_amenities    ? explode(',', $this->room_amenities) : [];
        $gateClosingTime = $this->gate_closing_time ? date('H:i', strtotime($this->gate_closing_time)) : '';
        $PGRules = $this->PG_rules    ? explode(',', $this->PG_rules) : [];
        $commonAmenities = $this->common_amenities    ? explode(',', $this->common_amenities) : [];
        $stateId = $this->propertyCity->state_id;
        $state = State::where('id', $stateId)->first();
        if ($state) {
            $stateTitle = $state['title'];
        } else {
            $stateTitle = "NULL";
        }
        //Get Area
        $areaTitle = $this->propertyArea ? $this->propertyArea->title : 'NULL';
        if (!$areaTitle) {
            $pincodeDetails = Zipcodes::where('zip_code', $this->pincode)->first();
            $area = $pincodeDetails ? Area::where('id', $pincodeDetails->area_id)->first() : '';
            $areaTitle = $area ? $area->title : 'NULL';
        }

        $expectedDurationOfMonth =  $this->expected_duration_of_month ? (float)($this->expected_duration_of_month) : 0;
        $expectedDurationOfYear =  $this->expected_duration_of_year ? (float)($this->expected_duration_of_year) : 0;

        $createdDate = $this->created_at ? Carbon::parse($this->created_at)->format('Y-m-d') : NULL;
        $updatedDate = $this->updated_at ? Carbon::parse($this->updated_at)->format('Y-m-d') : NULL;
        $publishedDate = $this->published_date ? Carbon::parse($this->published_date)->format('Y-m-d') : NULL;

        //Influencer
        $visibleFor = $this->property_visible_for ? (float)($this->property_visible_for) : 0;
        $influencerVideoLink = $this->influencer_video_link ? $this->influencer_video_link : 'NULL';
        $influencerPropertyLookingFor = $this->influencer_property_looking_for ? $this->influencer_property_looking_for : 'NULL';

        $influencerLeadCriteria = $this->influencer_lead_criteria ? $this->influencer_lead_criteria : 'NULL';
        $influencerCommissionOnLead  = $this->influencer_commission_on_lead  ? $this->influencer_commission_on_lead : 'NULL';
        $influencerLeadStateId  = $this->influencer_lead_state_id  ? explode(',', $this->influencer_lead_state_id) : [];
        $influencerLeadCityId  = $this->influencer_lead_city_id  ? $this->influencer_lead_city_id : [];
        $influencerLeadAgeGroup  = $this->influencer_lead_age_group  ? $this->influencer_lead_age_group : [];

        $influencerLeadSalariedBusiness  = $this->influencer_lead_salaried_business  ? $this->influencer_lead_salaried_business : [];
        $influencerLeadAnnualIncomeRange  = $this->influencer_lead_annual_income_range  ? $this->influencer_lead_annual_income_range : [];

        $influencerCommissionOnSale  = $this->influencer_commission_on_sale  ? $this->influencer_commission_on_sale : 'NULL';
        $influencerSaleCriteria  = $this->influencer_sale_criteria  ? $this->influencer_sale_criteria : 'NULL';
        $influencerSaleStateId  = $this->influencer_sale_state_id  ? $this->influencer_sale_state_id : [];
        $influencerSaleCityId  = $this->influencer_sale_city_id  ? $this->influencer_sale_city_id : [];
        $influencerSaleAgeGroup  = $this->influencer_sale_age_group ? $this->influencer_sale_age_group : [];
        $influencerSaleSalariedBusiness  = $this->influencer_sale_salaried_business ? $this->influencer_sale_salaried_business : [];

        $influencerSaleAnnualIncomeRange  = $this->influencer_sale_annual_income_range ? $this->influencer_sale_annual_income_range : [];
        $influencerSaleTokenAdvance = $this->influencer_sale_token_advance ? (float)($this->influencer_sale_token_advance) : 0;

        //Flatemate
        $flatmatePropertyType = $this->flatmates_property_type  ? $this->flatmates_property_type : 'NULL';
        $flatmateTenantType = $this->flatmates_tenant_type  ? $this->flatmates_tenant_type : 'NULL';
        $flatmateRoomType = $this->flatmates_room_type  ? $this->flatmates_room_type : 'NULL';
        $flatmateNonVegAllowed = $this->flatmates_non_veg_allowed  ? $this->flatmates_non_veg_allowed : 'NULL';
        $flatmateBathroom = $bathroom;
        $flatmateGatedSecurity = $this->flatmates_gated_security  ? $this->flatmates_gated_security : 'NULL';
        $flatmateFloor = $this->flatmates_floor  ? (float)($this->flatmates_floor) : 0;
        $flatmatesPreferredTenantType = $this->flatmates_preferred_tenant_type ? explode(',', $this->flatmates_preferred_tenant_type) : [];
        $possessionDate = NULL;        //$this->possession_date ? FormateDate($this->possession_date, 'd/m/Y') : NULL;


        if ($visibleFor == 1) {
            $ElasticSearchPropertyIndex = env('ESInfluencerPropertyIndex');
            $ElasticSearchPropertyType = env('ESInfluencerPropertyType');
        } else {
            $ElasticSearchPropertyIndex = env('ElasticSearchPropertyIndex');
            $ElasticSearchPropertyType = env('ElasticSearchPropertyType');
        }

        $params = [
            'index' => $ElasticSearchPropertyIndex,
            'type' => $ElasticSearchPropertyType,
            'id' => $this->id,
            'body' => [
                'PropertyId' => $propertyId,
                'UserId' => $propertyUserId,
                'Type' => $propertyType,
                'Category' => $propertyCategory,
                'SubCategory' => $propertySubCategory,
                'Title' => $propertyTitle,
                'OwnerName' => $propertyUserFullName,
                'BuilderName' => $propertyBuilderName,
                'Address' => $propertyAddress,
                'State' => $stateTitle,
                'City' => $propertyCity,
                'Pincode' => $propertyPincode,
                'Price' => $propertyPrice,
                'MainImageName' => $propertyMainImageName,
                'Description' => $propertyDescription,
                'Bhk' => $propertyBHK,
                'Balcony' => $balcony,
                'Bathroom' => $bathroom,
                'TenantTypes' => $tenantTypeTitles,
                'IdealForBusiness' => $idealForBusinessTitles,
                'PersonalWashroom' => $personalWashroom,
                'CafeteriaArea' => $cafeteriaArea,
                'FurnishType' => $furnishType,
                'Floor' => $floor,
                'AvailableFrom' => $availableFrom,
                'MonthlyRent' => $monthlyRent,
                'MaintenanceCharges' => $maintenanceCharges,
                'CarpetArea' => $carpetArea,
                'NoOfOpenSite' => $noOfOpenSite,
                'WidthOfRoad' => $widthOfRoad,
                'BoundaryWallMade' => $boundaryWallMade,
                'PlotArea' => $plotArea,
                'AreaUnit' => $areaUnit,
                'CornerPlot' => $cornerPlot,
                'TransactionType' => $transactionType,
                'TokenAdvance' => $tokenAdvance,
                'PriceNegotiable' => $priceNegotiable,
                'PropertyAge' => $propertyAge,
                'PropertyStatus' => $propertyStatus,
                'PreferredTenant' => $preferredTenant,
                'PGHostelName' => $PGHostelName,
                'PGRoomType' => $PGRoomType,
                'PGType' => $PGType,
                'AttachBathroom' => $attachBathroom,
                'FoodFacility' => $foodFacility,
                'Parking' => $parking,
                'RoomAmenities' => $roomAmenities,
                'GateClosingTime' => $gateClosingTime,
                'PGRules' => $PGRules,
                'CommonAmenities' => $commonAmenities,
                'SecurityDeposit' => $securityDeposit,
                'BuiltupArea' => $builtupArea,
                'Area' => $areaTitle,
                'ExpectedDurationOfMonth' => $expectedDurationOfMonth,
                'ExpectedDurationOfYear' => $expectedDurationOfYear,
                'CreatedDate' => $createdDate,
                'UpdatedDate' => $updatedDate,
                'PublishedDate' => $publishedDate,

                'FlatmatePropertyType' => $flatmatePropertyType,
                'FlatmateTenantType' => $flatmateTenantType,
                'FlatmateRoomType' => $flatmateRoomType,
                'FlatmateNonVegAllowed' => $flatmateNonVegAllowed,
                'FlatmateBathroom' => $flatmateBathroom,
                'FlatmateGatedSecurity' => $flatmateGatedSecurity,
                'FlatmateFloor' => $flatmateFloor,
                'FlatmatesPreferredTenantType' => $flatmatesPreferredTenantType,
                'PossessionDate' => $possessionDate,

                'IsApproved' => $isPropertyApproved,
                'IsPublished' => $isPropertyPublished,

                'VisibleFor' => $visibleFor,
                'InfluencerVideoLink' => $influencerVideoLink,
                'InfluencerPropertyLookingFor' => $influencerPropertyLookingFor,
                'InfluencerLeadCriteria' => $influencerLeadCriteria,
                'InfluencerCommissionOnLead' => $influencerCommissionOnLead,
                'InfluencerLeadStateId' => $influencerLeadStateId,
                'InfluencerLeadCityId' => $influencerLeadCityId,
                'InfluencerLeadAgeGroup' => $influencerLeadAgeGroup,
                'InfluencerLeadSalariedBusiness' => $influencerLeadSalariedBusiness,
                'InfluencerLeadAnnualIncomeRange' => $influencerLeadAnnualIncomeRange,
                'InfluencerCommissionOnSale' => $influencerCommissionOnSale,
                'InfluencerSaleCriteria' => $influencerSaleCriteria,
                'InfluencerSaleStateId' => $influencerSaleStateId,
                'InfluencerSaleCityId' => $influencerSaleCityId,
                'InfluencerSaleAgeGroup' => $influencerSaleAgeGroup,
                'InfluencerSaleSalariedBusiness' => $influencerSaleSalariedBusiness,
                'InfluencerSaleAnnualIncomeRange' => $influencerSaleAnnualIncomeRange,
                'InfluencerSaleTokenAdvance' => $influencerSaleTokenAdvance,
            ]
        ];


        return $params;
    }

    public function getElasticSearchGetIndexParametres()
    {
        return $params = [
            'index' => env('ElasticSearchPropertyIndex'),
            'type' => env('ElasticSearchPropertyType'),
            'id' => $this->id
        ];
    }

    public function getESGetInfluencerIndexParametres()
    {
        return $params = [
            'index' => env('ESInfluencerPropertyIndex'),
            'type' => env('ESInfluencerPropertyType'),
            'id' => $this->id
        ];
    }


    public function getElasticSearchUpdateApproveStatusParametres()
    {
        $isPublished = $this->is_published ? 1 : 0;
        $isApproved = $this->is_property_approved ? 1 : 0;

        return $params = [
            'index' => env('ElasticSearchPropertyIndex'),
            'type' => env('ElasticSearchPropertyType'),
            'id' => $this->id,
            'body' => [
                'doc' => [
                    'IsApproved' => $isApproved, 'IsPublished' => $isPublished
                ]
            ]
        ];
    }

    public function getESUpdateApproveByStatus($isApproved)
    {
        return $params = [
            'index' => env('ElasticSearchPropertyIndex'),
            'type' => env('ElasticSearchPropertyType'),
            'id' => $this->id,
            'body' => [
                'doc' => [
                    'IsApproved' => $isApproved
                ]
            ]
        ];
    }

    public function getElasticSearchUpdatePublishStatusParametres()
    {
        $isPublished = $this->is_published ? 1 : 0;
        return $params = [
            'index' => env('ElasticSearchPropertyIndex'),
            'type' => env('ElasticSearchPropertyType'),
            'id' => $this->id,
            'body' => [
                'doc' => [
                    'IsPublished' => $isPublished
                ]
            ]
        ];
    }
}
