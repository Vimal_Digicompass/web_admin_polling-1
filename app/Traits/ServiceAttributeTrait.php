<?php

namespace App\Traits;

use Carbon\Carbon;
use App\Models\Master\State;
use App\Models\Master\Zipcodes;
use App\Models\Master\Area;
use App\Models\Master\ServiceCategory;

use Illuminate\Support\Facades\Config;


/**
 * Class ServiceAttributeTrait
 *
 * @author  gokul trident
 */
trait ServiceAttributeTrait
{

    public function getElasticSearchCreateIndexParametres()
    {
        $serviceId = $this->id;
        $userId = $this->user_id;

        $title = $this->title;
        $userFullName = $this->user ? $this->user->fullname : 'NULL';

        $serviceCity = $this->serviceCity ? $this->serviceCity->title : 'NULL';
        $stateId = $this->serviceCity->state_id;
        $state = State::where('id', $stateId)->first();
        if ($state) {
            $stateTitle = $state['title'];
        } else {
            $stateTitle = "NULL";
        }


        $serviceType = $this->serviceType ? $this->ServiceType : 'NULL';

        $shortDescription = $this->short_description ? $this->short_description : 'NULL';

        $minPrice = $this->min_price ? (float)($this->min_price) : 0;
        $maxPrice = $this->max_price ? (float)($this->max_price) : 0;

        $serviceImageName = $this->main_image_name; //$this->ServiceMainImage;

        $category = '';
        $subCategory = $this->serviceSubCategory ? $this->serviceSubCategory->title : 'NULL';
        if ($subCategory) {
            $categoryData = ServiceCategory::where('id', $this->serviceSubCategory->service_category_id)->first();
            $category = $categoryData ? $categoryData->title : '';
        }

        $areaTitles = [];
        $ZipCodes = [];
        $areas = $this->serviceAreas;
        if ($areas != null && count($areas)) {
            $areaIds = $areas->pluck('area_id')->all();
            $allAreas = Area::whereIn('id', $areaIds)->get();
            if ($allAreas != null && count($allAreas)) {
                $areaTitles = $allAreas->pluck('title')->all();

                //Get area zips
                $allZipCodes = ZipCodes::whereIn('area_id', $areaIds)->get();
                if ($allZipCodes != null && count($allZipCodes)) {
                    $ZipCodes = $allZipCodes->pluck('zip_code')->all();
                }
            }
        }

        $benefits = $this->benefits ? $this->benefits : 'NULL';
        $warranty = $this->benefits ? $this->benefits : 'NULL';
        $offers = $this->offers ? $this->offers : 'NULL';
        $delivery = $this->delivery ? $this->delivery : 'NULL';

        $paymentTypes = [];
        $paymentModeIds = $this->payment_mode_id ? $this->payment_mode_id : '';
        if ($paymentModeIds) {
            $paymentModeIdsArray = explode(',', $paymentModeIds);

            //Get all paymentType
            $paymentTypesConfig = Config::get('constants.payment-type');
            foreach ($paymentTypesConfig as $key => $data) {
                if (in_array($key, $paymentModeIdsArray)) {
                    array_push($paymentTypes, $data);
                }
            }
        }

        $createdDate = $this->created_at ? Carbon::parse($this->created_at)->format('Y-m-d') : NULL;
        $updatedDate = $this->updated_at ? Carbon::parse($this->updated_at)->format('Y-m-d') : NULL;
        $publishedDate = $this->published_date ? Carbon::parse($this->published_date)->format('Y-m-d') : NULL;

        $isPublished = $this->is_published ? 1 : 0;
        $isApproved = $this->is_service_approved  ? 1 : 0;

        return $params = [
            'index' => env('ElasticSearchServiceIndex'),
            'type' => env('ElasticSearchServiceType'),
            'id' => $this->id,
            'body' => [
                'ServiceId' => $serviceId,
                'UserId' => $userId,
                'Title' => $title,
                'OwnerName' => $userFullName,
                'City' => $serviceCity,
                'State' => $stateTitle,
                'Type' => $serviceType,
                'Category' => $category,
                'SubCategory' => $subCategory,
                'ShortDescription' => $shortDescription,
                'MinPrice' => $minPrice,
                'MaxPrice' => $maxPrice,
                'MainImageName' => $serviceImageName,
                'Areas' => $areaTitles,
                'Zips' => $ZipCodes,
                'Benefits' => $benefits,
                'Offers' => $offers,
                'Delivery' => $delivery,
                'PaymentTypes' => $paymentTypes,
                'CreatedDate' => $createdDate,
                'UpdatedDate' => $updatedDate,
                'PublishedDate' => $publishedDate,
                'IsApproved' => $isApproved,
                'IsPublished' => $isPublished
            ]
        ];
    }

    public function getElasticSearchGetIndexParametres()
    {
        return $params = [
            'index' => env('ElasticSearchServiceIndex'),
            'type' => env('ElasticSearchServiceType'),
            'id' => $this->id
        ];
    }

    public function getElasticSearchUpdateApproveStatusParametres()
    {
        $isPublished = $this->is_published ? 1 : 0;
        $isApproved = $this->is_service_approved ? 1 : 0;

        return $params = [
            'index' => env('ElasticSearchServiceIndex'),
            'type' => env('ElasticSearchServiceType'),
            'id' => $this->id,
            'body' => [
                'doc' => [
                    'IsApproved' => $isApproved, 'IsPublished' => $isPublished
                ]
            ]
        ];
    }

    public function getESUpdateApproveByStatus($isApproved)
    {
        return $params = [
            'index' => env('ElasticSearchServiceIndex'),
            'type' => env('ElasticSearchServiceType'),
            'id' => $this->id,
            'body' => [
                'doc' => [
                    'IsApproved' => $isApproved
                ]
            ]
        ];
    }


    public function getElasticSearchUpdatePublishStatusParametres()
    {
        $isPublished = $this->is_published ? 1 : 0;
        return $params = [
            'index' => env('ElasticSearchServiceIndex'),
            'type' => env('ElasticSearchServiceType'),
            'id' => $this->id,
            'body' => [
                'doc' => [
                    'IsPublished' => $isPublished
                ]
            ]
        ];
    }
}
