<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlotArea extends Model
{
    use HasFactory;

    protected $connection = 'mysql_1';

    protected $table = 'plot_area_master';
    public $timestamps = false;

    protected $fillable = [
        'title',
        'sortorder',
        'is_active',
        'city_id'
    ];
}
