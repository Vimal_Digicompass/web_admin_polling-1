<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KYCRejectedStatus extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $table = 'kycrejectreason';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'title'];
}
