<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'transaction_type_master';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'is_active', 'sortorder', 'title'];
}
