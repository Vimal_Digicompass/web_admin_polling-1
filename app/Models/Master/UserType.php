<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    use HasFactory;
    
    protected $connection = 'mysql';

    protected $table = 'usertype';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'title', 'titlevalue'];
}
