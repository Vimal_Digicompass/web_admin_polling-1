<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ElectricalMaster extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'electrical_master';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'is_active', 'sortorder', 'title'];
}
