<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostType extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $table = 'post_type';

    protected $primaryKey = 'post_type_id';

    protected $fillable = ['post_type_id','value' ,'title'];
}
