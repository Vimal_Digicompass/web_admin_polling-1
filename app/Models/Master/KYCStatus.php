<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KYCStatus extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $table = 'kycstatus';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'title'];
}
