<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlatIdealBusniess extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'project_flat_ideal_for_business';

    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['id', 'flat_id', 'ideal_business_id'];
}
