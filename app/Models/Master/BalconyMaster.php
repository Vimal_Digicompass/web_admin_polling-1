<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BalconyMaster extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'balcony_master';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'is_active', 'sortorder', 'title', 'is_applicable_for_property', 'is_applicable_for_project'];
}
