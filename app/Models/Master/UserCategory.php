<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCategory extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $table = 'user_categories';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'title'];
}
