<?php
namespace App\Models\Project;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use App\Traits\ProjectAttributeTrait;


class Project extends Model
{
    use ProjectAttributeTrait;

    protected $connection = 'mysql_1';
    protected $table = 'projects';
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'parent_id',
        'category_id',
        'project_rera_id',
        'project_name',
        'project_area',
        'address',
        'city_id',
        'description',
        'total_no_of_units',
        'available_no_of_units',
        'construction_status_id',
        'area_unit_id',
        'total_floors',
        'area_id',
        'pincode',
        'price_negotiable',
        'approval_authority',
        'is_project_approved',
        'reject_message',
        'is_send_for_approve_request',
        'is_published',
        'main_image_name',
        'completion_certificate_image_name',
        'master_bedroom_Id',
        'balcony_id',
        'floor_kitchen_id',
        'other_bedroom_id',
        'living_or_dining_id',
        'floor_toilet_id',
        'internal_door_id',
        'fitting_kitchen_id',
        'fitting_toilet_id',
        'windows_id',
        'main_door_id',
        'electrical_id',
        'wall_kitchen_id',
        'wall_toilet_id',
        'interior_id',
        'launch_date',
        'updated_at',
        'published_date',
        'video_file_name',
        'crm_remark',
        'crm_outbound_call_status',
        'crm_inbound_call_status',
        'is_crm_inbound',
        'assigned_to',
        'approved_by',
        'project_status',
        'influencer_project_post_to',
        'influencer_project_looking_for',
        'influencer_unit_wise_lead_commission',
        'influencer_commission_on_lead',
        'influencer_lead_criteria',
        'influencer_lead_state_id',
        'influencer_lead_city_id',
        'influencer_lead_age_group',
        'influencer_lead_salaried_business',
        'influencer_lead_annual_income_range',
        'influencer_video_link',
        'influencer_sale_unitwise_commission',
        'influencer_commission_on_sale',
        'influencer_sale_criteria',
        'influencer_sale_state_id',
        'influencer_sale_city_id',
        'influencer_sale_age_group',
        'influencer_sale_salaried_business',
        'influencer_sale_annual_income_range',
        'influencer_sale_token_advance',
        'is_pending',
        'posted_date'


    ];

    protected $attributes = [];

    protected $hidden = [];

    protected $casts = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * It will return User.
     */
    public function user(): belongsTo
    {
        return $this->belongsTo('App\Models\SqFt\User', 'user_id');
    }

    public function projectCategory(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\ProjectCategory', 'category_id');
    }

    public function projectCity(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\City', 'city_id');
    }

    public function constructionStatus(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\ConstructionStatus', 'construction_status_id');
    }

    public function masterBedroom(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\BedroomMaster', 'master_bedroom_Id');
    }

    public function balcony(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\BalconyMaster', 'balcony_id');
    }

    public function floorKitchen(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\KitchenMaster', 'floor_kitchen_id');
    }

    public function otherBedroom(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\BedroomMaster', 'other_bedroom_id');
    }

    public function livingOrDining(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\LivingMaster', 'living_or_dining_id');
    }

    public function floorToilet(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\ToiletMaster', 'floor_toilet_id');
    }

    public function fittingKitchen(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\KitchenMaster', 'fitting_kitchen_id');
    }

    public function fittingToilet(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\ToiletMaster', 'fitting_toilet_id');
    }

    public function approvalAuthority(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\ApprovalAuthority', 'approval_authority');
    }

    public function windows(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\WindowMaster', 'windows_id');
    }

    public function internalDoor(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\DoorMaster', 'internal_door_id');
    }

    public function mainDoor(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\DoorMaster', 'main_door_id');
    }

    public function electrical(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\ElectricalMaster', 'electrical_id');
    }

    public function wallKitchen(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\KitchenMaster', 'wall_kitchen_id');
    }

    public function wallToilet(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\ToiletMaster', 'wall_toilet_id');
    }

    public function interior(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\InteriorMaster', 'interior_id');
    }

    public function amenities(): HasMany
    {
        return $this->hasMany('App\Models\Project\ProjectAmenities', 'project_id');
    }


    public function getProjectApproveStatusAttribute()
    {
        $status = Config::get('constants.project-approve-status');
        return isset($status[$this->is_project_approved]) ? $status[$this->is_project_approved] : '';
    }


    public function projectArea(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\Area', 'area_id');
    }

    public function ProjectVisibleFor()
    {
        $projectVisibleForArray = Config::get('constants.property-visible-for');
        return isset($projectVisibleForArray[$this->influencer_project_post_to]) ? $projectVisibleForArray[$this->influencer_project_post_to] : '';
    }
}
