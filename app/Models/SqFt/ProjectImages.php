<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectImages extends Model
{
    use HasFactory;

    protected $connection = 'mysql_1';

    protected $table = 'project_images';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'id', 'project_id', 'image_name'
    ];
}
