<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\SqFt\ServiceImages;
use App\Models\SqFt\ServiceArea;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\libraries\AWSElasticSearch;

use App\Models\Service\Service as ServiceService;

class Service extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'services';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'user_id', 'title', 'service_type_id', 'sub_category_id',
        'payment_mode_id', 'city_id', 'min_price', 'delivery', 'max_price',
        'short_description', 'benefits', 'warranty', 'offers',
        'is_service_approved', 'is_send_for_approve_request', 'reject_message',
        'crm_remark', 'crm_outbound_call_status', 'is_crm_inbound',
        'crm_inbound_call_status', 'assigned_to', 'is_published',
        'main_image_name', 'sortorder',
        'created_at', 'updated_at', 'published_date', 'approved_by', 'hours'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function getimages(): HasMany
    {
        return $this->hasMany(ServiceImages::class, 'service_id', 'id');
    }
    public function getarea(): HasMany
    {
        return $this->hasMany(ServiceArea::class, 'service_id', 'id');
    }


    protected static function booted()
    {


        static::updated(function ($service) {
            $service->refreshESIndex();
        });
    }


    private function refreshESIndex()
    {
        $AWSElasticSearch = new AWSElasticSearch();
        $serviceElastic = ServiceService::where('id', $this->id)->first();
        //Check service index already exists
        $isExists = $AWSElasticSearch->checkIndexExists($serviceElastic);

        if ($isExists) {
            $AWSElasticSearch->deleteIndexDoc($serviceElastic, 'S');
        }
        // if ($serviceElastic->is_service_approved == 1 && $serviceElastic->is_published == 1) {
        $AWSElasticSearch->createIndex($serviceElastic);
        // }

        unset($AWSElasticSearch);
    }
}
