<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyTenant extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'property_tenant_type';

    protected $primaryKey = 'id';
  public $timestamps = false;
    protected $fillable = [
        'id', 'property_id', 'tenant_type_id'
    ];
}
