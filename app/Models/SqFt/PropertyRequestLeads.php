<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyRequestLeads extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'property_request_leads';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'user_id', 'property_id', 'cost', 'is_viewed', 'lead_type', 'external_user_id',
        'third_party_template_id', 'prefix', 'transaction_num', 'updated_at'
    ];
}
