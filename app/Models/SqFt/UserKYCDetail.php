<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserKYCDetail extends Model
{
    use HasFactory;

    protected $connection = 'mysql_1';

    protected $table = 'user_kyc_details';

    const CREATED_AT = 'created_date';

    protected $fillable = [
        'id', 'user_id', 'pan_no', 'pan_file_name',
        'aadhar_no', 'aadhar_file_name', 'aadhar_back_file_name',
        'voting_no', 'voting_front_file_name', 'voting_back_file_name',
        'driving_license_no', 'driving_license_file_name', 'driving_license_back_file_name',
        'gst_no', 'gst_file_name', 'certificate_no', 'certificate_file_name',
        'cancelled_check_no', 'cancelled_check_file_name', 'pan_status',
        'aadhar_status', 'voting_status', 'gst_status', 'certificate_status',
        'cheque_status', 'driving_status', 'certificate_reject_reason',
        'voting_reject_reason', 'gst_reject_reason', 'pan_reject_reason',
        'aadhar_reject_reason', 'cheque_reject_reason', 'driving_reject_reason',
        ' driving_reject_reason', 'updated_at', 'created_date'
    ];

    protected $primaryKey = 'id';
}
