<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectRequestLeads extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'project_request_leads';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'user_id', 'project_id', 'flat_ids', 'cost', 'is_viewed', 'lead_type',
        'external_user_id',
        'third_party_template_id', 'prefix', 'transaction_num', 'updated_at'
    ];
}
