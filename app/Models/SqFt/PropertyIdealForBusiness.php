<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyIdealForBusiness extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'property_ideal_for_business';

    protected $primaryKey = 'id';
      public $timestamps = false;

    protected $fillable = [
        'id', 'property_id', 'ideal_business_id'
    ];
}
