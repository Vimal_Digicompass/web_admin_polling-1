<?php

namespace App\Models\SqFt;

use App\libraries\AWSElasticSearch;
use App\Models\Project\Project as ProjectProject;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\SqFt\ProjectAmenities;
use App\Models\SqFt\ProjectImages;
use App\Models\SqFt\ProjectFlats;
use App\Models\SqFt\User;
use Illuminate\Support\Facades\Log;

class Project extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'projects';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'user_id', 'category_id',
        'project_rera_id', 'project_name',
        'project_area', 'address', 'city_id',
        'description', 'total_no_of_units',
        'available_no_of_units', 'construction_status_id',
        'area_unit_id', 'total_floors', 'pincode', 'price_negotiable',
        'approval_authority', 'main_image_name', 'completion_certificate_image_name',
        'video_file_name', 'master_bedroom_Id', 'balcony_id', 'floor_kitchen_id',
        'other_bedroom_id', 'living_or_dining_id', 'floor_toilet_id',
        'internal_door_id', 'fitting_kitchen_id', 'fitting_toilet_id',
        'windows_id', 'main_door_id', 'electrical_id', 'wall_kitchen_id',
        'wall_toilet_id', 'interior_id', 'launch_date', 'is_project_approved',
        'reject_message', 'crm_remark', 'crm_outbound_call_status', 'is_crm_inbound',
        'crm_inbound_call_status', 'is_send_for_approve_request', 'is_published',
        'project_status', 'created_at', 'updated_at', 'published_date', 'posted_date',
        'assigned_to', 'approved_by', 'area_id', 'influencer_video_link',
        'influencer_sale_token_advance', 'influencer_sale_annual_income_range',
        'influencer_sale_salaried_business', 'influencer_sale_age_group',
        'influencer_sale_city_id', 'influencer_sale_state_id',
        'influencer_sale_criteria', 'influencer_commission_on_sale',
        'influencer_sale_unitwise_commission', 'influencer_lead_annual_income_range',
        'influencer_lead_salaried_business', 'influencer_lead_age_group', 'influencer_lead_city_id',
        'influencer_lead_state_id', 'influencer_lead_criteria', 'influencer_commission_on_lead',
        'influencer_unit_wise_lead_commission', 'influencer_project_looking_for',
        'influencer_project_post_to', 'parent_id', 'is_pending'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getamenities(): HasMany
    {
        return $this->hasMany(ProjectAmenities::class, 'project_id', 'id');
    }

    public function getimages(): HasMany
    {
        return $this->hasMany(ProjectImages::class, 'project_id', 'id');
    }
    public function getflats(): HasMany
    {
        return $this->hasMany(ProjectFlats::class, 'project_id', 'id');
    }

    /**
     * The booted method of the model.
     *
     * @return void
     */
    protected static function booted()
    {

        Log::info('booted to elastic search');
        static::updated(function ($project) {
            $project->refreshESIndex();
        });
    }

    private function refreshESIndex()
    {

        Log::info('coming to elastic search');
        $AWSElasticSearch = new AWSElasticSearch();
        $projectElastic = ProjectProject::where('id', $this->id)->first();
        //Check project index already exists
        $isExists = $AWSElasticSearch->checkIndexExists($projectElastic);
        // echo $isExists;
        // exit;
        if ($isExists) {
            Log::info('exists index');
            $AWSElasticSearch->deleteIndexDoc($projectElastic, 'PR');
        }
        // if ($projectElastic->is_project_approved == 1 && $projectElastic->is_published == 1) {
            Log::info('create index');
            $AWSElasticSearch->createIndex($projectElastic);
        // }

        unset($AWSElasticSearch);
        Log::info('Done  elastic search');
    }
}
