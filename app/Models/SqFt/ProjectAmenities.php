<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Master\Amenities;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ProjectAmenities extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'project_amenities';

    protected $primaryKey = 'id';

    public $timestamps = false;
    protected $fillable = [
        'id', 'project_id', 'amenity_id'
    ];

    public function getamenitiesmaster(): hasMany
    {
        return $this->hasMany(Amenities::class, 'id', 'amenity_id');
    }
}
