<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceArea extends Model
{
    use HasFactory;

    protected $connection = 'mysql_1';
    protected $table = 'service_areas';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id', 'service_id', 'area_id'
    ];
}
