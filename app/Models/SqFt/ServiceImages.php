<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceImages extends Model
{
    use HasFactory;

    protected $connection = 'mysql_1';

    protected $table = 'service_images';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'id', 'service_id', 'image_name'
    ];
}
