<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceRequestLeads extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'service_request_leads';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'user_id', 'service_id', 'is_viewed', 'lead_type', 'external_user_id', 'third_party_template_id', 'prefix',
        'transaction_num', 'cost', 'updated_at', 'service_post_requirements_id'
    ];
}
