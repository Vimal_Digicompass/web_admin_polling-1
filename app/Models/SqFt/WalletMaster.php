<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletMaster extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'wallet_master';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'is_active', 'package_name','validity'];
}
