<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'users';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'firstname', 'lastname', 'email',
        'username', 'company_name', 'mobile_number', 'whatsapp_number',
        'password', 'user_type', 'role_id', 'team_lead_id',
        'address', 'city_id', 'profile_city_id', 'profile_state_id',
        'pincode', 'website_url', 'confirmed', 'remember_token',
        'otp', 'otp_expire', 'is_whatsup_update_required',
        'is_active', 'category_id', 'is_mobile_verified',
        'is_password_updated', 'is_profile_updated', 'is_kyc_updated',
        'image_name', 'latitude', 'longitude', 'isPrimary',
        'is_send_for_kyc_request', 'is_kyc_approved',
        'kyc_rejcted_reason', 'crm_remark', 'crm_outbound_call_status',
        'is_crm_inbound', 'crm_inbound_call_status', 'android_device_token',
        'android_reg_device_id', 'is_email_verified', 'created_at',
        'package_expiry_date', 'is_auto_password', 'is_changed_password',
        'is_changed_mobile_password', 'updated_at', 'deleted_at', 'created_by',
        'assigned_to', 'approved_by', 'gender', 'experienced_type',
        'years_of_experience', 'highest_qualification_id', 'kyc_approved_rejected_date',
        'last_login_date', 'email_expire', 'email_expire_token'
    ];
}
