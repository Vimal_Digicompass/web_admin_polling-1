<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLoggedinActivity extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';
    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_log_id';

    protected $table = 'user_loggedin_activity';


    protected $fillable = [
        'user_log_id',
        'user_id',
        'login_time',
        'logout_time',
        'date',
        'date_unixtime',
        'is_online'
    ];
}
