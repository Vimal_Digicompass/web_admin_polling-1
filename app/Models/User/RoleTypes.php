<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleTypes extends Model
{
    use HasFactory;

    const CRM_USER = 2;
    const BACKEND_OPS_TEAM = 3;
    const SUBADMIN = 4;

    protected $table = 'role_type';

    protected $fillable = ['title', 'role_type_id'];

    protected $guarded = ['role_type_id'];

    protected $primaryKey = 'role_type_id';

    protected $connection = "mysql";
}
