<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class RolePermissionMapping extends Model
{
    use HasFactory,SoftDeletes;

    protected $connection = 'mysql';
    protected $dates = ['deleted_at'];
    protected $guarded = ['role_perm_id'];
    protected $table = 'role_permission_mapping';
    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];
}
