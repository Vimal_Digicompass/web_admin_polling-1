<?php

namespace App\Models\Property;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use App\Traits\PropertyAttributeTrait;


class Property extends Model
{
    use PropertyAttributeTrait;

    protected $connection = 'mysql_1';
    protected $table = 'properties';
    public $timestamps = false;

    protected $fillable = [

        'user_id',
        'title',
        'builder_name',
        'property_type_id',
        'property_category_id',
        'property_sub_category_id',
        'bhk_id',
        'furnish_type_id',
        'main_image_name',
        'floor_id',
        'bathroom_id',
        'balcony_id',
        'ideal_for_business_id',
        'width_of_road',
        'transaction_type_id',
        'available_from',
        'monthly_rent',
        'maintenance_charges',
        'security_deposit',
        'builtup_area',
        'construction_type_id',
        'carpet_area',
        'no_open_side_id',
        'negotiable',
        'price',
        'corner_plot',
        'area_unit',
        'plot_area',
        'token_advance',
        'cafeteria_area_id',
        'boundary_wall_made',
        'personal_washroom',
        'expected_duration_of_stay',
        'expected_duration_of_month',
        'expected_duration_of_year',
        'property_age',
        'address',
        'pincode',
        'city_id',
        'description',
        'sortorder',
        'PG_or_hostel_name',
        'PG_type',
        'PG_preferred_tenant',
        'room_type',
        'attach_bathroom',
        'food_facility',
        'parking',
        'rent',
        'room_amenities',
        'gate_closing_time',
        'PG_rules',
        'common_amenities',
        'is_property_approved',
        'reject_message',
        'is_published',
        'property_status_id',
        'is_send_for_approve_request',
        'created_at',
        'updated_at',
        'published_date',
        'latitude',
        'longitude',
        'crm_remark',
        'crm_outbound_call_status',
        'crm_inbound_call_status',
        'is_crm_inbound',
        'assigned_to',
        'approved_by',
        'area_id',
        'property_status',
        'property_visible_for',
        'parent_id',
        'influencer_video_link',
        'influencer_property_looking_for',
        'influencer_lead_criteria',
        'influencer_commission_on_lead',
        'influencer_lead_state_id',
        'influencer_lead_city_id',
        'influencer_lead_age_group',
        'influencer_lead_salaried_business',
        'influencer_lead_annual_income_range',
        'influencer_commission_on_sale',
        'influencer_sale_criteria',
        'influencer_sale_state_id',
        'influencer_sale_city_id',
        'influencer_sale_age_group',
        'influencer_sale_salaried_business',
        'influencer_sale_annual_income_range',
        'influencer_sale_token_advance',
        'is_pending',
        'flatmates_property_type',
        'flatmates_tenant_type',
        'flatmates_room_type',
        'flatmates_non_veg_allowed',
        'flatmates_gated_security',
        'possession_date',
        'flatmates_floor',
        'flatmates_preferred_tenant_type'

    ];

    protected $attributes = [];

    protected $hidden = [];

    protected $casts = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

   


    /**
     * It will return User.
     */
    public function user(): belongsTo
    {
        return $this->belongsTo('App\Models\SqFt\User', 'user_id');
    }

    public function assignedTo(): belongsTo
    {
        return $this->belongsTo('App\Models\SqFt\User', 'assigned_to');
    }

    public function propertyType(): belongsTo
    {
        return $this->belongsTo('App\Models\Property\PropertyType', 'property_type_id');
    }

    public function propertyCategory(): belongsTo
    {
        return $this->belongsTo('App\Models\Property\PropertyCategories', 'property_category_id');
    }

    public function propertySubCategory(): belongsTo
    {
        return $this->belongsTo('App\Models\Property\PropertySubCategories', 'property_sub_category_id');
    }

    public function propertyBHK(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\BHKMaster', 'bhk_id');
    }

    public function propertyBalcony(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\BalconyMaster', 'balcony_id');
    }

    public function propertyBathroom(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\BathroomMaster', 'bathroom_id');
    }

    public function propertyFurnishType(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\FurnishMaster', 'furnish_type_id');
    }

    public function propertyFloor(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\FloorMaster', 'floor_id');
    }

    public function constructionType(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\ConstructionStatus', 'construction_type_id');
    }

    public function transactionType(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\TransactionType', 'transaction_type_id');
    }

    public function propertyCity(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\City', 'city_id');
    }

    public function propertyArea(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\Area', 'area_id');
    }

    public function propertyStatus(): belongsTo
    {
        return $this->belongsTo('App\Models\Property\PropertyStatus', 'property_status_id');
    }

    public function noOfOpenSite(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\OpenSite', 'no_open_side_id');
    }

    public function tenantTypes(): HasMany
    {
        return $this->hasMany('App\Models\Property\PropertyTenantType', 'property_id');
    }


    public function idealForBusiness(): HasMany
    {
        return $this->hasMany('App\Models\Property\PropertyIdealForBusiness', 'property_id');
    }


    public function PGroomType()
    {
        $PGroomType = Config::get('constants.PG-Room-Type');
        return isset($PGroomType[$this->room_type]) ? $PGroomType[$this->room_type] : '';
    }
    public function getPropertyNegotiableAttribute()
    {
        $negotiableArray = Config::get('constants.PROPERTY_NEGOTIABLE');
        return '';
    }

    public function getPropertyApproveStatusAttribute()
    {
        $status = Config::get('constants.property-approve-status');
        return isset($status[$this->is_property_approved]) ? $status[$this->is_property_approved] : '';
    }
    public function PropertyVisibleFor()
    {
        $propertyVisibleForArray = Config::Get('constants.property-visible-for');
        return isset($propertyVisibleForArray[$this->property_visible_for]) ? $propertyVisibleForArray[$this->property_visible_for] : '';
    }

    public function getCafeteriaAreaAttribute()
    {
        $cafeteria = Config::get('constants.pantry-cafeteria');
        return isset($cafeteria[$this->cafeteria_area_id]) ? $cafeteria[$this->cafeteria_area_id] : '';
    }

    public function areaUnit(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\PlotArea', 'area_unit');
    }
}
