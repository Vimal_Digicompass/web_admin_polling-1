<?php

namespace App\Models\Property;

use Illuminate\Database\Eloquent\Model;

class PropertyIdealForBusiness extends Model
{
    protected $table = 'property_ideal_for_business';
    public $timestamps = false;
    protected $connection = 'mysql_1';

    protected $fillable = [
        'property_id',
        'ideal_business_id',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
}
