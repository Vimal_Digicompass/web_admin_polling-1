<?php
namespace App\Models\Property;
use Illuminate\Database\Eloquent\Model;
class PropertySubCategories extends Model
{
    protected $table = 'property_subcategories_master';
    protected $connection = 'mysql_1';
    public $timestamps = false;

    protected $fillable = [
        'title',
        'sortorder',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

 




}
