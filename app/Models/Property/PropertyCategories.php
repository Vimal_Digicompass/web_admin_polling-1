<?php

namespace App\Models\Property;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;

class PropertyCategories extends Model
{
    protected $table = 'property_categories_master';
    public $timestamps = false;
    protected $connection = 'mysql_1';

    protected $fillable = [
        'title',
        'sortorder',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */


    public function subCategories(): HasMany
    {
        return $this->hasMany('App\Models\Property\PropertySubCategories', 'property_category_id');
    }

}
