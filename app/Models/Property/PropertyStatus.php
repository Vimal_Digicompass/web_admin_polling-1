<?php

namespace App\Models\Property;

use Illuminate\Database\Eloquent\Model;
use App\Traits\PropertyAttributeTrait;

class PropertyStatus extends Model
{
    use PropertyAttributeTrait;
    protected $connection = 'mysql_1';

    protected $table = 'property_status_master';
    public $timestamps = false;

    protected $fillable = [
        'title',
        'sortorder',
        'is_active'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
}
