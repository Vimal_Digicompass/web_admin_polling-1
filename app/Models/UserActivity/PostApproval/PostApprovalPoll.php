<?php

namespace App\Models\UserActivity\PostApproval;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostApprovalPoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $primaryKey  = 'post_approve_poll_id';

    protected $fillable = ['city_id', 'user_id','post_id','post_type'];

    protected $table = 'postapproval_poll';
}
