<?php

namespace App\Models\UserActivity\NotEnquirePost;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotEnquirePostChangelog extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_notenquire_post_changelog_id';

    protected $fillable = ['post_assign_trn_id ', 'fieldname','oldvalue','newvalue','done'];

    protected $table = 'user_notenquire_post_changelog';
}
