<?php

namespace App\Models\UserActivity\NotEnquirePost;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotEnquirePostTransaction extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_notenquire_post_assign_trn_id';

    protected $fillable = ['city_id', 'user_id'];

    protected $table = 'user_notenquire_post_assign_trn';
}
