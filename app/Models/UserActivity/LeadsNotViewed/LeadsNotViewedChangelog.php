<?php

namespace App\Models\UserActivity\LeadsNotViewed;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class LeadsNotViewedChangelog extends Model
{
    use HasFactory, softDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'leadsnotview_changelog_id';

    protected $table = 'leadsnotview_changelog';
}
