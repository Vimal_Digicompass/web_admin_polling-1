<?php

namespace App\Models\UserActivity\LeadsNotViewed;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class LeadsNotViewedAssign extends Model
{
    use HasFactory;
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $fillable = ['crm_user_id', 'user_id','post_id', 'city_id','post_type' , 'updatestatus','followup_date'];

    protected $primaryKey  = 'leadsnotview_assign_id';

    protected $table = 'leadsnotview_assign';
}
