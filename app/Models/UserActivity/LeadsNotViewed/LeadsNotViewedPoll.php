<?php

namespace App\Models\UserActivity\LeadsNotViewed;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeadsNotViewedPoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $primaryKey  = 'leadsnotview_poll_id';

    protected $fillable = ['city_id', 'user_id','post_id','post_type'];

    protected $table = 'leadsnotview_poll';
}
