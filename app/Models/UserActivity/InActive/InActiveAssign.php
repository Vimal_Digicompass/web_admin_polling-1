<?php

namespace App\Models\UserActivity\InActive;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InActiveAssign extends Model
{
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_notactive_assign_id';

    protected $fillable = ['user_notactive_assign_trn_id', 'crm_user_id', 'city_id', 'user_id', 'status', 'followup_date', 'updatestatus', 'remarks'];

    protected $table = 'user_notactive_assign';
}
