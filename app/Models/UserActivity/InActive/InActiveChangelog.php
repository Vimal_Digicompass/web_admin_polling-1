<?php

namespace App\Models\UserActivity\InActive;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InActiveChangelog extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_notactive_changelog_id';

    protected $fillable = ['user_notactive_assign_trn_id', 'fieldname', 'oldvalue', 'newvalue', 'done'];

    protected $table = 'user_notactive_changelog';
}
