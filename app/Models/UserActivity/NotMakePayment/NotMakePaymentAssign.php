<?php

namespace App\Models\UserActivity\NotMakePayment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotMakePaymentAssign extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_makepayment_assign_id';

    protected $fillable = ['makepayment_trn_id', 'crm_user_id', 'city_id', 'user_id','status', 'followup_date', 'updatestatus', 'remarks'];

    protected $table = 'user_makepayment_assign';
}
