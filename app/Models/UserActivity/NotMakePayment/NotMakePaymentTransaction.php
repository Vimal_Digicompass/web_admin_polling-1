<?php

namespace App\Models\UserActivity\NotMakePayment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotMakePaymentTransaction extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_makepayment_trn_id';

    protected $fillable = ['city_id', 'user_id'];

    protected $table = 'user_makepayment_assign_trn';
}
