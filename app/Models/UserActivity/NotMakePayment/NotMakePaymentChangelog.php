<?php

namespace App\Models\UserActivity\NotMakePayment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotMakePaymentChangelog extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_makepayment_changelog_id';

    protected $fillable = ['makepayment_trn_id', 'fieldname', 'oldvalue', 'newvalue', 'done'];

    protected $table = 'user_makepayment_changelog';
}
