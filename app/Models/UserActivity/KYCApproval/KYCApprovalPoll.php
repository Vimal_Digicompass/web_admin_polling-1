<?php

namespace App\Models\UserActivity\KYCApproval;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KYCApprovalPoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $primaryKey  = 'kyc_approval_poll_id';

    protected $fillable = ['city_id', 'user_id'];

    protected $table = 'kycapproval_poll';
}
