<?php

namespace App\Models\UserActivity\KYCApproval;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class KYCApprovalAssign extends Model
{
    use  HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $fillable = ['crm_user_id', 'user_id', 'city_id', 'followup_date', 'remarks', 'status'];

    protected $primaryKey  = 'kyc_approval_assign_id';

    protected $table = 'kycapproval_assign';

  
}
