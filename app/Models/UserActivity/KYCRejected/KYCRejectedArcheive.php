<?php

namespace App\Models\UserActivity\KYCRejected;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class KYCRejectedArcheive extends Model
{
    use HasFactory,SoftDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'kyc_rej_arch_id';

    protected $fillable = ['city_id','user_id','status'];

    protected $table = 'kycrejected_archieve';
}
