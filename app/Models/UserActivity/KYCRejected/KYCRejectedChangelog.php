<?php

namespace App\Models\UserActivity\KYCRejected;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class KYCRejectedChangelog extends Model
{
    use HasFactory,softDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'kyc_rej_changelog_id';

    protected $table = 'kycrejected_changelog';

}
