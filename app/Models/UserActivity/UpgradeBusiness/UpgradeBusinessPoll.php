<?php

namespace App\Models\UserActivity\UpgradeBusiness;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UpgradeBusinessPoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $primaryKey  = 'upgradebusiness_poll_id';

    protected $fillable = ['city_id', 'user_id'];

    protected $table = 'upgradebusiness_poll';
}
