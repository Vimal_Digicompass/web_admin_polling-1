<?php

namespace App\Models\UserActivity\WalletRechargeFailed;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletRechargeFailedChangelog extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_wpfailed_changelog_id';

    protected $fillable = ['wpfailed_trn_id', 'fieldname', 'oldvalue', 'newvalue', 'done'];

    protected $table = 'user_wpfailed_changelog';
}
