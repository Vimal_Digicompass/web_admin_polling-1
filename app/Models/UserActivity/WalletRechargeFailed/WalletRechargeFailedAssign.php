<?php

namespace App\Models\UserActivity\WalletRechargeFailed;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletRechargeFailedAssign extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_wpfailed_assign_id';

    protected $fillable = ['wpfailed_trn_id', 'crm_user_id', 'city_id', 'user_id', 'package_id','status', 'followup_date', 'updatestatus', 'remarks'];

    protected $table = 'user_wpfailed_assign';
}
