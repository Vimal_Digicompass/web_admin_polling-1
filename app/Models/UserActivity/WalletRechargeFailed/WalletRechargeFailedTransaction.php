<?php

namespace App\Models\UserActivity\WalletRechargeFailed;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletRechargeFailedTransaction extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_wpfailed_trn_id';

    protected $fillable = ['city_id', 'user_id', 'package_id'];

    protected $table = 'user_wpfailed_assign_trn';
}
