<?php

namespace App\Models\UserActivity\WalletRechargeFailed;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletRechargeFailedPoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $primaryKey  = 'user_wpfailed_poll_id';

    protected $fillable = ['city_id', 'user_id','package_id' , 'wpfailed_trn_id'];

    protected $table = 'user_wpfailed_poll';
}
