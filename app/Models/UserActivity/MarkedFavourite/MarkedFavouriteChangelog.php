<?php

    namespace App\Models\UserActivity\MarkedFavourite;

    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;


    class MarkedFavouriteChangelog extends Model
    {
        use HasFactory, softDeletes;

        protected $connection = 'mysql';

        protected $dates = ['deleted_at'];

        protected $primaryKey  = 'fav_changelog_id';

        protected $table = 'favourites_changelog';
    }
