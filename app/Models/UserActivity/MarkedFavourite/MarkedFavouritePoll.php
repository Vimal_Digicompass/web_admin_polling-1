<?php

namespace App\Models\UserActivity\MarkedFavourite;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MarkedFavouritePoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $primaryKey  = 'fav_poll_id';

    protected $fillable = ['city_id', 'user_id','post_id','post_type'];

    protected $table = 'favourites_poll';
}
