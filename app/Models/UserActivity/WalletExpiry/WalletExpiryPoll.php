<?php

namespace App\Models\UserActivity\WalletExpiry;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletExpiryPoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $primaryKey  = 'user_cpexp_poll_id';

    protected $fillable = ['city_id', 'user_id' , 'cpexp_trn_id','package_id'];

    protected $table = 'user_cpexp_poll';
}
