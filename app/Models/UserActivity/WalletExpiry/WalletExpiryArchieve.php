<?php

namespace App\Models\UserActivity\WalletExpiry;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletExpiryArchieve extends Model
{

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_cpexp_arch_id';

    protected $fillable = ['city_id', 'user_id' ,'status', 'cpexp_trn_id','package_id'];

    protected $table = 'user_cpexp_archieve';
}
