<?php

namespace App\Models\UserActivity\WalletExpiry;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletExpiryTransaction extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_cpexp_trn_id';

    protected $fillable = ['city_id', 'user_id','package_id'];

    protected $table = 'user_cpexp_assign_trn';
}
