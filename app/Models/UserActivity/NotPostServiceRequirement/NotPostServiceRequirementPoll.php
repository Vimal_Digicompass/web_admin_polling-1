<?php

namespace App\Models\UserActivity\NotPostServiceRequirement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotPostServiceRequirementPoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';


    protected $primaryKey  = 'user_notpost_req_poll_id';

    protected $fillable = ['city_id', 'user_id', 'user_notpost_req_assign_trn_id', 'user_notenquire_post_assign_trn_id'];

    protected $table = 'user_notpost_req_poll';
}
