<?php

namespace App\Models\UserActivity\NotPostServiceRequirement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotPostServiceRequirementChangelog extends Model
{
    use HasFactory, SoftDeletes;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_notpost_req_changelog_id';

    protected $fillable = ['user_notpost_req_assign_trn_id', 'fieldname', 'oldvalue', 'newvalue', 'done'];

    protected $table = 'user_notpost_req_changelog';
}
