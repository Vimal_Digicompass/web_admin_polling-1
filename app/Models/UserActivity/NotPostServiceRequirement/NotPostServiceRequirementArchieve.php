<?php

namespace App\Models\UserActivity\NotPostServiceRequirement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotPostServiceRequirementArchieve extends Model
{
    use HasFactory;


    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_notpost_req_arch_id';

    protected $fillable = ['city_id', 'user_id', 'status', 'user_notpost_req_assign_trn_id'];

    protected $table = 'user_notpost_req_archieve';
}
