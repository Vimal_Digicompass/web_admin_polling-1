<?php

namespace App\Models\UserActivity\PostRejected;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostRejectedChangelog extends Model
{
    use HasFactory, softDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'post_rej_changelog_id';

    protected $table = 'post_rejected_changelog';
}
