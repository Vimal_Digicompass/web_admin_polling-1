<?php
namespace App\Models\UserActivity\PostRejected;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PostRejectedArcheive extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'post_rej_arch_id';

    protected $fillable = ['city_id', 'post_id','user_id' ,'post_type', 'status'];

    protected $table = 'postrejected_archieve';
}
