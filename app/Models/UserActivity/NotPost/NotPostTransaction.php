<?php

namespace App\Models\UserActivity\NotPost;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotPostTransaction extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_notpost_assign_trn_id';

    protected $fillable = ['city_id', 'user_id'];

    protected $table = 'user_notpost_assign_trn';
}
