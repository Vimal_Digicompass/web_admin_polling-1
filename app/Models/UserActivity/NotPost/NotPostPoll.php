<?php

namespace App\Models\UserActivity\NotPost;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotPostPoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $primaryKey  = 'user_notpost_poll_id';

    protected $fillable = ['city_id', 'user_id', 'user_notpost_assign_trn_id'];

    protected $table = 'user_notpost_poll';
}
