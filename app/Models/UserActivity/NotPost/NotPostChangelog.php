<?php

namespace App\Models\UserActivity\NotPost;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotPostChangelog extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_notpost_changelog_id';

    protected $fillable = ['user_notpost_assign_trn_id', 'fieldname', 'oldvalue', 'newvalue', 'done'];

    protected $table = 'user_notpost_changelog';
}
