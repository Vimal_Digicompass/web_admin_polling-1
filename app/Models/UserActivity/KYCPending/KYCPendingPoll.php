<?php

namespace App\Models\UserActivity\KYCPending;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KYCPendingPoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $primaryKey  = 'kyc_pending_poll_id';

    protected $fillable = ['city_id', 'user_id'];

    protected $table = 'kycpending_poll';
}
