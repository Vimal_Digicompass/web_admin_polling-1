<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReassignToPoll extends Model
{
    use HasFactory;

    protected $fillable = ['id','sendtopollby','crm_user_id','created_at','updated_at',
            'criteria_id','teamlead_id','data_id','city_id'];

    protected $table = 'reassign_to_poll';

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];
}
