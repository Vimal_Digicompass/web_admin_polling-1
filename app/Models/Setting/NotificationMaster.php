<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationMaster extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $guarded = ['notification_id '];

    protected $fillable = [
        'criteria_id', 'notification_id', 'crm_user_id', 'assign_id', 'is_read', 'description'
    ];

    protected $table = 'notification_master';

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];
    public function criteriapath()
    {
        return $this->hasMany('App\Models\User\Permission', 'permission_id', 'criteria_id');
    }
}
