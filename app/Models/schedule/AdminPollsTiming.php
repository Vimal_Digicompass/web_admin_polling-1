<?php

namespace App\Models\Schedule;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Models\Master\City;
use App\Models\Setting\CriteriaMasters;

class AdminPollsTiming extends Model
{
    use HasFactory,softDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $guarded = ['poll_timing_id'];

    protected $table = 'polling_timing';

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public function city(): HasOne
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function criteriamaster(): HasOne
    {
        return $this->hasOne(CriteriaMasters::class, 'criteria_id', 'criteria_id');
    }


    public function logs(): HasMany
    {
        return $this->hasMany(AdminPollsTimingLog::class, 'poll_timing_id', 'poll_timing_id');
    }

    public static function getNextId(): int
    {
        $lastId = (self::query()
            ->orderBy('poll_timing_id', 'DESC')
            ->first())->poll_timing_id;
        if (isset($lastId))
            return $lastId + 1;
        else
            return 1;
    }
}
