<?php

namespace App\Models\Schedule;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class AdminPollsTimingLog extends Model
{
    use HasFactory, softDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];
    
    protected $primaryKey = 'polling_timing_log_id';

    protected $guarded = ['polling_timing_log_id'];

    protected $table = 'polling_timing_log';

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];
}
