<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use App\Traits\ServiceAttributeTrait;

class Service extends Model
{
    use ServiceAttributeTrait;

    protected $table = 'services';
    protected $connection = 'mysql_1';
    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'title',
        'service_type_id',
        'sub_category_id',
        'payment_mode_id',
        'city_id',
        'min_price',
        'max_price',
        'short_description',
        'benefits',
        'warranty',
        'offers',
        'delivery',
        'sortorder',
        'updated_at',
        'is_service_approved',
        'reject_message',
        'is_send_for_approve_request',
        'is_published',
        'main_image_name',
        'published_date',
        'crm_remark',
        'crm_outbound_call_status',
        'crm_inbound_call_status',
        'is_crm_inbound',
        'assigned_to',
        'approved_by',
        'hours'

    ];

    protected $attributes = [];

    protected $hidden = [];

    protected $casts = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];



    public function user(): belongsTo
    {
        return $this->belongsTo('App\Models\SqFt\User', 'user_id');
    }

    public function serviceCategoryType(): belongsTo
    {
        return $this->belongsTo('App\Models\Service\ServiceCategories', 'service_type_id');
    }

    public function serviceSubCategory(): belongsTo
    {
        return $this->belongsTo('App\Models\Service\ServiceSubCategories', 'sub_category_id');
    }

    public function getServiceTypeAttribute()
    {
        $serviceType = Config::get('constants.service-type');
        return isset($serviceType[$this->service_type_id]) ? $serviceType[$this->service_type_id] : '';
    }

    public function serviceCity(): belongsTo
    {
        return $this->belongsTo('App\Models\Master\City', 'city_id');
    }

    public function serviceAreas(): hasMany
    {
        return $this->hasMany('App\Models\Service\ServiceArea', 'service_id');
    }

    public function getServiceApproveStatusAttribute()
    {
        $status = Config::get('constants.service-approve-status');
        return isset($status[$this->is_service_approved]) ? $status[$this->is_service_approved] : '';
    }
}
