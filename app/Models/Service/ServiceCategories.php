<?php


namespace App\Models\Service;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ServiceAttributeTrait;

class ServiceCategories extends Model
{
    use ServiceAttributeTrait;

    protected $table = 'service_categories_master';
    public $timestamps = false;
    protected $connection = 'mysql_1';

    protected $fillable = [
        'title',
        'sortorder',
        'is_active'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'service_categories_master';
}
