<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;

class ServiceArea extends Model
{

    protected $table = 'service_areas';
    public $timestamps = false;
    protected $connection = 'mysql_1';

    protected $fillable = [
        'area_id',
        'service_id'

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];


}
