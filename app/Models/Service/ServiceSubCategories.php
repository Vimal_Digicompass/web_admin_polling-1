<?php


namespace App\Models\Service;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ServiceAttributeTrait;


class ServiceSubCategories extends Model
{
    use ServiceAttributeTrait;

    protected $table = 'service_subcategories_master';
    public $timestamps = false;
    protected $connection = 'mysql_1';

    protected $fillable = [
        'title',
        'sortorder',
        'is_active',
        'service_category_id'
    ];

    protected $attributes = [];

    protected $hidden = [];

    protected $casts = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'service_subcategories_master';


    public function category(): belongsTo
    {
        return $this->belongsTo('App\Containers\Service\Models\ServiceCategories', 'category_id');
    }

    public function subCategories(): HasMany
    {
        return $this->hasMany('App\Containers\Service\Models\ServiceSubCategories', 'sub_category_id');
    }
}
