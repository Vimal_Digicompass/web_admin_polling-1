<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Jenssegers\Mongodb\Eloquent\Model;
use App\Models\User;

class Searches extends Model
{

    protected $connection = 'mongodb';

    protected $collection = 'searches';
    
    public $timestamps = true;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'post_user_id', 'user_id');
    }
}
