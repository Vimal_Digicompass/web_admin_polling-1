<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Jenssegers\Mongodb\Eloquent\Model;
use App\Models\User;

class UserLogServiceProvider extends Model
{

    protected $connection = 'mongodb';

    protected $collection = 'user_logs_service_provider';
    public $timestamps = true;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
