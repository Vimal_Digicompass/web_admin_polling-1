<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'user_id';

    protected $fillable = [
        'user_id',
        'first_name', 'last_name', 'username', 'email', 'state_id',
        'mobilenumber', 'password', 'role_id', 'team_lead_id',
        'city_id', 'assign_limit', 'is_active', 'is_online',
        'image_name', 'is_follow_up', 'is_auto_password', 'role_type',
        'remember_token', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    public function roles()
    {
        return $this->hasMany('App\Models\User\Roles', 'role_id', 'role_id');
    }
}
