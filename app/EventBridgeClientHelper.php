<?php

declare(strict_types=1);

namespace App;

use Aws\EventBridge\EventBridgeClient;
use AWS\Elasticsearch\ClientBuilder;
use Illuminate\Support\Collection;

class EventBridgeClientHelper
{
    private EventBridgeClient $client;
    private array $eventRulesArray = [];
    private Collection $eventRulesCollection;

    public function __construct()
    {
        $this->client = makeAWSClient("eventBridge");
        $this->getRules();
    }

    private function getRules(string $nextToken = null)
    {
        //set limit for rules
        $attributes = [
            'Limit' => 20,
        ];

        //if recursive call add next token from previous call
        if (isset($nextToken))
            $attributes['NextToken'] = $nextToken;

        //get rules from eventbridge
        $result = $this->client->listRules($attributes);

        //compile rules from previous calls
        if (count($result['Rules']) > 0) {
            $this->eventRulesArray = array_merge($this->eventRulesArray, $result['Rules']);
        }

        //make recursive call to get remaining rules
        if (isset($result['NextToken']))
            $this->getRules($result['NextToken']);

        //convert array to laravel collection for easier manipulations
        $this->eventRulesCollection = collect($this->eventRulesArray);

        return;
    }

    public function getRuleNames(): array
    {
        return $this->eventRulesCollection->pluck('Name')->all();
    }
}
