<?php

namespace App\Http\Controllers\Payment;

use App\Models\UserActivity\NotMakePayment\NotMakePaymentAssign;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use App\Models\Master\CallStatus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\State;
use App\Models\LeadRequest;
use App\Models\UserWalletTransaction;
use App\Models\Master\City;
use MongoDB\BSON\UTCDateTime;
use App\Exports\NotMakePaymentExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Master\UserType;
use App\User as CRMUser;
use App\Models\SqFt\User;
use Yajra\DataTables\DataTables;

class PaymentPendingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $scenario = "Payment Pending";
        $content = "Users who not making payment but having no of leads for the posts";
        $role_id = Auth::user()->role_id;

        $criteriaId = Config::get('constants.CRITERION.PAYMENT_PENDING');
        $commonFunctionData = commonFunctionData($role_id, $criteriaId);
        $state = $commonFunctionData['state'];
        $notificationData = $commonFunctionData['notificationData'];
        $notificationDataNew = $commonFunctionData['notificationDataNew'];
        $userType = $commonFunctionData['userType'];
        $callStatus = $commonFunctionData['callStatus'];
        $KYCStatus = $commonFunctionData['KYCStatus'];
        $KYCRejectedStatus = $commonFunctionData['KYCRejectedStatus'];
        $KYCDocumentList = $commonFunctionData['KYCDocumentList'];
        $UserCategory = $commonFunctionData['UserCategory'];
        $PermissionDetail = $commonFunctionData['PermissionDetail'];
        $checkpermission = $commonFunctionData['checkpermission'];
        if ($checkpermission->count() == 0) {
            return view('error.500');
        }
        return view('Scenario/Payment/paymentpending', compact(
            'scenario',
            'criteriaId',
            'content',
            'state',
            'id',
            'UserCategory',
            'KYCDocumentList',
            'userType',
            'callStatus',
            'KYCStatus',
            'KYCRejectedStatus',
            'criteriaId',
            'PermissionDetail',
            'notificationData',
            'notificationDataNew'
        ));
    }

    public function getnotmakepaymentlist(Request $request)
    {

        $id = $request->input('id');
        $roleData = CRMUser::where('user_id', $id)->first();
        $role_id = $roleData['role_id'];
        $getCustomerData = $this->getNotMakePaymentData($request);
        return DataTables::of($getCustomerData)
            ->addColumn('action', function ($getCustomerData) use ($role_id) {
                $criteriaId = Config::get('constants.CRITERION.PAYMENT_PENDING');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_kycupdate = $PermissionDetail[0]['is_kycupdate'];
                if ($is_kycupdate == 0) {
                    return '<td><button id=' . $getCustomerData->trn_id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                    <button type="button" id=' . $getCustomerData->trn_id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools detailviewdata btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                } else {
                    return '<td><button id=' . $getCustomerData->trn_id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                    <button id=' . $getCustomerData->trn_id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Upload Document" class="tools uploaddocument btn btn-danger btn-icon submit"> <i data-feather="upload"></i></button>
                    <button type="button" id=' . $getCustomerData->trn_id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools detailviewdata btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                }
            })

            ->addColumn('name', function ($getCustomerData) {
                return  $getCustomerData->firstname . ' ' . $getCustomerData->lastname;
            })->addColumn('usertype', function ($getCustomerData) {
                $usertype = UserType::where('titlevalue', $getCustomerData->user_type)->first();
                if ($usertype) {
                    return $usertype['title'];
                } else {
                    return "";
                }
            })->addColumn('city', function ($getCustomerData) {
                $city = City::where('id', $getCustomerData->city_id)->first();
                if ($city) {
                    return $city['title'];
                } else {
                    return "";
                }
            })->addColumn('totalnoofleads', function ($getCustomerData) {

                $getLeadcount = LeadRequest::where('post_user_id', $getCustomerData->id)
                    ->where('is_viewed', false)
                    ->count();
                return $getLeadcount;
            })->addColumn('state', function ($getCustomerData) {
                $state = City::where('id', $getCustomerData->city_id)->first();
                if ($state) {
                    $stateid = $state['state_id'];
                    $statetitle = State::where('id', $stateid)->first();
                    if ($statetitle) {
                        return $statetitle['title'];
                    } else {
                        return "";
                    }
                } else {
                    return "";
                }
            })
            ->addColumn('remarks', function ($getCustomerData) {
                $getRemarks = NotMakePaymentAssign::select('remarks')
                    ->where('makepayment_trn_id', $getCustomerData->trn_id)
                    ->first();
                if ($getRemarks) {
                    return $getRemarks['remarks'];
                } else {
                    return '';
                }
            })->addColumn('crmusername', function ($getCustomerData) {

                $getcrmData = NotMakePaymentAssign::select('crm_user_id')
                    ->where('makepayment_trn_id', $getCustomerData->trn_id)
                    ->first();
                $crm_user_id =  $getcrmData['crm_user_id'];
                $crmData = CRMUser::where('user_id', $crm_user_id)->first();
                return $crmData['username'];
            })
            ->addColumn('followup_date', function ($getCustomerData) {
                $getFollowdate = NotMakePaymentAssign::select('followup_date')
                    ->where('makepayment_trn_id', $getCustomerData->trn_id)
                    ->first();
                if ($getFollowdate['followup_date'] != NULL) {
                    return Carbon::createFromFormat('Y-m-d', $getFollowdate['followup_date'])
                        ->setTimezone('Asia/Calcutta')
                        ->format('d-m-Y');
                } else {
                    return '';
                }
            })->addColumn('mobile_number', function ($getCustomerData)  use ($role_id) {
                $criteriaId = Config::get('constants.CRITERION.PAYMENT_PENDING');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_mobilenumberread = $PermissionDetail[0]['is_mobilenumberread'];
                if ($is_mobilenumberread == 0) {
                    return "-";
                } else {
                    return $getCustomerData->mobile_number;
                }
            })->addColumn('checkbox', function ($getCustomerData) {
                return "";
            })->addColumn('status_id', function ($getCustomerData) {
                $getStatus = NotMakePaymentAssign::select('status')
                    ->where('makepayment_trn_id', $getCustomerData->trn_id)
                    ->first();
                return $getStatus['status'];
            })->addColumn('status', function ($getCustomerData) {
                $getStatus = NotMakePaymentAssign::select('status')
                    ->where('makepayment_trn_id', $getCustomerData->trn_id)
                    ->first();
                return getCallStatus($getStatus);
            })
            ->rawColumns(['action', 'active', 'name', 'remarks', 'status'])
            ->make(true);
    }

    public function getNotMakePaymentData($request)
    {

        $id = $request->input('id');
        $roleData = CRMUser::where('user_id', $id)->first();
        if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.TEAM_LEAD')) {
            $getTeamInfo = CRMUser::select('user_id')->where('team_lead_id', $id)->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
            $getTeamInfo = CRMUser::select('user_id')->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else {
            $user_id = [$id];
        }
        $callstatus = $request->input('searchcallstatus');
        $usertype = $request->input('usertype');
        $city = $request->input('city');

        $queryPendingAssign = NotMakePaymentAssign::query();
        if ($callstatus != Config::get('constants.CALL_STATUS.ALL')) {

            $queryPendingAssign = $queryPendingAssign->where('status', $callstatus);
        } else {
            if (
                Auth::user()->role_type != Config::get('constants.ROLE_TYPE.TEAM_LEAD') ||
                Auth::user()->role_type != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')
            ) {
                $queryPendingAssign = $queryPendingAssign->where('status', '!=', Config::get('constants.CALL_STATUS.CLOSED')); //closed
            }
        }
        if ($city == "") {
            $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id);
        } else {
            $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id)->where('city_id', $city);
        }

        $queryPendingAssign = $queryPendingAssign->where('updatestatus', '!=', 2);
        $queryPendingAssign = $queryPendingAssign->orderBy('created_at', 'DESC')->get();
        // return $queryPendingAssign;
        $Arr = [];
        foreach ($queryPendingAssign as $key => $value) {
            $query = User::query();
            if ($usertype == "") {
                $getusertype = UserType::pluck('titlevalue');
                $query  =  $query->whereIn('user_type', $getusertype);
            } else {
                $query  =  $query->where('user_type', $usertype);
            }
            if ($city == "") {
                $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id);
            } else {
                $query =  $query->where('city_id', $city);
                $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id)->where('city_id', $city);
            }
            $user_id = $value['user_id'];
            $trn_id = $value['makepayment_trn_id'];
            $package_id = $value['package_id'];

            $query =  $query->where('id', $user_id)->first();
            if ($query) {
                $query->trn_id = $trn_id;
                $query->package_id = $package_id;
                $userWalletTransactionCount = UserWalletTransaction::where('user_id', $user_id)
                    ->where('transaction_time_as_date', '>=', new UTCDateTime(Carbon::now()->subHours((31 * 24))))
                    ->where('transaction_time_as_date', '<=', new UTCDateTime(Carbon::now()))
                    ->where('is_success', true)
                    ->count();
                if ($userWalletTransactionCount == 0) {
                    $query->hideout = 0;
                } else {
                    $update = NotMakePaymentAssign::where('makepayment_trn_id', $trn_id)
                        ->where('user_id', $user_id)
                        ->first();
                    $updatestatus = $update['updatestatus'];
                    if ($updatestatus == 0) {
                        $query->hideout = 1;
                        NotMakePaymentAssign::where('makepayment_trn_id', $trn_id)
                        ->where('user_id', $user_id)->update([
                            'updatestatus' => 3
                        ]);
                    } else {
                        if ($updatestatus == 3) {
                            $query->hideout = 1;
                        } else {
                            $query->hideout = 0;
                        }
                    }
                }
                array_push($Arr, $query);
            }
        }
        $Arr = collect($Arr);
        return $Arr;
    }

    public function notmakepaymentexport(Request $request)
    {
        unlinkfiles('public/reports/');
        $status = $request->input('searchcallstatus');
        $getCallStatus = CallStatus::where('id', $status)->first();
        $strReplace = str_replace(" ", "_", $getCallStatus['title']);
        $fileNamewithDate = $strReplace . '_' . date('d_m_Y') . '.xlsx';
        $url = 'public/reports/' . $fileNamewithDate;
        $urlresponse = "reports/" . $fileNamewithDate;
        $getCustomerData = $this->getNotMakePaymentData($request);
        $getCustomerData = array_filter($getCustomerData->toArray(), function ($a) {
            return $a['hideout'] !== 1;
        });
        if (count($getCustomerData) == 0) {
            return 0;
        } else {
            $text = "Not Make Payment";
            Excel::store(new NotMakePaymentExport($getCustomerData, $request, $text), '/' . $url, 'local');
            return '/' . $urlresponse;
        }
    }
}
