<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;


use Illuminate\Support\Facades\Config;
use App\Models\UserActivity\PostRejected\PostRejectedAssign;


use Illuminate\Support\Facades\Auth;


class RejectedPostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index($id)
    {
        $scenario = "Rejected";
        $criteriaId = "";
        $content = "User uploaded post has been rejected by admin";
        $role_id = Auth::user()->role_id;
        $criteriaId = Config::get('constants.CRITERION.POST_REJECTED');
        $commonFunctionData = commonFunctionData($role_id, $criteriaId);
        $state = $commonFunctionData['state'];
        $userType = $commonFunctionData['userType'];
        $notificationData = $commonFunctionData['notificationData'];
        $notificationDataNew = $commonFunctionData['notificationDataNew'];
        $callStatus = $commonFunctionData['callStatus'];
        $PostRejectedStatus = $commonFunctionData['PostRejectedStatus'];
        $KYCStatus = $commonFunctionData['KYCStatus'];
        $ServiceCategory = $commonFunctionData['ServiceCategory'];
        $PostType = $commonFunctionData['PostType'];
        $KYCRejectedStatus = $commonFunctionData['KYCRejectedStatus'];
        $KYCDocumentList = $commonFunctionData['KYCDocumentList'];
        $UserCategory = $commonFunctionData['UserCategory'];
        $ProjectCategory = $commonFunctionData['ProjectCategory'];
        $PropertyType = $commonFunctionData['PropertyType'];
        $PermissionDetail = $commonFunctionData['PermissionDetail'];
        $checkpermission = $commonFunctionData['checkpermission'];

        $propertycount = PostRejectedAssign::where('post_type', 'P')
            ->where('crm_user_id', $id)
            ->where('updatestatus', '!=', 2)
            ->count();
        $propertywaitingcount = PostRejectedAssign::where('post_type', 'P')
            ->where('crm_user_id', $id)
            ->where('updatestatus', '=', 0)
            ->count();
        $propertyprocessedcount = PostRejectedAssign::where('post_type', 'P')
            ->where('crm_user_id', $id)
            ->where('updatestatus', '=', 1)
            ->count();
        $servicecount = PostRejectedAssign::where('post_type', 'S')
            ->where('updatestatus', '!=', 2)
            ->where('crm_user_id', $id)
            ->count();
        $serviceprocessedcount = PostRejectedAssign::where('post_type', 'S')
            ->where('updatestatus', '=', 1)
            ->where('crm_user_id', $id)
            ->count();
        $servicewaitingcount = PostRejectedAssign::where('post_type', 'S')
            ->where('updatestatus', '=', 0)
            ->where('crm_user_id', $id)
            ->count();
        $projectcount = PostRejectedAssign::where('post_type', 'PR')
            ->where('updatestatus', '!=', 2)
            ->where('crm_user_id', $id)
            ->count();
        $projectwaitingcount = PostRejectedAssign::where('post_type', 'PR')
            ->where('updatestatus', '=', 0)
            ->where('crm_user_id', $id)
            ->count();
        $projectprocessedcount = PostRejectedAssign::where('post_type', 'PR')
            ->where('updatestatus', '=', 1)
            ->where('crm_user_id', $id)
            ->count();
        if ($checkpermission->count() == 0) {
            return view('error.500');
        }
        return view('Scenario/Post/post', compact(
            'scenario',
            'content',
            'servicecount',
            'propertycount',
            'projectcount',
            'projectwaitingcount',
            'propertywaitingcount',
            'servicewaitingcount',
            'projectprocessedcount',
            'propertyprocessedcount',
            'serviceprocessedcount',
            'state',
            'id',
            'PostType',
            'UserCategory',
            'KYCDocumentList',
            'userType',
            'callStatus',
            'KYCStatus',
            'KYCRejectedStatus',
            'criteriaId',
            'PermissionDetail',
            'ServiceCategory',
            'ProjectCategory',
            'PropertyType',
            'PostRejectedStatus',
            'notificationData',
            'notificationDataNew'
        ));
    }


    public function editunitandflat()
    {
        $scenario = "Units";
        return view('Scenario/Post/editunitandflatpost', compact('scenario'));
    }
}
