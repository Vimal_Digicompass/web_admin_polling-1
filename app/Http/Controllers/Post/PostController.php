<?php

namespace App\Http\Controllers\Post;

use App\libraries\AWSElasticSearch;
use App\Models\SqFt\PropertyIdealForBusiness;
use App\Models\SqFt\PropertyTenant;
use App\Models\Master\UserType;
use App\Models\Master\City;
use App\Models\Master\CallStatus;
use Illuminate\Support\Arr;
use App\Models\Master\PostRejectedStatus;
use App\Models\Master\KYCRejectedStatus;
use App\Models\Master\State;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Master\PropertyCategory;
use App\User as CRMUser;
use App\Models\SqFt\User;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Models\SqFt\Project;
use App\Models\Service\ServiceArea;
use App\Models\SqFt\Property;
use App\Models\Master\ServiceCategory;
use App\Models\Master\ProjectCategory;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\BHKMaster;
use App\Models\Master\IdealBusniess;
use App\Models\Master\FloorMaster;
use App\Models\Master\TransactionType;
use App\Models\Master\FlatIdealBusniess;
use Carbon\Carbon;
use App\Exports\PostExport;
use App\Models\SqFt\Service;
use App\Models\SqFt\ProjectImages;
use App\Models\SqFt\ProjectAmenities;
use App\Models\SqFt\PropertyImages;
use App\Models\SqFt\ServiceImages;
use App\Models\SqFt\ProjectFlats;
use Illuminate\Support\Facades\Config;
use App\Models\UserActivity\PostApproval\PostApprovalAssign;
use App\Models\UserActivity\PostApproval\PostApprovalArcheive;
use App\Models\UserActivity\PostApproval\PostApprovalChangelog;
use App\Models\UserActivity\PostRejected\PostRejectedAssign;
use App\Models\UserActivity\PostRejected\PostRejectedArcheive;
use App\Models\UserActivity\PostRejected\PostRejectedChangelog;
use Exception;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(
            'editpost',
            'updatepoststatus',
            'editflat'
        );
    }

    public function getpostlist(Request $request)
    {
        $id = $request->input('id');
        $roleData = CRMUser::where('user_id', $id)->first();
        $role_id = $roleData['role_id'];
        $PostType = $request->input('posttype');
        $criteriaId = $request->input('criteriaId');
        $getPostData = $this->getPostApprovalData($request);
        // return $getPostData;
        return DataTables::of($getPostData)
            ->addColumn('action', function ($getPostData) use ($role_id, $criteriaId, $PostType) {
                // $criteriaId = Config::get('constants.CRITERION.POST_APPROVAL');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_postupdate = $PermissionDetail[0]['is_postupdate'];
                if ($is_postupdate == 0) {
                    return '<td><button id=' . $getPostData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                            <button type="button" id=' . $getPostData->id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools postdetailview btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                } else {
                    return '<td><button id=' . $getPostData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                            <a target="_blank" href="/post/edit/' . $PostType . '/' . $getPostData->id . '/' . $criteriaId . '" id=' . $getPostData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Edit Post" class="tools editpost btn btn-danger btn-icon submit"> <i style="position:relative;top:6px;" data-feather="edit-2"></i></a>
                            <button type="button" id=' . $getPostData->id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools postdetailview btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                }
            })
            ->addColumn('name', function ($getPostData) {
                return $getPostData['user']->firstname . ' ' . $getPostData['user']->lastname;
            })->addColumn('usertype', function ($getPostData) {
                $usertype = UserType::where('titlevalue', $getPostData['user']->user_type)->first();
                if ($usertype) {
                    return $usertype['title'];
                } else {
                    return "";
                }
            })->addColumn('checkbox', function ($getCustomerData) {

                return "";
            })->addColumn('mobile_number', function ($getCustomerData) use ($role_id, $criteriaId) {
                // $criteriaId = Config::get('constants.CRITERION.POST_APPROVAL');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_mobilenumberread = $PermissionDetail[0]['is_mobilenumberread'];
                if ($is_mobilenumberread == 0) {
                    return "-";
                } else {
                    return $getCustomerData['user']->mobile_number;
                }
            })->addColumn('city', function ($getPostData) {
                $city = City::where('id', $getPostData->city_id)->first();
                if ($city) {
                    return $city['title'];
                } else {
                    return "";
                }
            })->addColumn('state', function ($getPostData) {
                $state = City::where('id', $getPostData->city_id)->first();
                if ($state) {
                    $stateid = $state['state_id'];
                    $statetitle = State::where('id', $stateid)->first();
                    if ($statetitle) {
                        return $statetitle['title'];
                    } else {
                        return "";
                    }
                } else {
                    return "";
                }
            })
            ->addColumn('remarks', function ($getPostData)  use ($PostType, $criteriaId) {
                if ($criteriaId == Config::get('constants.CRITERION.POST_APPROVAL')) {
                    $getRemarks = PostApprovalAssign::query();
                } else {
                    $getRemarks = PostRejectedAssign::query();
                }
                $getRemarks = $getRemarks->select('remarks')
                    ->where('post_id', $getPostData->id)
                    ->where('post_type', $PostType)
                    ->first();
                if ($getRemarks) {
                    return $getRemarks['remarks'];
                } else {
                    return '';
                }
            })
            ->addColumn('followup_date', function ($getPostData) use ($PostType, $criteriaId) {
                if ($criteriaId == Config::get('constants.CRITERION.POST_APPROVAL')) {
                    $getFollowdate = PostApprovalAssign::query();
                } else {
                    $getFollowdate = PostRejectedAssign::query();
                }
                $getFollowdate = $getFollowdate->select('followup_date')
                    ->where('post_id', $getPostData->id)
                    ->where('post_type', $PostType)
                    ->first();
                if ($getFollowdate['followup_date'] != NULL) {
                    return Carbon::createFromFormat('Y-m-d', $getFollowdate['followup_date'])
                        ->setTimezone('Asia/Calcutta')
                        ->format('d-m-Y');
                } else {
                    return '';
                }
            })->addColumn('status_id', function ($getPostData) use ($PostType, $criteriaId) {
                if ($criteriaId == Config::get('constants.CRITERION.POST_APPROVAL')) {
                    $getStatus  = PostApprovalAssign::query();
                } else {
                    $getStatus  = PostRejectedAssign::query();
                }
                $getStatus = $getStatus->select('status')
                    ->where('post_id', $getPostData->id)
                    ->where('post_type', $PostType)
                    ->first();

                return $getStatus['status'];
            })->addColumn('status', function ($getPostData) use ($PostType, $criteriaId) {
                if ($criteriaId == Config::get('constants.CRITERION.POST_APPROVAL')) {
                    $getStatus  = PostApprovalAssign::query();
                } else {
                    $getStatus  = PostRejectedAssign::query();
                }
                $getStatus = $getStatus->select('status')
                    ->where('post_id', $getPostData->id)
                    ->where('post_type', $PostType)
                    ->first();
                return getCallStatus($getStatus);
            })->addColumn('crmusername', function ($getPostData) use ($PostType, $criteriaId) {
                if ($criteriaId == Config::get('constants.CRITERION.POST_APPROVAL')) {
                    $getcrmData  = PostApprovalAssign::query();
                } else {
                    $getcrmData  = PostRejectedAssign::query();
                }
                $getcrmData = $getcrmData->select('crm_user_id')
                    ->where('post_id', $getPostData->id)
                    ->where('post_type', $PostType)
                    ->first();
                $crm_user_id =  $getcrmData['crm_user_id'];
                $crmData = CRMUser::where('user_id', $crm_user_id)->first();
                return $crmData['username'];
            })
            ->addColumn('post_category', function ($getPostData) use ($PostType) {
                if ($PostType == "PR") {
                    $category = ProjectCategory::where('id', $getPostData->category_id)->first();
                } else if ($PostType == "P") {
                    $category = PropertyCategory::where('id', $getPostData->property_category_id)->first();
                } else {
                    $category = ServiceCategory::where('id', $getPostData->service_type_id)->first();
                }
                if ($category) {
                    return $category['title'];
                } else {
                    return "";
                }
            })->addColumn('post_Date', function ($getPostData) use ($PostType) {
                if ($PostType == "PR") {
                    $postedDate = Project::where('id', $getPostData->id)->first();
                    if ($postedDate) {
                        return Carbon::parse($postedDate['posted_date'])->format('d-m-Y');
                    } else {
                        return "";
                    }
                } else if ($PostType == "P") {
                    $postedDate = Property::where('id', $getPostData->id)->first();
                } else {
                    $postedDate = Service::where('id', $getPostData->id)->first();
                }
                if ($postedDate) {
                    return Carbon::parse($postedDate['created_at'])->format('d-m-Y');
                } else {
                    return "";
                }
            })->addColumn('reject_message', function ($getPostData) use ($PostType) {

                $selectreason = PostRejectedStatus::where('id', $getPostData->reject_message)
                    ->first();
                if ($selectreason) {
                    return $selectreason['reason'];
                } else {
                    return "";
                }
            })->addColumn('posted_date', function ($getPostData) use ($PostType) {
                if ($PostType == "PR") {
                    $postedDate = Project::where('id', $getPostData->id)->first();
                    if ($postedDate) {
                        return Carbon::parse($postedDate['posted_date'])->format('d-m-Y');
                    } else {
                        return "";
                    }
                } else if ($PostType == "P") {
                    $postedDate = Property::where('id', $getPostData->id)->first();
                } else {
                    $postedDate = Service::where('id', $getPostData->id)->first();
                }
                if ($postedDate) {
                    return Carbon::parse($postedDate['created_at'])->format('d-m-Y');
                } else {
                    return "";
                }
            })->addColumn('post_status', function ($getPostData) use ($PostType) {
                if ($PostType == "PR") {
                    $Data = Project::where('id', $getPostData->id)->first();
                    $is_approved = $getPostData['is_project_approved'];
                } else if ($PostType == "P") {
                    $Data = Property::where('id', $getPostData->id)->first();
                    $is_approved = $getPostData['is_property_approved'];
                } else {
                    $Data = Service::where('id', $getPostData->id)->first();
                    $is_approved = $Data['is_service_approved'];
                }
                if ($is_approved == 0) {
                    return '<span class="badge badge-info">Waiting</span>';
                } else if ($is_approved == 1) {
                    return '<span class="badge badge-success">Approved</span>';
                } else if ($is_approved == 2) {
                    return '<span class="badge badge-danger">Rejected</span>';
                } else {
                    return "";
                }
            })->addColumn('publish_status', function ($getPostData) use ($role_id, $criteriaId, $PostType) {

                $is_published = $getPostData->is_published;
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_publishactive = $PermissionDetail[0]['is_publishactive'];
                if ($is_publishactive == 0) {
                    if ($is_published == 1) {
                        return ' <td><span class="badge badge-success">ACTIVE</span></td>';
                    } else if ($is_published == 2) {
                        return ' <td><span class="badge badge-success">SOLD OUT</span></td>';
                    } else if ($is_published == 3) {
                        return ' <td><span class="badge badge-success">RENTED OUT</span></td>';
                    } else if ($is_published == 4) {
                        return ' <td><span class="badge badge-success">LEASED OUT</span></td>';
                    } else if ($is_published == 0) {
                        return ' <td><span class="badge badge-danger">IN-ACTIVE</span></td>';
                    }
                } else {
                    if ($PostType == "P") {
                        if ($is_published == 1) {
                            return ' <td><button type="button" id="' . $getPostData->id . '_' . $is_published . '"  style="width: 100px;" class="btn btn-primary publishstatusinactive">ACTIVE</button></td>';
                        } else if ($is_published == 2) {
                            return ' <td><button type="button" id="' . $getPostData->id . '_' . $is_published . '"  style="width: 100px;" class="btn btn-primary publishstatusinactive">SOLD OUT</button></td>';
                        } else if ($is_published == 3) {
                            return ' <td><button type="button" id="' . $getPostData->id . '_' . $is_published . '"  style="width: 100px;" class="btn btn-primary publishstatusinactive">RENTED OUT</button></td>';
                        } else if ($is_published == 4) {
                            return ' <td><button type="button" id="' . $getPostData->id . '_' . $is_published . '"  style="width: 100px;" class="btn btn-primary publishstatusinactive">LEASED OUT</button></td>';
                        } else if ($is_published == 0) {
                            return ' <td><button type="button" id="' . $getPostData->id . '_' . $is_published . '"  style="width: 100px;" class="btn btn-danger publishstatusinactive">IN ACTIVE</button></td>';
                        }
                    } else {
                        if ($is_published == 1) {
                            return ' <td><button type="button" id="' . $getPostData->id . '_0"  style="width: 100px;" class="btn btn-primary publishstatusinactive">ACTIVE</button></td>';
                        } else {
                            return ' <td><button type="button" id="' . $getPostData->id . '_1"  style="width: 100px;" class="btn btn-danger publishstatusinactive">IN-ACTIVE</button></td>';
                        }
                    }
                }
            })->rawColumns(['action', 'status', 'remarks', 'post_status', 'publish_status'])
            ->make(true);
    }

    public function getPostApprovalData($request)
    {

        $id = $request->input('id');
        $PostType = $request->input('posttype');
        $criteriaId = $request->input('criteriaId');
        $roleData = CRMUser::where('user_id', $id)->first();
        if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.TEAM_LEAD')) {
            $getTeamInfo = CRMUser::select('user_id')->where('team_lead_id', $id)->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
            $getTeamInfo = CRMUser::select('user_id')->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else {
            $user_id = [$id];
        }
        $fromdate =  Carbon::parse($request->input('fromdate') . " 00:00:00");
        $todate =  Carbon::parse($request->input('todate') . " 23:59:59");
        $status = $request->input('status');
        $searchcallstatus = $request->input('searchcallstatus');
        $city = $request->input('city');
        if ($criteriaId == Config::get('constants.CRITERION.POST_APPROVAL')) {
            $queryPendingAssign = PostApprovalAssign::query();
        } else {
            $queryPendingAssign  = PostRejectedAssign::query();
        }

        if ($searchcallstatus != Config::get('constants.CALL_STATUS.ALL')) {

            $queryPendingAssign = $queryPendingAssign->where('status', $searchcallstatus);
        } else {
            if (
                Auth::user()->role_type != Config::get('constants.ROLE_TYPE.TEAM_LEAD') ||
                Auth::user()->role_type != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')
            ) {
                $queryPendingAssign = $queryPendingAssign->where('status', '!=', Config::get('constants.CALL_STATUS.CLOSED')); //closed
            }
        }
        $queryPendingAssign = $queryPendingAssign->where('updatestatus', '!=', 2);
        if ($PostType == "PR") {
            $query = Project::query();
            $query = $query->whereBetween('posted_date', array($fromdate, $todate));
            $ProjectCategory = $request->input('projectcategory');
            if ($ProjectCategory != 0) {
                $query = $query->where('category_id', $ProjectCategory);
            }
            if ($status != 6) {
                $query = $query->where('is_project_approved', $status);
            }
            $query = $query->with('user');
        } else if ($PostType == "P") {
            $query = Property::query();
            $query = $query->whereBetween('created_at', array($fromdate, $todate));
            $PropertyCategory = $request->input('propertycategory');
            if ($status != 6) {
                $query = $query->where('is_property_approved', $status);
            }
            if ($PropertyCategory != "") {
                $query = $query->where('property_category_id', $PropertyCategory);
            }
            $PropertyType = $request->input('propertytype');
            if ($PropertyType != "") {
                $query = $query->where('property_type_id', $PropertyType);
            }
            $UserType = $request->input('usertype');
            if ($UserType != "") {
                $query = $query->whereHas('user', function ($que) use ($UserType) {
                    $que->where('user_type', $UserType);
                });
            }
            $query = $query->with('user');
        } else if ($PostType == "S") {
            $query = Service::query();
            $query = $query->whereBetween('created_at', array($fromdate, $todate));
            $ServiceCategory = $request->input('servicecategory');
            if ($ServiceCategory != "") {
                $query = $query->where('service_type_id', $ServiceCategory);
            }
            if ($status != 6) {
                $query = $query->where('is_service_approved', $status);
            }
            $ServiceSubCategory = $request->input('servicesubcategory');
            if ($ServiceSubCategory != "") {
                $query = $query->where('sub_category_id', $ServiceSubCategory);
            }
            $query = $query->with('user');
        }
        if (
            $city == ""
        ) {
            $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id);
        } else {
            $query = $query->where('city_id', $city);
            $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id)->where('city_id', $city);
        }
        $queryPendingAssign = $queryPendingAssign
            ->where('post_type', $PostType)
            ->orderBy('created_at', 'DESC')
            ->get();
        $pluckPostId = $queryPendingAssign->pluck('post_id');
        $query = $query->whereIn('id', $pluckPostId);
        $query =  $query->orderBy('created_at', 'DESC')->get();
        $Arr = [];
        if ($criteriaId == Config::get('constants.CRITERION.POST_REJECTED')) {
            foreach ($query as $key => $val) {
                if ($PostType == 'P') {
                    $isapproved = $val['is_property_approved'];
                } else if ($PostType == 'PR') {
                    $isapproved = $val['is_project_approved'];
                } else {
                    $isapproved = $val['is_service_approved'];
                }
                if ($isapproved == 2) {
                    $val->hideout = 0;
                } else {
                    $update = PostRejectedAssign::where('post_type', $PostType)->where('post_id', $val['id'])->first();
                    $updatestatus = $update['updatestatus'];
                    if ($updatestatus == 0) {
                        $val->hideout = 1;
                        PostRejectedAssign::where('post_type', $PostType)->where('post_id', $val['id'])->update([
                            'updatestatus' => 3
                        ]);
                    } else {
                        if ($updatestatus == 3) {
                            $val->hideout = 1;
                        } else {
                            $val->hideout = 0;
                        }
                    }
                }
                array_push($Arr, $val);
            }
        } else {
            foreach ($query as $key => $val) {
                $val->hideout = 0;
                array_push($Arr, $val);
            }
        }
        return $Arr;
    }


    public function postexport(Request $request)
    {
        unlinkfiles('public/reports/');
        $PostType = $request->input('posttype');
        if ($PostType == "PR") {
            $text = "post_Project";
        } else if ($PostType == "P") {
            $text = "post_Property";
        } else {
            $text = "post_Service";
        }
        $fileNamewithDate = $text . '_' . date('d_m_Y') . '.xlsx';
        $url = 'public/reports/' . $fileNamewithDate;
        $urlresponse = "reports/" . $fileNamewithDate;
        $getPostData = $this->getPostApprovalData($request);
        $criteriaId = $request->input('criteriaId');
        if ($criteriaId == Config::get('constants.CRITERION.POST_REJECTED')) {
            $getPostData = array_filter($getPostData, function ($a) {
                return $a->hideout !== 1;
            });
        }
        if (count($getPostData) == 0) {
            return 0;
        } else {
            Excel::store(new PostExport($getPostData, $request), '/' . $url, 'local');
            return '/' . $urlresponse;
        }
    }

    public function getrejectionstatus(Request $request)
    {
        try {
            $postType = $request->input('posttype');
            $data = PostRejectedStatus::select('id', 'reason')->where('type', $postType)->get();
            return updatedJsonResponse(["data" => $data, 'message' => "Post status updated successfully"]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function updatepoststatus(Request $request): JsonResponse
    {
        try {
            $text = updatepoststatus($request);
            $status = $request->input('poststatus');
            $postType = $request->input('posttype');
            $id = $request->input('post_id');
            $criteriaId = $request->input('criteriaId');
            $type_details = 'Home';

            // DB::beginTransaction();
            $updateArr = [
                'updatestatus' => 1
            ];
            if ($request->input('criteriaId') == Config::get('constants.CRITERION.POST_REJECTED')) {
                PostRejectedAssign::where('post_id', $id)
                    ->where('post_type', $postType)
                    ->update($updateArr);
                PostRejectedArcheive::where('post_id', $id)
                    ->where('post_type', $postType)->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
                $getUserId = PostRejectedAssign::where('post_id', $id)
                    ->where('post_type', $postType)
                    ->first();
            } else {
                PostApprovalArcheive::where('post_id', $id)
                    ->where('post_type', $postType)->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
                PostApprovalAssign::where('post_id', $id)
                    ->where('post_type', $postType)->update($updateArr);
                $getUserId = PostApprovalAssign::where('post_id', $id)
                    ->where('post_type', $postType)
                    ->first();
            }
            // DB::commit();
            $user_id = $getUserId['user_id'];
            $this->changelogPostStatus($status, $text, $request, $criteriaId);

            if ($status == 2) {
                if ($postType == "PR") {
                    $projectList = Project::select('project_name')->where('id', $id)->first();
                    $projectName = $projectList['project_name'];
                    $title = 'Project Rejected';
                    $notificationCriteria = (int)Config::get('constants.CRITERION.ON_PROJECT_REJECTED');
                    $message_body = $projectName . ' ' . Config::get('constants.NOTIFICATIONCONTENT.PROJECT_REJECTED');
                    $type_details = 'ProjectDetail';
                } else if ($postType == "P") {
                    $getPropertyData = Property::join('cities', 'properties.city_id', '=', 'cities.id')
                        ->join('property_type_master', 'properties.property_type_id', '=', 'property_type_master.id')
                        ->where('properties.id', $id)
                        ->select(
                            'cities.title as city',
                            'property_type_master.title as propertytype',
                            'properties.*',
                        )->get();
                    $propertyContent = $getPropertyData[0]['propertytype'] . ' ' . $getPropertyData[0]['address'] . ' ' . $getPropertyData[0]['city'];

                    $title = 'Property Rejected';
                    $notificationCriteria = (int)Config::get('constants.CRITERION.ON_PROPERTY_REJECTED');
                    $message_body = $propertyContent . ' ' . Config::get('constants.NOTIFICATIONCONTENT.PROPERTY_REJECTED');
                    $type_details = 'PropertyDetail';
                } else {
                    $getServiceData = Service::with([
                        'user' => function ($userQuery) {
                            $userQuery->join('cities', 'users.city_id', '=', 'cities.id')
                                ->join('state_master', 'cities.state_id', '=', 'state_master.id')
                                ->join('user_categories', 'users.category_id', '=', 'user_categories.id')
                                ->select(
                                    'users.*',
                                    'cities.title as city',
                                    'state_master.title as state',
                                    'user_categories.title as categoryname'
                                );
                        }
                    ])
                        ->join('cities', 'services.city_id', '=', 'cities.id')
                        ->join('service_categories_master', 'services.service_type_id', '=', 'service_categories_master.id')
                        ->join('service_subcategories_master', 'services.sub_category_id', '=', 'service_subcategories_master.id')
                        ->where('services.id', $id)
                        ->select(
                            'cities.title as city',
                            'service_categories_master.title as servicecategory',
                            'service_subcategories_master.title as servicesubcategory',
                            'services.*',
                        )->get();
                    $title = $getServiceData[0]['user']['firstname'] . ' ' . $getServiceData[0]['user']['lastname'];
                    $serviceDataContent = $getServiceData[0]['servicecategory'] . ' ' . $getServiceData[0]['servicesubcategory'] . ' ' . $getServiceData[0]['city'];
                    $message_body = $serviceDataContent . ' ' . Config::get('constants.NOTIFICATIONCONTENT.SERVICE_APPROVED');
                    $notificationCriteria = (int)Config::get('constants.CRITERION.ON_SERVICE_REJECTED');
                    $type_details = 'ServiceDetail';
                }
                $postBody = [
                    'post_id' => (int)$id,
                    'is_approved' => (int) $status,
                    'post_type' => $postType,
                    'poll_reject_status' => 0,
                    'is_published' => false,
                    'published_date' => date('Y-m-d H:i:s')
                ];
                $notifyBody = [
                    'criteria_id' => $notificationCriteria,
                    'user_id' => (int)$user_id,
                    'title' => $title,
                    'message_body' => $message_body,
                    'type' =>$type_details,
                    'linkId' => (int)$id
                ];
            } else {
                if ($postType == "PR") {
                    $projectList = Project::select('project_name')->where('id', $id)->first();
                    $projectName = $projectList['project_name'];
                    $message_body = $projectName . ' ' . Config::get('constants.NOTIFICATIONCONTENT.PROJECT_APPROVED');
                    $title = 'Project Approved';
                    $notificationCriteria = (int)Config::get('constants.CRITERION.ON_PROJECT_APPROVED');
                    $type_details = 'ProjectDetail';
                } else if ($postType == "P") {
                    $getPropertyData = Property::join('cities', 'properties.city_id', '=', 'cities.id')
                        ->join('property_type_master', 'properties.property_type_id', '=', 'property_type_master.id')
                        ->where('properties.id', $id)
                        ->select(
                            'cities.title as city',
                            'property_type_master.title as propertytype',
                            'properties.*',
                        )->get();
                    $propertyContent = $getPropertyData[0]['propertytype'] . ' ' . $getPropertyData[0]['address'] . ' ' . $getPropertyData[0]['city'];
                    $title = 'Property Approved';
                    $message_body = $propertyContent . ' ' . Config::get('constants.NOTIFICATIONCONTENT.PROPERTY_APPROVED');
                    $notificationCriteria = (int)Config::get('constants.CRITERION.ON_PROPERTY_APPROVED');
                    $type_details = 'PropertyDetail';
                } else {
                    $getServiceData = Service::with([
                        'user' => function ($userQuery) {
                            $userQuery->join('cities', 'users.city_id', '=', 'cities.id')
                                ->select(
                                    'users.*',
                                    'cities.title as city',
                                );
                        }
                    ])
                        ->join('cities', 'services.city_id', '=', 'cities.id')
                        ->join('service_categories_master', 'services.service_type_id', '=', 'service_categories_master.id')
                        ->join('service_subcategories_master', 'services.sub_category_id', '=', 'service_subcategories_master.id')
                        ->where('services.id', $id)
                        ->select(
                            'cities.title as city',
                            'service_categories_master.title as servicecategory',
                            'service_subcategories_master.title as servicesubcategory',
                            'services.*',
                        )->get();
                    $title = $getServiceData[0]['user']['firstname'] . ' ' . $getServiceData[0]['user']['lastname'];
                    $serviceDataContent = $getServiceData[0]['servicecategory'] . ' ' . $getServiceData[0]['servicesubcategory'] . ' ' . $getServiceData[0]['city'];
                    $message_body = $serviceDataContent . ' ' . Config::get('constants.NOTIFICATIONCONTENT.SERVICE_APPROVED');
                    $notificationCriteria = (int)Config::get('constants.CRITERION.ON_SERVICE_APPROVED');
                    $type_details = 'ServiceDetail';
                }
                $postBody = [
                    'post_id' => (int)$id,
                    'is_approved' => (int) $status,
                    'post_type' => $postType,
                    'poll_reject_status' => 1,
                    'is_published' => true,
                    'published_date' => date('Y-m-d H:i:s')
                ];
                $notifyBody = [
                    'criteria_id' => $notificationCriteria,
                    'user_id' => (int)$user_id,
                    'title' => $title,
                    'message_body' => $message_body,
                    'type' =>$type_details,
                    'linkId' => (int)$id
                ];
            }

            $url = env("MONGO_DB_API_URL") . 'api/v1/posts';
            $urlNotify = env("MONGO_DB_API_URL") . 'api/v1/notification/push/send';
            $header = ['Accept' => "application/json", "Authorization" => 'Bearer ' . env('MONGO_API_KEY')];
            $response = Http::withHeaders($header)->post($urlNotify, $notifyBody);
            Http::withHeaders($header)->post($url, $postBody);
            // DB::commit();
            return updatedJsonResponse(['Response' => $response->json(), 'message' => "Post status updated successfully"]);
        } catch (Exception $e) {
            // DB::rollBack();
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage(), 'Trace' => $e->getTrace()]));
        }
    }

    public function changelogPostStatus($status, $text, $request, $criteriaId)
    {
        $Arr = preparePOSTChangeLog($status, $text, $request);
        DB::beginTransaction();
        if ($criteriaId == Config::get('constants.CRITERION.POST_APPROVAL')) {
            PostApprovalChangelog::insert($Arr);
        } else if ($criteriaId == Config::get('constants.CRITERION.POST_REJECTED')) {
            PostRejectedChangelog::insert($Arr);
        }
        DB::commit();
    }

    public function publishstatusupdate(Request $request): JsonResponse
    {
        try {
            $status = $request->input('status');
            $post_id = $request->input('id');
            $postType = $request->input('posttype');
            $published_date = date('Y-m-d H:i:s');
            $flatstatus = 1;
            if ($status == 0) {
                $published_date = null;
                $flatstatus = 0;
            }
            if ($postType == "PR") {
                $update = Project::where('id', $post_id)->first();
                $update = $update->update([
                    'project_status' => $status,
                    'published_date' => $published_date
                ]);
                ProjectFlats::where('project_id', $post_id)->update([
                    'is_published' => $status,
                    'flat_status' => $flatstatus
                ]);
            } else if ($postType == "P") {
                $update = Property::where('id', $post_id)->first();
                $update = $update->update([
                    'property_status' => $status,
                    'published_date' => $published_date
                ]);
            } else {
                $update = Service::where('id', $post_id)->first();
                $update = $update->update([
                    'service_status' => $status,
                    'published_date' => $published_date
                ]);
            }
            if ($status != 0) {
                $published = true;
            } else {
                $published = false;
            }
            DB::beginTransaction();
            $postBody = [
                'post_id' => (int)$post_id,
                'post_type' => $postType,
                'is_published' => $published,
                'published_date' => $published_date
            ];
            $updateArr = [
                'updatestatus' => 1
            ];
            if ($request->input('criteriaId') == Config::get('constants.CRITERION.POST_REJECTED')) {
                PostRejectedAssign::where('post_id', $post_id)
                    ->where('post_type', $postType)
                    ->update($updateArr);
                PostRejectedArcheive::where('post_id', $post_id)
                    ->where('post_type', $postType)->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            } else {
                PostApprovalArcheive::where('post_id', $post_id)
                    ->where('post_type', $postType)->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
                PostApprovalAssign::where('post_id', $post_id)
                    ->where('post_type', $postType)->update($updateArr);
            }
            $url = env("MONGO_DB_API_URL") . 'api/v1/posts';
            Http::withHeaders(['Accept' => "application/json", "Authorization" => 'Bearer ' . env('MONGO_API_KEY')])->post($url, $postBody);
            DB::commit();
            return updatedJsonResponse();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function getpoststatus(Request $request): JsonResponse
    {
        try {
            $post_id = $request->input('id');
            $postType = $request->input('posttype');
            if ($postType == "PR") {
                $data = Project::where('id', $post_id)->first();
                if ($data) {
                    $is_approved = $data['is_project_approved'];
                } else {
                    $is_approved = 0;
                }
            } else if ($postType == "P") {
                $data = Property::where('id', $post_id)->first();
                if ($data) {
                    $is_approved = $data['is_property_approved'];
                } else {
                    $is_approved = 0;
                }
            } else {
                $data = Service::where('id', $post_id)->first();
                if ($data) {
                    $is_approved = $data['is_service_approved'];
                } else {
                    $is_approved = 0;
                }
            }
            return updatedJsonResponse(['message' => "success", 'data' => $is_approved]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function deleteimage(Request $request): JsonResponse
    {
        try {
            $imagetype = $request->input('imagetype');
            $imageextension = $request->input('imageextension');
            $posttype = $request->input('posttype');
            $post_id = $request->input('postid');
            $id = $request->input('id');
            $imagename = $request->input('imagename');
            if ($posttype == "PR") {
                if ($imageextension == "image") {
                    $destDirPath = Config::get('constants.PROJECT_FILE_UPLOAD_PATH') . "/" . $post_id . '/images' . '/' . $imagename;
                } else if ($imageextension == "video") {
                    $destDirPath = Config::get('constants.PROJECT_FILE_UPLOAD_PATH') . "/" . $post_id . '/video' . '/' . $imagename;
                } else {
                    $destDirPath = Config::get('constants.PROJECT_FILE_UPLOAD_PATH') . "/" . $post_id . '/' . $imageextension . '/' . $imagename;
                }
            } else if ($posttype == "S") {
                if ($imageextension == "image") {
                    $destDirPath = Config::get('constants.SERVICE_FILE_UPLOAD_PATH') . "/" . $post_id . '/images' . '/' . $imagename;
                }
            } else {
                if ($imageextension == "image") {
                    $destDirPath = Config::get('constants.PROPERTY_FILE_UPLOAD_PATH') . "/" . $post_id . '/images' . '/' . $imagename;
                }
            }
            deleteFile($destDirPath, true);
            if ($posttype == "S") {
                if ($imagetype == "mainimage") {
                    Service::where('id', $post_id)->update(["main_image_name" => ""]);
                } else {
                    ServiceImages::where('id', $id)->delete();
                }
            } else if ($posttype == "PR") {
                if ($imagetype == "mainimage") {
                    Project::where('id', $post_id)->update(["main_image_name" => null]);
                } else if ($imagetype == "certificateimage") {
                    Project::where('id', $post_id)->update(["completion_certificate_image_name" => null]);
                } else if ($imagetype == "video") {
                    Project::where('id', $post_id)->update(["video_file_name" => null]);
                } else if ($imagetype == "flatimage") {
                    ProjectFlats::where('id', $id)->update(["floor_plan_file_name" => null]);
                } else {
                    ProjectImages::where('id', $id)->delete();
                }
            } else {
                if ($imagetype == "mainimage") {
                    Property::where('id', $post_id)->update(["main_image_name" => null]);
                } else {
                    PropertyImages::where('id', $id)->delete();
                }
            }
            return updatedJsonResponse([
                'message' => "Document deleted successfully",
                'imagetype' => $imagetype,
                'posttype' => $posttype,
                'post_id' => $post_id,
                'image_id' => $id
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    public function editpost(Request $request)
    {
        try {
            $postType = $request->input('posttype');
            $postid = $request->input('postid');
            $postBody = [];
            if ($postType == "S") {
                $role_type = Auth::user()->role_type;
                $Arr = [];
                $payment_mode_id = implode(',', $request->input('payment_mode_id'));
                $payment_mode_id = rtrim($payment_mode_id, ",");
                $Arr['payment_mode_id'] = $payment_mode_id;
                // $Arr['service_type_id'] = $request->input('service_type_id');
                // $sub_category_id = $request->input('sub_category_id');
                // $Arr['sub_category_id'] = $sub_category_id;
                $benefits = $request->input('benefits');
                $Arr['benefits'] = $benefits;
                $offers = $request->input('offers');
                $Arr['offers'] = $offers;
                $delivery = $request->input('delivery');
                $Arr['delivery'] = $delivery;
                $hours = $request->input('hours');
                $Arr['hours'] = $hours;
                $short_description = $request->input('short_description');
                $Arr['short_description'] = $short_description;
                $warranty  = $request->input('warranty');
                $Arr['warranty'] = $warranty;
                $mainimage = $request->has('main_image_name');
                if ($mainimage) {
                    $mainimagefile = $request->file('main_image_name');
                    $fileName = 'main_' . strtolower(removeSpacesFromFileName($mainimagefile->getClientOriginalName(), 'post'));
                    $destDirPath = Config::get('constants.SERVICE_FILE_UPLOAD_PATH') . "/" . $postid . '/images' . '/' . $fileName;
                    putObjectToS3($mainimagefile, $destDirPath);
                    $Arr['main_image_name'] = $fileName;
                }
                if ($request->has('otherimage')) {
                    $i = 1;
                    foreach ($request->file('otherimage') as $file) {
                        $fileName = $i . time() . '_' . strtolower(removeSpacesFromFileName($file->getClientOriginalName(), 'post'));
                        $destDirPath = Config::get('constants.SERVICE_FILE_UPLOAD_PATH') . "/" . $postid . '/images' . '/' . $fileName;
                        putObjectToS3($file, $destDirPath);
                        ServiceImages::create(['service_id' => $postid, "image_name" => $fileName]);
                        $i++;
                    }
                }

                if ($role_type != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                    $Arr['is_published'] = 0;
                    $Arr['is_service_approved'] = 0;
                    $Arr['published_date'] = null;
                }
                if ($request->input('area_id')) {
                    if (count($request->input('area_id')) != 0) {
                        ServiceArea::where('service_id', $postid)->delete();
                        for ($i = 0; $i < count($request->input('area_id')); $i++) {
                            ServiceArea::create([
                                'service_id' => $postid,
                                'area_id' => $request->input('area_id')[$i]
                            ]);
                        }
                    }
                }
                $serviceUpdate = Service::where('id', $postid)->first();
                $serviceUpdate = $serviceUpdate->update($Arr);
            } else if ($postType == "PR") {
                $role_type = Auth::user()->role_type;
                $Arr = [];
                $getProjectCategory = Project::where('id', $postid)->first();
                $category = $getProjectCategory['category_id'];

                $Arr['project_rera_id'] = $request->input('projectreraid');
                $Arr['address'] = $request->input('address');
                $Arr['project_name'] = $request->input('projectname');
                $Arr['project_area'] = $request->input('projectarea');
                $Arr['total_no_of_units'] = $request->input('totalnoofunits');
                $Arr['description'] = $request->input('description');
                $Arr['available_no_of_units'] = $request->input('availablenoofunits');
                $Arr['construction_status_id'] = $request->input('constructionstatus');
                $Arr['area_unit_id'] = $request->input('areaid');
                $Arr['launch_date'] = Carbon::parse($request->input('launchdate'))->format('Y-m-d');
                $Arr['approval_authority'] = $request->input('approvalauthority');
                $Arr['price_negotiable'] = $request->input('pricenegotiable');
                $Arr['total_floors'] = $request->input('total_floors');

                if ($category == 1) {
                    $Arr['master_bedroom_Id'] = $request->input('masterbedroom');
                    $Arr['balcony_id'] = $request->input('balcony');
                    $Arr['floor_kitchen_id'] = $request->input('floorkitchen');
                    $Arr['other_bedroom_id'] = $request->input('otherbedroomo');
                    $Arr['living_or_dining_id'] = $request->input('living');
                    $Arr['floor_toilet_id'] = $request->input('Ftoilet');
                    $Arr['internal_door_id'] = $request->input('idoor');
                    $Arr['fitting_kitchen_id'] = $request->input('fitkitchen');
                    $Arr['fitting_toilet_id'] = $request->input('fittoilet');
                    $Arr['windows_id'] = $request->input('window');
                    $Arr['main_door_id'] = $request->input('mdoor');
                    $Arr['electrical_id'] = $request->input('electrical');
                    $Arr['wall_kitchen_id'] = $request->input('wkitchen');
                    $Arr['wall_toilet_id'] = $request->input('wtoilet');
                    $Arr['interior_id'] = $request->input('interior');
                }
                $mainimage = $request->has('main_image_name');
                if ($mainimage) {
                    $mainimagefile = $request->file('main_image_name');
                    $fileName = 'main_' . strtolower(removeSpacesFromFileName($mainimagefile->getClientOriginalName(), 'post'));
                    $destDirPath = Config::get('constants.PROJECT_FILE_UPLOAD_PATH') . "/" . $postid . '/images' . '/' . $fileName;
                    putObjectToS3($mainimagefile, $destDirPath);
                    $Arr['main_image_name'] = $fileName;
                }
                $certificate = $request->has('certificate');
                if ($certificate) {
                    $mainimagefile = $request->file('certificate');
                    $fileName = 'cert_' . strtolower(removeSpacesFromFileName($mainimagefile->getClientOriginalName(), 'post'));
                    $destDirPath = Config::get('constants.PROJECT_FILE_UPLOAD_PATH') . "/" . $postid . '/images' . '/' . $fileName;
                    putObjectToS3($mainimagefile, $destDirPath);
                    $Arr['completion_certificate_image_name'] = $fileName;
                }
                $video_file_name = $request->has('video_image_name');
                if ($video_file_name) {
                    $mainimagefile = $request->file('video_image_name');
                    $fileName = 'video_' . strtolower(removeSpacesFromFileName($mainimagefile->getClientOriginalName(), 'post'));
                    $destDirPath = Config::get('constants.PROJECT_FILE_UPLOAD_PATH') . "/" . $postid . '/video' . '/' . $fileName;
                    putObjectToS3($mainimagefile, $destDirPath);
                    $Arr['video_file_name'] = $fileName;
                }
                if ($request->has('otherimage')) {
                    $i = 1;

                    foreach ($request->file('otherimage') as $file) {

                        $fileName = $i . time() . '_' . strtolower(removeSpacesFromFileName($file->getClientOriginalName(), 'post'));
                        $destDirPath = Config::get('constants.PROJECT_FILE_UPLOAD_PATH') . "/" . $postid . '/images' . '/' . $fileName;
                        putObjectToS3($file, $destDirPath);
                        ProjectImages::create(['project_id' => $postid, "image_name" => $fileName]);

                        $i++;
                    }
                }
                if ($role_type != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                    $Arr['is_published'] = 0;
                    $Arr['is_project_approved'] = 0;
                    $Arr['published_date'] = null;
                }

                $projectUpdated = Project::where('id', $postid)->first();
                $projectUpdated = $projectUpdated->update($Arr);
                $amenities = $request->input('documenttype');
                if (count($amenities) != 0) {
                    ProjectAmenities::where('project_id', $postid)->delete();
                    for ($i = 0; $i < count($amenities); $i++) {
                        ProjectAmenities::create(['project_id' => $postid, 'amenity_id' => $amenities[$i]]);
                    }
                }
            } else if ($postType == "P") {
                $role_type = Auth::user()->role_type;
                $validation = $this->validate($request, [
                    'builder_name' => 'nullable',
                    'bhk_id' => 'nullable',
                    'property_sub_category_id' => 'nullable',
                    'furnish_type_id' => 'nullable',
                    'floor_id' => 'nullable',
                    'bathroom_id' => 'nullable',
                    'balcony_id' => 'nullable',
                    'width_of_road' => 'nullable',
                    'construction_type_id' => 'nullable',
                    'available_from' => 'nullable',
                    'monthly_rent' => 'nullable',
                    'transaction_type_id' => 'nullable',
                    'maintenance_charges' => 'nullable',
                    'security_deposit' => 'nullable',
                    'builtup_area' => 'nullable',
                    'carpet_area' => 'nullable',
                    'no_open_side_id' => 'nullable',
                    'price' => 'nullable',
                    'negotiable' => 'nullable',
                    'expected_duration_of_stay' => 'nullable',
                    'expected_duration_of_month' => 'nullable',
                    'expected_duration_of_year' => 'nullable',
                    'property_age' => 'nullable',
                    'corner_plot' => 'nullable',
                    'area_unit' => 'nullable',
                    'plot_area' => 'nullable',
                    'token_advance' => 'nullable',
                    'price_negotiable' => 'nullable',
                    'boundary_wall_made' => 'nullable',
                    'personal_washroom' => 'nullable',
                    'cafeteria_area_id' => 'nullable',
                    'address' => 'nullable',
                    'description' => 'nullable',
                    'main_image_name' => 'nullable',
                    'PG_or_hostel_name' => 'nullable',
                    'PG_type' => 'nullable',
                    'PG_preferred_tenant' => 'nullable',
                    'room_type' => 'nullable',
                    'attach_bathroom' => 'nullable',
                    'food_facility' => 'nullable',
                    'parking' => 'nullable',
                    'room_amenities' => 'nullable',
                    'gate_closing_time' => 'nullable',
                    'PG_rules' => 'nullable',
                    'common_amenities' => 'nullable',
                    'flatmates_property_type' => 'nullable',
                    'flatmates_tenant_type' => 'nullable',
                    'flatmates_room_type' => 'nullable',
                    'flatmates_non_veg_allowed' => 'nullable',
                    'flatmates_gated_security' => 'nullable',
                    'flatmates_floor' => 'nullable',
                    'flatmates_preferred_tenant_type' => 'nullable',
                    'possession_date' => 'nullable',
                    // 'tenant_type_id' => 'nullable',
                    // 'ideal_business_id' => 'nullable'

                ]);
                // return $validation;
                if (isset($validation['available_from'])) {
                    $validation['available_from'] = Carbon::parse($validation['available_from'])->format('Y-m-d');
                }
                if (isset($validation['possession_date'])) {
                    $validation['possession_date'] = Carbon::parse($validation['possession_date'])->format('Y-m-d');
                }
                if (isset($validation['PG_rules'])) {
                    $validation['PG_rules'] = implode(',', $validation['PG_rules']);
                }
                if (isset($validation['room_amenities'])) {
                    $validation['room_amenities'] = implode(',', $validation['room_amenities']);
                }
                if (isset($validation['common_amenities'])) {
                    $validation['common_amenities'] = implode(',', $validation['common_amenities']);
                }
                if (isset($validation['PG_preferred_tenant'])) {
                    $validation['PG_preferred_tenant'] = implode(',', $validation['PG_preferred_tenant']);
                }
                if (isset($validation['food_facility'])) {
                    $validation['food_facility'] = implode(',', $validation['food_facility']);
                }
                if (isset($validation['PG_type'])) {
                    $validation['PG_type'] = implode(',', $validation['PG_type']);
                }
                if (isset($validation['flatmates_preferred_tenant_type'])) {
                    $validation['flatmates_preferred_tenant_type'] = implode(',', $validation['flatmates_preferred_tenant_type']);
                }
                $mainimage = $request->has('main_image_name');
                if ($mainimage) {
                    $mainimagefile = $request->file('main_image_name');
                    $fileName =  'main_' . strtolower(removeSpacesFromFileName($mainimagefile->getClientOriginalName(), 'post'));
                    $destDirPath = Config::get('constants.PROPERTY_FILE_UPLOAD_PATH') . "/" . $postid . '/images' . '/' . $fileName;
                    putObjectToS3($mainimagefile, $destDirPath);
                    $validation['main_image_name'] = $fileName;
                }

                if ($request->has('otherimage')) {

                    $i = 1;
                    foreach ($request->file('otherimage') as $file) {

                        $fileName = $i . time() . '_' . strtolower(removeSpacesFromFileName($file->getClientOriginalName(), 'post'));
                        $destDirPath = Config::get('constants.PROPERTY_FILE_UPLOAD_PATH') . "/" . $postid . '/images' . '/' . $fileName;
                        putObjectToS3($file, $destDirPath);
                        PropertyImages::create(['property_id' => $postid, "image_name" => $fileName]);

                        $i++;
                    }
                }
                if ($request->input('ideal_business_id')) {
                    if (count($request->input('ideal_business_id')) != 0) {
                        PropertyIdealForBusiness::where('property_id', $postid)->delete();
                        for ($i = 0; $i < count($request->input('ideal_business_id')); $i++) {
                            PropertyIdealForBusiness::create([
                                'property_id' => $postid,
                                'ideal_business_id' => $request->input('ideal_business_id')[$i]
                            ]);
                        }
                    }
                }
                if ($request->input('tenant_type_id')) {
                    if (count($request->input('tenant_type_id')) != 0) {
                        PropertyTenant::where('property_id', $postid)->delete();
                        for ($i = 0; $i < count($request->input('tenant_type_id')); $i++) {
                            PropertyTenant::create(['property_id' => $postid, 'tenant_type_id' => $request->input('tenant_type_id')[$i]]);
                        }
                    }
                }
                if ($role_type != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                    $validation['is_published'] = 0;
                    $validation['is_property_approved'] = 0;
                    $validation['published_date'] = null;
                }
                $propertyUpdated = Property::where('id', $postid)->first();
                $propertyUpdated = $propertyUpdated->update($validation);
            } else {
                echo "Else block-" . $postType;
                exit;
            }
            $updateArr['updatestatus'] = 1;
            if ($request->input('criteriaId') == Config::get('constants.CRITERION.POST_APPROVAL')) {
                PostApprovalAssign::where('post_id', $postid)
                    ->where('post_type', $postType)
                    ->update($updateArr);
                PostApprovalArcheive::where('post_id', $postid)
                    ->where('post_type', $postType)
                    ->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.POST_REJECTED')) {
                PostRejectedAssign::where('post_id', $postid)
                    ->where('post_type', $postType)
                    ->update($updateArr);
                PostRejectedArcheive::where('post_id', $postid)
                    ->where('post_type', $postType)
                    ->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            }
            $role_type = Auth::user()->role_type;
            if ($role_type != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                $url = env("MONGO_DB_API_URL") . 'api/v1/posts';
                $postBody = [
                    'post_id' => (int)$postid,
                    'post_type' => $postType,
                    'is_published' => false,
                    'published_date' => null,
                    'is_approved' => 0
                ];

                Http::withHeaders(['Accept' => "application/json", "Authorization" => 'Bearer ' . env('MONGO_API_KEY')])->post($url, $postBody);
            }

            return updatedJsonResponse(['message' => "Post updated successfully"]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage(), 'Trace' => $e->getTrace()]));
        }
    }


    public function unitandflat($id)
    {
        $scenario = "Units/Flats";
        $criteriaId  = "";
        $role_id = Auth::user()->role_id;
        $PermissionDetail = getPermissionAndAccess(0, $role_id);
        $commonFunctionData = commonFunctionData($role_id, $criteriaId);
        $notificationData = $commonFunctionData['notificationData'];
        $notificationDataNew = $commonFunctionData['notificationDataNew'];
        $getData = ProjectFlats::join('furnish_type_master', 'project_flats.furnish_type_id', '=', 'furnish_type_master.id')
            ->join('floor_master', 'project_flats.floor_id', '=', 'floor_master.id')
            ->Leftjoin('bhk_master', 'project_flats.bhk_id', '=', 'bhk_master.id')
            ->select('project_flats.*', 'bhk_master.title as bhk', 'floor_master.title as Floor_Title', 'furnish_type_master.title as Furnish_Title')
            ->where('project_id', $id)
            ->get();
        return view('Scenario/Post/unitandflatpost', compact(
            'scenario',
            'criteriaId',
            'id',
            'PermissionDetail',
            'getData',
            'notificationData',
            'notificationDataNew'
        ));
    }
    public function editview($type, $id, $criteriaId)
    {
        if ($type == "P") {
            $text = 'Property';
        } else if ($type == "PR") {
            $text = 'Project';
        } else {
            $text = 'Service';
        }
        $scenario = "Edit " . $text;
        $content = "Edit all type of post (Service, Property and Project";
        $role_id = Auth::user()->role_id;
        $PermissionDetail = getPermissionAndAccess(0, $role_id);
        $commonFunctionData = commonFunctionData($role_id, $criteriaId);
        $PosRejecttArr = $commonFunctionData['PostRejectedStatus']->toArray();
        $notificationData = $commonFunctionData['notificationData'];
        $notificationDataNew = $commonFunctionData['notificationDataNew'];
        $PermissionDetail = $commonFunctionData['PermissionDetail'];
        $checkpermission = $commonFunctionData['checkpermission'];
        $PostRejectedStatus = Arr::where($PosRejecttArr, function ($value, $key) use ($type) {
            return $value['type'] == $type;
        });
        return view('Scenario/Post/postedit', compact(
            'scenario',
            'criteriaId',
            'id',
            'content',
            'PermissionDetail',
            'notificationData',
            'type',
            'PostRejectedStatus',
            'id',
            'criteriaId',
            'notificationDataNew',
            'checkpermission',
            'PermissionDetail'

        ));
    }
    public function flatdetails(Request $request)
    {
        $flatId = $request->input('id');
        $getData = ProjectFlats::where('project_flats.id', $flatId)
            ->join('furnish_type_master', 'project_flats.furnish_type_id', '=', 'furnish_type_master.id')
            ->select('project_flats.*', 'furnish_type_master.title as Furniture_Title')
            ->get();
        $postId = $request->input('postid');
        $getPostData = Project::where('id', $postId)->first();
        $projectcategory = $getPostData['category_id'];
        $IdealBusniess = IdealBusniess::where('is_active', 1)->get();
        $TransactionType = TransactionType::where('is_active', 1)->get();
        $BHKMaster = BHKMaster::where('is_active', 1)->get();
        $FloorMaster = FloorMaster::where('is_active', 1)->get();
        $returnHTML = view('Components.projectflatmodal', compact(
            'getData',
            'TransactionType',
            'BHKMaster',
            'IdealBusniess',
            'FloorMaster',
            'projectcategory'
        ))->render();
        return $returnHTML;
    }

    public function editflat(Request $request)
    {
        try {
            $role_type = Auth::user()->role_type;
            $flatId = $request->input('flatid');
            $postId = $request->input('postid');
            $getPostData = Project::where('id', $postId)->first();
            $flatcount = ProjectFlats::where('project_id', $postId)->count();
            $projectcategory = $getPostData['category_id'];
            $Arr = [];
            $bhk = $request->input('bhk');
            $bhktitle = "";
            if (isset($bhk)) {
                $Arr['bhk_id'] = $request->input('bhk');
                $bhktitle =  BHKMaster::where('id', $Arr['bhk_id'])->first();
                $bhktitle = $bhktitle['title'];
            }
            $loanBank = $request->input('loan_offered_bank_ids');
            if (isset($loanBank)) {
                $Arr['loan_offered_bank_ids'] = implode(',', $request->input('loan_offered_bank_ids'));
            }
            $Arr['builtup_area'] = $request->input('builtuparea');
            $Arr['carpet_area'] = $request->input('carpetarea');
            $Arr['floor_id'] = $request->input('floor');
            $floortitle = "";
            $floortitle = FloorMaster::where('id', $Arr['floor_id'])->first();


            $Arr['transaction_type_id'] = $request->input('transactiontype');
            if ($Arr['transaction_type_id'] == 2) {
                $Arr['flat_age'] = $request->input('flat_age');
            }

            $Arr['price'] = $request->input('price');
            $possession = $request->input('possessiondate');
            if (isset($possession)) {
                $Arr['possession_date'] = Carbon::parse($request->input('possessiondate'))->format('Y-m-d');
            }
            $mainimage = $request->has('floor_plan_file_name');
            if ($mainimage) {
                $mainimagefile = $request->file('floor_plan_file_name');
                $fileName = strtolower(removeSpacesFromFileName($mainimagefile->getClientOriginalName(), 'post'));
                $destDirPath = Config::get('constants.PROJECT_FILE_UPLOAD_PATH') . "/" . $postId . '/flats' . '/' . $flatId . '/' . $fileName;
                putObjectToS3($mainimagefile, $destDirPath);
                $Arr['floor_plan_file_name'] = $fileName;
            }
            // return $projectcategory;
            if ($projectcategory == 2) { //commercial - units
                $idealbusiness = $request->input('idealbusiness');
                $Arr['title'] = 'Units ' . $flatcount;
                if (isset($idealbusiness)) {
                    FlatIdealBusniess::where('flat_id', $flatId)->delete();
                    for ($i = 0; $i < count($idealbusiness); $i++) {
                        FlatIdealBusniess::create([
                            'flat_id' => $flatId,
                            'ideal_business_id' => $idealbusiness[$i]
                        ]);
                    }
                }
                $msg = "Unit detail updated successfully";
            } else { // residential - flats
                $Arr['title'] = 'Units ' . $flatcount;
                if ($bhktitle != "") $Arr['title'] .= ' - ' . $bhktitle;
                if ($floortitle) $Arr['title'] .= ' - ' . $floortitle = $floortitle['title'];
                $msg = "Flat details updated successfully";
            }

            $update = Project::where('id', $postId)->first();
            if ($role_type != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                $update = $update->update([
                    'is_published' => 0,
                    'is_project_approved' => 0,
                    'published_date' => null
                ]);
                $Arr['is_published'] = 0;
                $Arr['flat_status'] = 0;
                $url = env("MONGO_DB_API_URL") . 'api/v1/posts';
                $postBody = [
                    'post_id' => (int)$postId,
                    'post_type' => 'P',
                    'is_published' => false,
                    'published_date' => null,
                    'is_approved' => 0
                ];
                Http::withHeaders(['Accept' => "application/json", "Authorization" => 'Bearer ' . env('MONGO_API_KEY')])->post($url, $postBody);
            }
            ProjectFlats::where('id', $flatId)->update($Arr);

            return updatedJsonResponse(['message' => $msg]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function getpostapprovestatus(Request $request)
    {
        $id = $request->input('id');
        $posttype = $request->input('posttype');
        if ($posttype == "S") {
            $getpoststatus = Service::where('id', $id)->first();
            return $getpoststatus['is_service_approved'];
        } else if ($posttype == "P") {
            $getpoststatus = Property::where('id', $id)->first();
            return $getpoststatus['is_property_approved'];
        } else if ($posttype == "PR") {
            $getpoststatus = Project::where('id', $id)->first();
            return $getpoststatus['is_project_approved'];
        }
    }
}
