<?php

namespace App\Http\Controllers\Post;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Models\UserActivity\PostApproval\PostApprovalAssign;
use Illuminate\Support\Facades\Auth;



use Exception;

class ApprovedPostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index($id)
    {
        $scenario = "Approval";
        $content = "User uploaded the post for the approval - pending from admin";
        $role_id = Auth::user()->role_id;
        $criteriaId = Config::get('constants.CRITERION.POST_APPROVAL');
        $commonFunctionData = commonFunctionData($role_id,$criteriaId);
        $state = $commonFunctionData['state'];
        $userType = $commonFunctionData['userType'];
        $callStatus = $commonFunctionData['callStatus'];
        $PostRejectedStatus = $commonFunctionData['PostRejectedStatus'];
        $KYCStatus = $commonFunctionData['KYCStatus'];
        $ServiceCategory = $commonFunctionData['ServiceCategory'];
        $PostType = $commonFunctionData['PostType'];
        $KYCRejectedStatus = $commonFunctionData['KYCRejectedStatus'];
        $KYCDocumentList = $commonFunctionData['KYCDocumentList'];
        $UserCategory = $commonFunctionData['UserCategory'];
        $ProjectCategory = $commonFunctionData['ProjectCategory'];
        $PropertyType = $commonFunctionData['PropertyType'];
        $notificationData = $commonFunctionData['notificationData'];
        $notificationDataNew = $commonFunctionData['notificationDataNew'];
        $PermissionDetail = $commonFunctionData['PermissionDetail'];
        $checkpermission = $commonFunctionData['checkpermission'];
        $propertycount = PostApprovalAssign::where('post_type', 'P')
            ->where('crm_user_id', $id)
            ->where('updatestatus', '!=', 2)
            ->count();
        $propertywaitingcount = PostApprovalAssign::where('post_type', 'P')
            ->where('crm_user_id', $id)
            ->where('updatestatus', '=', 0)
            ->count();
        $propertyprocessedcount = PostApprovalAssign::where('post_type', 'P')
            ->where('crm_user_id', $id)
            ->where('updatestatus', '=', 1)
            ->count();
        $servicecount = PostApprovalAssign::where('post_type', 'S')
            ->where('updatestatus', '!=', 2)
            ->where('crm_user_id', $id)
            ->count();
        $serviceprocessedcount = PostApprovalAssign::where('post_type', 'S')
            ->where('updatestatus', '=', 1)
            ->where('crm_user_id', $id)
            ->count();
        $servicewaitingcount = PostApprovalAssign::where('post_type', 'S')
            ->where('updatestatus', '=', 0)
            ->where('crm_user_id', $id)
            ->count();
        $projectcount = PostApprovalAssign::where('post_type', 'PR')
            ->where('updatestatus', '!=', 2)
            ->where('crm_user_id', $id)
            ->count();
        $projectwaitingcount = PostApprovalAssign::where('post_type', 'PR')
            ->where('updatestatus', '=', 0)
            ->where('crm_user_id', $id)
            ->count();
        $projectprocessedcount = PostApprovalAssign::where('post_type', 'PR')
            ->where('updatestatus', '=', 1)
            ->where('crm_user_id', $id)
            ->count();
        if ($checkpermission->count() == 0) {
            return view('error.500');
        }
        return view('Scenario/Post/post', compact(
            'scenario',
            'content',
            'projectcount',
            'propertycount',
            'servicecount',
            'projectwaitingcount',
            'propertywaitingcount',
            'servicewaitingcount',
            'projectprocessedcount',
            'propertyprocessedcount',
            'serviceprocessedcount',
            'state',
            'id',
            'PostType',
            'UserCategory',
            'KYCDocumentList',
            'userType',
            'callStatus',
            'KYCStatus',
            'KYCRejectedStatus',
            'criteriaId',
            'PermissionDetail',
            'ServiceCategory',
            'ProjectCategory',
            'PropertyType',
            'PostRejectedStatus',
            'notificationData',
            'notificationDataNew'
        ));
    }
}
