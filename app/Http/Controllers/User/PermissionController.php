<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Setting\NotificationMaster;
use Illuminate\Http\Request;
use App\Models\User\Roles;
use App\Models\User\Permission;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\User\RolePermissionMapping;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Exceptions\HttpResponseException;


class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // start - to show index page of the permission creation - listview
    public function index($id = null)
    {
        $scenario = "Permissions";
        $content = "Permission and Role mapping panel";
        $roledata = Roles::where('role_title', "!=", 'Super Admin')->get();
        $permission = Permission::all();
        $role_id = Auth::user()->role_id;
        $user_id = Auth::user()->user_id;
        $RolePermissionMapping = RolePermissionMapping::all();
        if ($id == null) {
            $requestedRoleID = 0;
        } else {
            $requestedRoleID = $id;
        }
        $id = '';
        $criteriaId = "";
        $notificationData = NotificationMaster::where('crm_user_id', $user_id)
        ->orderBy('notification_id', 'DESC')
        ->get();
        $notificationDataNew = NotificationMaster::where('is_read', 0)
        ->where('crm_user_id', $user_id)->orderBy('notification_id', 'DESC')
        ->count();
        return view('User/permission_listview', compact(
            'scenario',
            'content',
            'roledata',
            'permission',
            'id',
            'criteriaId',
            'notificationData',
            'notificationDataNew',
            'requestedRoleID'
        ));
    }

    public function getrolebasedpermission(Request $request): JsonResponse
    {
        try {
            $role_id = $request->input('role_id');
            $selectpermissiondata = RolePermissionMapping::where('role_id', $role_id)->get();
            return successJsonResponce([
                'message' => "success.",
                'data' => $selectpermissiondata
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    public function updaterolebasedpermission(Request $request): JsonResponse
    {
        try {
            $role_id = $request->input('role_id');
            $value = $request->input('value');
            $columnname = $request->input('columnname');
            $permission_id = $request->input('permission_id');
            $selectpermissiondata = RolePermissionMapping::where('role_id', $role_id)
                ->where('permission_id', $permission_id)
                ->count();
            DB::beginTransaction();
            if ($selectpermissiondata == 0) {
                RolePermissionMapping::create([
                    $columnname => $value,
                    'role_id' => $role_id,
                    'permission_id' => $permission_id
                ]);
                DB::commit();
                return successJsonResponce([
                    'message' => "Permission added successfully."
                ]);
            } else {
                RolePermissionMapping::where('role_id', $role_id)
                    ->where('permission_id', $permission_id)
                    ->update([$columnname => $value]);
                DB::commit();
                return updatedJsonResponse([
                    'message' => "Permission updated successfully."
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    //end - to show index page of the  permission - listview
}
