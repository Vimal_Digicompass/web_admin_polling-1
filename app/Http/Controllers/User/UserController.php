<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Http;
use App\Models\User\Roles;
use Illuminate\Support\Facades\Hash;
use App\Models\Master\State;
use App\Models\Master\City;
use App\Models\Setting\NotificationMaster;
use App\User;
use Carbon\Carbon;
use App\Models\SqFt\User as SqFtUser;
use App\Models\User as MongoDB;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(
            'performancetest'
        );;
    }

    public function serviceperformancetest()
    {
        $ServiceAreaPath = '../tests/service/servicearea.json';
        $ServiceArea = json_decode(file_get_contents($ServiceAreaPath), true);
        $ServiceImagePath = '../tests/service/serviceimage.json';
        $ServiceImage = json_decode(file_get_contents($ServiceImagePath), true);
        $UserPath = '../tests/service/user.json';
        $Users = json_decode(file_get_contents($UserPath), true);
        $CategoryPath = '../tests/service/servicecategory.json';
        $Category = json_decode(file_get_contents($CategoryPath), true);
        $SubCateoryPath = '../tests/service/servicesubcategory.json';
        $SubCategory = json_decode(file_get_contents($SubCateoryPath), true);
        $RandomString = ['Testing', 'Servicet testing', 'Plumber service', 'Test service'];
        $serviceArr = [];
        for ($i = 0; $i < 50; $i++) {
            $index = array_rand($RandomString);
            $description = $RandomString[$index];

            $categoryIndex = array_rand($Category);
            $category_id = $Category[$categoryIndex]['id'];

            $userIndex = array_rand($Users);
            $userDetail = $Users[$userIndex];

            $deviceToken = $userDetail['android_device_token'];
            $registeredId = $userDetail['android_reg_device_id'];
            $userId = $userDetail['id'];
            $Arr = [
                "AccessToken" => "MjUyLTg1REEyUzMtQURTUzVELUVJNUI0QTIyMTE=",
                "Areas" => $ServiceArea,
                "Benefits" => "",
                "CityId" => "1172",
                "Delivery" => "120",
                "MainImage" => "",
                "hours" => '5',
                "DeviceToken" => $deviceToken,
                "MaxPrice" => "4500",
                "MinPrice" => "2000",
                "Offers" => "",
                "PaymentModeIds" => [
                    [
                        "PaymentModeId" => "1"
                    ],
                    [
                        "PaymentModeId" => "2"
                    ]
                ],
                "Photos" => $ServiceImage,
                "RegisterId" => $registeredId,
                "ServiceId" => "0",
                "ServiceTypeId" => $category_id,
                "ShortDescription" => $description,
                "SubCategoryId" => "1",
                "UserId" => $userId,
                "Warranty" => ""
            ];
            array_push($serviceArr,$Arr);
        }
        return $serviceArr;
    }
    // start - to show index page of the user creation - listview
    public function index(Request $request, $id = null)
    {
        $scenario = "Users";
        $content = "CRM user creation panel";
        $roledata = Roles::all();
        $user_id = Auth::user()->user_id;
        $role_id = Auth::user()->role_id;
        $id = '';
        $teamleaddata  = DB::table('users')
            ->join('roles', 'users.role_id', '=', 'roles.role_id')
            ->where('role_type', 1)->get(); // 1 team lead
        $statedata = State::where('is_active', 1)->get();
        if (request()->ajax()) {

            $userinfo = User::with('roles')
                ->where('user_id', "!=", Auth::user()->user_id)
                ->orderby('user_id', 'desc')->get();

            return DataTables::of($userinfo)
                ->addColumn('action', function ($userinfo) {
                    return '<td><button type="button" id=' . $userinfo->user_id . ' data-tooltip="tooltip" data-placement="top" title="Edit"  class="useredit btn btn-primary btn-icon submit"> <i data-feather="edit-2"></i></button></td>';
                })->addcolumn('image', function ($userinfo) {
                    $path = $userinfo->image_name;
                    return getFileURLPublicPath($path);
                })->addColumn('active', function ($userinfo) {
                    if ($userinfo->is_active == 1) {
                        return ' <td><button type="button" id=' . $userinfo->user_id . ' style="width: 100px;" class="btn btn-primary statusinactive">ACTIVE</button></td>';
                    } else {
                        return ' <td><button type="button" id=' . $userinfo->user_id . ' style="width: 100px;" class="btn btn-danger statusactive">IN-ACTIVE</button></td>';
                    }
                })
                ->addColumn('name', function ($userinfo) {
                    return $userinfo->first_name . ' ' . $userinfo->last_name;
                })
                ->addColumn('rolename', function ($userinfo) {
                    return $userinfo->roles[0]['role_title'];
                })
                ->editColumn('created_at', function ($userinfo) {
                    return $userinfo->created_at->format('d-m-Y H:i:s');
                })
                ->editColumn('updated_at', function ($userinfo) {
                    return Carbon::createFromFormat('Y-m-d H:i:s', $userinfo->updated_at)
                        ->setTimezone('Asia/Calcutta')
                        ->format('d-m-Y H:i:s');
                })
                ->rawColumns(['action', 'active'])
                ->make(true);
        }
        $criteriaId = "";


        $notificationData = NotificationMaster::where('crm_user_id', $user_id)
            ->orderBy('notification_id', 'DESC')
            ->get();
        $notificationDataNew = NotificationMaster::where('is_read', 0)
            ->where('crm_user_id', $user_id)->orderBy('notification_id', 'DESC')
            ->count();
        return view('User/user_listview', compact(
            'scenario',
            'content',
            'id',
            'roledata',
            'teamleaddata',
            'criteriaId',
            'statedata',
            'notificationData',
            'notificationDataNew',
        ));
    }
    //end - to show index page of the user creation - listview
    public function notification(Request $request, $id = null)
    {
        $scenario = "Notification";
        $user_id = Auth::user()->user_id;
        $role_id = Auth::user()->role_id;
        NotificationMaster::where('is_read', 0)
            ->where('crm_user_id', $user_id)
            ->update([
                'is_read' => 1
            ]);
        $notificationData = NotificationMaster::where('crm_user_id', $user_id)
            ->orderBy('notification_id', 'DESC')
            ->get();
        $notificationDataNew = NotificationMaster::where('is_read', 0)
            ->where('crm_user_id', $user_id)->orderBy('notification_id', 'DESC')
            ->count();
        $content = "To view all the notification assigned to the CRM user";
        $criteriaId = "";
        $commonFunctionData = commonFunctionData($role_id, $criteriaId);
        $PermissionDetail = $commonFunctionData['PermissionDetail'];
        return view(
            'User/notification',
            compact(
                'scenario',
                'content',
                'id',
                'criteriaId',
                'notificationDataNew',
                'notificationData',
                'PermissionDetail'

            )
        );
    }
    public function  notificationajaxcall()
    {
        $user_id = Auth::user()->user_id;

        $notificationData = NotificationMaster::where('crm_user_id', $user_id)
        ->with('criteriapath')
            ->orderBy('notification_id', 'DESC')
            ->take(50)
            ->latest()
            ->get();
        $notificationDataNew = NotificationMaster::where('is_read', 0)
            ->where('crm_user_id', $user_id)->orderBy('notification_id', 'DESC')
            ->count();
        $returnHTML = view('Components.notificationview', compact(
            'notificationDataNew',
            'notificationData',
        ))->render();
        return $returnHTML;
    }
    public function getnotificationlist()
    {
        $user_id = Auth::user()->user_id;
        $notificationData = NotificationMaster::where('crm_user_id', $user_id)
            ->orderBy('notification_id', 'DESC')
            ->take(50)
            ->latest()
            ->get();
        return DataTables::of($notificationData)
            ->editColumn('created_at', function ($userinfo) {
                return $userinfo->created_at->format('d-m-Y H:i:s');
            })
            ->editColumn('updated_at', function ($userinfo) {
                return $userinfo->updated_at->format('d-m-Y H:i:s');
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function enduserupdate(Request $request): JsonResponse
    {
        try {
            $user_id = $request->input('user_id');
            $Arr = [];
            $MongoArr = [];
            $firstname = $request->input('firstname');
            $Arr['firstname'] = $firstname;
            $MongoArr['firstname'] = $firstname;
            $MongoArr['user_id'] = (int)$user_id;
            $lastname = $request->input('lastname');
            $MongoArr['lastname'] = $lastname;
            $Arr['lastname'] = $lastname;
            $mobile_number = $request->input('mobile_number');
            $Arr['mobile_number'] = $mobile_number;
            $MongoArr['mobile_number'] = $mobile_number;
            $whatsapp_number = $request->input('whatsapp_number');
            $MongoArr['whatsapp_number'] = $whatsapp_number;
            $Arr['whatsapp_number'] = $whatsapp_number;
            $company_name = $request->input('company_name');
            $Arr['company_name'] = $company_name;
            $website_url = $request->input('website_url');
            $Arr['website_url'] = $website_url;
            $address = $request->input('address');
            $Arr['address'] = $address;
            $city_id = $request->input('city_id');
            $Arr['city_id'] = $city_id;
            $MongoArr['city_id'] = $city_id;
            $pincode = $request->input('pincode');
            $Arr['pincode'] = $pincode;
            $user_type = $request->input('user_type');
            $Arr['user_type'] = $user_type;
            $MongoArr['user_type'] = $user_type;
            $category_id = $request->input('category_id');
            $Arr['category_id'] = $category_id;
            $email = $request->input('email');
            $Arr['email'] = $email;
            $MongoArr['email'] = $email;
            SqFtUser::where('id', $user_id)->update($Arr);
            $url = env("MONGO_DB_API_URL") . 'api/v1/users';
            Http::withHeaders(['Accept' => "application/json", "Authorization" => 'Bearer ' . env('MONGO_API_KEY')])->post($url, $MongoArr);
            return updatedJsonResponse(["message" => "Profile updated successfully"]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function getstate(Request $request): JsonResponse
    {
        try {

            $stateid = $request->input('stateid');
            $citydata = City::where('state_id', $stateid)
                ->where('is_active', 1)->get();
            return successJsonResponce([
                'message' => "success.",
                'data' => $citydata
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    public function getteamlead(Request $request): JsonResponse
    {
        try {
            $teamleaddata  = DB::table('users')
                ->where('is_active', 1)
                // ->join('roles', 'users.role_id', '=', 'roles.role_id')
                ->where('role_type', 1)->get();
            return successJsonResponce([
                'message' => "success.",
                'data' => $teamleaddata
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    public function create(Request $request): JsonResponse
    {
        try {
            $username = $request->input('username');
            $firstname = $request->input('firstname');
            $lastname = $request->input('lastname');
            $role = $request->input('role');
            $useremail = $request->input('useremail');
            $city = $request->input('city');
            $state = $request->input('state');
            $teamlead = $request->input('teamlead');
            $mobilenumber = $request->input('mobilenumber');
            $assignlimit = $request->input('assignlimit');
            $user_id = $request->input('user_id');
            $role_type = Roles::where('role_id', $role)->first();
            $roleType = $role_type['role_type_id'];
            $password = $request->input('password');
            $password = Hash::make($password);
            $userdata = [
                'password' =>$password,
                "role_id" => $role,
                'role_type' => $roleType,
                'username' => $username,
                'first_name' => $firstname,
                'last_name' => $lastname,
                'assign_limit' => $assignlimit,
                'email' => $useremail,
                'city_id' => $city,
                'state_id' => $state,
                'team_lead_id' => $teamlead,
                'mobilenumber' => $mobilenumber
            ];
            if ($request->hasFile('filedata')) {
                $fileNamefrontfile = strtolower(removeSpacesFromFileName($request->file('filedata')->getClientOriginalName(), 'profile'));
                $filename = 'profileimage/' . $fileNamefrontfile;
                putObjectToS3($request->file('filedata'), $filename);
                $userdata['image_name'] = $filename;
            }
            DB::beginTransaction();
            if ($user_id == 0) {
                if (User::where('username', '=', $username)->count() == 0) {
                    $userdata['is_active'] = 1;
                    $userdata['is_online'] = 1;
                    User::create($userdata);
                    DB::commit();
                    return createdJsonResponse();
                } else {
                    return badRequestJsonResponse(['message' => 'Username already exists']);
                }
            } else {
                if (User::where('username', '=', $username)->count() == 0) {
                    User::where('user_id', $user_id)->update($userdata);
                    DB::commit();
                    return updatedJsonResponse();
                } else {
                    if (User::where('user_id', $user_id)->where('username', '=', $username)->count() == 1) {
                        User::where('user_id', $user_id)->update($userdata);
                        DB::commit();
                        return updatedJsonResponse();
                    } else {
                        return badRequestJsonResponse(['message' => 'Username already exists']);
                    }
                }
            }
            // }
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function statusupdate(Request $request): JsonResponse
    {
        try {
            $status = $request->input('status');
            $user_id = $request->input('id');
            DB::beginTransaction();
            if ($status == 0) {
                $Arr = ['is_active' => $status, 'is_online' => 0];
            } else {
                $Arr = ['is_active' => $status];
            }
            User::where('user_id', $user_id)->update($Arr);
            DB::commit();
            return updatedJsonResponse();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    public function getuserdetail(Request $request): JsonResponse
    {
        try {
            $user_id = $request->input('id');
            $fromdate =  Carbon::parse($request->input('fromdate') . " 00:00:00");
            $todate =  Carbon::parse($request->input('todate') . " 23:59:59");
            $getrole = User::where('user_id', $user_id)->first();
            $role_id = $getrole['role_id'];
            $getPermission = getPermissionAndAccess(0, $role_id);
            $getDashData = getUserDahboardData("", [$user_id], $getPermission, $fromdate, $todate);
            return updatedJsonResponse(['data' => $getDashData]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function teamleadgetuserdetail(Request $request): JsonResponse
    {
        try {
            $user_id = $request->input('id');
            $fromdate =  Carbon::parse($request->input('fromdate') . " 00:00:00");
            $todate =  Carbon::parse($request->input('todate') . " 23:59:59");
            $value = User::where('user_id', $user_id)->first();
            $PermissionDetails = getPermissionAndAccess(0, $value['role_id']);
            $getCRMUserID = User::where('team_lead_id', $value['user_id'])->get();
            $userData = [];
            foreach ($getCRMUserID as $key => $val) {
                $Arr = getUserDahboardData(1, [$val['user_id']], $PermissionDetails, $fromdate, $todate);
                $val['permissionDetail'] = $Arr;
                array_push($userData, $val);
            }
            return updatedJsonResponse(['data' => $userData]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    public function profileupdate(Request $request): JsonResponse
    {
        try {

            $firstname = $request->input('firstname');
            $lastname = $request->input('lastname');
            $user_id = $request->input('profile_user_id');
            $useremail = $request->input('useremail');
            $userdata = [
                'first_name' => $firstname,
                'last_name' => $lastname,
                'email' => $useremail
            ];
            if ($request->hasFile('filedata')) {
                $fileNamefrontfile = strtolower(removeSpacesFromFileName($request->file('filedata')->getClientOriginalName(), 'profile'));
                $filename = 'profileimage/' . $fileNamefrontfile;
                putObjectToS3($request->file('filedata'), $filename);
                $userdata['image_name'] = $filename;
            }
            DB::beginTransaction();
            User::where('user_id', $user_id)->update($userdata);
            DB::commit();
            return updatedJsonResponse(["message" => "Profile updated successfully"]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function deleteprofilepic(Request $request): JsonResponse
    {
        try {
            $user_id = $request->input('id');
            DB::beginTransaction();
            $getuser = User::where('user_id', $user_id)->first();
            $image_name = $getuser['image_name'];
            Storage::disk('s3')->delete($image_name);
            User::where('user_id', $user_id)->update(['image_name' => null]);
            DB::commit();
            return updatedJsonResponse();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
}
