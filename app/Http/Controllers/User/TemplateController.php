<?php

namespace App\Http\Controllers\User;

use App\EventBridgeClientHelper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Models\schedule\AdminPollsTiming;
use App\Models\Master\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\Setting\CriteriaMasters;
use App\Models\Setting\NotificationMaster;

class TemplateController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function criteriaSchedules()
    {

        $data = [];

        $data['cities'] = City::query()
            ->select([
                'id',
                'title'
            ])
            ->orderBy('title', 'ASC')
            ->where('is_active', 1)
            ->get()
            ->toArray();
        $scenario = "Schedule Polling";
        $content = "Edit the schedule as per the requirement";
        $id = '';
        $criteriaId = "";
        $user = Auth::user();
        $user_id = $user->user_id;
        $id = $user_id;
        $role_type = $user->role_type;
        $role_id = $user->role_id;
        $PermissionDetail = getPermissionAndAccess(0, $role_id);
        $data['criteria'] = CriteriaMasters::all();
        $data['rule_names'] = (new EventBridgeClientHelper())->getRuleNames();
        $notificationData = NotificationMaster::where('crm_user_id', $user_id)
            ->orderBy('notification_id', 'DESC')
            ->get();
        $notificationDataNew = NotificationMaster::where('is_read', 0)
        ->where('crm_user_id', $user_id)->orderBy('notification_id', 'DESC')
        ->count();
        return view('User.schedules', compact(
            'data',
            'scenario',
            'criteriaId',
            'id',
            'PermissionDetail',
            'content',
            'notificationDataNew',
            'notificationData'
        ));
    }

    public function getScheduleDetail(Request $request)
    {
        $query = AdminPollsTiming::query()
            ->select([
                'poll_timing_id',
                'criteria_id',
                'rule_name',
                'city_id',
            ]);
        if (((int) $request->input('criteria_id')) != -1)
            $query = $query->where('criteria_id', '=', (int) $request->input('criteria_id'));
        if (($request->input('rule_name')) != -1)
            $query = $query->where('rule_name', '=', $request->input('rule_name'));
        if (((int) $request->input('city_id')) != -1)
            $query = $query->where('city_id', '=', (int) $request->input('city_id'));

        $schedules = $query
            ->with(['criteriamaster' => function ($query) {
                $query->select(['criteria_id', 'criteria_name']);
            }])
            ->with(['city' => function ($query) {
                $query->select(['id', 'title']);
            }])
            ->get();
        $message = "Data fetched successfully";
        $criteriaKeys = CriteriaMasters::all();
        return response()->json(compact('schedules', 'criteriaKeys', 'message'));
    }

    public function storeOrEdit(Request $request): JsonResponse
    {

        try {
            $criteriaId =   (int) $request->input('criteria_id');
            $cityId =   (int) $request->input('city_id');
            $ruleName = (string) $request->input('rule_name');
            $Id = (int) $request->input('id');
            DB::beginTransaction();
            if ($criteriaId == -1 || $cityId == -1 || $ruleName == -1) {
                return updatedJsonResponse(['code' => 201, 'message' => 'Please select valid inputs']);
            }
            if ($request->input('id')) {
                AdminPollsTiming::where('poll_timing_id', $Id)->update([
                    'rule_name' => $ruleName
                ]);
                DB::commit();
                return updatedJsonResponse(['code' => 200, 'message' => 'Record updated successfully']);
            } else {
                if (AdminPollsTiming::where('criteria_id', $criteriaId)->where('city_id', $cityId)->count() == 0) {
                    AdminPollsTiming::create([
                        'criteria_id' => $criteriaId,
                        'rr_rule_name' => Config::get('constants.SCHEDULING_RULE.ROUND_ROBIN'),
                        'followup_rule_name' => Config::get('constants.SCHEDULING_RULE.FOLLOW_UP'),
                        'city_id' => $cityId,
                        'rule_name' => $ruleName
                    ]);
                    DB::commit();
                    return createdJsonResponse(['code' => 200, 'message' => 'Record created successfully']);
                } else {
                    DB::commit();
                    return createdJsonResponse(['code' => 201, 'message' => 'Record already exists']);
                }
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
}
