<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;

class PerformanceTestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(
            'serviceperformancetest',
            'propertyperformancetest'
        );
    }

    public function serviceperformancetest()
    {
        $ServiceAreaPath = '../tests/service/servicearea.json';
        $ServiceArea = json_decode(file_get_contents($ServiceAreaPath), true);

        $UserPath = '../tests/service/user.json';
        $Users = json_decode(file_get_contents($UserPath), true);
        $SubCateoryPath = '../tests/service/servicesubcategory.json';
        $SubCategory = json_decode(file_get_contents($SubCateoryPath), true);
        $RandomString = ['Testing', 'Servicet testing', 'Plumber service', 'Test service'];
        $serviceArr = [];
        for ($i = 0; $i < 50; $i++) {

            //Description
            $index = array_rand($RandomString);
            $description = $RandomString[$index];

            //subcategory and category name
            $getSubandCategoryIndex = array_rand($SubCategory);
            $sub_category_id = $SubCategory[$getSubandCategoryIndex]['id'];
            $category_id = $SubCategory[$getSubandCategoryIndex]['service_category_id'];
            //user list
            $userIndex = array_rand($Users);
            $userDetail = $Users[$userIndex];
            $deviceToken = $userDetail['android_device_token'];
            $registeredId = $userDetail['android_reg_device_id'];
            $userId = $userDetail['id'];
            $Arr = [
                "AccessToken" => "MjUyLTg1REEyUzMtQURTUzVELUVJNUI0QTIyMTE=",
                "Areas" => $ServiceArea,
                "Benefits" => "",
                "CityId" => "1172",
                "Delivery" => "120",
                "MainImage" => "",
                "hours" => '5',
                "DeviceToken" => $deviceToken,
                "MaxPrice" => "4500",
                "MinPrice" => "2000",
                "Offers" => "",
                "PaymentModeIds" => [
                    [
                        "PaymentModeId" => "1"
                    ],
                    [
                        "PaymentModeId" => "2"
                    ]
                ],
                "Photos" => [],
                "RegisterId" => $registeredId,
                "ServiceId" => "0",
                "ServiceTypeId" => $category_id,
                "ShortDescription" => $description,
                "SubCategoryId" => $sub_category_id,
                "UserId" => $userId,
                "Warranty" => ""
            ];
            array_push($serviceArr, $Arr);
        }
        return $serviceArr;
    }

    public function propertyperformancetest()
    {

        $UserPath = '../tests/property/user.json';
        $Users = json_decode(file_get_contents($UserPath), true);

        $SubCateoryPath = '../tests/property/propertysubcategory.json';
        $SubCategory = json_decode(file_get_contents($SubCateoryPath), true);

        $RandomString = ['Testing', 'property testing', 'property testing', 'Test property'];
        $propertyArr = [];
        for ($i = 0; $i < 50; $i++) {

            //Description
            $index = array_rand($RandomString);
            $description = $RandomString[$index];

            //subcategory and category name
            $getSubandCategoryIndex = array_rand($SubCategory);
            $sub_category_id = $SubCategory[$getSubandCategoryIndex]['id'];
            $category_id = $SubCategory[$getSubandCategoryIndex]['property_category_id'];
            $property_type_id = $SubCategory[$getSubandCategoryIndex]['property_type_id'];
            //user list
            $userIndex = array_rand($Users);
            $userDetail = $Users[$userIndex];
            $deviceToken = $userDetail['android_device_token'];
            $registeredId = $userDetail['android_reg_device_id'];
            $userId = $userDetail['id'];
            $Arr = [
                "AccessToken" => "eyJraWQiOiJtK2JUV0RJZ0VXaGVQTFUzV0s1cEo1R1BJdVwvQjJqUUdIUnVTYjJRMXh5ND0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIwMTdhM2EwYy03ZjJiLTQyMjEtOGVlMS0yZDQ4N2RmODZhMzEiLCJldmVudF9pZCI6ImE3ZDlhNzZlLWY1NTUtNDllMi05NDIxLTk4ODBlZjE2NzU1YSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1OTc5OTA3MzEsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5hcC1zb3V0aC0xLmFtYXpvbmF3cy5jb21cL2FwLXNvdXRoLTFfTG5wVUF6TmNKIiwiZXhwIjoxNTk4MDA2NDE0LCJpYXQiOjE1OTgwMDI4MTQsImp0aSI6IjQ0ZDcwNzY5LThjNWEtNDYzMC05NzRlLWI1YmE4NjljNjc2YSIsImNsaWVudF9pZCI6IjU0ZHE3cGJvNjBxNXUxZ3Y0cHI5dXNoM2gwIiwidXNlcm5hbWUiOiI4OTQwMTYxMTEzIn0.LhZricxSPufC3CpnaUklNL9dChe7vYuw3tRAxHhhazuY3pLQRTw4mAOtpKK2eP_ww3ftcFHbjqj3pTg5VMTm-OZ_oCBdgi9BW9SLdeCIoQJ3N2a65yGSAtq2J391KqCVO3SNvhXO2irWYh2qUguUbz9Lmp1XuUxrhK09neZQzT90bGIMAmt38ulOhgMk_8M3367Vs_RuOUDOrr3pDetPYhpa3NrfZQAV51vw7oJtcG33tCiANjbhunA9NGBoQDWI1Ho_Mr0FfLy-Xa0mYWAHVtPxNP-R-xyU_cs5Ht1zA6EwgUH1PqLf_70I8Rr6FwgPLEKVlJjEvN2uANhwbcBAtA",
                "RegisterId" => $registeredId,
                "DeviceToken" => $deviceToken,
                "BuilderName" => "",
                "RoomType" => "Single Room",
                "PropertyAge" => "",
                "Photos" => [],
                "BathroomId" => 3,
                "GateClosingTime" => "",
                "FurnishedTypeId" => "2",
                "TenantType" => [
                    [
                        "TenantTypeId" => "Boys"
                    ]
                ],
                "IdealForBusiness" => [],
                "WidthOfRoad" => "",
                "TransactionTypeId" => "",
                "PGType" => [],
                "Longitude" => "",
                "FloorTypeId" => "1",
                "PGRoomTypeId" => "",
                "Description" => $description,
                "AvailableFrom" => "11/08/2020",
                "AreaUnit" => "",
                "ExpectedDurationOfMonth" => "",
                "FlatmatePropertyType" => "Independent House or Villa",
                "Latitude" => "",
                "NonVegAllowed" => "Yes",
                "PropertyId" => "",
                "ExpectedDurationOfYear" => "",
                "PropertyTypeId" => $property_type_id,
                "ConstructionStatusId" => "2",
                "MainImage" => "",
                "UserId" => $userId,
                "Rent" => "",
                "SecurityDeposit" => "10000",
                "PGORHostelName" => "",
                "IsPersonalWashroom" => "",
                "CafetAreaId" => "",
                "AttachBathroom" => "",
                "BalconyId" => -1,
                "PreferredTenant" => [
                    [
                        "Value" => "Student"
                    ],
                    [
                        "Value" => "Professional"
                    ]
                ],
                "CategoryTypeId" => $category_id,
                "CommonAmenities" => [
                    [
                        "Value" => "WIFI"
                    ],
                    [
                        "Value" => "LIFT"
                    ]
                ],
                "BoundaryWallMade" => "",
                "MaintenanceCharges" => "",
                "Parking" => [
                    [
                        "Value" => "Car"
                    ],
                    [
                        "Value" => "Bike"
                    ]
                ],
                "NoOfOpenSides" => "",
                "SubCategoryId" => $sub_category_id,
                "Location" => [
                    "AreaId" => "797",
                    "Address" => "DLF SEZ",
                    "Pincode" => "600020",
                    "CityId" => "1172"
                ],
                "CornerPlot" => "",
                "MonthlyRent" => "5000",
                "PGRules" => [],
                "FoodFacility" => [],
                "BuiltupArea" => "2000",
                "CarpetArea" => "",
                "BHKId" => -1,
                "Price" => "",
                "Negotiable" => "1",
                "GatedSecurity" => "Yes",
                "TokenAdvance" => "",
                "RoomAmenities" => [
                    [
                        "Value" => "TV"
                    ],
                    [
                        "Value" => "AC"
                    ]
                ],
                "PlotArea" => ""
            ];
            array_push($propertyArr, $Arr);
        }
        return $propertyArr;
    }
}
