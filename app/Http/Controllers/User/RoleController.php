<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Setting\NotificationMaster;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\User\Roles;
use App\Models\User\RoleTypes;
use App\User;
use Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;


class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // start - to show index page of the user creation - listview
    public function index()
    {
        try {
            $scenario = "Roles";
            $content = "Role creation panel";
            $user_id = Auth::user()->user_id;
            $role_type = RoleTypes::all();
            $id='';
            if (request()->ajax()) {
                $roles = Roles::with('roletypes')->orderby('role_id', 'desc')
                    ->where('role_title', "!=", "Super Admin")
                    ->get();
                return DataTables::of($roles)
                    ->addColumn('action', function ($roles) {
                        return '<td><a href="permissions/' . $roles->role_id . '"
                                class="btn btn-primary btn-icon submit permission" data-placement="top" data-tooltip="tooltip" data-placement="top" title="Permission"> <i
                                        data-feather="lock" style="margin-top:7px;"></i></a>
                                <button type="button" id=' . $roles->role_id . ' data-tooltip="tooltip" data-placement="top" title="Delete"
                                class="btn btn-danger delete btn-icon submit"> <i
                                        data-feather="trash-2"></i></button>
                                        <button type="button" id=' . $roles->role_id . ' data-tooltip="tooltip" data-placement="top" title="Edit" data-toggle="modal"
                                    data-target="#exampleModaledit" class="edit btn btn-primary btn-icon submit"> <i
                                        data-feather="edit-2"></i></button></td>';
                    })->addColumn('noofuser', function ($roles) {
                        return User::where('role_id', $roles->role_id)->count();
                    })->addColumn('role_type', function ($roles) {
                        return $roles->roletypes[0]['title'];
                    })->addColumn('role_type_id', function ($roles) {
                        return $roles->roletypes[0]['role_type_id'];
                    })->editColumn('created_at', function ($roles) {
                        return $roles->created_at->format('d-m-Y H:i:s');
                    })->editColumn('updated_at', function ($roles) {
                        return Carbon::createFromFormat('Y-m-d H:i:s', $roles->updated_at)
                            ->setTimezone('Asia/Calcutta')
                            ->format('d-m-Y H:i:s');
                    })

                    ->rawColumns(['action', 'noofuser'])
                    ->make(true);
            }
            $criteriaId = "";
            $notificationData = NotificationMaster::where('crm_user_id', $user_id)
            ->orderBy('notification_id', 'DESC')
            ->get();
            $notificationDataNew = NotificationMaster::where('is_read', 0)
            ->where('crm_user_id', $user_id)->orderBy('notification_id', 'DESC')
            ->count();
            return view('User/role_listview', compact(
                'scenario',
                'content',
                'criteriaId',
                'role_type',
                'id',
                'notificationDataNew',
                'notificationData'
            ));
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    //end - to show index page of the user creation - listview

    public function create(Request $request): JsonResponse
    {
        try {
            $role_title = $request->input('role_title');
            $role_id = $request->input('role_id');
            $role_type_id = $request->input('roletype');

            if ($role_id == 0) {
                if (Roles::where('role_title', '=', $role_title)->count() == 0) {
                    DB::beginTransaction();
                    Roles::create([
                        'role_title' => $role_title,
                        'role_type_id' => $role_type_id
                    ]);
                    DB::commit();
                    return createdJsonResponse();
                } else {
                    return badRequestJsonResponse(['message' => 'Role already exists']);
                }
            } else {
                if (Roles::where('role_title', '=', $role_title)->count() == 0) {
                    DB::beginTransaction();
                    Roles::where('role_id', $role_id)->update([
                        "role_title" => $role_title,
                        'role_type_id' => $role_type_id
                    ]);
                    User::where('role_id', $role_id)->update([
                        'role_type' => $role_type_id
                    ]);
                    DB::commit();
                    return updatedJsonResponse();
                } else {
                    if (Roles::where('role_id', $role_id)->where('role_title', '=', $role_title)->count() == 1) {
                        DB::beginTransaction();
                        Roles::where('role_id', $role_id)->update([
                            "role_title" => $role_title,
                            'role_type_id' => $role_type_id

                        ]);
                        User::where('role_id', $role_id)->update([
                            'role_type' => $role_type_id
                        ]);
                        DB::commit();
                        return updatedJsonResponse();
                    } else {
                        return badRequestJsonResponse(['message' => 'Role already exists']);
                    }
                }
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    public function delete(Request $request): JsonResponse
    {
        try {
            $role_id = $request->input('role_id');
            Roles::where('role_id', $role_id)->delete();
            User::where('role_id', $role_id)->delete();
            return successJsonResponce([
                'message' => "Selected role deleted successfully.",
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
}
