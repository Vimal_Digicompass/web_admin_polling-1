<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Exception;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('Auth.changepassword');
    }


    public function changepasswordupdate(Request $request):JsonResponse{

        try{
            $input = $request->only(['oldpassword', 'password']);
            $oldpassword = $input['oldpassword'];
            $password = $input['password'];
            $password = Hash::make($password);
            $userID = Auth::user()->user_id;
            if (Auth::attempt(['user_id' => $userID, 'password' => $oldpassword])) {
                User::where("user_id", $userID)->update(array('password' => $password));
                Auth::logout();
                return successJsonResponce([
                    'message' => "success",
                    'nextpage' => "/",
                ]);
            } else {
                return successJsonResponce(['message' => 'Incorrect old password']);

            }
        }catch(Exception $e){
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }

    }
}
