<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\User;
use Exception;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    public function __construct()
    {
        $this->middleware('guest')->except(
            'index',
            'sendresetlink',
            'updatepassword',
            'resetpassword',
        );
    }

    public function index()
    {
        return view('Auth.forgotpassword');
    }


    public function sendresetlink(Request $request): JsonResponse
    {

        try {

            $input = $request->only(['username']);
            $username = $input['username'];
            $getusernamedata = User::where("username", $username)
                ->where('is_active', 1)->get();
            if (count($getusernamedata) == 0) {
                return successJsonResponce([
                    'message' => "Username does not exists"
                ]);
            } else {
                $user_id = $getusernamedata[0]['user_id'];
                $username = $getusernamedata[0]['username'];
                $email = $getusernamedata[0]['email'];
                $encryptUser_Id = Crypt::encrypt($user_id);
                $subject = Config::get('constants.EMAIL_SUBJECT.RESET_PASSWORD');
                $path = Config::get('constants.EMAIL_SUBJECT.RESET_PASSWORD_PATH') . $encryptUser_Id;
                Mail::send(
                    'Email.resetemail',
                    [
                        'username' => $username,
                        'path' => $path
                    ],
                    function ($message) use ($email, $subject) {
                        $message->to($email)->subject($subject);
                    }
                );
                return successJsonResponce([
                    'message' => "Password Reset Link send to Email Address",
                ]);
            }

        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    public function updatepassword($id)
    {
        try {
            $id = Crypt::decrypt($id);
            return view('Auth.updatepassword', compact('id'));
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    public function resetpassword(Request $request): JsonResponse
    {

        try {
            $input = $request->only(['password', 'id']);
            $password = $input['password'];
            $id = $input['id'];
            $password = Hash::make($password);
            User::where("user_id", $id)->update(array('password' => $password));
            return successJsonResponce([
                'message' => "success",
                'nextpage' => "/",
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
}
