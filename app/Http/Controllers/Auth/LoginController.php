<?php

namespace App\Http\Controllers\Auth;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\User\UserLoggedinActivity;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except(
            'logout',
            'userLoginActivity',
            'updateonlinestatus'
        );
    }


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index()
    {
        return view('Auth.login');
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }

        $user = Auth::user();
        $user_id = $user->user_id;
        $this->userLoginActivity($user_id, 'login');
        User::where('user_id', $user_id)->update(['is_online' => 1]);
        Auth::login($user, $remember = true);
        return successJsonResponce([
            'message' => "success",
            'nextpage' => $this->redirectPath()
        ]);
    }
    public function username()
    {
        return 'username';
    }

    protected function credentials(Request $request)
    {
        // echo "hai";
        // exit;
        return array_merge($request->only($this->username(), 'password'), ['is_active' => 1]);
    }
    public function updateonlinestatus(Request $request): JsonResponse
    {
        try {

            $onlinestatus = $request->input('onlinestatus');
            $user_id = Auth::user()->user_id;
            User::where('user_id', $user_id)->update(['is_online' => $onlinestatus]);
            return successJsonResponce([
                'message' => "success.",
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    public function logout(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->user_id;
        $this->userLoginActivity($user_id, 'logout');
        $this->guard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        if ($response = $this->loggedOut($request)) {
            return $response;
        }
        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect('/');
    }

    public function userLoginActivity($userid, $type)
    {
        try {

            $today = Carbon::today()->copy()->startOfDay()->format('d-m-Y');
            $unixtime = Carbon::today()->timestamp;

            $time = Carbon::now()->format('H:i:s');
            if ($type == "login") {
                UserLoggedinActivity::create([
                    "user_id" => $userid,
                    'is_online' => 1,
                    'date' => $today,
                    'date_unixtime' => $unixtime,
                    'login_time' => $time
                ]);
            } else {
                $getid =  UserLoggedinActivity::where("date_unixtime", $unixtime)
                    ->where('user_id', $userid)
                    ->orderBy('user_log_id', 'desc')
                    ->first();
                if ($getid) {
                    $getuserlogid = $getid['user_log_id'];
                    UserLoggedinActivity::where('user_log_id', $getuserlogid)->update(['is_online' => 0, 'logout_time' => $time]);
                }
                User::where('user_id', $userid)->update(['is_online' => 0]);
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
}
