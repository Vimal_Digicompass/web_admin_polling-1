<?php

namespace App\Http\Controllers\KYC;

use Exception;
use Illuminate\Support\Facades\Http;
use App\Models\UserActivity\KYCApproval\KYCApprovalAssign;
use App\Models\UserActivity\KYCRejected\KYCRejectedAssign;
use App\Models\UserActivity\KYCPending\KYCPendingArcheive;
use App\Models\UserActivity\KYCApproval\KYCApprovalArcheive;
use App\Models\UserActivity\KYCRejected\KYCRejectedArcheive;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\KYCRejectedStatus;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\UserActivity\KYCPending\KYCPendingChangelog;
use App\Models\UserActivity\KYCApproval\KYCApprovalChangelog;
use App\Models\UserActivity\KYCRejected\KYCRejectedChangelog;
use App\Models\UserActivity\KYCPending\KYCPendingAssign;
use Illuminate\Support\Facades\Config;
use App\Models\SqFt\UserKYCDetail;
use App\Models\SqFt\User;
use App\Models\User as MongoUser;


class KYCController extends Controller
{

    public function getkycdocument(Request $request): JsonResponse
    {

        try {
            $html = prepareKYCDocument($request);
            return createdJsonResponse(['data' => $html]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function getkycrejectionstatus(Request $request): JsonResponse
    {
        try{
            $getRejectReason =  KYCRejectedStatus::all();
            return createdJsonResponse(['data' => $getRejectReason]);
        }catch(Exception $e){
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message'=>$e->getMessage()]));
        }
    }
    public function approverejectkycdoc(Request $request): JsonResponse
    {
        try {

            $id = $request->input('id');
            $explodeId = explode("_", $id);
            $user_id = $explodeId[1];
            $status = $explodeId[2];
            prepareKYCDocumentApproveReject($request);
            $updateArr = [
                'updatestatus' => 1
            ];
            DB::beginTransaction();
            if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_APPROVAL')) {
                KYCApprovalAssign::where('user_id', $user_id)->update($updateArr);
                KYCApprovalArcheive::where('user_id', $user_id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_PENDING')) {
                KYCPendingAssign::where('user_id', $user_id)->update($updateArr);
                KYCPendingArcheive::where('user_id', $user_id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
            } else {
                KYCRejectedAssign::where('user_id', $user_id)->update($updateArr);
                KYCRejectedArcheive::where('user_id', $user_id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
            }
            DB::commit();
            if ($status == 1) {
                return createdJsonResponse(['message' => "Approved successfully"]);
            } else {
                return createdJsonResponse(['message' => "Rejected successfully"]);
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function docdownload(Request $request): JsonResponse
    {
        try {
            $id = $request->input('id');
            $Arr = downloadKYCDoc($id);
            $fileURL = $Arr['fileURL'];
            $fileName = $Arr['fileName'];
            $documentNo = $Arr['documentNo'];
            return createdJsonResponse(['url' => $fileURL, 'documentNo'=> $documentNo ,'filename' => $fileName]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    public function getKYCStatusInfo(Request $request): JsonResponse
    {
        $id = $request->input('id');
        try {
            $getUserDetail = User::where('id', (int) $id)->first();
            if ($getUserDetail) {

                // $is_kyc_updated =    $getUserDetail['is_kyc_updated'];
                // if ($is_kyc_updated == 0) {
                    if ($getUserDetail['user_type'] == "C") {
                        return createdJsonResponse(['code' => 200]);
                    }
                // }

                // else {
                $getUserKYCDetail = UserKYCDetail::where('user_id', $id)->first();
                $panstatus =    $getUserKYCDetail['pan_status'];
                $aadharstatus =     $getUserKYCDetail['aadhar_status'];
                $votingstatus =    $getUserKYCDetail['voting_status'];
                $gststatus  =  $getUserKYCDetail['gst_status'];
                $certificatestatus = $getUserKYCDetail['certificate_status'];
                $drivingstatus = $getUserKYCDetail['driving_status'];
                $chequestatus =  $getUserKYCDetail['cheque_status'];

                if (
                    $panstatus == 0 && $aadharstatus == 0 && $votingstatus == 0
                    && $gststatus == 0 && $certificatestatus == 0 && $drivingstatus == 0
                    && $chequestatus == 0
                ) {
                    return createdJsonResponse(['code' => 205, 'message' => "Upload Documents or Approve/Reject the document to proceed"]);
                }

                if (
                    $panstatus == 2 || $aadharstatus == 2 || $votingstatus == 2
                    || $gststatus == 2 || $certificatestatus == 2 || $drivingstatus == 2
                    || $chequestatus == 2
                ) {
                    return createdJsonResponse(['code' => 200]); //reject
                }
                $aadhar_file_name = $getUserKYCDetail['aadhar_file_name'];
                $pan_file_name = $getUserKYCDetail['pan_file_name'];
                $voting_front_file_name = $getUserKYCDetail['voting_front_file_name'];
                $gst_file_name = $getUserKYCDetail['gst_file_name'];
                $certificate_file_name = $getUserKYCDetail['certificate_file_name'];
                $driving_license_file_name = $getUserKYCDetail['driving_license_file_name'];
                $cancelled_check_file_name = $getUserKYCDetail['cancelled_check_file_name'];
                $Arr = [];
                $index = 0;
                if ($aadhar_file_name != "") {
                    $index++;
                    if ($aadharstatus == 1) {
                        array_push($Arr, "aadhar");
                    }
                }

                if ($pan_file_name != "") {
                    $index++;
                    if ($panstatus == 1) {
                        array_push($Arr, "pan");
                    }
                }

                if ($voting_front_file_name != "") {
                    $index++;
                    if ($votingstatus == 1) {
                        array_push($Arr, "voting");
                    }
                }

                if ($gst_file_name != "") {
                    $index++;
                    if ($gststatus == 1) {
                        array_push($Arr, "gst");
                    }
                }

                if ($certificate_file_name != "") {
                    $index++;
                    if ($certificatestatus == 1) {
                        array_push($Arr, "certificate");
                    }
                }

                if ($driving_license_file_name != "") {
                    $index++;
                    if ($drivingstatus == 1) {
                        array_push($Arr, "driving");
                    }
                }

                if ($cancelled_check_file_name != "") {
                    $index++;
                    if ($chequestatus == 1) {
                        array_push($Arr, "cheque");
                    }
                }

                if (count($Arr)!= 0) {
                    return createdJsonResponse(['code' => 200, 'Arr' => count($Arr), 'index' => $index]); //approve
                } else {
                    return createdJsonResponse(['code' => 201, 'Arr' => count($Arr), 'index' => $index, 'message' => "Upload Documents or Approve/Reject the document to proceed"]);
                }
                // }
            } else {
                return createdJsonResponse(['code' => 201, 'message' => "Upload Documents or Approve/Reject the document to proceed"]);
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function download($documenttype, $id, $filetype)
    {
        try {
            $id = $documenttype . '_' . $id . '_' . $filetype;
            $Arr = downloadKYCDoc($id);
            $fileURL = $Arr['fileURL'];
            $fileName = $Arr['fileName'];
            $contentType = getFileTypeByFileName($fileName);
            ob_end_clean();
            header('Content-type: ' . $contentType);
            header('Content-disposition: attachment; filename="' . $fileName . '"');
            header('Content-Transfer-Encoding: binary');
            header('Pragma: no-cache');
            readfile($fileURL);
            exit;
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }


    public function updatekycdocument(Request $request): JsonResponse
    {
        try {
            $user_id = $request->input('user_id');
            $getUserKYCDetail = UserKYCDetail::where('user_id', $user_id)->first();
            if (!$getUserKYCDetail) {
                UserKYCDetail::create([
                    'user_id' => $user_id,
                ]);
            }
            updateKYCDocument($request);
            User::where('id', $user_id)->update([
                'is_kyc_updated' => 1
            ]);
            MongoUser::where('user_id', $user_id)->update([
                'is_kyc_updated' => 1
            ]);
            $updateArr = [
                'updatestatus' => 1
            ];
            DB::beginTransaction();
            if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_APPROVAL')) {
                KYCApprovalAssign::where('user_id', $user_id)->update($updateArr);
                KYCApprovalArcheive::where('user_id', $user_id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_PENDING')) {
                KYCPendingAssign::where('user_id', $user_id)->update($updateArr);
                KYCPendingArcheive::where('user_id', $user_id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
            } else {
                KYCRejectedArcheive::where('user_id', $user_id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
                KYCRejectedAssign::where('user_id', $user_id)->update($updateArr);
            }
            DB::commit();
            return updatedJsonResponse(['message' => "KYC Document updated successfully"]);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    public function statusupdate(Request $request): JsonResponse
    {
        try {
            $status = $request->input('status');
            $user_id = $request->input('id');
            DB::beginTransaction();
            User::where('id', $user_id)->update(['is_active' => $status]);
            $postBody = [
                'user_id' => (int)$user_id,
                'is_active' => (int) $status
            ];
            $updateArr = [
                'updatestatus' => 1
            ];
            if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_APPROVAL')) {
                KYCApprovalAssign::where('user_id', $user_id)->update($updateArr);
                KYCApprovalArcheive::where('user_id', $user_id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_PENDING')) {
                KYCPendingAssign::where('user_id', $user_id)->update($updateArr);
                KYCPendingArcheive::where('user_id', $user_id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
            } else {
                KYCRejectedArcheive::where('user_id', $user_id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
                KYCRejectedAssign::where('user_id', $user_id)->update($updateArr);
            }
            $url = env("MONGO_DB_API_URL") . 'api/v1/users';
            Http::withHeaders(['Accept' => "application/json", "Authorization" => 'Bearer ' . env('MONGO_API_KEY')])->post($url, $postBody);
            DB::commit();
            return updatedJsonResponse();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function updatekycstatus(Request $request): JsonResponse
    {
        try {
            $status = $request->input('kycstatus');
            $user_id = $request->input('user_id');
            $criteriaId = $request->input('criteriaId');
            $getUserData = User::where('id', $user_id)->first();
            $is_kyc_updated = $getUserData['is_kyc_updated'];
            // $is_kyc_approved = $getUserData['is_kyc_approved'];
            $user_type  = $getUserData['user_type'];
            if ($user_type  != "C") {
                if ($is_kyc_updated == 0) {
                    return updatedJsonResponse(['code' => 201,
                    'message' => "No document to proceed Approve/Reject"]);
                }
            }
            $text = updateKYCstatus($request);
            DB::beginTransaction();
            $updateArr = [
                'updatestatus' => 1
            ];
            if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_APPROVAL')) {
                KYCApprovalAssign::where('user_id', $user_id)->update($updateArr);
                KYCApprovalArcheive::where('user_id', $user_id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_PENDING')) {
                KYCPendingAssign::where('user_id', $user_id)->update($updateArr);
                KYCPendingArcheive::where('user_id', $user_id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
            } else {
                KYCRejectedArcheive::where('user_id', $user_id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
                KYCRejectedAssign::where('user_id', $user_id)->update($updateArr);
            }
            DB::commit();

            $this->changelogKYCStatus($status, $text, $user_id, $criteriaId);
            if ($status == 2) {
                $postBody = [
                    'user_id' => (int)$user_id,
                    'is_kyc_approved' => (int) $status,
                    'poll_kyc_reject_status' => 0
                ];

                $notifyBody = [
                    'user_id' => (int)$user_id,
                    'criteria_id' => (int)Config::get('constants.CRITERION.ON_KYC_REJECTED'),
                    'title' => '',
                    'message_body' => Config::get('constants.NOTIFICATIONCONTENT.KYC_REJECTED'),
                    'type' =>'MyInfoKyc',
                    'linkId' => (int)$user_id
                ];
            } else {
                $postBody = [
                    'user_id' => (int)$user_id,
                    'is_kyc_approved' => (int) $status,
                    'poll_kyc_reject_status' => 1
                ];
                $notifyBody = [
                    'criteria_id' => (int)Config::get('constants.CRITERION.ON_KYC_APPROVED'),
                    'user_id' => (int)$user_id,
                    'title' => '',
                    'message_body' => Config::get('constants.NOTIFICATIONCONTENT.KYC_APPROVAL'),
                    'type' =>'MyInfoKyc',
                    'linkId' => (int)$user_id
                ];
            }
            $url = env("MONGO_DB_API_URL") . 'api/v1/users';
            $urlNotify = env("MONGO_DB_API_URL") . 'api/v1/notification/push/send';
            $header = ['Accept' => "application/json", "Authorization" => 'Bearer ' . env('MONGO_API_KEY')];
            Http::withHeaders($header)->post($url, $postBody);

            Http::withHeaders($header)->post($urlNotify, $notifyBody);
            DB::commit();
            return updatedJsonResponse(['message' => "KYC status updated successfully"]);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function changelogKYCStatus($status, $text, $user_id, $criteriaId)
    {
        $Arr = prepareKYCChangeLog($status, $text, $user_id);
        DB::beginTransaction();
        if ($criteriaId == Config::get('constants.CRITERION.KYC_APPROVAL')) {
            KYCApprovalChangelog::insert($Arr);
        } else if ($criteriaId == Config::get('constants.CRITERION.KYC_PENDING')) {
            KYCPendingChangelog::insert($Arr);
        } else {
            KYCRejectedChangelog::insert($Arr);
        }
        DB::commit();
    }


    public function getrejectstatus(Request $request): JsonResponse
    {
        $user_id = $request->input('id');
        $getuserkycdetail = UserKYCDetail::where('user_id', $user_id)->get();
        return createdJsonResponse(['message' => "", 'data' => $getuserkycdetail]);
    }

    public function getkycapprovestatus(Request $request)
    {
        $user_id = $request->input('id');
        $getkycstatus = User::where('id', $user_id)->first();
        return $getkycstatus['is_kyc_approved'];
    }
    public function getkycdocumentstatus(Request $request)
    {
        $user_id = $request->input('id');
        $usertype  = $request->input('usertype');
        $text = "";
        $value = $request->input('usercategory');
        $selectdocuments = $request->input('selectdocument');
        $getuserkycdetail = UserKYCDetail::where('user_id', $user_id)->first();
        if ($getuserkycdetail) {
            if ($value == 1) {
                if ($usertype != "C") {

                    if (
                        $selectdocuments == 2 ||
                        $selectdocuments == 3 ||
                        $selectdocuments == 5
                    ) {
                        // nothing to do
                    } else {
                        if (
                            $getuserkycdetail['aadhar_no'] == NULL &&
                            $getuserkycdetail['voting_no'] == NULL &&
                            $getuserkycdetail['driving_license_no'] == NULL
                        ) {
                            $text = "Please select any one of the Aadhar, VoterID or Driving License";
                        }
                    }
                } else {
                    if ($selectdocuments = 1) {
                        // nothing to do
                    } else {
                        if ($getuserkycdetail['pan_no'] == NULL) {
                            $text = "Please select the PAN";
                        }
                    }
                }
            } else if ($value == 2) {
                if (
                    $selectdocuments == 2 ||
                    $selectdocuments == 3 ||
                    $selectdocuments == 5
                ) {
                    // nothing to do
                } else {
                    if (
                        $getuserkycdetail['aadhar_no'] == NULL &&
                        $getuserkycdetail['voting_no'] == NULL &&
                        $getuserkycdetail['driving_license_no'] == NULL
                    ) {
                        $text = "Please select any one of the Aadhar, VoterID or Driving License";
                    }
                }
            } else if ($value == 3) {
                if ($selectdocuments == 1 || $selectdocuments == 4 || $selectdocuments == 6) {
                    //nothing to do
                } else {
                    if (
                        $getuserkycdetail['pan_no'] == NULL &&
                        $getuserkycdetail['gst_no'] == NULL &&
                        $getuserkycdetail['cancelled_check_file_name'] == NULL
                    ) {
                        $text = "Please select any one PAN, Cancelled Cheque or GST";
                    }
                }
            } else if ($value == 4 || $value == 5) {
                if (
                    $selectdocuments == 1 ||
                    $selectdocuments == 4 ||
                    $selectdocuments == 6 ||
                    $selectdocuments == 7
                ) {
                    // nothing to do
                } else {
                    if (
                        $getuserkycdetail['pan_no'] == NULL &&
                        $getuserkycdetail['gst_no'] == NULL &&
                        $getuserkycdetail['cancelled_check_file_name'] == NULL &&
                        $getuserkycdetail['certificate_file_name'] == NULL
                    ) {
                        $text = "Please select any one PAN, Cancelled Cheque, Certificate or GST";
                    }
                }
            }
        }
        return $text;
    }
}
