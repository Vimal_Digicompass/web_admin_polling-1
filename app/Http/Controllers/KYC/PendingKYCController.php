<?php

namespace App\Http\Controllers\KYC;

use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Master\City;
use Exception;
use App\Http\Controllers\Controller;
use App\Exports\KYCExport;
use Illuminate\Http\Request;
use App\Models\Master\UserCategory;
use App\Models\Master\CallStatus;
use App\Models\Master\State;
use App\Models\SqFt\User;
use App\Models\Master\UserType;
use App\User as CRMUser;
use App\Models\UserActivity\KYCPending\KYCPendingAssign;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Config;


class PendingKYCController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $scenario = "Pending";
        $role_id = Auth::user()->role_id;

        $criteriaId = Config::get('constants.CRITERION.KYC_PENDING');
        $content = "User not submitted KYC for approval - Pending from User";
        $commonFunctionData = commonFunctionData($role_id, $criteriaId);
        $state = $commonFunctionData['state'];
        $notificationData = $commonFunctionData['notificationData'];
        $notificationDataNew = $commonFunctionData['notificationDataNew'];
        $userType = $commonFunctionData['userType'];
        $callStatus = $commonFunctionData['callStatus'];
        $KYCStatus = $commonFunctionData['KYCStatus'];
        $KYCRejectedStatus = $commonFunctionData['KYCRejectedStatus'];
        $KYCDocumentList = $commonFunctionData['KYCDocumentList'];
        $UserCategory = $commonFunctionData['UserCategory'];
        $PermissionDetail = $commonFunctionData['PermissionDetail'];
        $checkpermission = $commonFunctionData['checkpermission'];

        if ($checkpermission->count() == 0) {
            return view('error.500');
        }
        return view('Scenario/KYC/pendingkyc', compact(
            'scenario',
            'content',
            'state',
            'userType',
            'callStatus',
            'id',
            'KYCStatus',
            'KYCRejectedStatus',
            'KYCDocumentList',
            'UserCategory',
            'criteriaId',
            'PermissionDetail',
            'notificationData',
            'notificationDataNew'
        ));
    }
    public function getkycpendinglist(Request $request)
    {

        $id = $request->input('id');
        $roleData = CRMUser::where('user_id', $id)->first();
        $role_id = $roleData['role_id'];
        $getCustomerData = $this->getKYCPendingData($request);
        // return $getCustomerData;
        return DataTables::of($getCustomerData)
            ->addColumn('action', function ($getCustomerData) use ($role_id) {
                $criteriaId = Config::get('constants.CRITERION.KYC_PENDING');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_kycupdate = $PermissionDetail[0]['is_kycupdate'];
                if ($is_kycupdate == 0) {
                    return '<td><button id=' . $getCustomerData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                    <button type="button" id=' . $getCustomerData->id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools detailview btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                } else {
                    return '<td><button id=' . $getCustomerData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                    <button id=' . $getCustomerData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Upload Document" class="tools uploaddocument btn btn-danger btn-icon submit"> <i data-feather="upload"></i></button>
                    <button type="button" id=' . $getCustomerData->id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools detailview btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                }
            })
            ->addColumn('kycdocument', function ($getCustomerData)  use ($role_id) {
                // $criteriaId = Config::get('constants.CRITERION.KYC_PENDING');
                // $PermissionDetail = getPermissionAndAccess($criteriaId,$role_id);
                // $is_kycupdate = $PermissionDetail[0]['is_kycupdate'];
                // if ($is_kycupdate == 0) {
                // return '<td><button id=' . $getCustomerData->id . ' disabled type="button" data-tooltip="tooltip" data-placement="top" title="View Document" style="width: 100px;" class="tools viewdocument btn btn-primary  submit">View</button></td>';
                // } else {
                return '<td><button id=' . $getCustomerData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="View Document" style="width: 100px;" class="tools viewdocument btn btn-primary  submit">View</button></td>';
                // }
            })
            ->addColumn('name', function ($getCustomerData) {
                return $getCustomerData->firstname . ' ' . $getCustomerData->lastname;
            })->addColumn('usertype', function ($getCustomerData) {
                $usertype = UserType::where('titlevalue', $getCustomerData->user_type)->first();
                if ($usertype) {
                    return $usertype['title'];
                } else {
                    return "";
                }
            })->addColumn('active', function ($getCustomerData) {
                if ($getCustomerData->is_active == 1) {
                    return ' <td><span class="badge badge-success">ACTIVE</span></td>';
                } else {
                    return ' <td><span class="badge badge-danger">IN-ACTIVE</span></td>';
                }
            })->addColumn('city', function ($getCustomerData) {
                $city = City::where('id', $getCustomerData->city_id)->first();
                if ($city) {
                    return $city['title'];
                } else {
                    return "";
                }
            })->addColumn('state', function ($getCustomerData) {
                $state = City::where('id', $getCustomerData->city_id)->first();
                if ($state) {
                    $stateid = $state['state_id'];
                    $statetitle = State::where('id', $stateid)->first();
                    if ($statetitle) {
                        return $statetitle['title'];
                    } else {
                        return "";
                    }
                } else {
                    return "";
                }
            })
            ->addColumn('remarks', function ($getCustomerData) {
                $getRemarks = KYCPendingAssign::select('remarks')
                    ->where('user_id', $getCustomerData->id)
                    ->first();
                if ($getRemarks) {
                    return $getRemarks['remarks'];
                } else {
                    return '';
                }
            })
            ->addColumn('followup_date', function ($getCustomerData) {
                $getFollowdate = KYCPendingAssign::select('followup_date')
                    ->where('user_id', $getCustomerData->id)
                    ->first();
                if ($getFollowdate['followup_date'] != NULL) {
                    return Carbon::createFromFormat('Y-m-d', $getFollowdate['followup_date'])
                        ->setTimezone('Asia/Calcutta')
                        ->format('d-m-Y');
                } else {
                    return '';
                }
            })->addColumn('mobile_number', function ($getCustomerData)  use ($role_id) {
                $criteriaId = Config::get('constants.CRITERION.KYC_PENDING');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_mobilenumberread = $PermissionDetail[0]['is_mobilenumberread'];
                if ($is_mobilenumberread == 0) {
                    return "-";
                } else {
                    return $getCustomerData->mobile_number;
                }
            })->addColumn('checkbox', function ($getCustomerData) {

                return "";
            })->addColumn('status_id', function ($getCustomerData) {
                $getStatus = KYCPendingAssign::select('status')
                    ->where('user_id', $getCustomerData->id)
                    ->first();
                return $getStatus['status'];
            })->addColumn('crmusername', function ($getCustomerData) {
                $getcrmData = KYCPendingAssign::select('crm_user_id')
                    ->where('user_id', $getCustomerData->id)
                    ->first();
                $crm_user_id =  $getcrmData['crm_user_id'];
                $crmData = CRMUser::where('user_id', $crm_user_id)->first();
                return $crmData['username'];
            })->addColumn('status', function ($getCustomerData) {
                $getStatus = KYCPendingAssign::select('status')
                    ->where('user_id', $getCustomerData->id)
                    ->first();
                return getCallStatus($getStatus);
            })
            ->addColumn('category', function ($getCustomerData) {
                $category = UserCategory::where('id', $getCustomerData->category_id)->first();
                if ($category) {
                    return $category['title'];
                } else {
                    return "";
                }
            })->editColumn('dateofregistration', function ($getCustomerData) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $getCustomerData->created_at)
                    ->setTimezone('Asia/Calcutta')
                    ->format('d-m-Y');
            })->addColumn('kycstatus', function ($getCustomerData) {
                $is_kyc_updated = $getCustomerData->is_kyc_updated;
                $is_kyc_approved = $getCustomerData->is_kyc_approved;
                $user_type = $getCustomerData->user_type;
                $is_send_for_kyc_request = $getCustomerData->is_send_for_kyc_request;
                if ($user_type == "C") {
                    if ($is_send_for_kyc_request == 0) {
                        return '<span class="badge badge-primary">Document Pending</span>';
                    } else {
                        return '<span class="badge badge-info">Waiting</span>';
                    }
                } else {
                    if ($is_send_for_kyc_request == 0) {
                        if ($is_kyc_approved == 0) {
                            return '<span class="badge badge-info">Waiting</span>';
                        } else if ($is_kyc_approved == 1) {
                            return '<span class="badge badge-success">Approved</span>';
                        } else if ($is_kyc_approved == 2) {
                            return '<span class="badge badge-danger">Rejected</span>';
                        } else {
                            return '<span class="badge badge-primary">Document Pending</span>';
                        }
                    } else {
                        if ($is_kyc_approved == 0) {
                            return '<span class="badge badge-info">Waiting</span>';
                        } else if ($is_kyc_approved == 1) {
                            return '<span class="badge badge-success">Approved</span>';
                        } else if ($is_kyc_approved == 2) {
                            return '<span class="badge badge-danger">Rejected</span>';
                        } else {
                            return "";
                        }
                    }
                }
            })
            ->rawColumns(['action', 'active', 'status', 'remarks', 'kycdocument', 'kycstatus'])
            ->make(true);
    }

    public function getKYCPendingData($request)
    {
        $id = $request->input('id');
        $roleData = CRMUser::where('user_id', $id)->first();
        $role_id = $roleData['role_id'];
        if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.TEAM_LEAD')) {
            $getTeamInfo = CRMUser::select('user_id')->where('team_lead_id', $id)->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
            $getTeamInfo = CRMUser::select('user_id')->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else {
            $user_id = [$id];
        }
        $query = User::query();
        $fromdate =  Carbon::parse($request->input('fromdate') . " 00:00:00");
        $todate =  Carbon::parse($request->input('todate') . " 23:59:59");
        $callstatus = $request->input('status');
        $query = $query->whereBetween('updated_at', array($fromdate, $todate));

        $usertype = $request->input('usertype');
        if ($usertype == "") {
            $getusertype = UserType::pluck('titlevalue');
            $query = $query->whereIn('user_type', $getusertype);
        } else {
            $query = $query->where('user_type', $usertype);
        }
        $usercategory = $request->input('usercategory');
        if ($usercategory == "") {
            $getusercategory = UserCategory::pluck('id');
            $query = $query->whereIn('category_id', $getusercategory);
        } else {
            $query = $query->where('category_id', $usercategory);
        }
        $city = $request->input('city');

        $queryPendingAssign = KYCPendingAssign::query();

        // if ($callstatus != 5) {
        //     $queryPendingAssign = $queryPendingAssign->where('status', $callstatus);
        // }
        if ($callstatus != Config::get('constants.CALL_STATUS.ALL')) {

            $queryPendingAssign = $queryPendingAssign->where('status', $callstatus);
        } else {
            if (Auth::user()->role_type != Config::get('constants.ROLE_TYPE.TEAM_LEAD') || Auth::user()->role_type != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                $queryPendingAssign = $queryPendingAssign->where('status', '!=', Config::get('constants.CALL_STATUS.CLOSED')); //closed
            }
        }
        if ($city == "") {
            $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id);
        } else {
            $query = $query->where('city_id', $city);
            $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id)->where('city_id', $city);
        }
        $queryPendingAssign = $queryPendingAssign->where('updatestatus', '!=', 2);
        $queryPendingAssign = $queryPendingAssign->orderBy('created_at', 'DESC')->get();
        $pluckCustId = $queryPendingAssign->pluck('user_id');
        $query = $query->whereIn('id', $pluckCustId);
        $query = $query->orderBy('created_at', 'DESC')->get();
        $Arr = [];

        foreach ($query as $key => $val) {
            // return $val;
            $kycupdated = $val['is_kyc_updated'];
            $is_send_for_kyc_request = $val['is_send_for_kyc_request'];
            if (($kycupdated == 0 || $kycupdated == 1) && $is_send_for_kyc_request  == 0) {
                $val->hideout = 0;
            } else {
                $update = KYCPendingAssign::where('user_id', $val['id'])->first();
                $updatestatus = $update['updatestatus'];
                if ($updatestatus == 0) {
                    $val->hideout = 1;
                    KYCPendingAssign::where('user_id', $val['id'])->update([
                        'updatestatus' => 3
                    ]);
                } else {
                    if ($updatestatus == 3) {
                        $val->hideout = 1;
                    } else {
                        $val->hideout = 0;
                    }
                }
            }
            array_push($Arr, $val);
        }
        return $Arr;
    }
    public function excelexport(Request $request)
    {
        unlinkfiles('public/reports/');
        $status = $request->input('status');
        $getCallStatus = CallStatus::where('id', $status)->first();
        $strReplace = str_replace(" ", "_", $getCallStatus['title']);
        $fileNamewithDate = $strReplace . '_' . date('d_m_Y') . '.xlsx';
        $url = 'public/reports/' . $fileNamewithDate;
        $urlresponse = "reports/" . $fileNamewithDate;
        $getCustomerData = $this->getKYCPendingData($request);
        $getCustomerData = array_filter($getCustomerData, function ($a) {
            return $a->hideout !== 1;
        });
        if (count($getCustomerData) == 0) {
            return 0;
        } else {
            $text = "KYC Pending Report";
            Excel::store(new KYCExport($getCustomerData, $request, $text), '/' . $url, 'local');
            return '/' . $urlresponse;
        }
    }
}
