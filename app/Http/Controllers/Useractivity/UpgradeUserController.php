<?php

namespace App\Http\Controllers\Useractivity;

use Illuminate\Support\Facades\Config;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Master\CallStatus;
use App\Models\UserMeta;
use App\Models\Master\KYCStatus;
use App\Models\SqFt\User;
use App\Models\Master\State;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Master\City;
use App\Models\Master\UserType;
use App\Exports\UpgradeUserExport;
use App\Models\Master\UserCategory;
use Exception;
use App\User as CRMUser;
use Yajra\DataTables\DataTables;
use App\Models\UserActivity\UpgradeBusiness\UpgradeBusinessAssign;
use Illuminate\Support\Facades\Auth;


use Illuminate\Http\Request;

class UpgradeUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $scenario = "Upgrade to Business User";
        $content = "Commoner - Clicked Upgrade to Business but did not complete Upgrade";
        $role_id = Auth::user()->role_id;
        $criteriaId = Config::get('constants.CRITERION.UPGRADE_TO_BUSINESS_USER');
        $commonFunctionData = commonFunctionData($role_id, $criteriaId);
        $state = $commonFunctionData['state'];
        $userType = $commonFunctionData['userType'];
        $callStatus = $commonFunctionData['callStatus'];
        $KYCStatus = $commonFunctionData['KYCStatus'];
        $KYCRejectedStatus = $commonFunctionData['KYCRejectedStatus'];
        $KYCDocumentList = $commonFunctionData['KYCDocumentList'];
        $UserCategory = $commonFunctionData['UserCategory'];
        $PermissionDetail = $commonFunctionData['PermissionDetail'];
        $checkpermission = $commonFunctionData['checkpermission'];
        $notificationData = $commonFunctionData['notificationData'];
        $notificationDataNew = $commonFunctionData['notificationDataNew'];
        if ($checkpermission->count() == 0) {
            return view('error.500');
        }
        return view(
            'Scenario/Useractivity/upgradeuser',
            compact(
                'scenario',
                'content',
                'state',
                'id',
                'UserCategory',
                'KYCDocumentList',
                'userType',
                'callStatus',
                'KYCStatus',
                'KYCRejectedStatus',
                'criteriaId',
                'PermissionDetail',
                'notificationData',
                'notificationDataNew'
            )
        );
    }
    //
    public function getupgradeuserlist(Request $request)
    {
        $id = $request->input('id');
        $roleData = CRMUser::where('user_id', $id)->first();
        $role_id = $roleData['role_id'];
        $getCustomerData = $this->getUpgradeBusinessData($request);
        return DataTables::of($getCustomerData)
            ->addColumn('action', function ($getCustomerData) use ($role_id) {
                $criteriaId = Config::get('constants.CRITERION.UPGRADE_TO_BUSINESS_USER');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_kycupdate = $PermissionDetail[0]['is_kycupdate'];
                if ($is_kycupdate == 0) { // permission for kyc update is 0
                    return '<td><button id=' . $getCustomerData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                    <button type="button" id=' . $getCustomerData->id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools detailview btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                } else {
                    return '<td><button id=' . $getCustomerData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                    <button id=' . $getCustomerData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Upload Document" class="tools uploaddocument btn btn-danger btn-icon submit"> <i data-feather="upload"></i></button>
                    <button type="button" id=' . $getCustomerData->id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools detailview btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                }
            })
            ->addColumn('kycdocument', function ($getCustomerData) use ($role_id) {

                return '<td><button id=' . $getCustomerData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="View Document" style="width: 100px;" class="tools viewdocument btn btn-primary  submit">View</button></td>';
            })
            ->addColumn('name', function ($getCustomerData) {
                return '<td><a href="javascript:void(0)" id=' . $getCustomerData->id . ' class="click">' . $getCustomerData->firstname . ' ' . $getCustomerData->lastname . '</a></td>';
            })->addColumn('usertype', function ($getCustomerData) {
                $usertype = UserType::where('titlevalue', $getCustomerData->user_type)->first();
                if ($usertype) {
                    return $usertype['title'];
                } else {
                    return "";
                }
            })->addColumn('city', function ($getCustomerData) {
                $city = City::where('id', $getCustomerData->city_id)->first();
                if ($city) {
                    return $city['title'];
                } else {
                    return "";
                }
            })->addColumn('state', function ($getCustomerData) {
                $state = City::where('id', $getCustomerData->city_id)->first();
                if ($state) {
                    $stateid = $state['state_id'];
                    $statetitle = State::where('id', $stateid)->first();
                    if ($statetitle) {
                        return $statetitle['title'];
                    } else {
                        return "";
                    }
                } else {
                    return "";
                }
            })->addColumn('state_id', function ($getCustomerData) {
                $state = City::where('id', $getCustomerData->city_id)->first();
                if ($state) {
                    return $state['state_id'];
                } else {
                    return "";
                }
            })
            ->addColumn('mobile_number', function ($getCustomerData) use ($role_id) {
                $criteriaId = Config::get('constants.CRITERION.UPGRADE_TO_BUSINESS_USER');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_mobilenumberread = $PermissionDetail[0]['is_mobilenumberread'];
                if ($is_mobilenumberread == 0) {
                    return "-";
                } else {
                    return $getCustomerData->mobile_number;
                }
            })->addColumn('remarks', function ($getCustomerData) {
                $getRemarks = UpgradeBusinessAssign::select('remarks')
                    ->where('user_id', $getCustomerData->id)
                    ->first();
                if ($getRemarks) {
                    return $getRemarks['remarks'];
                } else {
                    return '';
                }
            })->addColumn('crmusername', function ($getCustomerData) {
                $getcrmData = UpgradeBusinessAssign::select('crm_user_id')
                    ->where('user_id', $getCustomerData->id)
                    ->first();
                $crm_user_id =  $getcrmData['crm_user_id'];
                $crmData = CRMUser::where('user_id', $crm_user_id)->first();
                return $crmData['username'];
            })
            ->addColumn('followup_date', function ($getCustomerData) {
                $getFollowdate = UpgradeBusinessAssign::select('followup_date')
                    ->where('user_id', $getCustomerData->id)
                    ->first();
                if ($getFollowdate['followup_date'] != NULL) {
                    return Carbon::createFromFormat('Y-m-d', $getFollowdate['followup_date'])
                        ->setTimezone('Asia/Calcutta')
                        ->format('d-m-Y');
                } else {
                    return '';
                }
            })->addColumn('checkbox', function ($getCustomerData) {

                return "";
            })->addColumn('status_id', function ($getCustomerData) {
                $getStatus = UpgradeBusinessAssign::select('status')
                    ->where('user_id', $getCustomerData->id)
                    ->first();
                return $getStatus['status'];
            })->addColumn('status', function ($getCustomerData) {
                $getStatus = UpgradeBusinessAssign::select('status')
                    ->where('user_id', $getCustomerData->id)
                    ->first();
                return getCallStatus($getStatus);
            })
            ->addColumn('category', function ($getCustomerData) {
                $category = UserCategory::where('id', $getCustomerData->category_id)->first();
                if ($category) {
                    return $category['title'];
                } else {
                    return "";
                }
            })->editColumn('dateofregistration', function ($getCustomerData) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $getCustomerData->created_at)
                    ->setTimezone('Asia/Calcutta')
                    ->format('d-m-Y');
            })->addColumn('kycstatus', function ($getCustomerData) {
                $is_kyc_updated = $getCustomerData->is_kyc_updated;
                $is_kyc_approved = $getCustomerData->is_kyc_approved;
                if ($is_kyc_updated == 1 && $is_kyc_approved == 0) {
                    return '<span class="badge badge-info">Waiting</span>';
                } else if ($is_kyc_updated == 1 && $is_kyc_approved == 1) {
                    return '<span class="badge badge-success">Approved</span>';
                } else if ($is_kyc_updated == 1 && $is_kyc_approved == 2) {
                    return '<span class="badge badge-danger">Rejected</span>';
                } else if ($is_kyc_updated == 0 && $is_kyc_approved == 1) {
                    return '<span class="badge badge-warning">Doc without Approved</span>';
                } else if ($is_kyc_updated == 0 && $is_kyc_approved == 0) {

                    return '<span class="badge badge-primary">Document Pending</span>';
                } else {
                    return "";
                }
            })
            ->rawColumns(['action', 'status', 'name', 'remarks', 'kycdocument', 'kycstatus'])
            ->make(true);
    }

    public function getUpgradeBusinessData($request)
    {
        $id = $request->input('id');
        $roleData = CRMUser::where('user_id', $id)->first();

        if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.TEAM_LEAD')) {
            $getTeamInfo = CRMUser::select('user_id')->where('team_lead_id', $id)->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
            $getTeamInfo = CRMUser::select('user_id')->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else {
            $user_id = [$id];
        }
        $query = User::query();

        $status = $request->input('status');
        if ($status == 0) {
            $query = $query->where('is_kyc_updated', 1)->where('is_kyc_approved', 0);
        } else if ($status == 1) {
            $query = $query->where('is_kyc_updated', 1)->where('is_kyc_approved', 1);
        } else if ($status == 2) {
            $query = $query->where('is_kyc_updated', 1)->where('is_kyc_approved', 2);
        } else if ($status == 4) {
            $query = $query->where('is_kyc_updated', 0)->where('is_kyc_approved', 0);
        } else if ($status == 5) {
            $query = $query->where('is_kyc_updated', 0)->where('is_kyc_approved', 1);
        } else if ($status == 6) {
            $query = $query->whereIn('is_kyc_updated', [0, 1])->whereIn('is_kyc_approved', [0, 1, 2]);
        }
        $usertype = $request->input('usertype');
        if ($usertype == "") {
            $getusertype = UserType::pluck('titlevalue');
            $query = $query->whereIn('user_type', $getusertype);
        } else {
            $query = $query->where('user_type', $usertype);
        }
        $usercategory = $request->input('usercategory');
        if ($usercategory == "") {
            $getusercategory = UserCategory::pluck('id');
            $query = $query->whereIn('category_id', $getusercategory);
        } else {
            $query = $query->where('category_id', $usercategory);
        }
        $city = $request->input('city');
        $getUpgradeBusinessData = UpgradeBusinessAssign::query();
        $callstatus = $request->input('searchcallstatus');
        if ($callstatus != Config::get('constants.CALL_STATUS.ALL')) {

            $getUpgradeBusinessData = $getUpgradeBusinessData->where('status', $callstatus);
        } else {
            if (Auth::user()->role_type != Config::get('constants.ROLE_TYPE.TEAM_LEAD') || Auth::user()->role_type != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                $getUpgradeBusinessData = $getUpgradeBusinessData->where('status', '!=', Config::get('constants.CALL_STATUS.CLOSED')); //closed
            }
        }
        if ($city == "") {
            $getUpgradeBusinessData = $getUpgradeBusinessData->select('user_id')
                ->whereIn('crm_user_id', $user_id)
                ->where('updatestatus', '!=', 2);
        } else {
            $query = $query->where('city_id', $city);
            $getUpgradeBusinessData = $getUpgradeBusinessData->select('user_id')
                ->where('city_id', $city)
                ->where('updatestatus', '!=', 2)
                ->whereIn('crm_user_id', $user_id);
        }
        $getUpgradeBusinessData = $getUpgradeBusinessData->orderBy('created_at', 'DESC')->get();
        $pluckCustId = $getUpgradeBusinessData->pluck('user_id');
        $query = $query->whereIn('id', $pluckCustId)->get();
        $Arr = [];
        foreach ($query  as $key => $value) {
            $id = $value['id'];
            $getupgratedate = UserMeta::where('admin_poll_upgrade_status', 1)
                ->where('user_id', $id)->first();
            $enddate = $getupgratedate['upgraded_to_business_at']->toDateTime();
            $dateofupgration = Carbon::parse($enddate)->format('d-m-Y');
            $value->dateofupgrade = $dateofupgration;
            $kycupdated = $value['is_kyc_updated'];
            if ($kycupdated == 0) {
                $value->hideout = 0;
            } else {
                $update = UpgradeBusinessAssign::where('user_id', $value['id'])->first();
                $updatestatus = $update['updatestatus'];
                if ($updatestatus == 0) {
                    $value->hideout = 1;
                    UpgradeBusinessAssign::where('user_id', $value['id'])->update([
                        'updatestatus' => 3
                    ]);
                } else {
                    if ($updatestatus == 3) {
                        $value->hideout = 1;
                    } else {
                        $value->hideout = 0;
                    }
                }
            }
            array_push($Arr, $value);
        }
        $Arr = collect($Arr);
        return $Arr;
    }

    public function excelexport(Request $request)
    {
        unlinkfiles('public/reports/');
        $status = $request->input('status');
        $getKYCStatus = KYCStatus::where('id', $status)->first();
        $strReplace = str_replace(" ", "_", $getKYCStatus['title']);
        $fileNamewithDate = $strReplace . '_' . date('d_m_Y') . '.xlsx';
        $url = 'public/reports/' . $fileNamewithDate;
        $urlresponse = "reports/" . $fileNamewithDate;
        $getCustomerData = $this->getUpgradeBusinessData($request);
        $getCustomerData = array_filter($getCustomerData->toArray(), function ($a) {
            return $a['hideout'] !== 1;
        });
        if (count($getCustomerData) == 0) {
            return 0;
        } else {
            // return $getCustomerData;
            $text = "Upgrade Commoner to Business User";
            Excel::store(new UpgradeUserExport($getCustomerData, $request, $text), '/' . $url, 'local');
            return '/' . $urlresponse;
        }
    }
}
