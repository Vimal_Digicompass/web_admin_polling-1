<?php

namespace App\Http\Controllers;

use App\Models\SqFt\Project;
use App\Models\SqFt\Property;
use App\Models\Setting\NotificationMaster;
use App\Models\Master\Area;
use App\Models\Property\PropertySubCategories;
use App\Models\SqFt\Service;
use Carbon\Carbon;
use App\Models\Master\ServiceCategory;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Models\Master\CallStatus;
use App\Models\Master\FurnishMaster;
use App\Models\Master\OpenSite;
use App\Models\Master\IdealBusniess;
use App\Models\Master\TransactionType;
use App\Models\Master\BHKMaster;
use App\Models\Master\FloorMaster;
use App\Models\Master\BathroomMaster;
use App\Models\Master\ConstructionStatus;
use App\Models\Master\ApprovalAuthority;
use App\Models\Master\Amenities;
use App\Models\Master\ServiceSubCategory;
use App\Models\Master\PropertyCategory;
use App\Models\Master\WindowMaster;
use App\Models\Master\InteriorMaster;
use App\Models\Master\ToiletMaster;
use App\Models\Master\LivingMaster;
use App\Models\Master\TenantMaster;
use App\Models\Master\KitchenMaster;
use App\Models\Master\ElectricalMaster;
use App\Models\Master\DoorMaster;
use App\Models\Master\BedroomMaster;
use App\Models\Master\BalconyMaster;
use App\Models\Setting\ReassignToPoll;
use App\Models\UserActivity\KYCApproval\KYCApprovalArcheive;
use App\Models\UserActivity\UpgradeBusiness\UpgradeBusinessArcheive;
use App\Models\UserActivity\WalletRechargeFailed\WalletRechargeFailedArchieve;
use App\Models\UserActivity\NotMakePayment\NotMakePaymentArchieve;
use App\Models\UserActivity\WalletExpiry\WalletExpiryArchieve;
use App\Models\UserActivity\KYCRejected\KYCRejectedArcheive;
use App\Models\UserActivity\KYCPending\KYCPendingArcheive;
use App\Models\UserActivity\LeadsNotViewed\LeadsNotViewedArcheive;
use App\Models\UserActivity\MarkedFavourite\MarkedFavouriteArcheive;
use App\Models\UserActivity\PostRejected\PostRejectedArcheive;
use App\Models\UserActivity\PostApproval\PostApprovalArcheive;
use App\Models\UserActivity\NotEnquirePost\NotEnquirePostArchieve;
use App\Models\UserActivity\InActive\InActiveArchieve;
use App\Models\UserActivity\NotPost\NotPostArchieve;
use App\Models\UserActivity\NotPostServiceRequirement\NotPostServiceRequirementArchieve;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\UserActivity\KYCPending\KYCPendingChangelog;
use App\Models\NotificationAdminpolls;
use App\Models\UserActivity\UpgradeBusiness\UpgradeBusinessChangelog;
use App\Models\UserActivity\WalletRechargeFailed\WalletRechargeFailedChangelog;
use App\Models\UserActivity\NotMakePayment\NotMakePaymentChangelog;
use App\Models\UserActivity\WalletExpiry\WalletExpiryChangelog;
use App\Models\UserActivity\KYCApproval\KYCApprovalChangelog;
use App\Models\UserActivity\KYCRejected\KYCRejectedChangelog;
use App\Models\UserActivity\LeadsNotViewed\LeadsNotViewedChangelog;
use App\Models\UserActivity\MarkedFavourite\MarkedFavouriteChangelog;
use App\Models\UserActivity\PostRejected\PostRejectedChangelog;
use App\Models\UserActivity\PostApproval\PostApprovalChangelog;
use App\Models\UserActivity\NotEnquirePost\NotEnquirePostChangelog;
use App\Models\UserActivity\NotPostServiceRequirement\NotPostServiceRequirementChangelog;
use App\Models\UserActivity\NotPost\NotPostChangelog;
use App\Models\UserActivity\InActive\InActiveChangelog;
use App\Models\UserActivity\KYCPending\KYCPendingAssign;
use App\Models\UserActivity\KYCApproval\KYCApprovalAssign;
use App\Models\UserActivity\NotMakePayment\NotMakePaymentAssign;
use App\Models\UserActivity\UpgradeBusiness\UpgradeBusinessAssign;
use App\Models\UserActivity\WalletExpiry\WalletExpiryAssign;
use App\Models\UserActivity\WalletRechargeFailed\WalletRechargeFailedAssign;
use App\Models\UserActivity\KYCRejected\KYCRejectedAssign;
use App\Models\UserActivity\LeadsNotViewed\LeadsNotViewedAssign;
use App\Models\UserActivity\MarkedFavourite\MarkedFavouriteAssign;
use App\Models\UserActivity\PostApproval\PostApprovalAssign;
use App\Models\UserActivity\PostRejected\PostRejectedAssign;
use App\Models\UserActivity\NotEnquirePost\NotEnquirePostAssign;
use App\Models\UserActivity\NotPostServiceRequirement\NotPostServiceRequirementAssign;
use App\Models\UserActivity\NotPost\NotPostAssign;
use App\Models\UserActivity\InActive\InActiveAssign;
use App\Models\UserActivity\KYCPending\KYCPendingPoll;
use App\Models\UserActivity\KYCApproval\KYCApprovalPoll;
use App\Models\UserActivity\WalletRechargeFailed\WalletRechargeFailedPoll;
use App\Models\UserActivity\NotMakePayment\NotMakePaymentPoll;
use App\Models\UserActivity\UpgradeBusiness\UpgradeBusinessPoll;
use App\Models\UserActivity\WalletExpiry\WalletExpiryPoll;
use App\Models\UserActivity\KYCRejected\KYCRejectedPoll;
use App\Models\UserActivity\LeadsNotViewed\LeadsNotViewedPoll;
use App\Models\UserActivity\MarkedFavourite\MarkedFavouritePoll;
use App\Models\UserActivity\PostApproval\PostApprovalPoll;
use App\Models\UserActivity\PostRejected\PostRejectedPoll;
use App\Models\UserActivity\NotEnquirePost\NotEnquirePostPoll;
use App\Models\UserActivity\InActive\InActivePoll;
use App\Models\UserActivity\NotPostServiceRequirement\NotPostServiceRequirementPoll;
use App\Models\UserActivity\NotPost\NotPostPoll;
use App\Models\SqFt\ServiceRequestLeads;
use App\User;
use App\Models\User as MongoUser;
use App\Models\SqFt\ProjectRequestLeads;
use App\Models\SqFt\PropertyRequestLeads;
use Illuminate\Routing\Controller as BaseController;
use App\Models\User\RolePermissionMapping;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sendnotification(Request $request) //: JsonResponse
    {
        try {

            $title          = '1SqFt';
            $content        = $request->input('notificationbody');
            $criteriaId = $request->input('criteriaId');
            $awsSnsClient = makeAWSClient("sns");

            $criteria = Config::get('constants.CRITERION');
            $criteria = array_search($criteriaId, $criteria);
            $resultTopicARN = $awsSnsClient->createTopic([
                'Name' => $criteria . now()->getTimestamp(), // REQUIRED
            ]);
            $data = [
                "type" => "Custom Notification"
            ];
            $topicARN = $resultTopicARN['TopicArn'];
            $record =  json_decode($request->input('data'), true);
            $record = array_unique($record);

            foreach ($record as $key => $value) {
                $Id = $value;
                $getData = MongoUser::where('user_id', $Id)->first();
                if ($getData) {
                    $usertype = $getData['user_type'];
                    $DeviceToken = $getData['device_token'];
                    if ($usertype === 'C') {
                        $appArn = env('COMMONER_ANDROID_APPLICATION_ARN');
                    } else {
                        $appArn = env('BUSINESS_ANDROID_APPLICATION_ARN');
                    }

                    if (strlen($DeviceToken) == 0) {
                        NotificationAdminpolls::create([
                            'user_id' => $Id,
                            'topicARN' => $topicARN,
                            'content' => $content,
                            'date' => date('d-m-Y H:i:s'),
                            'status' => Config::get('constants.STATUS.FAILED'),
                            'isvalidToken' => 'N',
                            'criteria' => $criteria,
                            'MessageId' => ''
                        ]);
                    } else {
                        try {
                            $result = $awsSnsClient->createPlatformEndpoint([
                                'PlatformApplicationArn' =>  $appArn,
                                'Token' => $DeviceToken,
                            ]);
                            //Todo: Catch the error and update status as invalid endpoint
                            $EndpointArn = $result['EndpointArn'];
                            $result = $awsSnsClient->subscribe([
                                'Endpoint' => $EndpointArn,
                                'Protocol' => 'application', // REQUIRED
                                'ReturnSubscriptionArn' => false,
                                'TopicArn' => $topicARN, // REQUIRED
                            ]);
                            NotificationAdminpolls::create([
                                'user_id' => $Id,
                                'topicARN' => $topicARN,
                                'content' => $content,
                                'date' => date('d-m-Y H:i:s'),
                                'status' => Config::get('constants.STATUS.QUEUED'),
                                'isvalidToken' => 'Y',
                                'criteria' => $criteria,
                                'MessageId' => ''
                            ]);
                        } catch (Exception $e) {
                            Log::info(["Notification error", $e->getMessage()]);
                            NotificationAdminpolls::create([
                                'user_id' => $Id,
                                'topicARN' => $topicARN,
                                'content' => $content,
                                'date' => date('d-m-Y H:i:s'),
                                'status' => Config::get('constants.STATUS.FAILED'),
                                'isvalidToken' => 'N',
                                'criteria' => $criteria,
                                'MessageId' => ''
                            ]);
                        }
                    }
                }
            }
            $publishResult = $awsSnsClient->publish([
                'Message' => json_encode([
                    "default" => $content,
                    "GCM" => json_encode([
                        "data" => [
                            "title" =>  $title,
                            "body" => $content,
                            "sound" => 'default'
                        ],
                    ]),
                ]),
                'MessageStructure' => 'json',
                'TopicArn' => $topicARN,
            ]);
            $MessageId = $publishResult['MessageId'];
            $awsSnsClient->deleteTopic([
                'TopicArn' => $topicARN, // REQUIRED
            ]);
            NotificationAdminpolls::where('topicARN', $topicARN)->where('isvalidToken', 'Y')
                ->update([
                    'MessageId' => $MessageId,
                    'status' => Config::get('constants.STATUS.THIRD_PARTY_ACKNOWLEDGED'),

                ]);
            return updatedJsonResponse(['message' => "Notification sent successfully"]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function getuser(Request $request): JsonResponse
    {
        try {
            $id = $request->input('user_id');
            $getUser = User::select('user_id', 'username')->where('team_lead_id', $id)
                ->get();
            return updatedJsonResponse(['message' => "success", 'data' => $getUser]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    public function getpermission(Request $request): JsonResponse
    {
        try {
            $id = $request->input('user_id');
            $getUser = User::select('role_id',)->where('user_id', $id)
                ->first();
            if ($getUser) {
                $getRoleId = $getUser['role_id'];
                $permissionList = RolePermissionMapping::where('role_id', $getRoleId)
                    ->Leftjoin('criteria_masters', 'role_permission_mapping.permission_id', '=', 'criteria_masters.criteria_id')
                    ->where('is_access', 1)
                    ->select('criteria_masters.criteria_id', 'criteria_masters.criteria_name')
                    ->get();
            } else {
                $permissionList = [];
            }

            return updatedJsonResponse(['message' => "success", 'data' => $permissionList]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function reassignlogic(Request $request)
    {
        try {
            $teamleadid = $request->input('teamlead');
            $userid = $request->input('user');
            $criteriaid = $request->input('permission');
            $role_type = Auth::user()->role_type;
            $doneBy = Auth::user()->user_id;
            if ($role_type == 1) {
                $teamleadid = $doneBy;
            }
            DB::beginTransaction();
            $currentDate = Carbon::now()->format('Y-m-d');
            foreach ($criteriaid as $key => $value) {
                if ($value == Config::get('constants.CRITERION.KYC_APPROVAL')) {
                    $getUnprocessedData = KYCApprovalAssign::select('kyc_approval_assign_id', 'city_id', 'user_id')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('kyc_approval_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['kyc_approval_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            KYCApprovalPoll::create([
                                'user_id' => $enduserId,
                                'city_id' => $cityId
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        KYCApprovalAssign::whereIn('kyc_approval_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } elseif ($value == Config::get('constants.CRITERION.KYC_PENDING')) {
                    $getUnprocessedData = KYCPendingAssign::select('kyc_pen_assign_id', 'city_id', 'user_id')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->orWhere('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('kyc_pen_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['kyc_pen_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            KYCPendingPoll::create([
                                'user_id' => $enduserId,
                                'city_id' => $cityId
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        KYCPendingAssign::whereIn('kyc_pen_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } else if ($value == Config::get('constants.CRITERION.KYC_REJECTED')) {
                    $getUnprocessedData = KYCRejectedAssign::select('kyc_rej_assign_id', 'city_id', 'user_id')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('kyc_rej_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['kyc_rej_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            KYCRejectedPoll::create([
                                'user_id' => $enduserId,
                                'city_id' => $cityId
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        KYCRejectedAssign::whereIn('kyc_rej_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } else if ($value == Config::get('constants.CRITERION.LEADS_NOT_VIEWED')) {
                    $getUnprocessedData = LeadsNotViewedAssign::select('leadsnotview_assign_id', 'city_id', 'user_id', 'post_id', 'post_type')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('leadsnotview_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['leadsnotview_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            $postId = $val['post_id'];
                            $postType = $val['post_type'];
                            LeadsNotViewedPoll::create([
                                'user_id' => $enduserId,
                                'city_id' => $cityId,
                                'post_id' => $postId,
                                'post_type' => $postType
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        LeadsNotViewedAssign::whereIn('leadsnotview_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } else if ($value == Config::get('constants.CRITERION.MARKED_FAVOURITE')) {
                    $getUnprocessedData = MarkedFavouriteAssign::select('fav_assign_id', 'city_id', 'user_id', 'post_id', 'post_type')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('fav_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['fav_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            $postId = $val['post_id'];
                            $postType = $val['post_type'];
                            MarkedFavouritePoll::create([
                                'user_id' => $enduserId,
                                'city_id' => $cityId,
                                'post_id' => $postId,
                                'post_type' => $postType
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        MarkedFavouriteAssign::whereIn('fav_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } else if ($value == Config::get('constants.CRITERION.POST_REJECTED')) {
                    $getUnprocessedData = PostRejectedAssign::select('post_rej_assign_id', 'city_id', 'user_id', 'post_id', 'post_type')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('post_rej_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['post_rej_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            $postId = $val['post_id'];
                            $postType = $val['post_type'];
                            PostRejectedPoll::create([
                                'user_id' => $enduserId,
                                'city_id' => $cityId,
                                'post_id' => $postId,
                                'post_type' => $postType
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        PostRejectedAssign::whereIn('post_rej_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } else if ($value == Config::get('constants.CRITERION.POST_APPROVAL')) {
                    $getUnprocessedData = PostApprovalAssign::select('post_approve_assign_id', 'city_id', 'user_id', 'post_id', 'post_type')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('post_approve_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['post_approve_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            $postId = $val['post_id'];
                            $postType = $val['post_type'];
                            PostApprovalPoll::create([
                                'user_id' => $enduserId,
                                'city_id' => $cityId,
                                'post_id' => $postId,
                                'post_type' => $postType
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        PostApprovalAssign::whereIn('post_approve_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } else if ($value == Config::get('constants.CRITERION.NOT_ENQUIRE_POST')) {
                    $getUnprocessedData = NotEnquirePostAssign::select('user_notenquire_post_assign_id', 'city_id', 'user_id', 'post_assign_trn_id')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('user_notenquire_post_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['user_notenquire_post_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            $trnId = $val['post_assign_trn_id'];
                            NotEnquirePostPoll::create([
                                'post_assign_trn_id' => $trnId,
                                'user_id' => $enduserId,
                                'city_id' => $cityId,
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        NotEnquirePostAssign::whereIn('user_notenquire_post_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } else if ($value == Config::get('constants.CRITERION.NOT_POST_SERVICE_REQ')) {
                    $getUnprocessedData = NotPostServiceRequirementAssign::select('user_notpost_req_assign_id', 'city_id', 'user_id', 'user_notpost_req_assign_trn_id')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('user_notpost_req_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['user_notpost_req_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            $trnId = $val['user_notpost_req_assign_trn_id'];
                            NotPostServiceRequirementPoll::create([
                                'user_notpost_req_assign_trn_id' => $trnId,
                                'user_id' => $enduserId,
                                'city_id' => $cityId,
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        NotPostServiceRequirementAssign::whereIn('user_notpost_req_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } else if ($value == Config::get('constants.CRITERION.INACTIVE_USER')) {
                    $getUnprocessedData = InActiveAssign::select('user_notactive_assign_id', 'city_id', 'user_id', 'user_notactive_assign_trn_id')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('user_notactive_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['user_notactive_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            $trnId = $val['user_notactive_assign_trn_id'];
                            InActivePoll::create([
                                'user_notactive_assign_trn_id' => $trnId,
                                'user_id' => $enduserId,
                                'city_id' => $cityId,
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        InActiveAssign::whereIn('user_notactive_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } else if ($value == Config::get('constants.CRITERION.NOT_POST')) {
                    $getUnprocessedData = NotPostAssign::select('user_notpost_assign_id', 'city_id', 'user_id', 'user_notpost_assign_trn_id')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('user_notactive_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['user_notpost_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            $trnId = $val['user_notpost_assign_trn_id'];
                            NotPostPoll::create([
                                'user_notpost_assign_trn_id' => $trnId,
                                'user_id' => $enduserId,
                                'city_id' => $cityId,
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        NotPostAssign::whereIn('user_notpost_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } else if ($value == Config::get('constants.CRITERION.RECHARGE_FAILED')) {
                    $getUnprocessedData = WalletRechargeFailedAssign::select('user_wpfailed_assign_id', 'city_id', 'user_id', 'wpfailed_trn_id')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('user_wpfailed_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['user_wpfailed_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            $package_id = $val['package_id'];
                            $trnId = $val['wpfailed_trn_id'];
                            WalletRechargeFailedPoll::create([
                                'wpfailed_trn_id' => $trnId,
                                'user_id' => $enduserId,
                                'city_id' => $cityId,
                                'package_id' => $package_id
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        WalletRechargeFailedAssign::whereIn('user_wpfailed_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } else if ($value == Config::get('constants.CRITERION.PAYMENT_PENDING')) {
                    $getUnprocessedData = NotMakePaymentAssign::select('user_makepayment_assign_id', 'city_id', 'user_id', 'makepayment_trn_id')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('user_makepayment_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['user_makepayment_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            $trnId = $val['makepayment_trn_id'];
                            NotMakePaymentPoll::create([
                                'makepayment_trn_id' => $trnId,
                                'user_id' => $enduserId,
                                'city_id' => $cityId,
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        NotMakePaymentAssign::whereIn('user_makepayment_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } else if ($value == Config::get('constants.CRITERION.PACKAGE_EXPIRY')) {
                    $getUnprocessedData = WalletExpiryAssign::select('user_cpexp_assign_id', 'city_id', 'user_id', 'cpexp_trn_id')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('user_cpexp_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['user_cpexp_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            $trnId = $val['cpexp_trn_id'];
                            $package_id = $val['package_id'];
                            WalletExpiryPoll::create([
                                'cpexp_trn_id' => $trnId,
                                'user_id' => $enduserId,
                                'city_id' => $cityId,
                                'package_id' => $package_id
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        WalletExpiryAssign::whereIn('user_cpexp_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                } else if ($value == Config::get('constants.CRITERION.UPGRADE_TO_BUSINESS_USER')) {
                    $getUnprocessedData = UpgradeBusinessAssign::select('upgradebusiness_assign_id', 'city_id', 'user_id')
                        ->where('crm_user_id', $userid)
                        ->where(function ($query) use ($currentDate) {
                            $query->where('updatestatus', 0)
                                ->orWhere('followup_date', $currentDate);
                        })
                        ->get();
                    $count = $getUnprocessedData->count();
                    $pluck = $getUnprocessedData->pluck('upgradebusiness_assign_id');
                    if ($count != 0) {
                        foreach ($getUnprocessedData as $key => $val) {
                            $dataId = $val['upgradebusiness_assign_id'];
                            $enduserId = $val['user_id'];
                            $cityId = $val['city_id'];
                            UpgradeBusinessPoll::create([
                                'user_id' => $enduserId,
                                'city_id' => $cityId,
                            ]);
                            ReassignToPoll::create([
                                'sendtopollby' => $doneBy,
                                'crm_user_id' => $userid,
                                'criteria_id' => $value,
                                'teamlead_id' => $teamleadid,
                                'data_id' => $dataId,
                                'city_id' => $cityId
                            ]);
                        }
                        UpgradeBusinessAssign::whereIn('upgradebusiness_assign_id', $pluck)->update([
                            'updatestatus' => 2
                        ]);
                    }
                }
            }
            DB::commit();
            return updatedJsonResponse(['message' => "Moved to poll successfully"]);
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }
    public function assigntopoll(Request $request, $id = null)
    {
        $scenario = "Reassign to Poll";
        $content = "Move assigned records to the Admin poll";
        $id = '';
        $criteriaId = "";
        $user = Auth::user();
        $user_id = $user->user_id;
        $id = $user_id;
        $role_type = $user->role_type;
        $role_id = $user->role_id;
        $notificationData = NotificationMaster::where('crm_user_id', $user_id)
            ->orderBy('notification_id', 'DESC')
            ->get();
        $notificationDataNew = NotificationMaster::where('is_read', 0)
            ->where('crm_user_id', $user_id)->orderBy('notification_id', 'DESC')
            ->count();
        $PermissionDetail = getPermissionAndAccess(0, $role_id);
        if ($role_type == 1) {
            $getuser = User::select('user_id', 'username')->where('team_lead_id', $user_id)
                ->where('is_active', 1)
                ->get();
            $getteamlead = [];
        } else {
            $getuser = [];
            $getteamlead =
                User::select('user_id', 'username')->where('role_type', 1)
                ->where('is_active', 1)
                ->get();
        }

        return view('setting/reassigntopoll', compact(
            'scenario',
            'content',
            'id',
            'criteriaId',
            'getteamlead',
            'getuser',
            'PermissionDetail',
            'notificationDataNew',
            'notificationData'
        ));
    }
    public function changelog($request)
    {
        $callingstatus = $request->input('callingstatus');
        $followdate = $request->input('followdate');
        $tinymce = $request->input('tinymce');
        $id = $request->input('user_id');

        if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_APPROVAL')) {
            $getBeforeData = KYCApprovalAssign::where('user_id', $id)->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_PENDING')) {
            $getBeforeData = KYCPendingAssign::where('user_id', $id)->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_REJECTED')) {
            $getBeforeData = KYCRejectedAssign::where('user_id', $id)->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.LEADS_NOT_VIEWED')) {
            $posttype = $request->input('posttype');
            $getBeforeData = LeadsNotViewedAssign::where('post_id', $id)
                ->where('post_type', $posttype)
                ->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.MARKED_FAVOURITE')) {
            $posttype = $request->input('posttype');
            $getBeforeData = MarkedFavouriteAssign::where('post_id', $id)
                ->where('post_type', $posttype)
                ->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.POST_APPROVAL')) {
            $posttype = $request->input('posttype');
            $getBeforeData = PostApprovalAssign::where('post_id', $id)
                ->where('post_type', $posttype)
                ->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.POST_REJECTED')) {
            $posttype = $request->input('posttype');
            $getBeforeData = PostRejectedAssign::where('post_id', $id)
                ->where('post_type', $posttype)
                ->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.NOT_ENQUIRE_POST')) {
            $getBeforeData = NotEnquirePostAssign::where('post_assign_trn_id', $id)
                ->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.NOT_POST_SERVICE_REQ')) {
            $getBeforeData = NotPostServiceRequirementAssign::where('user_notpost_req_assign_trn_id', $id)
                ->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.INACTIVE_USER')) {
            $getBeforeData = InActiveAssign::where('user_notactive_assign_trn_id', $id)
                ->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.NOT_POST')) {
            $getBeforeData = NotPostAssign::where('user_notpost_assign_trn_id', $id)
                ->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.RECHARGE_FAILED')) {
            $getBeforeData = WalletRechargeFailedAssign::where('wpfailed_trn_id', $id)
                ->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.PAYMENT_PENDING')) {
            $getBeforeData = NotMakePaymentAssign::where('makepayment_trn_id', $id)
                ->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.PACKAGE_EXPIRY')) {
            $getBeforeData = WalletExpiryAssign::where('cpexp_trn_id', $id)
                ->first();
        } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.UPGRADE_TO_BUSINESS_USER')) {
            $getBeforeData = UpgradeBusinessAssign::where('user_id', $id)
                ->first();
        }

        $Arr = [];
        if ($getBeforeData) {
            $BeforeDataCallingStatus = $getBeforeData['status'];
            $BeforeDataRemarks = $getBeforeData['remarks'];
            $BeforeDataFollowupDate = $getBeforeData['followup_date'];

            if ($followdate != $BeforeDataFollowupDate) {
                if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.KYC_REJECTED')
                    || $request->input('criteriaId') == Config::get('constants.CRITERION.KYC_APPROVAL')
                    || $request->input('criteriaId') == Config::get('constants.CRITERION.KYC_PENDING')
                ) {
                    array_push($Arr, [
                        'fieldname' => "Follow Up Date", 'newvalue' => $followdate,
                        'oldvalue' => $BeforeDataFollowupDate,
                        'user_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date('Y-m-d H:i:s')
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.NOT_ENQUIRE_POST')
                ) {
                    array_push($Arr, [
                        'fieldname' => "Follow Up Date", 'newvalue' => $followdate,
                        'oldvalue' => $BeforeDataFollowupDate,
                        'post_assign_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date('Y-m-d H:i:s')
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.NOT_POST_SERVICE_REQ')

                ) {
                    array_push($Arr, [
                        'fieldname' => "Follow Up Date", 'newvalue' => $followdate,
                        'oldvalue' => $BeforeDataFollowupDate,
                        'user_notpost_req_assign_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date('Y-m-d H:i:s')
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.INACTIVE_USER')

                ) {
                    array_push($Arr, [
                        'fieldname' => "Follow Up Date", 'newvalue' => $followdate,
                        'oldvalue' => $BeforeDataFollowupDate,
                        'user_notactive_assign_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date('Y-m-d H:i:s')
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.NOT_POST')

                ) {

                    array_push($Arr, [
                        'fieldname' => "Follow Up Date", 'newvalue' => $followdate,
                        'oldvalue' => $BeforeDataFollowupDate, 'user_notpost_assign_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.RECHARGE_FAILED')

                ) {

                    array_push($Arr, [
                        'fieldname' => "Follow Up Date", 'newvalue' => $followdate,
                        'oldvalue' => $BeforeDataFollowupDate, 'wpfailed_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.PAYMENT_PENDING')

                ) {

                    array_push($Arr, [
                        'fieldname' => "Follow Up Date", 'newvalue' => $followdate,
                        'oldvalue' => $BeforeDataFollowupDate, 'makepayment_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.PACKAGE_EXPIRY')

                ) {

                    array_push($Arr, [
                        'fieldname' => "Follow Up Date", 'newvalue' => $followdate,
                        'oldvalue' => $BeforeDataFollowupDate, 'cpexp_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.UPGRADE_TO_BUSINESS_USER')

                ) {

                    array_push($Arr, [
                        'fieldname' => "Follow Up Date", 'newvalue' => $followdate,
                        'oldvalue' => $BeforeDataFollowupDate, 'user_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else {

                    $posttype = $request->input('posttype');
                    array_push($Arr, [
                        'fieldname' => "Follow Up Date", 'newvalue' => $followdate,
                        'oldvalue' => $BeforeDataFollowupDate,
                        'post_id' => $id,
                        'post_type' => $posttype,
                        'done' => Auth::user()->username, 'created_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }
            if ($callingstatus != $BeforeDataCallingStatus) {
                $newstatus = CallStatus::where('id', $callingstatus)->first();
                $oldstatus =  CallStatus::where('id', $BeforeDataCallingStatus)->first();
                if ($newstatus) {
                    $newstatus = $newstatus['title'];
                } else {
                    $newstatus = "";
                }
                if ($oldstatus) {

                    $oldstatus = $oldstatus['title'];
                } else {
                    $oldstatus = "";
                }
                if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.KYC_REJECTED')
                    || $request->input('criteriaId') == Config::get('constants.CRITERION.KYC_APPROVAL')
                    || $request->input('criteriaId') == Config::get('constants.CRITERION.KYC_PENDING')
                ) {
                    array_push($Arr, [
                        'fieldname' => "Calling Status", 'newvalue' => $newstatus,
                        'oldvalue' => $oldstatus, 'user_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.NOT_ENQUIRE_POST')
                ) {
                    array_push($Arr, [
                        'fieldname' => "Calling Status", 'newvalue' => $newstatus,
                        'oldvalue' => $oldstatus, 'post_assign_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.NOT_POST_SERVICE_REQ')

                ) {
                    array_push($Arr, [
                        'fieldname' => "Calling Status", 'newvalue' => $newstatus,
                        'oldvalue' => $oldstatus, 'user_notpost_req_assign_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.INACTIVE_USER')

                ) {

                    array_push($Arr, [
                        'fieldname' => "Calling Status", 'newvalue' => $newstatus,
                        'oldvalue' => $oldstatus, 'user_notactive_assign_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.NOT_POST')

                ) {

                    array_push($Arr, [
                        'fieldname' => "Calling Status", 'newvalue' => $newstatus,
                        'oldvalue' => $oldstatus, 'user_notpost_assign_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.RECHARGE_FAILED')

                ) {

                    array_push($Arr, [
                        'fieldname' => "Calling Status", 'newvalue' => $newstatus,
                        'oldvalue' => $oldstatus, 'wpfailed_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.PAYMENT_PENDING')

                ) {

                    array_push($Arr, [
                        'fieldname' => "Calling Status", 'newvalue' => $newstatus,
                        'oldvalue' => $oldstatus, 'makepayment_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.PACKAGE_EXPIRY')

                ) {

                    array_push($Arr, [
                        'fieldname' => "Calling Status", 'newvalue' => $newstatus,
                        'oldvalue' => $oldstatus, 'cpexp_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.UPGRADE_TO_BUSINESS_USER')

                ) {

                    array_push($Arr, [
                        'fieldname' => "Calling Status", 'newvalue' => $newstatus,
                        'oldvalue' => $oldstatus, 'user_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else {
                    $posttype = $request->input('posttype');
                    array_push($Arr, [
                        'fieldname' => "Calling Status", 'newvalue' => $newstatus,
                        'oldvalue' => $oldstatus,
                        'done' => Auth::user()->username,
                        'post_id' => $id,
                        'post_type' => $posttype,
                        'created_at' => date("Y-m-d H:i:s")
                    ]);
                }
            }

            if ($tinymce != $BeforeDataRemarks) {
                if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.KYC_REJECTED')
                    || $request->input('criteriaId') == Config::get('constants.CRITERION.KYC_APPROVAL')
                    || $request->input('criteriaId') == Config::get('constants.CRITERION.KYC_PENDING')
                ) {
                    array_push($Arr, [
                        'fieldname' => "Remarks", 'newvalue' => $tinymce,
                        'oldvalue' => $BeforeDataRemarks, 'user_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date('Y-m-d H:i:s')
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.NOT_ENQUIRE_POST')
                ) {
                    array_push($Arr, [
                        'fieldname' => "Remarks", 'newvalue' => $tinymce,
                        'oldvalue' => $BeforeDataRemarks, 'post_assign_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date('Y-m-d H:i:s')
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.NOT_POST_SERVICE_REQ')

                ) {
                    array_push($Arr, [
                        'fieldname' => "Remarks", 'newvalue' => $tinymce,
                        'oldvalue' => $BeforeDataRemarks, 'user_notpost_req_assign_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date('Y-m-d H:i:s')
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.INACTIVE_USER')

                ) {

                    array_push($Arr, [
                        'fieldname' => "Remarks", 'newvalue' => $tinymce,
                        'oldvalue' => $BeforeDataRemarks, 'user_notactive_assign_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.NOT_POST')

                ) {

                    array_push($Arr, [
                        'fieldname' => "Remarks", 'newvalue' => $tinymce,
                        'oldvalue' => $BeforeDataRemarks, 'user_notpost_assign_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.RECHARGE_FAILED')

                ) {
                    array_push($Arr, [
                        'fieldname' => "Remarks", 'newvalue' => $tinymce,
                        'oldvalue' => $BeforeDataRemarks, 'wpfailed_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.PAYMENT_PENDING')

                ) {
                    array_push($Arr, [
                        'fieldname' => "Remarks", 'newvalue' => $tinymce,
                        'oldvalue' => $BeforeDataRemarks, 'makepayment_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.PACKAGE_EXPIRY')

                ) {
                    array_push($Arr, [
                        'fieldname' => "Remarks", 'newvalue' => $tinymce,
                        'oldvalue' => $BeforeDataRemarks, 'cpexp_trn_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else if (
                    $request->input('criteriaId') == Config::get('constants.CRITERION.UPGRADE_TO_BUSINESS_USER')

                ) {
                    array_push($Arr, [
                        'fieldname' => "Remarks", 'newvalue' => $tinymce,
                        'oldvalue' => $BeforeDataRemarks, 'user_id' => $id,
                        'done' => Auth::user()->username, 'created_at' => date("Y-m-d H:i:s")
                    ]);
                } else {
                    $posttype = $request->input('posttype');
                    array_push($Arr, [
                        'fieldname' => "Remarks", 'newvalue' => $tinymce,
                        'oldvalue' => $BeforeDataRemarks,
                        'post_id' => $id,
                        'post_type' => $posttype,
                        'done' => Auth::user()->username, 'created_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }
            DB::beginTransaction();
            if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_APPROVAL')) {
                KYCApprovalChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_PENDING')) {
                KYCPendingChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_REJECTED')) {
                KYCRejectedChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.LEADS_NOT_VIEWED')) {
                LeadsNotViewedChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.MARKED_FAVOURITE')) {
                MarkedFavouriteChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.POST_APPROVAL')) {
                PostApprovalChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.POST_REJECTED')) {
                PostRejectedChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.NOT_POST_SERVICE_REQ')) {
                NotPostServiceRequirementChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.NOT_ENQUIRE_POST')) {
                NotEnquirePostChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.INACTIVE_USER')) {
                InActiveChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.NOT_POST')) {
                NotPostChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.RECHARGE_FAILED')) {
                WalletRechargeFailedChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.PAYMENT_PENDING')) {
                NotMakePaymentChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.PACKAGE_EXPIRY')) {
                WalletExpiryChangelog::insert($Arr);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.UPGRADE_TO_BUSINESS_USER')) {
                UpgradeBusinessChangelog::insert($Arr);
            }

            DB::commit();
        }
    }
    public function updatecallstatus(Request $request): JsonResponse
    {
        try {

            $callingstatus = $request->input('callingstatus');
            $followdate = $request->input('followdate');
            if ($followdate) {
                $followdate = Carbon::createFromFormat('d-m-Y', $followdate)->format('Y-m-d');
            } else {
                $followdate = NULL;
            }
            $tinymce = $request->input('tinymce');
            $id = $request->input('user_id');
            DB::beginTransaction();
            $this->changelog($request);
            $updateArr = [
                'updatestatus' => 1,
                'status' => $callingstatus,
                'remarks' => $tinymce,
                'followup_date' => $followdate
            ];
            if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_APPROVAL')) {
                KYCApprovalAssign::where('user_id', $id)->update($updateArr);
                KYCApprovalArcheive::where('user_id', $id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_PENDING')) {
                KYCPendingAssign::where('user_id', $id)->update($updateArr);

                KYCPendingArcheive::where('user_id', $id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.KYC_REJECTED')) {
                KYCRejectedAssign::where('user_id', $id)->update($updateArr);
                KYCRejectedArcheive::where('user_id', $id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.LEADS_NOT_VIEWED')) {
                $posttype = $request->input('posttype');
                LeadsNotViewedAssign::where('post_id', $id)
                    ->where('post_type', $posttype)
                    ->update($updateArr);
                LeadsNotViewedArcheive::where('post_id', $id)
                    ->where('post_type', $posttype)
                    ->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.MARKED_FAVOURITE')) {
                $posttype = $request->input('posttype');
                MarkedFavouriteAssign::where('post_id', $id)
                    ->where('post_type', $posttype)
                    ->update($updateArr);
                MarkedFavouriteArcheive::where('post_id', $id)
                    ->where('post_type', $posttype)
                    ->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.POST_APPROVAL')) {
                $posttype = $request->input('posttype');
                PostApprovalAssign::where('post_id', $id)
                    ->where('post_type', $posttype)
                    ->update($updateArr);
                PostApprovalArcheive::where('post_id', $id)
                    ->where('post_type', $posttype)
                    ->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.POST_REJECTED')) {
                $posttype = $request->input('posttype');
                PostRejectedAssign::where('post_id', $id)
                    ->where('post_type', $posttype)
                    ->update($updateArr);
                PostRejectedArcheive::where('post_id', $id)
                    ->where('post_type', $posttype)
                    ->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.NOT_ENQUIRE_POST')) {
                NotEnquirePostAssign::where('post_assign_trn_id', $id)
                    ->update($updateArr);
                NotEnquirePostArchieve::where('user_notenquire_post_assign_trn_id', $id)
                    ->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.NOT_POST_SERVICE_REQ')) {
                NotPostServiceRequirementAssign::where('user_notpost_req_assign_trn_id', $id)
                    ->update($updateArr);
                NotPostServiceRequirementArchieve::where('user_notpost_req_assign_trn_id', $id)
                    ->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.INACTIVE_USER')) {
                InActiveAssign::where('user_notactive_assign_trn_id', $id)
                    ->update($updateArr);
                InActiveArchieve::where('user_notactive_assign_trn_id', $id)
                    ->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.NOT_POST')) {
                NotPostAssign::where('user_notpost_assign_trn_id', $id)
                    ->update($updateArr);
                NotPostArchieve::where('user_notpost_assign_trn_id', $id)
                    ->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.RECHARGE_FAILED')) {
                WalletRechargeFailedAssign::where('wpfailed_trn_id', $id)
                    ->update($updateArr);
                WalletRechargeFailedArchieve::where('wpfailed_trn_id', $id)
                    ->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.PAYMENT_PENDING')) {
                NotMakePaymentAssign::where('makepayment_trn_id', $id)
                    ->update($updateArr);
                NotMakePaymentArchieve::where('makepayment_trn_id', $id)
                    ->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.PACKAGE_EXPIRY')) {
                WalletExpiryAssign::where('cpexp_trn_id', $id)
                    ->update($updateArr);
                WalletExpiryArchieve::where('cpexp_trn_id', $id)
                    ->update([
                        'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                    ]);
            } else if ($request->input('criteriaId') == Config::get('constants.CRITERION.UPGRADE_TO_BUSINESS_USER')) {
                UpgradeBusinessAssign::where('user_id', $id)->update($updateArr);
                UpgradeBusinessArcheive::where('user_id', $id)->update([
                    'status' => Config::get('constants.ADMIN_POLLING_FLOW.PROCESSED')
                ]);
            }
            DB::commit();
            return updatedJsonResponse(['message' => "Call status updated successfully"]);
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }

    public function getservicesubcategory(Request $request)
    {
        $id = $request->input('id');
        $getData = ServiceSubCategory::where('service_category_id', $id)
            // ->where('is_active', 0)
            ->get();
        return updatedJsonResponse(['message' => "success", 'data' => $getData]);
    }
    public function getpropertycategory(Request $request)
    {
        $arr = [];
        $id = $request->input('id');
        if ($id == 2 || $id == 4) { // Sale and Leas
            $arr = [1, 2, 3, 4];
        } else { //Rent
            $arr = [1, 2, 4, 5, 6];
        }
        $getData = PropertyCategory::whereIn('id', $arr)
            ->get();
        return updatedJsonResponse(['message' => "success", 'data' => $getData]);
    }


    public function getchangelog(Request $request): JsonResponse
    {
        try {
            $id = $request->input('user_id');
            $criteriaId = $request->input('criteriaId');
            $date = Carbon::now()->subDays(7);
            if (
                $criteriaId == Config::get('constants.CRITERION.KYC_APPROVAL') ||
                $criteriaId == Config::get('constants.CRITERION.KYC_PENDING') ||
                $criteriaId == Config::get('constants.CRITERION.KYC_REJECTED')
            ) {
                $getChangeLog = KYCApprovalChangelog::where('user_id', $id)
                    ->orderBy('kyc_approval_changelog_id', 'DESC')
                    ->where('created_at', '>=', $date)
                    ->get()
                    ->merge(
                        KYCPendingChangelog::where('user_id', $id)
                            ->where('created_at', '>=', $date)
                            ->orderBy('kyc_pen_changelog_id', 'DESC')
                            ->get()
                    )->merge(
                        KYCRejectedChangelog::where('user_id', $id)
                            ->where('created_at', '>=', $date)
                            ->orderBy('kyc_rej_changelog_id', 'DESC')
                            ->get()
                    );
                $getChangeLog = $getChangeLog->sortByDesc('created_at');
            } else if ($criteriaId == Config::get('constants.CRITERION.LEADS_NOT_VIEWED')) {
                $PostType = $request->input('posttype');
                $getChangeLog = LeadsNotViewedChangelog::where('post_id', $id)
                    ->where('post_type', $PostType)
                    ->where('created_at', '>=', $date)
                    ->orderBy('leadsnotview_changelog_id', 'DESC')->get();
            } else if ($criteriaId == Config::get('constants.CRITERION.MARKED_FAVOURITE')) {
                $PostType = $request->input('posttype');
                $getChangeLog = MarkedFavouriteChangelog::where('post_id', $id)
                    ->where('post_type', $PostType)
                    ->where('created_at', '>=', $date)
                    ->orderBy('fav_changelog_id', 'DESC')->get();
            } else if (
                $criteriaId == Config::get('constants.CRITERION.POST_APPROVAL') ||
                $criteriaId == Config::get('constants.CRITERION.POST_REJECTED')
            ) {
                $PostType = $request->input('posttype');
                $getChangeLog = PostApprovalChangelog::where('post_id', $id)
                    ->where('post_type', $PostType)
                    ->orderBy('post_approve_changelog_id', 'DESC')
                    ->where('created_at', '>=', $date)
                    ->get()
                    ->merge(
                        PostRejectedChangelog::where('post_id', $id)
                            ->where('post_type', $PostType)
                            ->where('created_at', '>=', $date)
                            ->orderBy('post_rej_changelog_id', 'DESC')
                            ->get()
                    );
                $getChangeLog = $getChangeLog->sortByDesc('created_at');
            } else if ($criteriaId == Config::get('constants.CRITERION.NOT_ENQUIRE_POST')) {
                $getuserid = NotEnquirePostAssign::where('post_assign_trn_id', $id)->first();
                $getTrnId = NotEnquirePostAssign::where('user_id', $getuserid['user_id'])->pluck('post_assign_trn_id');
                $getChangeLog = NotEnquirePostChangelog::whereIn('post_assign_trn_id', $getTrnId)
                    ->orderBy('user_notenquire_post_changelog_id', 'DESC')
                    ->where('created_at', '>=', $date)
                    ->get();
            } else if ($criteriaId == Config::get('constants.CRITERION.NOT_POST_SERVICE_REQ')) {
                $getuserid = NotPostServiceRequirementAssign::where('user_notpost_req_assign_trn_id', $id)->first();
                $getTrnId = NotPostServiceRequirementAssign::where('user_id', $getuserid['user_id'])->pluck('user_notpost_req_assign_trn_id');
                $getChangeLog = NotPostServiceRequirementChangelog::whereIn('user_notpost_req_assign_trn_id', $getTrnId)
                    ->orderBy('user_notpost_req_changelog_id', 'DESC')
                    ->where('created_at', '>=', $date)
                    ->get();
            } else if ($criteriaId == Config::get('constants.CRITERION.INACTIVE_USER')) {
                $getuserid = InActiveAssign::where('user_notactive_assign_trn_id', $id)->first();
                $getTrnId = InActiveAssign::where('user_id', $getuserid['user_id'])->pluck('user_notactive_assign_trn_id');
                $getChangeLog = InActiveChangelog::whereIn('user_notactive_assign_trn_id', $getTrnId)
                    ->orderBy('user_notactive_changelog_id', 'DESC')
                    ->where('created_at', '>=', $date)
                    ->get();
            } else if ($criteriaId == Config::get('constants.CRITERION.NOT_POST')) {
                $getuserid = NotPostAssign::where('user_notpost_assign_trn_id', $id)->first();
                $getTrnId = NotPostAssign::where('user_id', $getuserid['user_id'])->pluck('user_notpost_assign_trn_id');
                $getChangeLog = NotPostChangelog::whereIn('user_notpost_assign_trn_id', $getTrnId)
                    ->orderBy('user_notpost_changelog_id', 'DESC')
                    ->where('created_at', '>=', $date)
                    ->get();
            } else if ($criteriaId == Config::get('constants.CRITERION.RECHARGE_FAILED')) {
                $getuserid = WalletRechargeFailedAssign::where('wpfailed_trn_id', $id)->first();
                $getTrnId = WalletRechargeFailedAssign::where('user_id', $getuserid['user_id'])->pluck('wpfailed_trn_id');
                $getChangeLog = WalletRechargeFailedChangelog::whereIn('wpfailed_trn_id', $getTrnId)
                    ->orderBy('user_wpfailed_changelog_id', 'DESC')
                    ->where('created_at', '>=', $date)
                    ->get();
            } else if ($criteriaId == Config::get('constants.CRITERION.PAYMENT_PENDING')) {
                $getuserid = NotMakePaymentAssign::where('makepayment_trn_id', $id)->first();
                $getTrnId = NotMakePaymentAssign::where('user_id', $getuserid['user_id'])->pluck('makepayment_trn_id');
                $getChangeLog = NotMakePaymentChangelog::whereIn('makepayment_trn_id', $getTrnId)
                    ->orderBy('user_makepayment_changelog_id', 'DESC')
                    ->where('created_at', '>=', $date)
                    ->get();
            } else if ($criteriaId == Config::get('constants.CRITERION.PACKAGE_EXPIRY')) {
                $getuserid = WalletExpiryAssign::where('cpexp_trn_id', $id)->first();
                $getTrnId = WalletExpiryAssign::where('user_id', $getuserid['user_id'])->pluck('cpexp_trn_id');
                $getChangeLog = WalletExpiryChangelog::whereIn('cpexp_trn_id', $getTrnId)
                    ->orderBy('user_cpexp_changelog_id', 'DESC')
                    ->where('created_at', '>=', $date)
                    ->get();
            } else if ($criteriaId == Config::get('constants.CRITERION.UPGRADE_TO_BUSINESS_USER')) {
                $PostType = $request->input('posttype');
                $getChangeLog = UpgradeBusinessChangelog::where('user_id', $id)
                    ->orderBy('upgradebusiness_changelog_id', 'DESC')
                    ->where('created_at', '>=', $date)
                    ->get();
            }

            return createdJsonResponse(['data' => $getChangeLog]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new HttpResponseException(badRequestJsonResponse(['message' => $e->getMessage()]));
        }
    }


    public function getdetailview(Request $request)
    {
        $PostType = $request->input('posttype');
        $post_id = $request->input('id');
        if ($PostType == "PR") {
            $data = Project::where('id', $post_id)->first();
            if ($data) {
                $is_approved = $data['is_project_approved'];
            } else {
                $is_approved = 0;
            }
            $CRUD = $request->input('operationtype');
            $projectcategory = $request->input('projectcategory');
            $getData = Project::with([
                'getimages',
                'getflats' => function ($flatJoinQuery) {
                    $flatJoinQuery->join('furnish_type_master', 'project_flats.furnish_type_id', '=', 'furnish_type_master.id')
                        ->join('floor_master', 'project_flats.floor_id', '=', 'floor_master.id')
                        ->Leftjoin('bhk_master', 'project_flats.bhk_id', '=', 'bhk_master.id')
                        ->select('project_flats.*', 'bhk_master.title as bhk', 'floor_master.title as Floor_Title', 'furnish_type_master.title as Furniture_Titile');
                },

                'getamenities' => function ($joinQuery) {
                    $joinQuery->join('amenities_master', 'project_amenities.amenity_id', '=', 'amenities_master.id');
                },
                'user' => function ($userQuery) {
                    $userQuery->join('cities', 'users.city_id', '=', 'cities.id')
                        ->join('state_master', 'cities.state_id', '=', 'state_master.id')
                        ->join('user_categories', 'users.category_id', '=', 'user_categories.id')
                        ->select(
                            'users.*',
                            'cities.title as city',
                            'state_master.title as state',
                            'user_categories.title as categoryname'
                        );
                }
            ])
                ->join('cities', 'projects.city_id', '=', 'cities.id')
                ->join('state_master', 'cities.state_id', '=', 'state_master.id')
                ->join('product_categories_master', 'projects.category_id', '=', 'product_categories_master.id')
                ->Leftjoin('approval_authority_master', 'projects.approval_authority', '=', 'approval_authority_master.id')
                ->Leftjoin('areas_master', 'projects.area_id', '=', 'areas_master.id')
                ->Leftjoin('construction_status_master', 'projects.construction_status_id', '=', 'construction_status_master.id')
                ->Leftjoin('bedroom_master', 'projects.master_bedroom_Id', '=', 'bedroom_master.id')
                ->Leftjoin('balcony_master', 'projects.balcony_id', '=', 'balcony_master.id')
                ->Leftjoin('kitchen_master', 'projects.floor_kitchen_id', '=', 'kitchen_master.id')
                ->Leftjoin('bedroom_master as otherbedroom', 'projects.other_bedroom_id', '=', 'otherbedroom.id')
                ->Leftjoin('living_or_dining_master', 'projects.living_or_dining_id', '=', 'living_or_dining_master.id')
                ->Leftjoin('toilet_master', 'projects.floor_toilet_id', '=', 'toilet_master.id')
                ->Leftjoin('door_master as internaldoor', 'projects.internal_door_id', '=', 'internaldoor.id')
                ->Leftjoin('door_master as maindoor', 'projects.main_door_id', '=', 'maindoor.id')
                ->Leftjoin('kitchen_master as kitchen', 'projects.fitting_kitchen_id', '=', 'kitchen.id')
                ->Leftjoin('toilet_master as toilet', 'projects.fitting_toilet_id', '=', 'toilet.id')
                ->Leftjoin('electrical_master', 'projects.electrical_id', '=', 'electrical_master.id')
                ->Leftjoin('windows_master', 'projects.windows_id', '=', 'windows_master.id')
                ->Leftjoin('kitchen_master as wallkitchen', 'projects.wall_kitchen_id', '=', 'wallkitchen.id')
                ->Leftjoin('toilet_master as walltoilet', 'projects.wall_toilet_id', '=', 'walltoilet.id')
                ->Leftjoin('interior_master', 'projects.interior_id', '=', 'interior_master.id')


                ->where('projects.id', $post_id)
                ->select(
                    'cities.title as city',
                    'interior_master.title as interiormaster',
                    'wallkitchen.title as wallkitchen',
                    'walltoilet.title as walltoilet',
                    'internaldoor.title as internaldoor',
                    'maindoor.title as maindoor',
                    'electrical_master.title as electricalmaster',
                    'windows_master.title as windowmaster',
                    'toilet.title as fittingtoilet',
                    'kitchen.title as fittingkitchen',
                    'kitchen_master.title as kitchenmaster',
                    'bedroom_master.title as bedroommaster',
                    'otherbedroom.title as otherbedroom',
                    'balcony_master.title as balconymaster',
                    'living_or_dining_master.title as diningmaster',
                    'toilet_master.title as toiletmaster',
                    'areas_master.title as area',
                    'construction_status_master.title as constructionstatus',
                    'approval_authority_master.title as approvalauthority',
                    'product_categories_master.title as projectcategory',
                    'state_master.title as state',
                    'projects.*',
                )->get();
            // return $getData;
            if ($projectcategory == 0) {
                $projectcategory = $getData[0]['category_id'];
            }

            $totalleads = ProjectRequestLeads::where('project_id', $post_id)->count();
            $totalunviewleads = ProjectRequestLeads::where('project_id', $post_id)
                ->where('is_viewed', 0)
                ->count();
            $totalviewleads = ProjectRequestLeads::where('project_id', $post_id)
                ->where('is_viewed', 1)
                ->count();
            $IdealBusniess = IdealBusniess::where('is_active', 1)->get();
            $TransactionType = TransactionType::where('is_active', 1)->get();
            if ($CRUD) {

                $amenityId = $getData[0]['getamenities']->pluck('amenity_id')->all();
                $ConstructionStatus = ConstructionStatus::where('is_active', 1)->get();
                $ApprovalAuthority = ApprovalAuthority::where('is_active', 1)->get();
                if ($projectcategory == 1) {
                    $Amenities = Amenities::select('id', 'title')
                        ->where('is_residential', 1)
                        // ->where('is_active', 1)
                        ->get();
                } else {
                    $Amenities = Amenities::select('id', 'title')
                        ->where('is_commercial', 1)
                        // ->where('is_active', 1)
                        ->get();
                }
                $MasterBedroom = BedroomMaster::where('is_active', 1)
                    ->where('type', 'M')
                    ->get();
                $OtherBedroom = BedroomMaster::where('is_active', 1)
                    ->where('type', 'O')
                    ->get();
                $BalconyMaster = BalconyMaster::where('is_active', 1)
                    ->where('is_applicable_for_project', 1)
                    ->where('is_active', 1)
                    ->get();
                $KitchenMaster = KitchenMaster::where('is_active', 1)
                    ->get();
                $FloorKitchenMaster = KitchenMaster::where('is_active', 1)
                    ->where('tab_type', 'FL')
                    ->get();
                $WallKitchenMaster = KitchenMaster::where('is_active', 1)
                    ->where('tab_type', 'WA')
                    ->get();
                $FittingKitchenMaster = KitchenMaster::where('is_active', 1)
                    ->where('tab_type', 'FI')
                    ->get();
                $LivingMaster  = LivingMaster::where('is_active', 1)
                    ->get();
                $FloorToiletMaster = ToiletMaster::where('is_active', 1)
                    ->where('tab_type', 'FL')
                    ->get();
                $FittingToiletMaster = ToiletMaster::where('is_active', 1)
                    ->where('tab_type', 'FI')
                    ->get();
                $WallToiletMaster = ToiletMaster::where('is_active', 1)
                    ->where('tab_type', 'WA')
                    ->get();
                $IDoorMaster = DoorMaster::where('is_active', 1)
                    ->where('type', 'I')
                    ->get();
                $MDoorMaster = DoorMaster::where('is_active', 1)
                    ->where('type', 'M')
                    ->get();
                $ElectricalMaster = ElectricalMaster::where('is_active', 1)
                    ->get();
                $WindowMaster = WindowMaster::where('is_active', 1)
                    ->get();
                $InteriorMaster = InteriorMaster::where('is_active', 1)
                    ->get();

                $AttachBathroom = config::get('constants.AttachBathroom');
                $PriceNegotiable = Config::get('constants.PROPERTY_NEGOTIABLE');
                $AreaUnit = Config::get('constants.AREA_UNIT');
                $returnHTML = view('Components.projectedit', compact(
                    'getData',
                    'ConstructionStatus',
                    'AttachBathroom',
                    'FittingKitchenMaster',
                    'WallKitchenMaster',
                    'FloorKitchenMaster',
                    'FittingToiletMaster',
                    'WallToiletMaster',
                    'FloorToiletMaster',
                    'AreaUnit',
                    'ApprovalAuthority',
                    'PostType',
                    'projectcategory',
                    'PriceNegotiable',
                    'Amenities',
                    'amenityId',
                    'InteriorMaster',
                    'WindowMaster',
                    'ElectricalMaster',
                    'MDoorMaster',
                    'IDoorMaster',
                    'LivingMaster',
                    'KitchenMaster',
                    'BalconyMaster',
                    'OtherBedroom',
                    'MasterBedroom',
                    'is_approved',
                    'post_id'
                ))->render();
            } else {
                // return $getData;

                $returnHTML = view('Components.projectmodal', compact(
                    'getData',
                    'projectcategory',
                    'totalleads',
                    'totalunviewleads',
                    'TransactionType',
                    'IdealBusniess',
                    'totalviewleads',
                    'PostType'
                ))->render();
            }
        } else if ($PostType == "S") {
            $CRUD = $request->input('operationtype');
            $data = Service::where('id', $post_id)->first();
            if ($data) {
                $is_approved = $data['is_service_approved'];
            } else {
                $is_approved = 0;
            }
            $getData = Service::with([
                'getimages',
                'getarea' => function ($joinQuery) {
                    $joinQuery->join('areas_master', 'service_areas.area_id', '=', 'areas_master.id');
                    // ->where('is_active',0);
                },
                'user' => function ($userQuery) {
                    $userQuery->join('cities', 'users.city_id', '=', 'cities.id')
                        ->join('state_master', 'cities.state_id', '=', 'state_master.id')
                        ->join('user_categories', 'users.category_id', '=', 'user_categories.id')
                        ->select(
                            'users.*',
                            'cities.title as city',
                            'state_master.title as state',
                            'user_categories.title as categoryname'
                        );
                }
            ])
                ->join('cities', 'services.city_id', '=', 'cities.id')
                ->join('state_master', 'cities.state_id', '=', 'state_master.id')
                ->join('service_categories_master', 'services.service_type_id', '=', 'service_categories_master.id')
                ->join('service_subcategories_master', 'services.sub_category_id', '=', 'service_subcategories_master.id')
                ->where('services.id', $post_id)
                ->select(
                    'cities.title as city',
                    'service_categories_master.title as servicecategory',
                    'service_subcategories_master.title as servicesubcategory',
                    'state_master.title as state',
                    'services.*',
                )->get();
            $service_category_id = $getData[0]['service_type_id'];
            $totalleads = ServiceRequestLeads::where('service_id', $post_id)->count();
            $totalunviewleads = ServiceRequestLeads::where('service_id', $post_id)
                ->where('is_viewed', 0)
                ->count();
            $totalviewleads = ServiceRequestLeads::where('service_id', $post_id)
                ->where('is_viewed', 1)
                ->count();
            $city_id = $getData[0]['city_id'];
            $AreaMaster = Area::where('is_active', 1)
                ->where('city_id', $city_id)
                ->get();
            if ($CRUD) {
                $ServiceCategory = ServiceCategory::where('is_active', 1)->get();
                $ServiceSubCategory = ServiceSubCategory::where('service_category_id', $service_category_id)
                    ->get();
                // echo $service_category_id;
                // return $ServiceSubCategory;
                $PaymentMode = Config::get('constants.PAYMENT_MODE');
                $returnHTML = view('Components.serviceedit', compact(
                    'getData',
                    'AreaMaster',
                    'ServiceCategory',
                    'ServiceSubCategory',
                    'PaymentMode',
                    'PostType',
                    'is_approved',
                    'post_id'
                ))->render();
            } else {

                $returnHTML = view('Components.servicemodal', compact(
                    'getData',
                    'totalleads',
                    'totalunviewleads',
                    'totalviewleads',
                    'PostType'

                ))->render();
            }
        } else {
            $CRUD = $request->input('operationtype');
            $getData = Property::with([
                'getimages',
                'gettenant' => function ($joinQuery) {
                    $joinQuery->Leftjoin('tenant_type_master', 'property_tenant_type.tenant_type_id', '=', 'tenant_type_master.id');
                },
                'getideal' => function ($joinQuery) {
                    $joinQuery->Leftjoin('ideal_for_business_master', 'property_ideal_for_business.ideal_business_id', '=', 'ideal_for_business_master.id');
                },
                'user' => function ($userQuery) {
                    $userQuery->join('cities', 'users.city_id', '=', 'cities.id')
                        ->Leftjoin('state_master', 'cities.state_id', '=', 'state_master.id')
                        ->Leftjoin('user_categories', 'users.category_id', '=', 'user_categories.id')
                        ->select(
                            'users.*',
                            'cities.title as city',
                            'state_master.title as state',
                            'user_categories.title as categoryname'
                        );
                }
            ])->join('cities', 'properties.city_id', '=', 'cities.id')
                ->join('state_master', 'cities.state_id', '=', 'state_master.id')
                ->join('property_categories_master', 'properties.property_category_id', '=', 'property_categories_master.id')
                ->Leftjoin('property_subcategories_master', 'properties.property_sub_category_id', '=', 'property_subcategories_master.id')
                ->Leftjoin('areas_master', 'properties.area_id', '=', 'areas_master.id')
                ->join('property_type_master', 'properties.property_type_id', '=', 'property_type_master.id')
                ->Leftjoin('bhk_master', 'properties.bhk_id', '=', 'bhk_master.id')
                ->Leftjoin('furnish_type_master', 'properties.furnish_type_id', '=', 'furnish_type_master.id')
                ->Leftjoin('floor_master', 'properties.floor_id', '=', 'floor_master.id')
                ->Leftjoin('bathroom_master', 'properties.bathroom_id', '=', 'bathroom_master.id')
                ->Leftjoin('balcony_master', 'properties.balcony_id', '=', 'balcony_master.id')
                ->Leftjoin('construction_status_master', 'properties.construction_type_id', '=', 'construction_status_master.id')
                ->Leftjoin('transaction_type_master', 'properties.transaction_type_id', '=', 'transaction_type_master.id')
                ->Leftjoin('open_site_master', 'properties.no_open_side_id', '=', 'open_site_master.id')
                ->Leftjoin('plot_area_master', 'properties.plot_area', '=', 'plot_area_master.id')
                ->where('properties.id', $post_id)
                ->select(
                    'cities.title as city',
                    'property_type_master.title as propertytype',
                    'property_categories_master.title as propertycategory',
                    'property_subcategories_master.title as propertysubcategory',
                    'state_master.title as state',
                    'areas_master.title as area',
                    'properties.*',
                    'bhk_master.title as bhktitle',
                    'furnish_type_master.title as furnishtitle',
                    'floor_master.title as floortitle',
                    'bathroom_master.title as bathroomtitle',
                    'balcony_master.title as balconytitle',
                    'construction_status_master.title as constructiontype',
                    'transaction_type_master.title as transactiontype',
                    'open_site_master.title as opensite',
                    'plot_area_master.title as plotarea'
                )->get();
            // return $getData;
            $subId  = $request->input('subcategoryid');
            if ($subId != "") {
                $getData[0]['property_sub_category_id'] = $subId;
            }
            $property_category_id = $getData[0]['property_category_id'];
            $totalleads = PropertyRequestLeads::where('property_id', $post_id)->count();
            $totalunviewleads = PropertyRequestLeads::where('property_id', $post_id)
                ->where('is_viewed', 0)
                ->count();
            $totalviewleads = PropertyRequestLeads::where('property_id', $post_id)
                ->where('is_viewed', 1)
                ->count();
            $TransactionType = TransactionType::where('is_active', 1)->get();
            $AttachBathroom = config::get('constants.AttachBathroom');
            $BHKMaster = BHKMaster::where('is_active', 1)->get();
            $FloorMaster = FloorMaster::where('is_active', 1)->get();
            $PGRules = Config::get('constants.PGRules'); // arrayinput
            $Opensite = OpenSite::where('is_active', 1)->get();
            $ConstructionStatus = ConstructionStatus::where('is_active', 1)->get();
            $PropertySubCategory = PropertySubCategories::where('property_category_id', $property_category_id)->get();
            $BathroomMaster = BathroomMaster::where('is_active', 1)->get();
            $BalconyMaster = BalconyMaster::where('is_active', 1)
                ->where('is_applicable_for_property', 1)
                ->get();
            $TenantMaster =  TenantMaster::where('is_active', 1)->get();
            $WashRoom = Config::get('constants.personal-washroom');
            $FlatProperty = Config::get('constants.property-flatmate-types');
            $PantryRoom = Config::get('constants.pantry-cafeteria');
            $NonVegAllow = Config::get('constants.Flatemates-non-veg-allowed');
            $Parking = Config::get('constants.Parking');
            $GatedSecurity = Config::get('constants.Flatemates-gated_security');
            $PreferFlatTenantType = Config::get('constants.Preferred-tenant-Types'); // arrayinput
            $PreferPGTenantType = Config::get('constants.PG-Preferred-Tenant'); //array
            $IdealBusiness = IdealBusniess::where('is_active', 1)->get();
            $RoomType = Config::get('constants.ROOMTYPE');
            $AreaUnit = Config::get('constants.AREA_UNIT');
            $PGRoomType = Config::get('constants.PGROOMTYPE');
            $Food  = Config::get('constants.FOOD'); //array
            $Gender = Config::get('constants.GENDER'); //array
            $BoundaryWall = Config::get('constants.boundary-wallmade');
            $RoomAmenities = Config::get('constants.ROOMAMENITIES'); // arrayinput
            $CommonAmenities  = Config::get('constants.COMMONAMENITIES'); //arrayinput
            $FurnishMaster = FurnishMaster::where('is_active', 1)->get();
            $FlatTenantType = Config::get('constants.Flatemates-tenant-Types');
            $PriceNegotiable = Config::get('constants.PROPERTY_NEGOTIABLE');
            $data = Property::where('id', $post_id)->first();
            if ($data) {
                $is_approved = $data['is_property_approved'];
            } else {
                $is_approved = 0;
            }
            if ($CRUD) {
                $returnHTML = view('Components.propertyedit', compact(
                    'getData',
                    'totalleads',
                    'AttachBathroom',
                    'BoundaryWall',
                    'totalunviewleads',
                    'totalviewleads',
                    'PostType',
                    'PGRoomType',
                    'Food',
                    'Gender',
                    'PGRules',
                    'PropertySubCategory',
                    'PreferPGTenantType',
                    'BHKMaster',
                    'PGRules', // arrayinput
                    'Opensite',
                    'ConstructionStatus',
                    'BathroomMaster',
                    'BalconyMaster',
                    'TenantMaster',
                    'WashRoom',
                    'FlatProperty',
                    'PantryRoom',
                    'NonVegAllow',
                    'Parking',
                    'is_approved',
                    'post_id',
                    'GatedSecurity',
                    'TransactionType',
                    'PreferFlatTenantType', // arrayinput
                    'PreferPGTenantType', //array
                    'IdealBusiness',
                    'RoomType',
                    'PGRoomType',
                    'Food', //array
                    'Gender', //array
                    'RoomAmenities', // arrayinput
                    'CommonAmenities', //arrayinput
                    'FurnishMaster',
                    'FlatTenantType',
                    'FloorMaster',
                    'PriceNegotiable',
                    'AreaUnit'
                ))->render();
            } else {
                $returnHTML = view('Components.propertymodal', compact(
                    'getData',
                    'totalleads',
                    'totalunviewleads',
                    'totalviewleads',
                    'PostType',
                    'PGRoomType',
                    'Food',
                    'Gender',
                    'PGRules',
                    'PreferPGTenantType',
                    'AttachBathroom',
                    'PriceNegotiable',
                    'RoomAmenities',
                    'CommonAmenities',
                    'Parking'
                ))->render();
            }
        }
        return $returnHTML;
    }
}
