<?php

return [

    'SEARCH' => [
        'PROPERTY'   => 'search-property',
        'PROJECT'    => 'search-project',
        'SERVICE'    => 'search-service',
    ],
    'ADD_NEW_POST' => [
        'PROPERTY'  =>  'add-new-property',
        'PROJECT'   =>  'add-new-project',
        'SERVICE'   =>  'add-new-service',
    ],
    'POST' => [
        'PROPERTY'  =>  'property',
        'PROJECT'   =>  'project',
        'SERVICE'   =>  'service',
    ],
    'REQUIREMENT' => [
        // 'PROPERTY'  =>  'require-property',
        // 'PROJECT'   =>  'require-project',
        'SERVICE'   =>  'require-service',
    ],
    'USER' => [
        'DASHBOARD'        => 'user-dashboard',
        'PROFILE'        => [
            'UPDATE'    => 'update-my-profile',
        ],
        'REQUIREMENTS'    => 'my-requirements',
        'FAVOURITES'    => 'my-favourites',
        'PROPERTIES'    => [
            'ALL'        =>    'my-all-properties',
            'APPROVED'    =>    'my-aproved-properties',
            'REJECTED'    =>    'my-rejected-properties',
            'WAITING'    =>    'my-waiting-properties',
        ],
        'SERVICES'        => [
            'ALL'        =>    'my-all-services',
            'APPROVED'    =>    'my-aproved-services',
            'REJECTED'    =>    'my-rejected-services',
            'WAITING'    =>    'my-waiting-services',
        ],
        'PROJECTS'        => [
            'ALL'        =>    'my-all-projects',
            'APPROVED'    =>    'my-aproved-projects',
            'REJECTED'    =>    'my-rejected-projects',
            'WAITING'    =>    'my-waiting-projects',
        ],
        'SEARCHES'        => [
            'PROPERTY'    => 'my-search-property',
            'PROJECT'    => 'my-search-project',
            'SERVICE'    => 'my-search-service',
        ],
    ],
    'LOGIN'            => 'login',
    'LOGOUT'        => 'logout',
    'CONTACT-US'     => 'contact-us',
    'FAQ'             => 'faq',
];
