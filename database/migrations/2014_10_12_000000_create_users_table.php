<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('users', function (Blueprint $table) {
            $table->Increments('user_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username');
            $table->string('mobilenumber');
            $table->string('password');
            $table->unsignedInteger('role_id')->index();
            $table->unsignedInteger('role_type')->index();
            $table->integer('team_lead_id')->default(0)->index();
            $table->integer('city_id')->default(0)->index();
            $table->integer('state_id')->default(0)->index();
            $table->integer('is_active')->default(0)->index();
            $table->integer('is_online')->default(0)->index();
            $table->string('image_name');
            $table->integer('is_follow_up')->default(0);
            $table->integer('is_auto_password')->default(0);
            $table->integer('user_limit')->default(0);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

