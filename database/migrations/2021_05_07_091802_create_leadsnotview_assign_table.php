<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsnotviewAssignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leadsnotview_assign', function (Blueprint $table) {
            $table->Increments('leadsnotview_assign_id');
            $table->integer('user_id')->index();
            $table->integer('city_id')->index();
            $table->integer('post_id')->index();
            $table->string('post_type')->index();
            $table->unsignedInteger('crm_user_id')->index();
            $table->integer('status')->default(0);
            $table->integer('published_status')->default(0);
            $table->string('remarks')->nullable();
            $table->date('followup_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leadsnotview_assign');
    }
}
