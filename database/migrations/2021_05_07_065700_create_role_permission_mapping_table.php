<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolePermissionMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_permission_mapping', function (Blueprint $table) {
            $table->Increments('role_perm_id');
            $table->unsignedInteger('role_id')->index();
            $table->unsignedInteger('permission_id')->index();
            $table->integer('is_access')->default(0);
            $table->integer('is_follow_up')->default(0);
            $table->integer('is_edit_applicable')->default(0);
            $table->integer('is_view_applicable')->default(0);
            $table->integer('is_mobilenumberread')->default(0);
            $table->integer('is_readcompleteddata')->default(0);
            $table->integer('is_bulkknotificationaccess')->default(0);
            $table->integer('is_exportaccess')->default(0);
            $table->integer('is_approve')->default(0);
            $table->integer('is_kycupdate')->default(0);
            $table->integer('is_useractive')->default(0);
            $table->integer('aasign_limit')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_permission_mapping');
    }
}
