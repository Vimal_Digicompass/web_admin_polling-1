<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserWpfailedChangelogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_wpfailed_changelog', function (Blueprint $table) {
            $table->Increments('user_wpfailed_changelog_id');
            $table->unsignedInteger('wpfailed_trn_id')->index();
            $table->string('fieldname');
            $table->string('newvalue');
            $table->string('oldvalue');
            $table->string('done')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_wpfailed_changelog');
    }
}
