<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLoggedinActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_loggedin_activity', function (Blueprint $table) {
            $table->Increments('user_log_id');
            $table->unsignedInteger('user_id')->index();
            $table->integer('is_online')->default(0);
            $table->integer('date_unixtime')->default(0);
            $table->date('date')->index();
            $table->datetime('login_time')->nullable();
            $table->datetime('logout_time')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_loggedin_activity');
    }
}
