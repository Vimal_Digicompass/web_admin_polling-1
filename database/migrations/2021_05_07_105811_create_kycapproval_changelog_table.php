<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKycapprovalChangelogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kycapproval_changelog', function (Blueprint $table) {
            $table->Increments('kyc_approval_changelog_id');
            $table->integer('user_id')->index();
            $table->string('fieldname');
             $table->unsignedInteger('crm_user_id')->index();
            $table->string('newvalue');
            $table->string('oldvalue');
            $table->string('done')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kycapproval_changelog');
    }
}
