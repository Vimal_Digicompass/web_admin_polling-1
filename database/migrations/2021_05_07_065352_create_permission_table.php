<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission', function (Blueprint $table) {
            $table->Increments('permission_id');
            $table->string('permission_name');
            $table->integer('display_order')->default(0);
            $table->integer('is_edit_applicable')->default(0);
            $table->integer('is_view_applicable')->default(0);
            $table->integer('is_access')->default(0);
            $table->integer('is_follow_up')->default(0);
            $table->integer('is_section')->default(0);
            $table->integer('is_mobilenumberread')->default(0);
            $table->integer('is_readcompleteddata')->default(0);
            $table->integer('is_bulkknotificationaccess')->default(0);
            $table->integer('is_exportaccess')->default(0);
            $table->integer('is_approve')->default(0);
            $table->integer('is_kycupdate')->default(0);
            $table->integer('is_useractive')->default(0);
            $table->integer('parent_permission_id')->default(0);
            $table->integer('aasign_limit')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission');
    }
}
