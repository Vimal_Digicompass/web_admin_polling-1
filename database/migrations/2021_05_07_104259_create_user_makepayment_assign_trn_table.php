<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMakepaymentAssignTrnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_makepayment_assign_trn', function (Blueprint $table) {
            $table->Increments('user_makepayment_trn_id');
            $table->integer('user_id')->index();
            $table->integer('city_id')->index();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_makepayment_assign_trn');
    }
}
