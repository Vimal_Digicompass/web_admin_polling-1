<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserNotenquirePostAssignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_notenquire_post_assign', function (Blueprint $table) {
            $table->Increments('user_notenquire_post_assign_id');
            $table->unsignedInteger('post_assign_trn_id')->index();
            $table->unsignedInteger('crm_user_id')->index();
            $table->integer('status')->default(0);
            $table->string('remarks')->nullable();
            $table->date('followup_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_notenquire_post_assign');
    }
}
