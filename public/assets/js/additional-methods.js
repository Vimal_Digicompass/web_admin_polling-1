// Older "accept" file extension method. Old docs: http://docs.$.com/Plugins/Validation/Methods/accept
$.validator.addMethod("extension", function (value, element, param) {
    param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpeg|jpg|pdf";
    return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
}, "Please select file type image/* or pdf");
