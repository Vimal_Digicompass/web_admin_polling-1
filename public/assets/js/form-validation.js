$(function () {
    'use strict';
    $.validator.addMethod("strongePassword", function (value) {
        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) && /[a-z]/.test(value) && /\d/.test(value) && /[A-Z]/.test(value);
    }, "The password must contain at least 1 number, at least 1 lower case letter, and at least 1 upper case letter");
    var errorMessage;
    var dynamicErrorMsg = function () {
        return errorMessage;
    };
    $.validator.addMethod("checkDocumentNo", function (value) {

        var selectdocuments = $("#selectdocuments").val();
        if (selectdocuments == 1) {
            var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
            errorMessage = "PAN Card must be Alphanumeric - Eg:QUERT1234Y";
            return regex.test(value)


        } else if (selectdocuments == 2) {
            var aadharcardrex = /^[2-9]{1}[0-9]{3}[0-9]{4}[0-9]{4}$/;
            errorMessage = "Please enter valid Aadhar Number";
            return aadharcardrex.test(value)


        } else if (selectdocuments == 3) {
            var voterregex = /^([a-zA-Z]){3}([0-9]){7}?$/g;
            errorMessage = "Please enter the valid Voter ID";
            return voterregex.test(value)

        } else if (selectdocuments == 5) {
            var drivinglicenserex = /^[a-zA-Z0-9]+$/;
            errorMessage = "Please enter the valid Driving license";
            return drivinglicenserex.test(value)

        } else if (selectdocuments == 4) {
            var gstrex = /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/;
            errorMessage = "Please enter the valid GST";
            return gstrex.test(value)
        }
    }, dynamicErrorMsg);

    // validate signup form on keyup and submit
    $("#loginform").validate({
        rules: {
            username: {
                required: true,
            },
            password: {
                required: true,
            },
        },
        messages: {
            username: {
                required: "Please enter a username",
            },
            password: {
                required: "Please provide a password",
            },
        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
        },
        submitHandler: function (form) {
            var input = new FormData($('#loginform')[0]);
            var url = '/login';
            var method = "POST";
            credentialcommongfunction(url, input, method, 'submit', 'Login')
        }
    });
    $("#updatepasswordform").validate({
        rules: {
            password: {
                required: true,
                minlength: 8,
                strongePassword: true
            },
            confirmpassword: {
                required: true,
                minlength: 8,
                equalTo: "#password",
                strongePassword: true
            },
        },
        messages: {
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 8 characters long"
            },
            confirmpassword: {
                required: "Please provide a confirm password",
                minlength: "Your password must be at least 8 characters long",
                equalTo: "Please enter the same password as above"
            },
        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
        },
        submitHandler: function (form) {
            var input = new FormData($('#updatepasswordform')[0]);
            var url = '/admin/resetpassword';
            var method = "POST";
            credentialcommongfunction(url, input, method, 'updatepassword', 'Update Password')
        }
    });
    var simplemde = "";
    if ($("#callremark").length) {
        simplemde = new SimpleMDE({
            element: $("#callremark")[0],
            toolbar: ["bold", "italic", "heading", "|", "quote", 'unordered-list', 'ordered-list'],
        });
    }



    $("#sendnotifyform").validate({
        rules: {
            notificationbody: {
                required: true
            },

        },
        messages: {
            notificationbody: {
                required: "Please enter the Content"
            },

        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger');
            $(element).addClass('form-control-danger');
        },
        submitHandler: function (form) {
            var input = new FormData($('#sendnotifyform')[0]);
            input.append('criteriaId', $('meta[name="criteria"]').attr('content'))
            input.append('data', JSON.stringify(NotifyArr))
            var url = '/sendnotification';
            var method = "POST";
            credentialcommongfunction(url, input, method, 'sendnotificationBtn', 'Send Notification')

        }
    });
    $("#callstatusform").validate({
        rules: {
            callingstatus: {
                required: true
            },
            // followdate: {
            //     required: true
            // },
        },
        messages: {
            callingstatus: {
                required: "Please select the status"
            },
            // followdate: {
            //     required: "Please select Follow up date"
            // },
        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger');
            $(element).addClass('form-control-danger');
        },
        submitHandler: function (form) {
            var input = new FormData($('#callstatusform')[0]);
            var callremark = $("#callremark").val();
            callremark = simplemde.markdown(callremark);
            input.append('tinymce', callremark)
            input.append('criteriaId', $('meta[name="criteria"]').attr('content'));
            var posttype = $("#posttype").val();
            input.append('posttype', posttype);
            var url = '/updatecallstatus';
            var method = "POST";
            credentialcommongfunction(url, input, method, 'callstatussubmit', 'Update')
            $("#callstatusform").trigger('reset');
            $("#callingstatus").val('');
            $("#callingstatus-error").text("");
            $("#callingstatus").select2();
            $(".form-group").removeClass('has-danger');
            $(".select2-selection").css('border-color', 'none')
            // $("#followdatediv").css('display', 'none')
            $("#callremark").val("");
            $(".CodeMirror-code").empty();
            simplemde.value("");

        }
    });

    $("#kycstatusform").validate({
        rules: {
            kycstatusaction: {
                required: true
            },
        },
        messages: {
            kycstatusaction: {

                required: "Please select the status"
            },


        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger');
            $(element).addClass('form-control-danger');
        },
        submitHandler: function (form) {
            var input = new FormData($('#kycstatusform')[0]);
            input.append('criteriaId', $('meta[name="criteria"]').attr('content'))
            var url = '/kyc/updatekycstatus';
            var method = "POST";
            var valid = true;
            if ($("#kycstatusaction").val() == 2) {
                var fields = $('.documenttype').filter(':checked').length;
                // var fields = $("input[name='documenttype']").attr('checked');
                if (fields == 0) {
                    valid = false;
                    $("#checkboxerror").text("please select document type")

                } else {
                    $("#checkboxerror").text("")
                }
            }
            if (valid == true) {
                credentialcommongfunction(url, input, method, 'kycstatussubmit', 'Update')
                $("#kycstatusform").trigger("reset");
                $("#checkboxerror").text("")
                $("#rejectedcheckbox").css("display", 'none');
                $(".documenttype").prop("checked", false)
                $(".reasonclass").css("display", "none");
                $("#kycstatusaction").val("");
                $("#kycstatusaction-error").text("");
                $("#callingstatus").select2();
                $("#kycstatusaction").select2();
                $("#poststatusactionreasondiv").css("display", 'none')
                $("#poststatusaction").val("");
                $("#poststatusaction-error").text("");
                $("#poststatusactionreason").val("");
                $("#poststatusactionreason-error").text("");
                $("#poststatusaction").select2();
                $("#poststatusactionreason").select2();
                $(".form-group").removeClass('has-danger');
                $(".select2-selection").css('border-color', 'none')
            }
            return false;

        }
    });
    $('#kycstatussubmit').on('click', (e) => {
        e.preventDefault();
        $('#kycstatusform').submit();
        $('#poststatusform').submit();
        console.log('Form submitted');
    })
    $("#poststatusform").validate({
        rules: {


            poststatusaction: {
                required: true
            },

        },
        messages: {

            poststatusaction: {

                required: "Please select the status"
            }

        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger');
            $(element).addClass('form-control-danger');
        },
        submitHandler: function (form) {
            var input = new FormData($('#poststatusform')[0]);
            input.append('criteriaId', $('meta[name="criteria"]').attr('content'))
            var posttype = $("#posttype").val();
            input.append('posttype', posttype);
            var url = '/post/updatepoststatus';
            var method = "POST";
            credentialcommongfunction(url, input, method, 'kycstatussubmit', 'Update')
            $("#kycstatusform").trigger("reset");
            $("#checkboxerror").text("")
            $("#rejectedcheckbox").css("display", 'none');
            $(".documenttype").prop("checked", false)
            $(".reasonclass").css("display", "none");
            $("#kycstatusaction").val("");
            $("#kycstatusaction-error").text("");
            $("#callingstatus").select2();
            $("#kycstatusaction").select2();
            $("#poststatusactionreasondiv").css("display", 'none')
            $("#poststatusaction").val("");
            $("#poststatusaction-error").text("");
            $("#poststatusactionreason").val("");
            $("#poststatusactionreason").select2();
            $("#poststatusactionreason-error").text("");
            $("#poststatusaction").select2();
            $(".form-group").removeClass('has-danger');
            $(".select2-selection").css('border-color', 'none')


        }
    });

    $('#documentuploadsubmit').on('click', (e) => {
        e.preventDefault();
        $('#documentuploadform').submit();
    })
    $("#documentuploadform").validate({
        rules: {
            selectcategory: {
                required: true
            },
            selectdocument: {
                required: true
            },
            frontfile: {
                required: true,
                extension: "jpeg|jpg|png|pdf"
            },
            backfile: {
                required: false,
                extension: "jpeg|jpg|png|pdf"
            },
            documentnumber: {
                required: true,
                checkDocumentNo: true
            }

        },
        messages: {
            selectcategory: {
                required: "Please select the category"
            },
            selectdocuments: {
                required: "Please select the Document to upload"
            },
            frontfile: {
                required: "Please select the Front file to upload",
                extension: "File type should be image*/pdf"
            },
            documentnumber: {
                required: "Please enter the document number",
                extension: "File type should be image*/pdf"
            }

        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger');
            $(element).addClass('form-control-danger');
        },
        submitHandler: function (form) {
            var usertype = $("#documentuser_type").val();
            var value = parseInt($("#selectcategory").val());
            var selectdocuments = parseInt($("#selectdocuments").val());
            var input = new FormData();
            var url = '/kyc/getkycdocumentstatus';
            var id = $("#documentuser_id").val();
            input.append('id', id);
            input.append('usertype', usertype);
            input.append('usercategory', value);
            input.append('selectdocument', selectdocuments);
            var method = "post";
            commonajaxcall(url, input, method).then(function (result) {
                if (result == "") {
                    $("#errormessage").text("");
                    var input = new FormData($('#documentuploadform')[0]);
                    input.append('criteriaId', $('meta[name="criteria"]').attr('content'))
                    var url = '/kyc/updatekycdocument';
                    var method = "POST";
                    credentialcommongfunction(url, input, method, 'documentuploadsubmit', 'Update')
                    $("#documentuploadform").trigger("reset");
                    $("#gallery-image").css('display', 'none');
                    $("#gallery-image1").css('display', 'none');
                    $("#frontimage").attr('src', '');
                    $("#backimage").attr('src', '');
                    $(".form-group").removeClass('has-danger');
                    $('#selectcategory').select2();
                    $("#selectdocuments").select2();
                    $(".select2-selection").css('border-color', 'none');
                } else {
                    $("#errormessage").text(text);
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    });
    $("#changepasswordform").validate({
        rules: {
            oldpassword: {
                required: true,
            },
            password: {
                required: true,
                minlength: 8,
                strongePassword: true
            },
            confirmpassword: {
                required: true,
                minlength: 8,
                equalTo: "#password",
                strongePassword: true
            },
        },
        messages: {
            oldpassword: {
                required: "Please provide old password",
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 8 characters long"
            },
            confirmpassword: {
                required: "Please provide a confirm password",
                minlength: "Your password must be at least 8 characters long",
                equalTo: "Please enter the same password as above"
            },
        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
        },
        submitHandler: function (form) {
            var input = new FormData($('#changepasswordform')[0]);
            var url = '/admin/changepasswordupdate';
            var method = "POST";
            credentialcommongfunction(url, input, method, 'changepassword', 'Change Password')
        }
    });
    $("#forgotpasswordform").validate({
        rules: {
            username: {
                required: true,
            }
        },
        messages: {

            username: "Please enter a username",
        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
        },
        submitHandler: function (form) {
            var input = new FormData($('#forgotpasswordform')[0]);
            var url = '/admin/sendresetlink';
            var method = "POST";
            credentialcommongfunction(url, input, method, 'forgotpassword', 'Send Link')
        }
    });
    $("#roleform").validate({
        rules: {
            role_title: {
                required: true,
            },
            roletype: {
                required: true
            }
        },
        messages: {
            role_title: "Please enter a role title",
            roletype: "Please select role type"
        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
        },
        submitHandler: function (form) {
            // form.submit();
            var input = new FormData($('#roleform')[0]);
            var url = '/users/rolecreate';
            var method = "POST";
            credentialcommongfunction(url, input, method, 'rolesubmit', 'Save')
        }
    });

    $("#profileform").validate({
        rules: {
            useremail: {
                required: true,
                email: true
            },

            firstname: {
                required: true,
                minlength: 3
            },
            lastname: {
                required: true,
                minlength: 3
            },

        },
        messages: {

            firstname: {
                required: "Please enter a firstname",
                minlength: "Name must consist of at least 3 characters"
            },
            lastname: {
                required: "Please enter a lastname",
                minlength: "Name must consist of at least 3 characters"
            },
            useremail: "Please enter a valid email address"

        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
        },
        submitHandler: function (form) {
            var input = new FormData($('#profileform')[0]);
            var url = '/users/profileupdate';
            var method = "POST";
            credentialcommongfunction(url, input, method, 'profilesubmit', 'Save')
        }
    });
    $("#userform").validate({
        rules: {
            username: {
                required: true,
                minlength: 3
            },
            role: {
                required: true,
            },
            city: {
                required: true
            },
            state: {
                required: true
            },
            useremail: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 8,
                strongePassword: true
            },
            confirmpassword: {
                required: true,
                minlength: 8,
                equalTo: "#password",
                strongePassword: true
            },
            firstname: {
                required: true,
                minlength: 3
            },
            lastname: {
                required: true,
                minlength: 3
            },

        },
        messages: {
            username: {
                required: "Please enter a username",
                minlength: "Name must consist of at least 3 characters"
            },
            firstname: {
                required: "Please enter a firstname",
                minlength: "Name must consist of at least 3 characters"
            },
            lastname: {
                required: "Please enter a lastname",
                minlength: "Name must consist of at least 3 characters"
            },
            useremail: "Please enter a valid email address",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 8 characters long"
            },
            confirmpassword: {
                required: "Please provide a password",
                minlength: "Your password must be at least 8 characters long",
                equalTo: "Please enter the same password as above"
            },
            role: "Please select the role",
            city: "Please select the city",
            state: "Please select the state"

        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
        },
        submitHandler: function (form) {
            var input = new FormData($('#userform')[0]);
            var url = '/users/usercreate';
            var method = "POST";
            var mobile = $("#mobilenumber").val();
            if (mobile === "") {
                credentialcommongfunction(url, input, method, 'usersubmit', 'Save');
            } else {
                var numb = mobile.match(/\d/g);
                numb = numb.join("");
                console.log(numb.length)
                if (numb.length === 12 || numb.length === 2) {
                    credentialcommongfunction(url, input, method, 'usersubmit', 'Save');
                } else {
                    $("#mobileerrors").css('display', 'block');
                    $("#mobileerrors").text("Invalid Mobile number");
                }
            }
        }
    });

    $("#profile_form").validate({
        rules: {

            city: {
                required: true
            },
            state: {
                required: true
            },
            category_id: {
                required: true
            },
            user_type: {
                required: true
            },
            pincode: {
                required: true,
                minlength: 6,
            },
            email: {
                required: true,
                email: true
            },
            company_name: {
                required: true
            },

            firstname: {
                required: true,
                minlength: 3
            },
            lastname: {
                required: true,
                minlength: 3
            },

        },
        messages: {

            firstname: {
                required: "Please enter a firstname",
                minlength: "Name must consist of at least 3 characters"
            },
            lastname: {
                required: "Please enter a lastname",
                minlength: "Name must consist of at least 3 characters"
            },
            email: "Please enter a valid email address",

            pincode: {
                required: "Please enter a pincode",
                minlength: "Your pincdoe must be at least 6 digits long",
            },
            mobile_number: {
                required: "Please enter a mobile number",
                maxlength: "Your mobile number must be at least 10 digits long",
            },
            user_type: "Please select the user_type",
            city: "Please select the city",
            state: "Please select the state",
            category_id: "Please select the user category",
            company_name:"Please enter the company name"

        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
        },
        submitHandler: function (form) {
            var input = new FormData($('#profile_form')[0]);
            var categoryName = $("#category_id option:selected").text();
            input.append('categoryname',categoryName)
            var url = '/users/enduserupdate';
            var method = "POST";
                credentialcommongfunction(url, input, method, 'profileupdatesubmit', 'Save');
        }
    });


    $("#reassignform").validate({
        rules: {
            teamlead: {
                required: true,
            },
            user: {
                required: true,
            },
            'permission[]': {
                required: true
            },

        },
        messages: {

            teamlead: "Please select the teamlead",
            user: "Please select the user",
            'permission[]': "Please select the permission"

        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger')
            $(element).addClass('form-control-danger')
        },
        submitHandler: function (form) {
            var input = new FormData($('#reassignform')[0]);
            var url = '/users/reassignlogic';
            var method = "POST";
            confirmationalert("Do you want to move the record to the poll for selected user", "Reassign",
                "Cancel", 'Reassign', url, input, method);

        }
    });

});
