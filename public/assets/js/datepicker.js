$(function () {
    'use strict';
    var date = new Date();
    if ($('#datePickerExample').length) {
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        $('#datePickerExample').datepicker({
            format: "mm/dd/yyyy",
            todayHighlight: true,
            autoclose: true
        });
        $('#datePickerExample').datepicker('setDate', today);
    }
    $('input[name="dates"]').daterangepicker({
        singleDatePicker: false,
        startDate: moment().subtract(7, 'd'),
        endDate: new Date(),
        maxDate: new Date(),
        locale: {
            format: 'DD-MM-YYYY'
        },
        "alwaysShowCalendars": true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
    });
    // $('input[name="pendingkycdate"]').daterangepicker({
    //     singleDatePicker: true,
    // });

    $('.followdate').daterangepicker({
        singleDatePicker: true,
        minDate: new Date(),
        startDate: new Date(),
        locale: {
            format: 'DD-MM-YYYY'
        }
    });

    // KYC Approval, KYC Pending and KYC Rejected
    $('.filterdate').daterangepicker({
        singleDatePicker: false,
        startDate: moment().subtract(7, 'd'),
        endDate: new Date(),
        maxDate: new Date(),
        locale: {
            format: 'DD-MM-YYYY'
        },
        "alwaysShowCalendars": true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
    });


});
