var roleTable;
var roleArr = [];
var userTable;
var userArr = [];
var cityinfo = [];
var onlinestatus = "";
// document.oncontextmenu = document.body.oncontextmenu = function () {
//     return false;
// }

$('.gallery-image').magnificPopup({
    type: 'image',
    // other options
});
$(document).on('click', '#sendnotify', function (e) {
    if (NotifyArr.length == 0) {
        Toast.fire({
            icon: 'error',
            title: "Please select records to send In-App notification"
        });
    } else {
        $(".text-danger").text("");
        $("#error").text('');
        $("#sendnotifyform").trigger("reset");
        $("#sendnotification").modal({
            backdrop: 'static',
            keyboard: false
        });
    }

})
$(document).on('click', ".moreinfo", function (e) {
    var id = this.id;
    input = new FormData();
    fromdate = $('#dashboarddate').data('daterangepicker').startDate.format('YYYY-MM-DD');
    todate = $('#dashboarddate').data('daterangepicker').endDate.format('YYYY-MM-DD');
    input.append('id', id);
    input.append('fromdate', fromdate);
    input.append('todate', todate);
    var url = 'getuserdetail';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        var html = "";
        var totalpendingcount = 0;
        var totalassignedcount = 0;
        var totalprocessedcount = 0;
        $("#useractivitybody").empty();
        $.each(result.data, function (key, item) {
            totalpendingcount = parseInt(totalpendingcount) + parseInt(item.waitingCount);
            totalassignedcount = parseInt(totalassignedcount) + parseInt(item.totalAssignCount);
            totalprocessedcount = parseInt(totalprocessedcount) + parseInt(item.totalProcessed);
            html += '<tr><td>' + item.criteria + '</td><td>' + item.totalAssignCount + '</td><td>' + item.totalProcessed + '</td><td>' + item.waitingCount + '</td></tr>';
        });
        $("#useractivitybody").append(html);
        $("#totalassignedusercount").text(totalassignedcount);
        $("#totalpendingusercount").text(totalpendingcount);
        $("#totalprocessedusercount").text(totalprocessedcount);
        $("#datecount").text($("#dashboarddate").val())
        $("#useractivity").modal({
            backdrop: 'static',
            keyboard: false
        });
    }).catch(error => {
        console.log(error);
    });

})

$(document).on('click', ".teamleadmoreinfo", function (e) {
    var id = this.id;
    input = new FormData();
    fromdate = $('#dashboarddate').data('daterangepicker').startDate.format('YYYY-MM-DD');
    todate = $('#dashboarddate').data('daterangepicker').endDate.format('YYYY-MM-DD');
    input.append('id', id);
    input.append('fromdate', fromdate);
    input.append('todate', todate);
    var url = 'teamleadgetuserdetail';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        date = $('#dashboarddate').val();
        $("#teamleadinfo").empty();
        var index = 1;
        var html = "";
        html += '<div class="col-md-12 grid-margin"><div class="card">' +
            '<div class="card-body"><div class="row"><div class="col-md-4">' +
            '<span style="font-size:14px;;font-weight:bold">Date: </span>' +
            '<span  style="font-size:14px;font-weight:bold">' + date + '</span><br></div></div><div class="row"><div class="col-md-4">' +
            '<span style="font-size:14px;">Overall Assigned count: </span>' +
            '<span id="totalassignedcount"  class="badge badge-primary" style="font-size:14px;"></span><br></div>' +
            '<div class="col-md-4"> <span style="font-size:14px;">Overall Processed count: </span>' +
            '<span style="font-size:14px;"  class="badge badge-success" id="totalprocessedcount" ></span><br></div>' +
            '<div class="col-md-4"><span style="font-size:14px;">Overall Pending count: </span>' +
            '<span id="totalpendingcount" class="badge badge-danger" style="font-size:14px"></span></div><div class="col-md-4"><span style="font-size:14px;">Overall FollowUp count: </span>' +
            '<span id="overallfollowupcount" class="badge badge-warning" style="font-size:14px"></span></div></div></div></div></div>';
        $("#teamleadinfo").append(html);
        var totalpendingcount = 0;
        var totalassignedcount = 0;
        var totalprocessedcount = 0;
        var overallfollowupcount = 0;
        $.each(result.data, function (key, item) {
            var html1 = "";
            html1 += '<div class="col-md-12 grid-margin"><div class="card"><div class="card-body"><div class="row"><div class="col-md-6"><span style="font-size:14px;font-weight:bold;">' + item.username + '</span></div><div class="col-md-6" style="text-align:right"><span style="font-size:14px;">Today Followup count: </span><span style="font-size:14px;" id="followupcount_' + index + '" class="badge badge-warning"></span></div></div>' +
                '<div class="row" style="margin-top:20px;"><div class="col-md-4">' +
                '<span style="font-size:14px;">Total Assigned count: </span>' +
                '<span id="individualtotalassignedcount_' + index + '"  class="badge badge-primary" style="font-size:14px;"></span><br></div>' +
                '<div class="col-md-4"> <span style="font-size:14px;">Total Processed count: </span>' +
                '<span style="font-size:14px;"  class="badge badge-success" id="individualtotalprocessedcount_' + index + '" ></span><br></div>' +
                '<div class="col-md-4"><span style="font-size:14px;">Total Pending count: </span>' +
                '<span id="individualtotalpendingcount_' + index + '" class="badge badge-danger" style="font-size:14px"></span></div><div>' +
                '<div class="table-responsive pt-3"><table class="table table-bordered"><thead>' +
                '<tr><th>Criteria</th><th>Total Assigned Count</th><th>Total Processed Count</th><th>Total Pending Count</th></tr></thead>' +
                '<tbody id="useractivitybody">';
            totalfollowupCount = 0;
            Individualtotalpendingcount = 0;
            Individualtotalassignedcount = 0;
            Individualtotalprocessedcount = 0;
            $.each(item.permissionDetail, function (key, items) {
                totalpendingcount = parseInt(totalpendingcount) + parseInt(items.waitingCount);
                totalassignedcount = parseInt(totalassignedcount) + parseInt(items.totalAssignCount);
                totalprocessedcount = parseInt(totalprocessedcount) + parseInt(items.totalProcessed);
                totalfollowupCount = parseInt(totalfollowupCount) + parseInt(items.totalFollowupCount);
                overallfollowupcount = parseInt(overallfollowupcount) + parseInt(items.totalFollowupCount);
                Individualtotalpendingcount = parseInt(Individualtotalpendingcount) + parseInt(items.waitingCount);
                Individualtotalassignedcount = parseInt(Individualtotalassignedcount) + parseInt(items.totalAssignCount);
                Individualtotalprocessedcount = parseInt(Individualtotalprocessedcount) + parseInt(items.totalProcessed);
                html1 += '<tr><td>' + items.criteria + '</td><td>' + items.totalAssignCount + '</td><td>' + items.totalProcessed + '</td><td>' + items.waitingCount + '</td></tr>'
            });

            html1 += '</tbody></table></div></div></div></div>';
            $("#teamleadinfo").append(html1);
            $("#followupcount_" + index).text(totalfollowupCount);
            $("#individualtotalpendingcount_" + index).text(Individualtotalpendingcount);
            $("#individualtotalassignedcount_" + index).text(Individualtotalassignedcount);
            $("#individualtotalprocessedcount_" + index).text(Individualtotalprocessedcount);
            index++;
            $("#totalassignedcount").text(totalassignedcount);
            $("#totalpendingcount").text(totalpendingcount);
            $("#totalprocessedcount").text(totalprocessedcount);
            $("#overallfollowupcount").text(overallfollowupcount);

        });

        $("#useractivity").modal({
            backdrop: 'static',
            keyboard: false
        });
    }).catch(error => {
        console.log(error);
    });

});


const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 4000
});

$('[data-tooltip="tooltip"]').tooltip();
$(document).ready(function () {
    var uri = window.location.toString();
    if (uri.indexOf("*") > 0) {
        var clean_uri = uri.substring(0, uri.indexOf("*"));
        window.history.replaceState({}, document.title, clean_uri);
    }
});
$("#logout").on('click', function (e) {
    var id = this.id;
    var input = new FormData();
    input.append('id', id);
    var url = '/logout';
    var method = "post";
    confirmationalert("Do you want to logout", "Logout",
        "Cancel", 'Logout', url, input, method)
})

$("#profileupdate").on('click', function (e) {


    $(".text-danger").text("");
    $("#error").text('');
    $("#profileform").trigger("reset");
    $("#profileupdatemodal").modal({
        backdrop: 'static',
        keyboard: false
    });
    $(".notifydiv").removeClass('has-danger');
});

$(".onlineselect").on('click', function (e) {

    if (document.getElementById('flexSwitchCheckChecked').checked) {
        onlinestatus = 0;
    } else {
        onlinestatus = 1;
    }
    input = new FormData();
    input.append('onlinestatus', onlinestatus);
    var url = '/admin/updateonlinestatus';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        console.log(result);
    }).catch(error => {
        console.log(error);
    });

});

function credentialcommongfunction(url, input, method, id, text) {
    $("#" + id).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Loading...');
    console.log(input);
    $("#error").text("");
    $("#success").text("");
    commonajaxcall(url, input, method)
        .then(result => {
            console.log(result);
            var message = result.message;
            if (message == 'success') {
                window.location.href = result.nextpage;
            } else {
                $("#" + id).html(text);
                if (id == "forgotpassword") {
                    $("#success").text(result.message);
                } else if (id == "documentuploadsubmit") {
                    $("#uploaddocumentmodal").modal('hide');
                        Toast.fire({
                            icon: 'success',
                            title: result.message
                        });
                }else if (id == "postsubmit") {
                    Toast.fire({
                        icon: 'success',
                        title: result.message
                    });
                    setTimeout(function (e) {
                        location.reload();

                    }, 1000)
                } else if (id == "flatsubmit") {
                    Toast.fire({
                        icon: 'success',
                        title: result.message
                    });
                    $("#postdetailview").modal('hide');
                    location.reload();
                } else if (id == "rolesubmit") {
                    $('#roleTable').DataTable().ajax.reload();
                    $("#rolemodal").modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: result.message
                    })
                } else if (id == "usersubmit") {
                    $('#userTable').DataTable().ajax.reload();
                    $("#usermodal").modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: result.message

                    });
                    getTeamLeadinfo();
                } else if (id == "profilesubmit") {
                    $("#profileupdatemodal").modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: result.message
                    });

                } else if (id == "profileupdatesubmit") {
                    Toast.fire({
                        icon: 'success',
                        title: result.message
                    });
                    $("#upgradeuser").DataTable().ajax.reload();
                    $("#userprofileeditmodal").modal('hide')
                } else if (id == "callstatussubmit" || id == "kycstatussubmit") {
                    if (result.code) {
                        Toast.fire({
                            icon: 'error',
                            title: 'Please upload KYC document to proceed'
                        });
                    } else {
                        Toast.fire({
                            icon: 'success',
                            title: result.message
                        });
                        $("#upgradeuser").DataTable().ajax.reload();
                        $('#kycapproval').DataTable().ajax.reload();
                        $('#walletexpiry').DataTable().ajax.reload();
                        $('#notmakepayment').DataTable().ajax.reload();
                        $('#walletrechargefailed').DataTable().ajax.reload();
                        $('#leadsnotviewed').DataTable().ajax.reload();
                        $('#markedfavourite').DataTable().ajax.reload();
                        $('#kycpendingreject').DataTable().ajax.reload();
                        $('#post').DataTable().ajax.reload();
                        $('#inactiveuser').DataTable().ajax.reload();
                        $('#notpost').DataTable().ajax.reload();
                        $('#notpostservicereq').DataTable().ajax.reload();
                        $('#notenquirepost').DataTable().ajax.reload();
                        $("#editviewmodal").modal('hide')
                    }

                } else if (id = "sendnotificationBtn") {
                    $("#sendnotification").modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: result.message
                    });
                } else if (id == "submit") {
                    $("#error").text(result.message);
                } else {
                    $("#error").text(result.message);
                }

            }
            $("#" + id).html(text);
        }).catch(error => {
            console.log(error);
            if (error.responseJSON.message) {
                if (error.responseJSON.errors) {
                    $("#error").text(error.responseJSON.errors.username[0]);
                } else {
                    $("#error").text(error.responseJSON.message);
                }


            } else {
                if (result.message == "CSRF token mismatch") {
                    $("#error").text(result.message);
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                } else {
                    $("#error").text(error.message);
                }

            }

            $("#" + id).html(text);
        })
}

function commonajaxcall(url, input, method) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    return new Promise((resolve, reject) => {
        $.ajax({
            type: method,
            url: url,
            data: input,
            contentType: false,
            processData: false,
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            }
        });
    });
}
$(".visible").on('click', function (e) {
    var id = this.id;
    var splitId = id.split('_');
    var useId = $('#' + splitId[0]);
    var inputType = useId.attr('type');
    if (inputType == "password") {
        useId.prop("type", "text");
    } else {
        useId.prop("type", "password");
    }

})

/* Role Module functionality */

$("#addrole").on('click', function (e) {
    changerolemodaldata("Add Role", "Save");
    $("#role_id").val(0)
});

$("#tbody").on('click', '.delete', function (e) {
    var id = this.id;
    var input = new FormData();
    input.append('role_id', id);
    var url = '/users/roledelete';
    var method = "POST";
    confirmationalert("Do you want to Delete the selected record", "Delete",
        "Cancel", 'Delete', url, input, method)

});
$("#tbody").on('click', '.edit', function (e) {
    var id = this.id;
    changerolemodaldata("Update Role", "Update")
    roleobj = roleArr[roleArr.map(function (item) {
        return item.role_id;
    }).indexOf(parseInt(id))];
    $("#role_title").val(roleobj.role_title)
    $("#role_id").val(id)
    $("#roletype").val(roleobj.role_type_id);
    $('#roletype').select2().trigger('change');

});

function changerolemodaldata(headerText, buttonText) {
    $("#roletext").text(headerText);
    $("#rolesubmit").text(buttonText);
    $("#role_title-error").text("");
    $("#roletype-error").text("")
    $("#roleform").trigger("reset");
    $("#rolemodal").modal({
        backdrop: 'static',
        keyboard: false
    });
    $("#error").text('');
}
/* Role Module functionality */

/*permission module functionality */

var permissionrole = $("#permissionrole").val();
var pathname = window.location.pathname;
if (pathname.includes("permissions")) {
    getRoleBasedPermission(permissionrole);
}
$("#permissionrole").on('change', function (e) {
    permissionrole = $("#permissionrole").val();
    $('input').val('');
    getRoleBasedPermission(permissionrole);
});

function getRoleBasedPermission(permissionrole) {
    input = new FormData();
    input.append('role_id', permissionrole);
    var url = '/users/getrolebasedpermission';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        console.log(result);
        $('input:checkbox').prop('checked', false);
        var data = result.data;
        $.each(data, function (index, item) {
            permission_id = item.permission_id;
            assign_limit = item.assign_limit;
            $("#assign_limit_" + permission_id).val(assign_limit);
            is_access = item.is_access;
            if (is_access == 1) {
                $("#is_access_" + permission_id).prop('checked', true);
            } else {
                $("#is_access_" + permission_id).prop('checked', false);
            }
            is_approve = item.is_approve;
            if (is_approve == 1) {
                $("#is_approve_" + permission_id).prop('checked', true);
            } else {
                $("#is_approve_" + permission_id).prop('checked', false);
            }
            is_bulkknotificationaccess = item.is_bulkknotificationaccess;
            if (is_bulkknotificationaccess == 1) {
                $("#is_bulkknotificationaccess_" + permission_id).prop('checked', true);
            } else {
                $("#is_bulkknotificationaccess_" + permission_id).prop('checked', false);
            }
            is_edit_applicable = item.is_edit_applicable;
            if (is_edit_applicable == 1) {
                $("#is_edit_applicable_" + permission_id).prop('checked', true);
            } else {
                $("#is_edit_applicable_" + permission_id).prop('checked', false);
            }
            is_exportaccess = item.is_exportaccess;
            if (is_exportaccess == 1) {
                $("#is_exportaccess_" + permission_id).prop('checked', true);
            } else {
                $("#is_exportaccess_" + permission_id).prop('checked', false);
            }
            is_follow_up = item.is_follow_up;
            if (is_follow_up == 1) {
                $("#is_follow_up_" + permission_id).prop('checked', true);
            } else {
                $("#is_follow_up_" + permission_id).prop('checked', false);
            }
            is_kycupdate = item.is_kycupdate;
            if (is_kycupdate == 1) {
                $("#is_kycupdate_" + permission_id).prop('checked', true);
            } else {
                $("#is_kycupdate_" + permission_id).prop('checked', false);
            }
            is_mobilenumberread = item.is_mobilenumberread;
            if (is_mobilenumberread == 1) {
                $("#is_mobilenumberread_" + permission_id).prop('checked', true);
            } else {
                $("#is_mobilenumberread_" + permission_id).prop('checked', false);
            }
            is_readcompleteddata = item.is_readcompleteddata;
            if (is_readcompleteddata == 1) {
                $("#is_readcompleteddata_" + permission_id).prop('checked', true);
            } else {
                $("#is_readcompleteddata_" + permission_id).prop('checked', false);
            }
            is_useractive = item.is_useractive;
            if (is_useractive == 1) {
                $("#is_useractive_" + permission_id).prop('checked', true);
            } else {
                $("#is_useractive_" + permission_id).prop('checked', false);
            }
            is_view_applicable = item.is_view_applicable;
            if (is_view_applicable == 1) {
                $("#is_view_applicable_" + permission_id).prop('checked', true);
            } else {
                $("#is_view_applicable_" + permission_id).prop('checked', false);
            }

            is_callstatus = item.is_callstatus;
            if (is_callstatus == 1) {
                $("#is_callstatus_" + permission_id).prop('checked', true);
            } else {
                $("#is_callstatus_" + permission_id).prop('checked', false);
            }

            is_postupdate = item.is_postupdate;
            if (is_postupdate == 1) {
                $("#is_postupdate_" + permission_id).prop('checked', true);
            } else {
                $("#is_postupdate_" + permission_id).prop('checked', false);
            }
            is_publishactive = item.is_publishactive;
            if (is_publishactive == 1) {
                $("#is_publishactive_" + permission_id).prop('checked', true);
            } else {
                $("#is_publishactive_" + permission_id).prop('checked', false);
            }


        });
    }).catch(error => {
        console.log(error);
    });
}
$(".assignlimit").on('change', function (e) {
    console.log(this.id);
    console.log(this.value)
    commonClickFunction(this.id, 'assign_limit', this.value, 'input');
});
$(".access").on('click', function (e) {
    id = this.id;
    columnname = "is_access";
    // alert(this.value)
    commonClickFunction(id, columnname, '', 'checkbox');
});
$(".mobilenumberaccess").on('click', function (e) {
    var id = this.id;
    columnname = "is_mobilenumberread";
    commonClickFunction(id, columnname, '', 'checkbox');
});
$(".followup").on('click', function (e) {
    var id = this.id;
    columnname = "is_follow_up";
    commonClickFunction(id, columnname, '', 'checkbox');
});

$(".callstatus").on('click', function (e) {
    var id = this.id;
    columnname = "is_callstatus";
    commonClickFunction(id, columnname, '', 'checkbox');
});

$(".postupdate").on('click', function (e) {
    var id = this.id;
    columnname = "is_postupdate";
    commonClickFunction(id, columnname, '', 'checkbox');
});
$(".publishstatus").on('click', function (e) {
    var id = this.id;
    columnname = "is_publishactive";
    commonClickFunction(id, columnname, '', 'checkbox');
});


$(".kycaccess").on('click', function (e) {
    var id = this.id;
    columnname = "is_kycupdate";
    commonClickFunction(id, columnname, '', 'checkbox');
});
$(".useractiveaccess").on('click', function (e) {
    var id = this.id;
    columnname = "is_useractive";
    commonClickFunction(id, columnname, '', 'checkbox');
});

$(".approveaccess").on('click', function (e) {
    var id = this.id;
    columnname = "is_approve";
    commonClickFunction(id, columnname, '', 'checkbox');
});
$(".exportaccess").on('click', function (e) {
    var id = this.id;
    columnname = "is_exportaccess";
    commonClickFunction(id, columnname, '', 'checkbox');
});
$(".notificationdataaccess").on('click', function (e) {
    var id = this.id;
    columnname = "is_bulkknotificationaccess";
    commonClickFunction(id, columnname, '', 'checkbox');
});
$(".editaccess").on('click', function (e) {
    var id = this.id;
    columnname = "is_edit_applicable";
    commonClickFunction(id, columnname, '', 'checkbox');
});
$(".viewaccess").on('click', function (e) {
    var id = this.id;
    columnname = "is_view_applicable";
    commonClickFunction(id, columnname, '', 'checkbox');
});
$(".completeddataaccess").on('click', function (e) {
    var id = this.id;
    columnname = "is_readcompleteddata";
    commonClickFunction(id, columnname, '', 'checkbox');
});

function commonClickFunction(id, columnname, assignvalue, inputtype) {
    permissionrole = $("#permissionrole").val();
    if (document.getElementById(id).checked) {
        value = 1;
        $("#" + id).prop('checked', true);
    } else {
        value = 0;
        $("#" + id).prop('checked', false);
    }
    var splitid = id.split("_");
    permission_id = splitid[splitid.length - 1];
    if (inputtype == 'input') {
        value = assignvalue;
        if (value == "") {
            value = 0;
        }
    }
    input = new FormData();
    input.append('role_id', permissionrole);
    input.append('columnname', columnname);
    input.append('permission_id', permission_id);
    input.append('value', value);
    var url = '/users/updaterolebasedpermission';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        console.log(result);
        Toast.fire({
            icon: 'success',
            title: result.message
        })
    }).catch(error => {
        Toast.fire({
            icon: 'error',
            title: error.message
        })
        console.log(error);
    });
}

/*permission module functionality - end */

/* User module functionality */
$("#states").on("change", function (e) {
    input = new FormData();
    input.append('stateid', this.value);
    var url = '/users/getstate';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        console.log(result)
        cityinfo = result.data;
        $("#city_id").empty();
        $("#city_id").append("<option value=''>Select</option>")
        $.each(cityinfo, function (index, item) {
            $("#city_id").append('<option value=' + item.id + '>' + item.title + '</option>')
        });
    }).catch(error => {
        console.log(error)
    });

})
$("#state").on("change", function (e) {
    input = new FormData();
    input.append('stateid', this.value);
    var url = '/users/getstate';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        console.log(result)
        cityinfo = result.data;
        $("#city").empty();
        $("#city").append("<option value=''>Select</option>")
        $.each(cityinfo, function (index, item) {
            $("#city").append('<option value=' + item.id + '>' + item.title + '</option>')
        });
    }).catch(error => {
        console.log(error)
    });

})

$("#adduser").on('click', function (e) {
    $("#state").val("");
    $("#city").val("");
    $("#state").select2();
    $("#city").select2();
    changeusermodaldata("Add User", "Save");
    $(".passworddiv").css('display', 'flex');
    $("#profilepic").attr('src', "");
    $(".imagedelete").attr('id', 0);
    $("#user_id").val(0)
    $(".imagedelete").css('display', "none");
});
$("#tbody").on('click', '.useredit', function (e) {
    var id = this.id;
    changeusermodaldata("Update User", "Update")
    var userinfo = userArr[userArr.map(function (item) {
        return item.user_id;
    }).indexOf(parseInt(id))];
    $("#firstname").val(userinfo.first_name);
    $("#lastname").val(userinfo.last_name);
    $("#mobilenumber").val(userinfo.mobilenumber);
    $("#useremail").val(userinfo.email);
    $("#teamlead").val(userinfo.team_lead_id);
    $('#teamlead').select2().trigger('change');
    $("#state").val(userinfo.state_id);
    $('#state').select2().trigger('change');
    $("#role").val(userinfo.role_id);
    $('#role').select2().trigger('change');
    $("#assignlimit").val(userinfo.assign_limit);
    $("#username").val(userinfo.username);
    $("#user_id").val(id);
    $(".passworddiv").css('display', 'none');
    if (userinfo.image != "") {
        $("#profilepic").attr('src', userinfo.image);
        $(".imagedelete").css('display', "block");
        $(".imagedelete").attr('id', id);
    }

    setTimeout(function () {
        $("#city").val(userinfo.city_id);
        $('#city').select2().trigger('change');
    }, 1000);

});
$("#tbody").on('click', '.statusinactive', function (e) {
    var id = this.id;
    var input = new FormData();
    input.append('id', id);
    input.append('status', 0);
    var url = '/users/statusupdate';
    var method = "POST";
    confirmationalert("Do you want to update the selected record", "Update",
        "Cancel", 'Status', url, input, method)

});

function getTeamLeadinfo() {
    var input = new FormData();
    input.append('id', 0);
    var url = '/users/getteamlead';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        console.log(result);
        var data = result.data;
        $("#teamlead").empty();
        $("#teamlead").append('<option value = 0 > Select Team lead </option>');
        $.each(data, function (index, item) {
            $("#teamlead").append('<option value=' + item.user_id + '>' + item.username + '</option>')
        });

    }).catch(error => {

        console.log(error);
    });
}
$("#tbody").on('click', '.statusactive', function (e) {
    var id = this.id;
    var input = new FormData();
    input.append('id', id);
    input.append('status', 1);
    var url = '/users/statusupdate';
    var method = "POST";
    confirmationalert("Do you want to update the selected record", "Update",
        "Cancel", 'Status', url, input, method);

});
$(".imagedelete").on('click', function (e) {
    var id = this.id;
    var input = new FormData();
    input.append('id', id);
    input.append('status', 0);
    var url = '/users/deleteprofilepic';
    var method = "POST";
    confirmationalert("Do you want to delete the selected profile pic", "Delete",
        "Cancel", 'PicDelete', url, input, method);
});

function changeusermodaldata(headerText, buttonText) {
    $("#usertext").text(headerText);
    $("#usersubmit").text(buttonText);
    $(".text-danger").text("");
    $("#error").text('');
    $("#errors").text('');
    $("#mobileerrors").text('');
    $("#userform").trigger("reset");
    $("#usermodal").modal({
        backdrop: 'static',
        keyboard: false
    });
}
/* User module functionality  end*/

/* sweet alert */
$("#callingstatus").on('change', function (e) {
    var callingstatus = $("#callingstatus").val();
    if (callingstatus != 3) {
        if (callingstatus != "") {
            if (callingstatus == 2) {
                $("#followdate").val(moment().format("DD-MM-YYYY"))
            } else {
                $("#followdate").val("")
            }

            $("#followdatediv").css('display', 'block');

        } else {

            $("#followdatediv").css('display', 'none');
        }

    } else {
        criteriaId = $('meta[name="criteria"]').attr('content')
        if (criteriaId == 1 || criteriaId == 2 || criteriaId == 3 ||
            criteriaId == 4 || criteriaId == 5 || criteriaId == 13 ||
            criteriaId == 15 || criteriaId == 16) {
            id = $("#user_id").val();
            var userinfo = CommonArr[CommonArr.map(function (item) {
                return item.id;
            }).indexOf(parseInt(id))];
            if (userinfo.status_id == 2) { // followup
                alertfunction(userinfo.followup_date)
            }

        } else {
            trn_id = $("#user_id").val();
            var userinfo = CommonArr[CommonArr.map(function (item) {
                return item.trn_id;
            }).indexOf(parseInt(trn_id))];
            if (userinfo.status_id == 2) { // followup
                alertfunction(userinfo.followup_date)
            }
        }
        $("#followdatediv").css('display', 'none');
    }
});

function alertfunction(followup_date) {
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false,
    });
    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: 'You are trying to change the calling status to Closed for the record which is in Follow-Up on ' + followup_date,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'ml-2',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        reverseButtons: true
    }).then(function (result) {
        if (result.value) {
            return true;
        } else {
            $("#callingstatus").val("");
            $("#callingstatus").select2();
            return false;
        }

    });
}

function confirmationalert(text, successbuttontext, cancelbuttontxt, confirmtype, url, input, method, id = "") {
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false,
    });
    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: text,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'ml-2',
        confirmButtonText: successbuttontext,
        cancelButtonText: cancelbuttontxt,
        reverseButtons: true
    }).then(function (result) {
        if (result.value) {
            // if (confirmtype == "Logout") {
            //     var a = document.createElement('a');
            //     a.style.display = 'none';
            //     a.href = '/logout';
            //     document.body.appendChild(a);
            //     a.click();
            //     return true;
            // }
            commonajaxcall(url, input, method).then(function (result) {
                if (confirmtype == "Delete") {
                    $('#roleTable').DataTable().ajax.reload();
                    return true;
                } else if (confirmtype == "Posted status") {

                    setTimeout(function (e) {
                        location.reload();
                    }, 1000)

                } else if (confirmtype == "PicDelete") {
                    $("#profilepic").attr('src', "");
                    $(".imagedelete").attr('id', 0);
                    $(".imagedelete").css('display', "none");

                } else if (confirmtype == "Logout") {
                    location.reload();
                    return true;
                } else if (confirmtype == "Approve") {
                    $("." + id).empty();
                    $("." + id).append('<span class="badge badge-success">Approved</span>');

                } else if (confirmtype == "Reject") {
                    $("." + id).empty();
                    $("." + id).append('<span class="badge badge-danger">Rejected</span>');

                } else if (confirmtype == "User status") {

                    $('#kycpendingreject').DataTable().ajax.reload();
                } else if (confirmtype == "Published status" || confirmtype == "Posted status") {
                    $("#postdetailview").modal('hide');
                    $('#post').DataTable().ajax.reload();
                } else if (confirmtype == "DeletePhoto") {
                    //nothing to do
                    imagetype = result.imagetype;
                    posttype = result.posttype;
                    postid = result.post_id;
                    image_id = result.image_id;
                    if (imagetype == "mainimage") {
                        $('#mainimagediv').remove();
                    } else if (imagetype == "certificateimage") {
                        $('#certificatediv').remove();
                    } else if (imagetype == "video") {
                        $('#videodiv').remove();
                    } else if (imagetype == "flatimage") {
                        $('#flatimagediv').remove();
                    } else {
                        $('#' + posttype + '_' + image_id + '_otherimage').remove();
                    }
                    var totalotherimage = parseInt($("#totalotherimage").val());
                    totalotherimage = totalotherimage - 1;
                    $("#totalotherimage").val(totalotherimage);

                } else if (confirmtype == "Reassign") {
                    $('#reassignform').trigger("reset");
                    $("#teamlead").val('');
                    $("#user").val('');
                    $("permission").val('');
                    $('.js-example-basic-multiple').select2()
                } else {

                    $('#userTable').DataTable().ajax.reload();

                }
                Toast.fire({
                    icon: 'success',
                    title: result.message
                });

            });
        } else {
            return false;
        }

    });
}
/* sweet alert end */

/**
 * User Activity - common js
 */

$("#posttype").on('change', function (e) {
    var value = this.value;
    if (value == "S") {
        $('.property').hide();
        $('.project').hide();
        $('.service').show();

    } else if (value == "PR") {
        $('.property').hide();
        $('.project').show();
        $('.service').hide();

    } else {
        $('.property').show();
        $('.project').hide();
        $('.service').hide();

    }

});

/**
 * get Servicesubcategory
 */
$("#Servicecategory").on('change', function (e) {
    var value = this.value;
    input = new FormData();
    input.append('id', value);
    var url = '/useractivity/getservicesubcategory';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        console.log(result)
        serviceSubCatInfo = result.data;
        $("#servicesubcategory").empty();
        $("#servicesubcategory").append("<option value=''>Select</option>")
        $.each(serviceSubCatInfo, function (index, item) {
            $("#servicesubcategory").append('<option value=' + item.id + '>' + item.title + '</option>')
        });
    }).catch(error => {
        console.log(error)
    });

})
$("#propertytype").on('change', function (e) {
    var value = this.value;
    input = new FormData();
    input.append('id', value);
    var url = '/useractivity/getpropertycategory';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        console.log(result)
        propertyCategory = result.data;
        $("#propertycategory").empty();
        $("#propertycategory").append("<option value=''>Select</option>")
        $.each(propertyCategory, function (index, item) {
            $("#propertycategory").append('<option value=' + item.id + '>' + item.title + '</option>')
        });
    }).catch(error => {
        console.log(error)
    });
})

notificationrealtime()

function notificationrealtime() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: '/users/notificationajaxcall',
        contentType: false,
        processData: false,
        success: function (result) {
            $("#nav-notifications").empty();
            $("#nav-notifications").append(result);
            feather.replace();
        },
        error: function (error) {
            console.log(error.message)
        }
    });
}

setInterval(function () {
    notificationrealtime();
}, 60000);

$("#download_btn").on('click', function () {
    var date = $('#dashboarddate').val();
    var element = document.getElementById('teamleadinfobody');
    var opt = {
        margin: .05,
        filename: date + '_dashboard.pdf',
        html2canvas: {
            scale: 1
        },
        jsPDF: {
            unit: 'in',
            format: 'letter',
            orientation: 'landscape'
        }
    };
    html2pdf().set(opt).from(element).save();

});

$("#btnExport").on('click', function (e) {
    var date = $('#dashboarddate').val();
    let file = new Blob([$('#teamleadinfobody').html()], {
        type: "application/vnd.ms-excel"
    });
    let url = URL.createObjectURL(file);
    let a = $("<a />", {
        href: url,
        download: date + "_download.xls"
    }).appendTo("body").get(0).click();
    e.preventDefault();
});

/**
 * End User Activity
 */
