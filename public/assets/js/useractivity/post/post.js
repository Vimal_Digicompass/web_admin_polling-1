$("#filtersubmit").on('click', function () {
    $('#post').DataTable().destroy();
    $("#filtersubmit").text("Processing...")
    common_load_data("click");
});
var CommonArr = [];
var NotifyArr = [];
common_load_data('onload');

function common_load_data(type) {

    var fromdate = "";
    var todate = "";
    if (type == "onload") {
        fromdate = moment().subtract(7, 'd').format('YYYY-MM-DD');
        todate = moment().format('YYYY-MM-DD');
    } else {
        fromdate = $('#fromdate').data('daterangepicker').startDate.format('YYYY-MM-DD');
        todate = $('#fromdate').data('daterangepicker').endDate.format('YYYY-MM-DD');
    }

    var state = $("#state").val();
    var city = $("#city").val();
    var searchcallstatus = $("#poststatus").val();
    var posttype = $("#posttype").val();
    var projectcategory = $("#projectcategory").val();
    var usertype = $("#usertype").val();
    var propertytype = $("#propertytype").val();
    var propertycategory = $("#propertycategory").val();
    var servicecategory = $("#Servicecategory").val();
    var servicesubcategory = $("#servicesubcategory").val();
    load_data(fromdate, todate, usertype, state, city, searchcallstatus, propertytype,
        propertycategory, servicecategory, servicesubcategory, projectcategory, posttype)
}

function load_data(fromdate = "", todate = "", usertype = "", state = "", city = "", status = "", propertytype = "",
    propertycategory = "", servicecategory = "", servicesubcategory = "", projectcategory = "", posttype = "") {
    var getnotifydata = $("#getnotifydata").text();
    var searchcallstatus = $("#searchcallstatus").val();
    var input = {};
    input['id'] = $('meta[name="fetchid"]').attr('content');
    input['fromdate'] = fromdate;
    input['todate'] = todate;
    input['state'] = state;
    input['city'] = city;
    input['status'] = status;
    input['posttype'] = posttype;
    input['searchcallstatus'] = searchcallstatus;
    input['criteriaId'] = $('meta[name ="criteria"]').attr('content');
    if (posttype == "PR") {
        $("#type").text('Project Category')
        input['projectcategory'] = projectcategory;
    } else if (posttype == "P") {
        $("#type").text('Property Type')
        input['usertype'] = usertype;
        input['propertytype'] = propertytype;
        input['propertycategory'] = propertycategory;

    } else if (posttype == "S") {
        $("#type").text('Service Category')
        input['servicecategory'] = servicecategory;
        input['servicesubcategory'] = servicesubcategory;
    }
    var obj = [];
    if (parseInt(getnotifydata) == 0) {
        obj = [{
                'data': 'name',
                'name': 'name',
                 'orderable': false,
            },
            {
                'data': 'usertype',
                'name': 'usertype'
            },
            {
                'data': 'mobile_number',
                'name': 'mobile_number'
            },
            {
                'data': 'post_Date',
                'name': 'post_Date'
            },

            {
                'data': 'state',
                'name': 'state'
            },
            {
                'data': 'city',
                'name': 'city'
            }, {
                'data': 'post_category',
                'name': 'post_category'
            },


            {
                'data': 'post_status',
                'name': 'post_status'
            },
            // {
            //     'data': 'rejected_date',
            //     'name': 'rejected_date'
            // },
            {
                'data': 'reject_message',
                'name': 'reject_message'
            }, {
                'data': 'crmusername',
                'name': 'crmusername'
            },
            {
                'data': 'publish_status',
                'name': 'publish_status'
            },

            {
                'data': 'status',
                'name': 'status'
            },
            {
                'data': 'followup_date',
                'name': 'followup_date'
            },
            {
                'data': 'remarks',
                'name': 'remarks'
            },
            {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ]
    } else {
        obj = [{
                'data': 'checkbox',
                'name': 'checkbox',
                'orderable': false,
                'className': 'select-checkbox'
            }, {
                'data': 'name',
                'name': 'name'
            }, {
                'data': 'usertype',
                'name': 'usertype'
            }, {
                'data': 'mobile_number',
                'name': 'mobile_number'
            }, {
                'data': 'post_Date',
                'name': 'post_Date'
            }, {
                'data': 'state',
                'name': 'state'
            }, {
                'data': 'city',
                'name': 'city'
            }, {
                'data': 'post_category',
                'name': 'post_category'
            }, {
                'data': 'post_status',
                'name': 'post_status'
            },
            // {
            // 'data': 'rejected_date',
            // 'name': 'rejected_date'
            // },
            {
                'data': 'reject_message',
                'name': 'reject_message'
            }, {
                'data': 'crmusername',
                'name': 'crmusername'
            }, {
                'data': 'publish_status',
                'name': 'publish_status'
            }, {
                'data': 'status',
                'name': 'status'
            }, {
                'data': 'followup_date',
                'name': 'followup_date'
            }, {
                'data': 'remarks',
                'name': 'remarks'
            }, {
                'data': 'action',
                'name': 'action',
                orderable: false,
                searchable: false
            }
        ]
    }
    var commontable = $('#post').DataTable({
        serverSide: true,
        responsive: false,
        processing: true,
        order: [],
        aasorting: [],
        scrollX: true,
        dom: 'lBfrtip',
        select: {
            style: 'multi', // 'single', 'multi', 'os', 'multi+shift'
            selector: 'td:first-child',
        },
        "ajax": {
            "url": "/post/getpostlist",
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            "type": "POST",
            data: input,
        },
        drawCallback: function (settings) {
            CommonArr = settings.json.data
            $('.tools').tooltip({
                container: 'body'
            });
            feather.replace();

        },
        rowCallback: function (row, data) {
            if (data.hideout) {
                var hideout = data.hideout;
                if (hideout == 1) {
                    $(row).addClass("blurrow");
                     $('td:eq(0)', row).removeClass('select-checkbox')
                }
            }

        },
        "columns": obj,
        "aLengthMenu": [
            [10, 30, 50],
            [10, 30, 50]
        ],
        "iDisplayLength": 10,
        "language": {
            search: "",
            "searchPlaceholder": "search",
        },
    });
    $("#filtersubmit").text("Search")
    commontable.on('select deselect draw', function () {
        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        var data = commontable.rows({
            selected: true,
            search: 'applied'
        }).data()
        NotifyArr = [];
        for (var i = 0; i < data.length; i++) {
            notify_Id = data[i]['user_id'];
            // if (data[i]['hideout']) {
            // if (data[i]['hideout'] == 0) {
            NotifyArr.push(
                notify_Id);
            // }
            // }
        }
        if (selectedRows == 0) {
            $('#CheckAllButton i').attr('class', 'fa fa-square-o');
        } else {
            if (selectedRows == all) {
                $('#CheckAllButton i').attr('class', 'fa fa-check-square-o');
            }


        }
    });
    $('#CheckAllButton').on('click', function () {
        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        if (selectedRows < all) {
            //Added search applied in case user wants the search items will be selected
            commontable.rows({
                search: 'applied'
            }).deselect();
            commontable.rows({
                search: 'applied'
            }).select();
        } else {
            commontable.rows({
                search: 'applied'
            }).deselect();
        }
    });
}

$(document).on('click', '.removeimage', async function (e) {
    var posttype = $(this).attr('posttype');
    var id = $(this).attr('imageid');
    var imagetype = $(this).attr('imagetype');
    var imagename = $(this).attr('imagename');
    var postid = $(this).attr('postid');
    var imageextension = $(this).attr('imageextension');
    var input = new FormData();
    input.append('id', id);
    input.append('posttype', posttype);
    input.append('imagetype', imagetype);
    input.append('imagename', imagename);
    input.append('postid', postid);
    input.append('imageextension', imageextension);
    var url = '/post/deleteimage';
    var method = "post";
    confirmationalert("Do you want to delete the document", "Delete",
        "Cancel", 'DeletePhoto', url, input, method, "classvalue");


});


$(document).on('click', '#postsubmit', (e) => {
    e.preventDefault();
    $('#projectform').submit();
    $('#serviceeditform').submit();
    $('#propertyeditform').submit();
    $('#flateditform').submit();
});

$(document).on('click', '.flatview', function (e) {
    var id = this.id;
    var split = id.split('_');
    var flatid = split[1];
    var postid = split[0];
    var input = new FormData();
    input.append('id', flatid);
    input.append('postid', postid);
    var url = '/post/flatdetails';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        var data = result;
        $("#postdetailview").empty();
        $("#postdetailview").append(data);
        $(".js-example-basic-multiple").select2();
        $('#possessiondate').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY'
            }
        });
        feather.replace();
        $("#postdetailview").modal({
            keyboard: false,
            backdrop: 'static'
        });
        sendflat();
        imageview()

    }).catch(error => {
        console.log(error);
    });
});
$(document).on('change', '#transaction_type_id', function (e) {
    value = this.value;
    if (value == 2) { //resale
        $("#propertyage").show();
    } else {
        $("#propertyage").hide();
    }
})
$(document).on('change', '#transactiontype', function (e) {
    value = this.value;
    if (value == 2) { //resale
        $("#propertyage").show();
    } else {
        $("#propertyage").hide();
    }
})
$(document).on('change', "#property_sub_category_id", function (e) {
    var subcatetegory_id = this.value;
    var posttype = $("#posttype").val();
    var id = $("#postid").val();
    var input = new FormData();
    input.append('id', id);
    input.append('posttype', posttype);
    input.append('operationtype', 'edit')
    input.append('subcategoryid', subcatetegory_id)
    var url = '/getdetailview';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        var data = result;
        $("#postdetailview").empty();
        $("#postdetailview").append(data);
        $(".js-example-basic-multiple").select2();
        $('#possession_date').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY'
            }
        });
        $('#available_from').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY'
            }
        });
        $('.timepicker').timepicker({
            timeFormat: 'H:i:s',
            step: 60,
            minTime: '0',
            maxTime: '23',
            dynamic: true,
            dropdown: true,
            scrollbar: true
        });

        feather.replace();
        $("#postdetailview").modal({
            keyboard: false,
            backdrop: 'static'
        });
        sendproperty();


    }).catch(error => {
        console.log(error);
    });
})



function sendflat() {

    $("#flateditform").validate({
        rules: {
            bhk: {
                required: true
            },
            builtuparea: {
                required: true
            },
            carpetarea: {
                required: true
            },
            floor: {
                required: true
            },
            transactiontype: {
                required: true
            },
            price: {
                required: true
            },
            possessiondate: {
                required: true
            },
            'idealbusiness[]': {
                required: true
            },
            floor_plan_file_name: {
                required: false,
                extension: "jpeg|jpg|png"
            },
        },
        messages: {
            bhk: {
                required: "Please select BHK"
            },
            builtuparea: {
                required: "Please enter the Builtup Area"
            },
            carpetarea: {
                required: "Please enter the Carpet Area"
            },
            floor: {
                required: "Please select the Floor"
            },
            transactiontype: {
                required: "Please select Trasaction type"
            },
            price: {
                minlength: 'This value length is invalid. It should be between 4 and 10 numbers long.',
                required: "Please enter the price",

            },

            possessiondate: {
                required: "Please select possessiondate"
            },
            'idealbusiness[]': {
                required: "Please select Ideal Business"
            },
            floor_plan_file_name: {

                extension: "File extension should be image/*"
            },

        },
        errorPlacement: function (label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass('has-danger');
            $(element).addClass('form-control-danger');
        },
        submitHandler: function (form) {
            var input = new FormData($('#flateditform')[0]);
            var url = '/post/editflat';
            var method = "POST";
            var valid = true;
            builtup_area = $("#builtuparea").val();
            carpet_area = $("#carpetarea").val();
            if (parseFloat(carpet_area) != null) {
                if (parseFloat(builtup_area) <= parseFloat(carpet_area)) {
                    Toast.fire({
                        icon: 'error',
                        title: "Built up area should be greater than carpet area"
                    })
                    valid = false
                }
            }
            if (valid == true) {
                credentialcommongfunction(url, input, method, 'flatsubmit', 'Update')
            }

        }

    });
}

$(document).on('change', '#service_type_id', function (e) {
    var value = this.value;
    input = new FormData();
    input.append('id', value);
    var url = '/useractivity/getservicesubcategory';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        console.log(result)
        serviceSubCatInfo = result.data;
        $("#sub_category_id").empty();
        $("#sub_category_id").append("<option value=''>Select</option>")
        $.each(serviceSubCatInfo, function (index, item) {
            $("#sub_category_id").append('<option value=' + item.id + '>' + item.title + '</option>')
        });
    }).catch(error => {
        console.log(error)
    });

})

$(document).on('change', '#otherimage', function (e) {
    $("#appendotherimage").empty() //
    for (var i = 0; i < this.files.length; i++) {
        var obj = window.URL.createObjectURL(this.files[i])
        splitobj = obj.split('/');
        lastData = splitobj[3]
        var html = "";
        html += '<div class="col-sm-6 ' + lastData + '" >' +
            '<a class="gallery-image"  href=' + obj + '><img src=' + obj + ' width="130px"' +
            'height="130px" title="1SqFt"  alt="1SqFt" style="></a>' +
            '<a href="javascript:void(0)" id=' + lastData + ' class="removeonloadimage"><i style="width:15px;height:15px"' +
            'data-feather="eye"></i></a>&nbsp;</div>'
        $("#appendotherimage").append(html) //
        // feather.replace();
        $('.gallery-image').magnificPopup({
            type: 'image',
            // other options
        });
    }
})

$(document).on('change', '#main_image_name', function (e) {
    $("#appendmainimage").empty() //
    for (var i = 0; i < this.files.length; i++) {
        var obj = window.URL.createObjectURL(this.files[i])
        splitobj = obj.split('/');
        lastData = splitobj[3]
        var html = "";
        html += '<div class="col-sm-6 ' + lastData + '" >' +
            '<a class="gallery-image"  href=' + obj + '><img src=' + obj + ' width="130px"' +
            'height="130px" title="1SqFt"  alt="1SqFt" style="></a>' +
            '<a href="javascript:void(0)" id=' + lastData + ' class="removeonloadimage"><i style="width:15px;height:15px"' +
            'data-feather="eye"></i></a>&nbsp;</div>'
        $("#appendmainimage").append(html) //
        // feather.replace();
        $('.gallery-image').magnificPopup({
            type: 'image',
            // other options
        });
    }
})

$(document).on('click', '.publishstatusinactive', function (e) {
    var id = this.id;
    var input = new FormData();
    var split_id = id.split("_");
    id = split_id[0];
    var status = split_id[1];
    var posttype = $("#posttype").val();
    if (posttype == 'P') {
        var selectoptionArr = [{
                id: 0,
                title: 'In Active'
            },
            {
                id: 1,
                title: 'Active'
            }, {
                id: 2,
                title: 'Sold Out'
            }, {
                id: 3,
                title: 'Rented Out'
            }, {
                id: 4,
                title: 'Leased Out'
            }
        ];
        var selectId = $("#propertystatusupdate");
        selectId.empty();
        var userinfo = CommonArr[CommonArr.map(function (item) {
            return item.id;
        }).indexOf(parseInt(id))];
        propertytype = userinfo.property_type_id;
        $("#propertyupdateid").val(id);
        if (propertytype == 2) {
            $.each(selectoptionArr, function (key, item) {
                if (parseInt(item.id) != 3 && parseInt(item.id) != 4) {
                    if (parseInt(status) == parseInt(item.id)) {
                        selectId.append('<option selected value=' + parseInt(item.id) + '>' + item.title + '</option>')
                    } else {
                        selectId.append('<option value=' + parseInt(item.id) + '>' + item.title + '</option>')
                    }

                }
            });
            $("#editpropertystatus").modal({
                backdrop: 'static',
                keyboard: false
            });
            //sale
        } else if (propertytype == 3) {
            //rent
            $.each(selectoptionArr, function (key, item) {
                if (parseInt(item.id) != 2 && parseInt(item.id) != 4) {
                    if (parseInt(status) == parseInt(item.id)) {
                        selectId.append('<option selected value=' + parseInt(item.id) + '>' + item.title + '</option>')
                    } else {
                        selectId.append('<option value=' + parseInt(item.id) + '>' + item.title + '</option>')
                    }

                }
            })
            $("#editpropertystatus").modal({
                backdrop: 'static',
                keyboard: false
            });
        } else if (propertytype == 4) {

            //lease
            $.each(selectoptionArr, function (key, item) {
                if (parseInt(item.id) != 2 && parseInt(item.id) != 3) {
                    if (parseInt(status) == parseInt(item.id)) {
                        selectId.append('<option selected value=' + parseInt(item.id) + '>' + item.title + '</option>')
                    } else {
                        selectId.append('<option value=' + parseInt(item.id) + '>' + item.title + '</option>')
                    }

                }
            })
            $("#editpropertystatus").modal({
                backdrop: 'static',
                keyboard: false
            });
        }

    } else {
        input.append('id', id);
        input.append('status', status);
        input.append('posttype', posttype);
        var url = '/post/publishstatusupdate';
        var method = "POST";
        confirmationalert("Do you want to update the selected record", "Update",
            "Cancel", 'Published status', url, input, method)
    }

})
$("#updatepropertystatusBtn").on('click', function (e) {
    var input = new FormData();
    var propertyupdateid = $("#propertyupdateid").val();
    var propertystatusupdate = $("#propertystatusupdate").val();
    var posttype = $("#posttype").val();
    input.append('id', propertyupdateid);
    input.append('status', propertystatusupdate);
    input.append('posttype', posttype);
    var url = '/post/publishstatusupdate';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        $("#editpropertystatus").modal('hide');
        $('#post').DataTable().ajax.reload();
    }).catch(error => {
        console.log(error);
    });
});
$(document).on('change', '#certificate', function (e) {
    $("#appendcertificateimage").empty() //
    for (var i = 0; i < this.files.length; i++) {
        var obj = window.URL.createObjectURL(this.files[i])
        splitobj = obj.split('/');
        lastData = splitobj[3]
        var html = "";
        html += '<div class="col-sm-6 ' + lastData + '" >' +
            '<a class="gallery-image"  href=' + obj + '><img src=' + obj + ' width="130px"' +
            'height="130px" title="1SqFt"  alt="1SqFt" style="></a>' +
            '<a href="javascript:void(0)" id=' + lastData + ' class="removeonloadimage"><i style="width:15px;height:15px"' +
            'data-feather="eye"></i></a>&nbsp;</div>'
        $("#appendcertificateimage").append(html) //
        // feather.replace();
        $('.gallery-image').magnificPopup({
            type: 'image',
            // other options
        });
    }
})


$(document).on('change', '#floor_plan_file_name', function (e) {
    $("#appendflatimage").empty() //
    for (var i = 0; i < this.files.length; i++) {
        var obj = window.URL.createObjectURL(this.files[i])
        splitobj = obj.split('/');
        lastData = splitobj[3]
        var html = "";
        html += '<div class="col-sm-6 ' + lastData + '" >' +
            '<a class="gallery-image"  href=' + obj + '><img src=' + obj + ' width="130px"' +
            'height="130px" title="1SqFt"  alt="1SqFt" style="></a>' +
            '<a href="javascript:void(0)" id=' + lastData + ' class="removeonloadimage"><i style="width:15px;height:15px"' +
            'data-feather="eye"></i></a>&nbsp;</div>'
        $("#appendflatimage").append(html) //
        // feather.replace();
        $('.gallery-image').magnificPopup({
            type: 'image',
            // other options
        });
    }
})
