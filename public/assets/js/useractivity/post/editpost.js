    var posttype = $("#posttype").val();
    var id = $("#post_id").val();
    var input = new FormData();
    input.append('id', id);
    input.append('posttype', posttype);
    input.append('operationtype', 'edit')
    if (posttype == "PR") {
        input.append('projectcategory', $("#projectcategory").val());
    }
    var url = '/getdetailview';
    var method = "POST";
    commonajaxcall(url, input, method).then(result => {
        var data = result;
        $("#append").empty();
        $("#append").append(data);
        if (posttype == "S") {
            $("#payment_mode_id").select2();
            $("#service_type_id").select2();
            $("#sub_category_id").select2();
            $("#area_id").select2();
            $("#hours").select2();
            feather.replace();
            sendservice();
            imageview()


        } else if (posttype == "PR") {
            $(".js-example-basic-multiple").select2();
            $("#constructionstatus").select2();
            $("#pricenegotiable").select2();
            $("#approvalauthority").select2();
            $("#areaid").select2();
            $('#launchdate').daterangepicker({
                singleDatePicker: true,
                locale: {
                    format: 'DD-MM-YYYY'
                }
            });
            feather.replace();

            sendproject();
            imageview()

        } else {


            $(".js-example-basic-multiple").select2();

            $('#possession_date').daterangepicker({
                singleDatePicker: true,
                locale: {
                    format: 'DD-MM-YYYY'
                }
            });
            $('#available_from').daterangepicker({
                singleDatePicker: true,
                locale: {
                    format: 'DD-MM-YYYY'
                }
            });
            $("#expected_duration_of_stay").datepicker({
                format: "mm-yyyy",
                startView: "months",
                minViewMode: "months"
            });

            $('.timepicker').timepicker({
                timeFormat: 'H:i:s',
                interval: 60,
                minTime: '0',
                maxTime: '23',
                defaultTime: '0',
                startTime: '10:00',
                dynamic: true,
                dropdown: true,
                scrollbar: true
            });

            feather.replace();

            sendproperty();
            imageview()
        }


    }).catch(error => {
        console.log(error);
    })

    function sendproject() {
        $.validator.addMethod('filesize', function (value, element, param) {
            if (element.files[0]) {
                var size = element.files[0].size;
                console.log("size", size)
                size = size / 1024;
                size = Math.round(size);
                return this.optional(element) || size <= param;
            } else {
                return true;
            }

        }, 'File size must be less than 5MB');
        $("#projectform").validate({
            rules: {
                pricenegotiable: {
                    required: true
                },
                projectreraid: {
                    required: true
                },
                projectname: {
                    required: true
                },

                projectarea: {
                    required: true
                },
                totalnoofunits: {
                    required: true
                },
                description: {
                    required: true
                },
                availablenoofunits: {
                    required: true
                },
                approvalauthority: {
                    required: true
                },
                launchdate: {
                    required: true
                },
                constructionstatus: {
                    required: true
                },
                areaid: {
                    required: true
                },
                main_image_name: {
                    required: false,
                    extension: "jpeg|jpg|png"
                },
                certificate: {
                    required: false,
                    extension: "jpeg|jpg|png"
                },
                video_image_name: {
                    required: false,
                    extension: "MP4|3GP|MKV|AVI|MOV|WEBM|WMV|MPG", //"mp4|MTS|AVI|MOV|mpg|m4v|mkv|mpeg|webm|wmv ",
                    filesize: 5000
                },
                'otherimage[]': {
                    required: false,
                    extension: "jpeg|jpg|png"
                }
            },
            messages: {
                pricenegotiable: {
                    required: "Please select price negotiable"
                },
                projectreraid: {
                    required: 'Please enter the project RERA id'
                },
                projectname: {
                    required: 'Please enter the project name'
                },
                projectarea: {
                    required: "Please enter the project area"
                },
                totalnoofunits: {
                    required: "please enter the Total no of units"
                },
                description: {
                    required: "Please enter the description"
                },
                availablenoofunits: {
                    required: "Please enter the Available no of units"
                },
                approvalauthority: {
                    required: 'Please select Approval authority'
                },
                launchdate: {
                    required: "Please select the Launch Date"
                },
                constructionstatus: {
                    required: "Please select the Construction status"
                },
                areaid: {
                    required: 'Please select the Area unit'
                },
                main_image_name: {
                    extension: "File type should be jpeg|jpg|png"
                },
                video_image_name: {
                    extension: "File type should be MP4|3GP|MKV|AVI|MOV|WEBM|WMV|MPG"
                },
                certificate: {
                    extension: "File type should be jpeg|jpg|png"
                },
                'otherimage[]': {
                    extension: "File type should be jpeg|jpg|png"
                }


            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger');
                $(element).addClass('form-control-danger');
            },
            submitHandler: function (form) {
                var input = new FormData($('#projectform')[0]);
                var posttype = $("#posttype").val();
                input.append('posttype', posttype)
                input.append('criteriaId', $('meta[name="criteria"]').attr('content'));
                var totalnoofunits = parseInt($("#totalnoofunits").val());
                var availablenoofunits = parseInt($("#availablenoofunits").val());
                var url = '/post/editpost';
                var method = "POST";
                var valid = true;
                var totalotherimage = parseInt($("#totalotherimage").val());
                if (totalotherimage == 0) {
                    if ($("#otherimage")[0].files.length > 7) {
                        Toast.fire({
                            icon: 'error',
                            title: "OtherImage - please choose maximum 7 image files"
                        });
                        valid = false;
                    }
                } else {
                    var uploadedtotalimage = $("#otherimage")[0].files.length;
                    var totalimage = uploadedtotalimage + totalotherimage;
                    var imagetoupload = 7 - parseInt(totalotherimage)
                    if (totalimage > 7) {
                        Toast.fire({
                            icon: 'error',
                            title: "OtherImage - please remove " + Math.abs(imagetoupload) + " image files"
                        });
                        valid = false;
                    }
                }
                if (availablenoofunits > totalnoofunits) {
                    Toast.fire({
                        icon: 'error',
                        title: "Available no of units always lesser than Total no of units"
                    });
                    valid = false;
                }
                if (valid == true) {
                    credentialcommongfunction(url, input, method, 'postsubmit', 'Update')
                }

            }

        });
    }

    function sendservice() {
        $("#serviceeditform").validate({
            rules: {
                service_type_id: {
                    required: true
                },
                sub_category_id: {
                    required: true
                },
                short_description: {
                    required: true
                },
                delivery: {
                    required: false,
                    maxlength: 365
                },
                payment_mode_id: {
                    required: true
                },
                main_image_name: {
                    required: false,
                    extension: "jpeg|jpg|png"
                },
                'otherimage[]': {
                    required: false,
                    extension: "jpeg|jpg|png"
                }
            },
            messages: {
                service_type_id: {
                    required: "Please select the category"
                },
                sub_category_id: {
                    required: "Please select the sub category"
                },
                short_description: {
                    required: "Please enter the short description"
                },
                payment_mode_id: {
                    required: "Please select payment mode"
                },
                delivery: {
                    maxlength: "Please enter numbers between 1 to 365"
                },
                main_image_name: {
                    extension: "File type should be image/*"
                },
                'otherimage[]': {
                    extension: "File type should be image/*"
                }
            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger');
                $(element).addClass('form-control-danger');
            },
            submitHandler: function (form) {
                var input = new FormData($('#serviceeditform')[0]);
                var posttype = $("#posttype").val();
                input.append('posttype', posttype)
                var url = '/post/editpost';
                var method = "POST";
                var valid = true;
                var totalotherimage = parseInt($("#totalotherimage").val());
                if (totalotherimage == 0) {
                    if ($("#otherimage")[0].files.length > 7) {
                        Toast.fire({
                            icon: 'error',
                            title: "OtherImage - please choose maximum 7 image files"
                        });
                        valid = false;
                    }
                } else {
                    var uploadedtotalimage = $("#otherimage")[0].files.length;
                    var totalimage = uploadedtotalimage + totalotherimage;
                    var imagetoupload = 7 - parseInt(totalotherimage)
                    if (totalimage > 7) {
                        Toast.fire({
                            icon: 'error',
                            title: "OtherImage - please remove " + Math.abs(imagetoupload) + " image files"
                        });
                        valid = false;
                    }
                }
                var delivery = $("#delivery").val();
                if (delivery > 365) {
                    Toast.fire({
                        icon: 'error',
                        title: "Please enter numbers between 1 to 365"
                    });
                    valid = false;
                }
                if (valid == true) {
                    credentialcommongfunction(url, input, method, 'postsubmit', 'Update')
                }
            }

        });
    }

    function sendproperty() {
        $("#propertyeditform").validate({
            rules: {
                bhk_id: {
                    required: true
                },
                'tenant_type_id[]': {
                    required: true
                },
                furnish_type_id: {
                    required: true
                },
                floor_id: {
                    required: true
                },
                available_from: {
                    required: true
                },
                monthly_rent: {
                    required: true
                },
                security_deposit: {
                    required: true
                },

                builtup_area: {
                    required: true
                },
                carpet_area: {
                    required: true
                },
                negotiable: {
                    required: true
                },
                'ideal_business_id[]': {
                    required: true
                },
                price: {
                    required: true //shouldbe 4 to 10
                },
                maintenance_charges: {
                    required: true
                },
                PG_or_hostel_name: {
                    required: true
                },
                width_of_road: {
                    required: true
                },
                room_type: {
                    required: true
                },
                attach_bathroom: {
                    required: true
                },
                'PG_type[]': {
                    required: true
                },
                'PG_preferred_tenant[]': {
                    required: true
                },
                'food_facility[]': {
                    required: true
                },
                gate_closing_time: {
                    required: true
                },
                PG_rules: {
                    required: true
                },
                flatmates_property_type: {
                    required: true
                },
                flatmates_tenant_type: {
                    required: true
                },
                flatmates_room_type: {
                    required: true
                },
                flatmates_floor: {
                    required: true
                },
                possession_date: {
                    required: true
                },
                boundary_wall_made: {
                    required: true
                },
                corner_plot: {
                    required: true
                },
                no_open_side_id: {
                    required: true
                },
                carpet_area: {
                    required: true
                },
                builtup_area: {
                    required: true
                },
                main_image_name: {
                    required: false,
                    extension: "jpeg|jpg|png"
                },
                'otherimage[]': {
                    required: false,
                    extension: "jpeg|jpg|png"
                }

            },
            messages: {

                main_image_name: {
                    extension: "File type should be image/*"
                },
                'otherimage[]': {
                    extension: "File type should be image/*"
                },
                price: {
                    minlength: 'This value length is invalid. It should be between 4 and 10 numbers long.',
                    required: "Please enter the price",
                },
                carpet_area: {
                    minlength: 'This value length is invalid. It should be between 3 and 10 numbers long.',
                    required: "Please enter the Carpet area",
                },
                builtup_area: {
                    minlength: 'This value length is invalid. It should be between 3 and 10 numbers long.',
                    required: "Please enter the Builtup area",
                }



            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger');
                $(element).addClass('form-control-danger');
            },
            submitHandler: function (form) {
                var input = new FormData($('#propertyeditform')[0]);
                var posttype = $("#posttype").val();
                input.append('posttype', posttype);
                var url = '/post/editpost';
                var method = "POST";
                var valid = true;
                plot_area = $("#plot_area").val();
                area_unit = $("#area_unit").val();

                if (plot_area != null) {
                    if (plot_area) {
                        if (plot_area == 0) {
                            Toast.fire({
                                icon: 'error',
                                title: "Plot Area must be greater than 0"
                            })
                            $("#plot_area").val('');
                            valid = false;
                        } else {
                            if (area_unit == 0) {
                                Toast.fire({
                                    icon: 'error',
                                    title: "Please select area unit"
                                })
                                valid = false;
                            }
                        }

                    }
                    if (area_unit) {
                        if (!plot_area) {
                            Toast.fire({
                                icon: 'error',
                                title: "Please enter the plot area"
                            })
                            valid = false;
                        }
                    }

                }
                builtup_area = $("#builtup_area").val();
                carpet_area = $("#carpet_area").val();
                if (parseFloat(carpet_area) != null) {
                    if (parseFloat(builtup_area) <= parseFloat(carpet_area)) {
                        Toast.fire({
                            icon: 'error',
                            title: "Built up area should be greater than carpet area"
                        })
                        valid = false
                    }
                }
                if (builtup_area = 0) {
                    Toast.fire({
                        icon: 'error',
                        title: "Built up area should be greater than 0"
                    })
                    valid = false
                }
                var totalotherimage = parseInt($("#totalotherimage").val());
                if (totalotherimage == 0) {
                    if ($("#otherimage")[0].files.length > 7) {
                        Toast.fire({
                            icon: 'error',
                            title: "OtherImage - please choose maximum 7 image files"
                        });
                        valid = false;
                    }
                } else {
                    var uploadedtotalimage = $("#otherimage")[0].files.length;
                    var totalimage = uploadedtotalimage + totalotherimage;
                    var imagetoupload = 7 - totalotherimage;
                    if (totalimage > 7) {
                        Toast.fire({
                            icon: 'error',
                            title: "OtherImage - please choose " + Math.abs(imagetoupload) + " image files"
                        });
                        valid = false;
                    }
                }
                if (valid == true) {
                    credentialcommongfunction(url, input, method, 'postsubmit', 'Update')

                }
            }

        });

    }
    $("#updaterejectreason").on('click', function (e) {
        var input = new FormData();
        var posttype = $("#posttype").val();
        var post_id = $("#post_id").val();
        input.append('post_id', post_id);
        input.append('posttype', posttype);
        var criteriaid = $("#criteriaid").val()
        input.append('criteriaId', criteriaid)
        input.append('status', 2);
        input.append('poststatus', 2);
        var rejectreason = $("#rejectreason").val();
        input.append('poststatusactionreason', rejectreason);
        var url = '/post/updatepoststatus';
        var method = "POST";
        commonajaxcall(url, input, method).then(result => {
            $("#editrejectreason").modal('hide');
            Toast.fire({
                icon: 'success',
                title: result.message
            });
            setTimeout(function (e) {
                location.reload();
            }, 1000)

        }).catch(error => {
            console.log(error);
        });
    });
$(document).on('click', ".approverejectpost", function (e) {
    var eventid = this.id;
    var split_id = eventid.split('_');
    var post_id = split_id[0];
    var posttype = split_id[2];
    var status = split_id[1];
    var input = new FormData();
    input.append('post_id', post_id);
    input.append('posttype', posttype);

    input.append('poststatus', status);
    input.append('status', status);
    var criteriaid = $("#criteriaid").val();
    input.append('criteriaId', criteriaid);
    var url = '/post/updatepoststatus';
    var method = "POST";
    if (status == 1) {
        confirmationalert("Do you want to update the selected record", "Update",
            "Cancel", 'Posted status', url, input, method);
    } else {

        $("#editrejectreason").modal({
            backdrop: 'static',
            keyboard: false
        });
    }

})
