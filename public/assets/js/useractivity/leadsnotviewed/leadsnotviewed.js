var CommonArr = [];
var NotifyArr = [];
$("#filtersubmit").on('click', function () {
    $('#leadsnotviewed').DataTable().destroy();
    $("#filtersubmit").text("Processing...")
    common_load_data("click");
});

common_load_data('onload');


function common_load_data(type) {

    var fromdate = "";
    var todate = "";
    if (type == "onload") {
        fromdate = moment().subtract(7, 'd').format('YYYY-MM-DD');
        todate = moment().format('YYYY-MM-DD');
    } else {
        fromdate = $('#postdate').data('daterangepicker').startDate.format('YYYY-MM-DD');
        todate = $('#postdate').data('daterangepicker').endDate.format('YYYY-MM-DD');
    }

    var state = $("#state").val();
    var city = $("#city").val();
    var searchcallstatus = $("#searchcallstatus").val();
    var posttype = $("#posttype").val();
    var projectcategory = $("#projectcategory").val();
    var usertype = $("#usertype").val();
    var propertytype = $("#propertytype").val();
    var propertycategory = $("#propertycategory").val();
    var servicecategory = $("#Servicecategory").val();
    var servicesubcategory = $("#servicesubcategory").val();
    load_data(fromdate, todate, usertype, state, city, searchcallstatus, propertytype,
        propertycategory, servicecategory, servicesubcategory, projectcategory, posttype)
}

function load_data(fromdate = "", todate = "", usertype = "", state = "", city = "", status = "", propertytype = "",
    propertycategory = "", servicecategory = "", servicesubcategory = "", projectcategory = "", posttype = "") {
    var getnotifydata = $("#getnotifydata").text();
    var input = {};
    input['id'] = $('meta[name="fetchid"]').attr('content');
    input['fromdate'] = fromdate;
    input['todate'] = todate;
    input['state'] = state;
    input['city'] = city;
    input['searchcallstatus'] = status;
    input['posttype'] = posttype;

    if (posttype == "PR") {
        $("#type").text('Project Category')
        input['projectcategory'] = projectcategory;
    } else if (posttype == "P") {
        $("#type").text('Property Type')
        input['usertype'] = usertype;
        input['propertytype'] = propertytype;
        input['propertycategory'] = propertycategory;

    } else if (posttype == "S") {
        $("#type").text('Service Category')
        input['servicecategory'] = servicecategory;
        input['servicesubcategory'] = servicesubcategory;
    }
    var obj = [];
    if (parseInt(getnotifydata) == 0) {
        obj = [{
                'data': 'name',
            'name': 'name',
                'orderable': false,
            },
            {
                'data': 'usertype',
                'name': 'usertype'
            },
            {
                'data': 'category',
                'name': 'category'
            },
            {
                'data': 'mobilenumber',
                'name': 'mobilenumber'
            },

            {
                'data': 'state',
                'name': 'state'
            },
            {
                'data': 'city',
                'name': 'city'
            }, {
                'data': 'totalnoofleads',
                'name': 'totalnoofleads'
            },

            {
                'data': 'totalnoofnotleads',
                'name': 'totalnoofnotleads'
            },

            {
                'data': 'status',
                'name': 'status'
            },
            {
                'data': 'followup_date',
                'name': 'followup_date'
            },
            {
                'data': 'remarks',
                'name': 'remarks'
            }, {
                'data': 'crmusername',
                'name': 'crmusername'
            },
            {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ]
    } else {
        obj = [{
                'data': 'checkbox',
                'name': 'checkbox',
                'orderable': false,
                'className': 'select-checkbox'
            }, {
                'data': 'name',
                'name': 'name'
            }, {
                'data': 'usertype',
                'name': 'usertype'
            }, {
                'data': 'category',
                'name': 'category'
            }, {
                'data': 'mobilenumber',
                'name': 'mobilenumber'
            }, {
                'data': 'state',
                'name': 'state'
            }, {
                'data': 'city',
                'name': 'city'
            }, {
                'data': 'totalnoofleads',
                'name': 'totalnoofleads'
            },

            {
                'data': 'totalnoofnotleads',
                'name': 'totalnoofnotleads'
            }, {
                'data': 'status',
                'name': 'status'
            }, {
                'data': 'followup_date',
                'name': 'followup_date'
            }, {
                'data': 'remarks',
                'name': 'remarks'
            }, {
                'data': 'crmusername',
                'name': 'crmusername'
            }, {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ]
    }
    var commontable = $('#leadsnotviewed').DataTable({
        serverSide: true,
        responsive: false,
        processing: true,

        scrollX: true,
        dom: 'lBfrtip',

        select: {
            style: 'multi', // 'single', 'multi', 'os', 'multi+shift'
            selector: 'td:first-child',
        },
        "ajax": {
            "url": "/useractivity/getleadsnotviewedlist",
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            "type": "POST",
            data: input,
        },
        drawCallback: function (settings) {
            CommonArr = settings.json.data
            $('.tools').tooltip({
                container: 'body'
            });
            feather.replace();

        },
        rowCallback: function (row, data) {
            var hideout = data.hideout;
            if (hideout == 1) {
                $(row).addClass("blurrow");
                $('td:eq(0)', row).removeClass('select-checkbox');
            }
        },
        "columns": obj,
        "aLengthMenu": [
            [10, 30, 50],
            [10, 30, 50]
        ],
        "iDisplayLength": 10,
        "language": {
            search: "",
            "searchPlaceholder": "search",
        }
    })
    $("#filtersubmit").text("Search")
    commontable.on('select deselect draw', function () {
        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        var data = commontable.rows({
            selected: true,
            search: 'applied'
        }).data()
        NotifyArr = [];

        for (var i = 0; i < data.length; i++) {
            notify_Id = data[i]['user'].id;

            if (data[i]['hideout'] == 0) {
                NotifyArr.push(
                    notify_Id);
            }

        }
        if (selectedRows == 0) {
            $('#CheckAllButton i').attr('class', 'fa fa-square-o');
        } else {
            if (selectedRows == all) {
                $('#CheckAllButton i').attr('class', 'fa fa-check-square-o');
            }


        }
    });
    $('#CheckAllButton').on('click', function () {
        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        if (selectedRows < all) {
            //Added search applied in case user wants the search items will be selected
            commontable.rows({
                search: 'applied'
            }).deselect();
            commontable.rows({
                search: 'applied'
            }).select();
        } else {
            commontable.rows({
                search: 'applied'
            }).deselect();
        }
    });
}
