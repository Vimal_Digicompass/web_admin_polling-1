//KYC - Pending
var CommonArr = [];
var NotifyArr = [];
$("#filtersubmit").on('click', function () {
    $('#kycpendingreject').DataTable().destroy();
    $("#filtersubmit").text("Processing...")
    common_load_data("click");
});

common_load_data('onload');


function common_load_data(type) {

    var fromdate = "";
    var todate = "";
    if (type == "onload") {
        fromdate = moment().subtract(7, 'd').format('YYYY-MM-DD');
        todate = moment().format('YYYY-MM-DD');
    } else {
        fromdate = $('#kycdate').data('daterangepicker').startDate.format('YYYY-MM-DD');
        todate = $('#kycdate').data('daterangepicker').endDate.format('YYYY-MM-DD');
    }

    var state = $("#state").val();
    var city = $("#city").val();
    var searchcallstatus = $("#searchcallstatus").val();
    var usertype = $("#usertype").val();
    var usercategory = $("#searchusercategory").val();
    load_data(fromdate, todate, usertype, state, city, searchcallstatus, usercategory)
}

function load_data(fromdate = "", todate = "", usertype = "", state = "", city = "", status = "", category = "") {
    var criteriaId = $('meta[name="criteria"]').attr('content');
    var path = "";
    if (criteriaId == 2) {
        path = '/kyc/getkycpendinglist';
    } else {
        path = '/kyc/getkycrejectedlist';
    }
    var getnotifydata = $("#getnotifydata").text();
    var obj = [];
    if (parseInt(getnotifydata) == 0) {
        obj = [{
                'data': 'name',
                'name': 'name',
                'orderable': false,
            },
            {
                'data': 'usertype',
                'name': 'usertype'
            },
            {
                'data': 'category',
                'name': 'category'
            },
            {
                'data': 'mobile_number',
                'name': 'mobile_number'
            },

            {
                'data': 'dateofregistration',
                'name': 'dateofregistration'
            },
            {
                'data': 'state',
                'name': 'state'
            },
            {
                'data': 'city',
                'name': 'city'
            },
            {
                'data': 'address',
                'name': 'address'
            },
            {
                'data': 'kycdocument',
                'name': 'kycdocument'
            },
            {
                'data': 'active',
                'name': 'active'
            },
            {
                'data': 'kycstatus',
                'name': 'kycstatus'
            },
            {
                'data': 'kyc_rejcted_reason',
                'name': 'kyc_rejcted_reason'
            }, {
                'data': 'crmusername',
                'name': 'crmusername'
            },
            {
                'data': 'status',
                'name': 'status'
            },
            {
                'data': 'followup_date',
                'name': 'followup_date'
            },
            {
                'data': 'remarks',
                'name': 'remarks'
            },
            {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ]
    } else {
        obj = [{
                'data': 'checkbox',
                'name': 'checkbox',
                'orderable': false,
                'className': 'select-checkbox'
            }, {
                'data': 'name',
                'name': 'name'
            }, {
                'data': 'usertype',
                'name': 'usertype'
            }, {
                'data': 'category',
                'name': 'category'
            }, {
                'data': 'mobile_number',
                'name': 'mobile_number'
            },

            {
                'data': 'dateofregistration',
                'name': 'dateofregistration'
            }, {
                'data': 'state',
                'name': 'state'
            }, {
                'data': 'city',
                'name': 'city'
            }, {
                'data': 'address',
                'name': 'address'
            }, {
                'data': 'kycdocument',
                'name': 'kycdocument'
            }, {
                'data': 'active',
                'name': 'active'
            }, {
                'data': 'kycstatus',
                'name': 'kycstatus'
            }, {
                'data': 'kyc_rejcted_reason',
                'name': 'kyc_rejcted_reason'
            }, {
                'data': 'crmusername',
                'name': 'crmusername'
            }, {
                'data': 'status',
                'name': 'status'
            }, {
                'data': 'followup_date',
                'name': 'followup_date'
            }, {
                'data': 'remarks',
                'name': 'remarks'
            }, {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ]
    }
    var commontable = $('#kycpendingreject').DataTable({
        serverSide: true,
        responsive: false,
        processing: true,
        scrollX: true,
        dom: 'lBfrtip',

        select: {
            style: 'multi', // 'single', 'multi', 'os', 'multi+shift'
            selector: 'td:first-child',
        },
        fixedColumns: {
            leftColumns: 1,
            rightColumns: 1
        },
        "ajax": {
            "url": path,
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            "type": "POST",
            data: {
                id: $('meta[name="fetchid"]').attr('content'),
                fromdate: fromdate,
                todate: todate,
                usertype: usertype,
                state: state,
                city: city,
                status: status,
                usercategory: category
            },
        },
        drawCallback: function (settings) {
            CommonArr = settings.json.data

            $('.tools').tooltip({
                container: 'body'
            });
            feather.replace();

        },
        rowCallback: function (row, data) {
            var hideout = data.hideout;
            if (hideout == 1) {
                $(row).addClass("blurrow");
                $('td:eq(0)', row).removeClass('select-checkbox');
            }
        },
        "columns": obj,
        "aLengthMenu": [
            [10, 30, 50],
            [10, 30, 50]
        ],
        "iDisplayLength": 10,
        "language": {
            search: "",
            "searchPlaceholder": "search",
        }
    })

    $("#filtersubmit").text("Search")
    commontable.on('select deselect draw', function (row) {
        console.log(row)
        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        var data = commontable.rows({
            selected: true,
            search: 'applied'
        }).data()
        NotifyArr = [];
        for (var i = 0; i < data.length; i++) {
            notify_Id = data[i]['id'];
            if (data[i]['hideout'] == 0) {
                NotifyArr.push(
                    notify_Id);
            }

        }
        if (selectedRows == 0) {
            $('#CheckAllButton i').attr('class', 'fa fa-square-o');
        } else {
            if (selectedRows == all) {
                $('#CheckAllButton i').attr('class', 'fa fa-check-square-o');
            }

            //
        }
    });
    $('#CheckAllButton').on('click', function () {

        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        if (selectedRows < all) {
            //Added search applied in case user wants the search items will be selected
            commontable.rows({
                search: 'applied'
            }).deselect();
            commontable.rows({
                search: 'applied'
            }).select();
        } else {
            commontable.rows({
                search: 'applied'
            }).deselect();
        }
    });
}
//End KYC - Approval
