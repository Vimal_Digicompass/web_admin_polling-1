 function imageview() {
     $('.gallery-image').magnificPopup({
         type: 'image',
         // other options
     });
 }

 $(document).on('change', '.backfile', function (e) {
     $("#gallery-image1").css('display', 'block')
     document.getElementById('gallery-image1').href = "";
     document.getElementById('backimage').src = window.URL.createObjectURL(this.files[0]);
     document.getElementById('gallery-image1').href = window.URL.createObjectURL(this.files[0]);

 })
 $(document).on('change', '.frontfile', function (e) {
     $("#gallery-image").css('display', 'block')
     document.getElementById('gallery-image').href = "";
     document.getElementById('frontimage').src = window.URL.createObjectURL(this.files[0]);
     document.getElementById('gallery-image').href = window.URL.createObjectURL(this.files[0]);

 })
 $(document).on('click', '.documenttype', function (e) {
     var id = this.id;

     var idvalue = id + "_value";
     var checked = this.checked;
     $.ajax({
         type: 'get',
         url: '/kyc/getkycrejectionstatus',
         data: input,
         contentType: false,
         processData: false,
         success: function (result) {
             var data = result.data;
             var html = "";
             var selectvalue = id + "_reject_reason";
             html = '<div id=' + idvalue + ' class="reasonclass col-sm-12"><div class="form-group">' +
                 '<label class="control-label">' + id.toUpperCase() + '<span style="color:red;">*</span></label>' +
                 '<select required id=' + selectvalue + ' name=' + selectvalue + ' class="js-example-basic-multiple w-100">' +
                 '<option value="">Select reason</option>';
             $.each(data, function (key, item) {
                 html += '<option  value="' + item.title + '">' + item.title + '</option>';
             });
             html += '</select></div></div>';
             if (checked) {
                 $("#kycstatusrow").append(html);
                 $("#checkboxerror").text("")
             } else {
                 $("#" + idvalue).remove();
                 $("#checkboxerror").text("Please select the document type");
             }
             $(".js-example-basic-multiple").select2();

         },
         error: function (error) {
             console.log(error.message)
         }
     });

 });

 $(document).on('change', '#callingstatus', function (e) {
     var value = this.value;
     if (value == 3) { // closed
         criteriaId = $('meta[name="criteria"]').attr('content')
         if (criteriaId == 1 || criteriaId == 2 || criteriaId == 3) {
             var input = new FormData();
             var url = '/kyc/getkycapprovestatus';
             var id = $("#user_id").val();
             input.append('id', id);
             var method = "post";
             commonajaxcall(url, input, method).then(function (result) {
                 if (result == 0) {
                     Toast.fire({
                         icon: 'error',
                         title: "Please approve/reject KYC before close the ticket"
                     });
                     $("#callingstatus").val('')
                     $("#callingstatus").select2();
                 }
             }).catch(function (error) {
                 console.log(error);
             });

         } else if (criteriaId == 4 || criteriaId == 5) {
             var input = new FormData();
             var url = '/post/getpostapprovestatus';
             var id = $("#user_id").val();
             input.append('id', id);
             input.append('posttype', $('#posttype').val());
             var method = "post";
             commonajaxcall(url, input, method).then(function (result) {
                 if (result == 0) {
                     Toast.fire({
                         icon: 'error',
                         title: "Please approve/reject Post before close the ticket"
                     });
                     $("#callingstatus").val('')
                     $("#callingstatus").select2();
                 }
             }).catch(function (error) {
                 console.log(error);
             });
         }
     }
 })
 $(document).on('click', '.detailview', function (e) {

     var id = this.id;
     var userinfo = CommonArr[CommonArr.map(function (item) {
         return item.id;
     }).indexOf(parseInt(id))];
     $("#detailviewfirstname").text(userinfo.firstname);
     $("#detailviewlastname").text(userinfo.lastname);
     $("#detailviewemail").text(userinfo.email);
     $("#detailviewcompany").text(userinfo.company_name);
     $("#detailviewusertype").text(userinfo.usertype);
     $("#detailviewcategory").text(userinfo.category);
     $("#detailviewpincode").text(userinfo.pincode);
     $('#detailviewcity').text(userinfo.city)
     $("#detailviewaddress").text(userinfo.address)
     $("#detailviewmobilenumber").text(userinfo.mobile_number)
     $("#detailviewmodal").modal({
         backdrop: 'static',
         keyboard: false
     });




 });
 $(document).on('click', '.updatestatus', function (e) {
     var id = this.id;
     $("#user_id").val(id);
     $("#post_id").val(id);
     $("#kyc_user_id").val(id);
     criteriaId = $('meta[name="criteria"]').attr('content')
     if (criteriaId == 1 || criteriaId == 2 || criteriaId == 3 || criteriaId == 16) {
         var input = new FormData();
         var url = '/kyc/getKYCStatusInfo';
         input.append('id', id);
         input.append('criteriaId', $('meta[name="criteria"]').attr('content'));
         var method = "post";
         $("#checkboxerror").text("")
         commonajaxcall(url, input, method).then(function (result) {
             console.log(result, "result");
             var code = result.code;
             $("#kycstatusaction").empty();
             if (code == 202) {
                 $("#kycstatusaction").append("<option value=''>Select</option><option value=2>Rejected</option>");



             } else if (code == 203) {
                 $("#kycstatusaction").append("<option value=''>Select</option><option value=1>Approved</option>");

             } else if (code == 200) {

                 $("#kycstatusaction").append("<option value=''>Select</option><option value=1>Approved</option><option value=2>Rejected</option>");
             } else {
                 $("#kycstatusaction").append("<option value=''>Select</option>");
                 $("#checkboxerror").text(result.message);
             }
             $("#user_id").val(id);
             $("#kyc_user_id").val(id);
             $("#post_id").val(id);

         }).catch(function (error) {
             console.log(error);
         });

     } else if (criteriaId == 4 || criteriaId == 5) {
         var post_id = $("#post_id").val();
         var posttype = $("#posttype").val();

         input = new FormData();
         input.append('id', post_id)
         input.append('posttype', posttype)
         method = 'post';
         url = '/post/getpoststatus';
         commonajaxcall(url, input, method).then(result => {

             var dataSet = result.data;
             if (dataSet == 1) {
                 $("#poststatusaction").append('<option value="" data-select2-id="25">Select</option><option value="2" data-select2-id="40">Rejected</option>');

             } else if (dataSet == 2) {

                 $("#poststatusaction").append('<option value="" data-select2-id="25">Select</option><option value="1" data-select2-id="40">Approved</option>');

             } else {
                 $("#poststatusaction").append('<option value="" data-select2-id="25">Select</option><option value="1" data-select2-id="40">Approved</option>' +
                     '<option value="2" style="" data-select2-id="41">Rejected</option');

             }
             $("#poststatusaction").select2();
         }).catch(function (error) {
             console.log(error)
         })
     }
     input = new FormData();
     // var id = $("#user_id").val();
     input.append('user_id', id);
     var posttype = $("#posttype").val();
     input.append('posttype', posttype);
     input.append('criteriaId', $('meta[name="criteria"]').attr('content'))
     var url = '/getchangelog';
     var method = "POST";
     commonajaxcall(url, input, method).then(result => {
         console.log(result);
         var data = result.data;
         var html = "";
         $("#changelog").empty();
         if (data.length == 0) {
             html = '<ul style="font-size:14px;list-style-type: none;padding: 5px;"><li>No History Log found</li></ul>';

         } else {
             html = '<ul style="font-size:14px;list-style-type: none;padding: 5px;height:350px;overflow-y:scroll">';
             $.each(data, function (index, item) {
                 var done = item.done;
                 var newvalue = item.newvalue;
                 var oldvalue = item.oldvalue;
                 var fieldname = item.fieldname;
                 if (oldvalue == null || oldvalue == "") {
                     oldvalue = "";
                     text = 'Added the';
                 } else {
                     text = 'Changed the';
                     if (newvalue == null || newvalue == "") {
                         oldvalue = oldvalue;
                     } else {
                         oldvalue = oldvalue + ' <strong>-></strong>';
                     }

                 }
                 if (newvalue == null || newvalue == "") {
                     newvalue = "-";
                 }
                 if (newvalue != "") {
                     var created_at = moment(item.created_at).format('DD-MM-YYYY HH:mm A');
                     html += '<li><p><strong>' + done + '</strong> - ' + text + ' ' + fieldname +
                         ' <span>on ' + created_at + '</span><br>' +
                         newvalue + '</li><hr>';
                 }
             });
             html += '</ul>';
         }

         $("#changelog").append(html);
     }).catch(error => {
         console.log(error.message);
     });

     $("#editviewmodal").modal({
         backdrop: 'static',
         keyboard: false
     })
     // $("#followdatediv").css('display', 'none');
     $("#kycstatusform").trigger("reset");
     $("#rejectedcheckbox").css("display", 'none');
     $(".documenttype").prop("checked", false)
     $(".reasonclass").css("display", "none");
     $("#kycstatusaction").val("");
     $("#callstatusform").trigger('reset');
     $("#callingstatus").val('');
     $("#callingstatus-error").text("");
     $("#kycstatusaction-error").text("");
     $("#callingstatus").select2();
     $("#kycstatusaction").select2();
     $("#poststatusactionreasondiv").css("display", 'none')
     $("#poststatusaction").val("");
     $("#poststatusaction").select2();
     $("#poststatusaction-error").text("");
     $("#poststatusactionreason").val("");
     $("#poststatusactionreason-error").text("");
     $(".form-group").removeClass('has-danger');
     $(".select2-selection").css('border-color', 'none')
     $("#kycstatussubmit").html("Update")
     $("#callstatussubmit").html("Update")
     $("#poststatusaction").empty();

 })
 $(document).on('click', '.userstatusinactive', function (e) {
     var id = this.id;
     var input = new FormData();
     var split_id = id.split("_");
     id = split_id[0];
     var status = split_id[1];
     input.append('id', id);
     input.append('status', status);
     var url = '/kyc/userstatusupdate';
     var method = "POST";
     confirmationalert("Do you want to update the selected record", "Update",
         "Cancel", 'User status', url, input, method)
 });

 ;

 $("#changelog-tab").on('click', function (e) {
     input = new FormData();
     var id = $("#user_id").val();
     input.append('user_id', id);
     var posttype = $("#posttype").val();
     input.append('posttype', posttype);
     input.append('criteriaId', $('meta[name="criteria"]').attr('content'))
     var url = '/getchangelog';
     var method = "POST";
     commonajaxcall(url, input, method).then(result => {
         console.log(result);
         var data = result.data;
         var html = "";
         $("#changelog").empty();
         if (data.length == 0) {
             html = '<ul style="font-size:14px;list-style-type: none;padding: 5px;"><li>No History Log found</li></ul>';

         } else {
             html = '<ul style="font-size:14px;list-style-type: none;padding: 5px;height:350px;overflow-y:scroll">';
             $.each(data, function (index, item) {
                 var done = item.done;
                 var newvalue = item.newvalue;
                 var oldvalue = item.oldvalue;
                 var fieldname = item.fieldname;
                 if (oldvalue == null || oldvalue == "") {
                     oldvalue = "";
                     text = 'Added the';
                 } else {
                     text = 'Changed the';
                     if (newvalue == null || newvalue == "") {
                         oldvalue = oldvalue;
                     } else {
                         oldvalue = oldvalue + ' <strong>-></strong>';
                     }

                 }
                 if (newvalue == null || newvalue == "") {
                     newvalue = "";
                 }
                 if (newvalue != "") {
                     var created_at = moment(item.created_at).format('DD-MM-YYYY HH:mm A');
                     html += '<li><p><strong>' + done + '</strong> - ' + text + ' ' + fieldname +
                         ' <span>on ' + created_at + '</span><br>' +
                         newvalue + '</li><hr>';
                 }

             });
             html += '</ul>';
         }
         feather.replace()
         $("#changelog").append(html);
     }).catch(error => {
         Toast.fire({
             icon: 'error',
             title: error.message
         })
         console.log(error);
     });

 });
 $("#exportexcel").on('click', function () {

     $("#exportexcel").attr("disabled", true)
     $("#exportexcel").text("Exporting...")
     criteriaId = $('meta[name="criteria"]').attr('content');
     var path = "";
     var input = {};
     if (criteriaId == 2) {
         var fromdate = $('#kycdate').data('daterangepicker').startDate.format('YYYY-MM-DD');
         var todate = $('#kycdate').data('daterangepicker').endDate.format('YYYY-MM-DD');
         var state = $("#state").val();
         var city = $("#city").val();
         var usertype = $("#usertype").val();
         path = '/kyc/pendingexcelexport';
         status = $("#searchcallstatus").val();
         usercategory = $("#searchusercategory").val()
         input = {
             id: $('meta[name="fetchid"]').attr('content'),
             fromdate: fromdate,
             todate: todate,
             usertype: usertype,
             state: state,
             city: city,
             status: status,
             usercategory: usercategory,
             criteriaId: $('meta[name="criteria"]').attr('content')
         };
     } else if (criteriaId == 1) {
         fromdate = $('#kycdate').data('daterangepicker').startDate.format('YYYY-MM-DD');
         todate = $('#kycdate').data('daterangepicker').endDate.format('YYYY-MM-DD');
         state = $("#state").val();
         searchcallstatus = $("#searchcallstatus").val()
         city = $("#city").val();
         usertype = $("#usertype").val();
         path = '/kyc/approvalexcelexport';
         status = $("#kycstatus").val();
         usercategory = "";
         input = {
             id: $('meta[name="fetchid"]').attr('content'),
             fromdate: fromdate,
             todate: todate,
             usertype: usertype,
             state: state,
             searchcallstatus: searchcallstatus,
             city: city,
             status: status,
             usercategory: usercategory,
             criteriaId: $('meta[name="criteria"]').attr('content')
         };

     } else if (criteriaId == 3) {
         var fromdate = $('#kycdate').data('daterangepicker').startDate.format('YYYY-MM-DD');
         var todate = $('#kycdate').data('daterangepicker').endDate.format('YYYY-MM-DD');
         state = $("#state").val();
         city = $("#city").val();
         usertype = $("#usertype").val();
         path = '/kyc/rejectedexcelexport';
         status = $("#searchcallstatus").val();
         usercategory = $("#searchusercategory").val();
         input = {
             id: $('meta[name="fetchid"]').attr('content'),
             fromdate: fromdate,
             todate: todate,
             usertype: usertype,
             state: state,
             city: city,
             status: status,
             usercategory: usercategory,
             criteriaId: $('meta[name="criteria"]').attr('content')
         };

     } else if (criteriaId == 6) {

         state = $("#state").val();
         city = $("#city").val();
         usertype = $("#usertype").val();
         packagename = $("#packagename").val()
         path = '/payment/walletrechargefailedexport';
         status = $("#searchcallstatus").val();
         input = {
             id: $('meta[name="fetchid"]').attr('content'),
             usertype: usertype,
             state: state,
             city: city,
             searchcallstatus: status,
             packagename: packagename,
             criteriaId: $('meta[name="criteria"]').attr('content')
         };

     } else if (criteriaId == 7) {

         state = $("#state").val();
         city = $("#city").val();
         packagename = $("#packagename").val()
         daystoexpiry = $("#daystoexpiry").val()
         path = '/payment/walletexpiryexport';
         status = $("#searchcallstatus").val();
         input = {
             id: $('meta[name="fetchid"]').attr('content'),
             packagename: packagename,
             state: state,
             city: city,
             searchcallstatus: status,
             daystoexpiry: daystoexpiry,
             criteriaId: $('meta[name="criteria"]').attr('content')
         };

     } else if (criteriaId == 8) {

         state = $("#state").val();
         city = $("#city").val();
         usertype = $("#usertype").val();
         path = '/payment/notmakepaymentexport';
         status = $("#searchcallstatus").val();
         input = {
             id: $('meta[name="fetchid"]').attr('content'),
             usertype: usertype,
             state: state,
             city: city,
             searchcallstatus: status,
             criteriaId: $('meta[name="criteria"]').attr('content')
         };

     } else if (criteriaId == 15 || criteriaId == 13 || criteriaId == 4 || criteriaId == 5) {

         if (criteriaId == 13) {
             fromdate = $('#postdate').data('daterangepicker').startDate.format('YYYY-MM-DD');
             todate = $('#postdate').data('daterangepicker').endDate.format('YYYY-MM-DD');
             path = '/useractivity/markedfavouriteexport';
             searchcallstatus = $("#searchcallstatus").val();
         } else if (criteriaId == 15) {
             path = '/useractivity/leadsnotviewexport';
             fromdate = $('#postdate').data('daterangepicker').startDate.format('YYYY-MM-DD');
             todate = $('#postdate').data('daterangepicker').endDate.format('YYYY-MM-DD');
             searchcallstatus = $("#searchcallstatus").val();
         } else if (criteriaId == 4 || criteriaId == 5) {
             path = '/post/postexport';
             fromdate = $('#fromdate').data('daterangepicker').startDate.format('YYYY-MM-DD');
             todate = $('#fromdate').data('daterangepicker').endDate.format('YYYY-MM-DD');
             status = $("#poststatus").val();
             searchcallstatus = $("#searchcallstatus").val();
         }

         state = $("#state").val();
         city = $("#city").val();
         posttype = $("#posttype").val();
         projectcategory = $("#projectcategory").val();
         usertype = $("#usertype").val();
         propertytype = $("#propertytype").val();
         propertycategory = $("#propertycategory").val();
         servicecategory = $("#Servicecategory").val();
         servicesubcategory = $("#servicesubcategory").val();
         input['id'] = $('meta[name="fetchid"]').attr('content');
         input['criteriaId'] = $('meta[name="criteria"]').attr('content');
         input['fromdate'] = fromdate;
         input['todate'] = todate;
         input['state'] = state;
         input['city'] = city;
         input['status'] = status;
         input['searchcallstatus'] = searchcallstatus;
         input['posttype'] = posttype;
         if (posttype == "PR") {
             $("#type").text('Project Category')
             input['projectcategory'] = projectcategory;
         } else if (posttype == "P") {
             $("#type").text('Property Category')
             input['usertype'] = usertype;
             input['propertytype'] = propertytype;
             input['propertycategory'] = propertycategory;

         } else if (posttype == "S") {
             $("#type").text('Service Category')
             input['servicecategory'] = servicecategory;
             input['servicesubcategory'] = servicesubcategory;
         }
     } else if (criteriaId == 10) {
         state = $("#state").val();

         searchcallstatus = $("#searchcallstatus").val()
         city = $("#city").val();
         usertype = $("#usertype").val();
         path = '/useractivity/nepexport';
         status = $("#kycstatus").val();
         input = {
             id: $('meta[name="fetchid"]').attr('content'),
             usertype: usertype,
             state: state,

             searchcallstatus: searchcallstatus,
             city: city,
             criteriaId: $('meta[name="criteria"]').attr('content')
         };

     } else if (criteriaId == 12) {
         state = $("#state").val();
         searchcallstatus = $("#searchcallstatus").val()

         lastsearchcategory = $("#lastsearchcategory").val()
         city = $("#city").val();
         usertype = $("#usertype").val();
         path = '/useractivity/npsexport';
         status = $("#kycstatus").val();
         input = {
             id: $('meta[name="fetchid"]').attr('content'),
             usertype: usertype,
             state: state,
             lastsearchedcategory: lastsearchcategory,
             searchcallstatus: searchcallstatus,
             city: city,
             criteriaId: $('meta[name="criteria"]').attr('content')
         };
     } else if (criteriaId == 14) {
         state = $("#state").val();
         searchcallstatus = $("#searchcallstatus").val()
         fromdate = $('#dates').data('daterangepicker').startDate.format('YYYY-MM-DD');
         todate = $('#dates').data('daterangepicker').endDate.format('YYYY-MM-DD');
         city = $("#city").val();
         usertype = $("#usertype").val();
         path = '/useractivity/inactiveexport';
         input = {
             id: $('meta[name="fetchid"]').attr('content'),
             usertype: usertype,
             state: state,
             searchcallstatus: searchcallstatus,
             city: city,
             fromdate: fromdate,
             todate: todate,
             criteriaId: $('meta[name="criteria"]').attr('content')
         };
     } else if (criteriaId == 11) {
         state = $("#state").val();
         searchcallstatus = $("#searchcallstatus").val()
         fromdate = $('#dates').data('daterangepicker').startDate.format('YYYY-MM-DD');
         todate = $('#dates').data('daterangepicker').endDate.format('YYYY-MM-DD');
         city = $("#city").val();
         usertype = $("#usertype").val();
         path = '/useractivity/notpostexport';
         input = {
             id: $('meta[name="fetchid"]').attr('content'),
             usertype: usertype,
             state: state,
             searchcallstatus: searchcallstatus,
             city: city,
             fromdate: fromdate,
             todate: todate,
             criteriaId: $('meta[name="criteria"]').attr('content')
         };
     } else {
         //nothing to do
         state = $("#state").val();
         city = $("#city").val();
         kycstatus = $("#kycstatus").val();
         usercategory = $("#searchusercategory").val()
         usertype = $("#usertype").val();
         searchcallstatus = $("#searchcallstatus").val();
         path = '/useractivity/upgradeuserexport';

         input = {
             id: $('meta[name="fetchid"]').attr('content'),

             searchcallstatus: searchcallstatus,
             usertype: usertype,
             state: state,
             city: city,
             status: kycstatus,
             usercategory: usercategory
         }
     }
     return $.ajax({
         "processing": true,
         "responsive": true,
         "url": path,
         "type": "POST",
         'headers': {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         data: input,
         success: function (data) {
             $("#exportexcel").attr("disabled", false);
             $("#exportexcel").text("Export to Excel");
             if (data == 0) {
                 Toast.fire({
                     icon: 'error',
                     title: "No data found"
                 });
             } else {
                 var a = document.createElement('a');
                 a.style.display = 'none';
                 url = data;
                 a.href = url;
                 split = data.split('/');
                 filename = split[split.length - 1];
                 a.download = filename;
                 document.body.appendChild(a);
                 a.click();
             }
         },
         error: function (error) {
             console.log("Error", error.messaga);
             $("#exportexcel").attr("disabled", false);
             $("#exportexcel").text("Export to Excel");
         }
     });


 });

 $("#poststatusaction").on('change', function (e) {
     var value = this.value;
     if (value == 2) {
         var input = new FormData();
         input.append('posttype', $("#posttype").val())
         $.ajax({
             type: 'post',
             url: '/post/getrejectionstatus',
             data: input,
             contentType: false,
             processData: false,
             success: function (result) {
                 var data = result.data;
                 $("#poststatusactionreason").empty();
                 $("#poststatusactionreason").append('<option value="">Select </option>')
                 $.each(data, function (key, item) {
                     $("#poststatusactionreason").append('<option value=' + item.id + '>' + item.reason + '</option>')
                 })
                 $("#poststatusactionreasondiv").css('display', 'block')
             },
             error: function (error) {
                 console.log(error.message)
             }
         });

     } else {
         $("#poststatusactionreasondiv").css('display', 'none')
     }
 })

 $("#kycstatusaction").on('change', function (e) {
     var value = this.value;
     if (value == 2) {
         var user_id = $("#kyc_user_id").val();
         input = new FormData();
         input.append('id', user_id)
         method = 'post';
         url = '/kyc/getrejectstatus';
         commonajaxcall(url, input, method).then(result => {
             var data = result.data;
             if (data.length == 0) {
                 $("#checkboxerror").text("Please reject document and try to update the status")
             } else {
                 $("#rejectedcheckbox").css("display", 'block');
                 $("#rejectedcheckbox").empty();
                 $.each(data, function (key, item) {
                     var pan_status = item.pan_status;
                     var aadhar_status = item.aadhar_status;
                     var voting_status = item.voting_status;
                     var gst_status = item.gst_status;
                     var certificate_status = item.certificate_status;
                     var driving_status = item.driving_status;
                     var cheque_status = item.cheque_status;
                     if (pan_status == 2) {
                         $("#rejectedcheckbox").append('<div class="form-check form-check-inline"><label class="form-check-label"><input type="checkbox" name="documenttype" id="pan" class="form-check-input documenttype">PAN Card <i class="input-frame"></i></label></div>')
                     }
                     if (aadhar_status == 2) {
                         $("#rejectedcheckbox").append('<div class="form-check form-check-inline"><label class="form-check-label"><input type="checkbox" name="documenttype" id="aadhar" class="form-check-input documenttype">Aadhar Card <i class="input-frame"></i></label></div>')
                     }
                     if (voting_status == 2) {
                         $("#rejectedcheckbox").append('<div class="form-check form-check-inline">' +
                             '<label class="form-check-label">' +
                             '<input type="checkbox" name="documenttype" id="voting"' +
                             'class="form-check-input documenttype">Voter ID' +
                             ' <i class="input-frame"></i></label></div>')
                     }
                     if (gst_status == 2) {
                         $("#rejectedcheckbox").append('<div class="form-check form-check-inline"><label class="form-check-label"><input type="checkbox" name="documenttype" id="gst" class="form-check-input documenttype">GST <i class="input-frame"></i></label></div>')

                     }
                     if (driving_status == 2) {
                         $("#rejectedcheckbox").append('<div class="form-check form-check-inline"><label class="form-check-label"><input type="checkbox" name="documenttype" id="driving" class="form-check-input documenttype">Driving License <i class="input-frame"></i></label></div>')

                     }
                     if (certificate_status == 2) {
                         $("#rejectedcheckbox").append('<div class="form-check form-check-inline"><label class="form-check-label"><input type="checkbox" name="documenttype" id="certificate" class="form-check-input documenttype">Certificate <i class="input-frame"></i></label></div>')

                     }
                     if (cheque_status == 2) {
                         $("#rejectedcheckbox").append('<div class="form-check form-check-inline"><label class="form-check-label"><input type="checkbox" name="documenttype" id="cheque" class="form-check-input documenttype">Cancelled Cheque <i class="input-frame"></i></label></div>')

                     }

                 })
                 // $(".documenttype").prop("checked", false)
                 $("#checkboxerror").text("")
             }

         }).catch(error => {
             console.log(error);
         });

     } else {
         $("#rejectedcheckbox").css("display", 'none');
         $(".reasonclass").css("display", "none");
         $(".documenttype").prop("checked", false)
         $("#checkboxerror").text("")
     }

 })
 $(document).on('change', '#selectcategory', function (e) {
     var value = this.value;
     $("#selectdocuments").empty();
     if (value == 1) {
         if (usertype == "C") {
             $("#selectdocuments").append('<option value="" data-select2-id="25">Select</option><option value="1" data-select2-id="40">PAN Card</option>')
         } else {
             $("#selectdocuments").append('<option value="" data-select2-id="25">Select</option><option value="1" data-select2-id="40">PAN Card</option>' +
                 '<option value="2" style="" data-select2-id="41">Aadhar Card</option><option value="3" style="" data-select2-id="42">Voter ID</option>' +
                 '<option value="5" style="" data-select2-id="44">Driving License</option>' +
                 '<option value="6" style="display: none;" data-select2-id="45">Cancelled Cheque</option>');
         }
     } else if (value == 2) {
         $("#selectdocuments").append('<option value="" data-select2-id="25">Select</option><option value="1" data-select2-id="40">PAN Card</option>' +
             '<option value="2" style="" data-select2-id="41">Aadhar Card</option><option value="3" style="" data-select2-id="42">Voter ID</option>' +
             '<option value="4" style="" data-select2-id="43">GST</option><option value="5" style="" data-select2-id="44">Driving License</option>' +
             '<option value="6" style="display: none;" data-select2-id="45">Cancelled Cheque</option>')


     } else if (value == 3) {
         $("#selectdocuments").append('<option value="" data-select2-id="25">Select</option><option value="1" data-select2-id="40">PAN Card</option>' +
             '<option value="4" style="" data-select2-id="43">GST</option>' +
             '<option value="6" style="display: none;" data-select2-id="45">Cancelled Cheque</option>');

     } else if (value == 4 || value == 5) {
         $("#selectdocuments").append('<option value="" data-select2-id="25">Select</option><option value="1" data-select2-id="40">PAN Card</option>' +
             '<option value="4" style="" data-select2-id="43">GST</option>' +
             '<option value="6" style="display: none;" data-select2-id="45">Cancelled Cheque</option><option value="7" style="display: none;" data-select2-id="46">Certificate</option>');
     }

     $("#selectdocuments").select2();
 })
 $(document).on('click', '.uploaddocument', function (e) {
     var id = this.id;
     var id = this.id;
     $("#documentuser_id").val(id);
     $("#selectdocuments").empty();
     $("#selectdocuments").append('<option value="" data-select2-id="25">Select</option><option value="1" data-select2-id="40">PAN Card</option>' +

         '<option value="2" style="" data-select2-id="41">Aadhar Card</option><option value="3" style="" data-select2-id="42">Voter ID</option><option value="4" style="" data-select2-id="43">GST</option><option value="5" style="" data-select2-id="44">Driving License</option>' +

         '<option value="6" style="display: none;" data-select2-id="45">Cancelled Cheque</option><option value="7" style="display: none;" data-select2-id="46">Certificate</option>');
     $("#selectdocuments").select2();
     var userinfo = CommonArr[CommonArr.map(function (item) {
         return item.id;
     }).indexOf(parseInt(id))];
     $("#documentnumberdiv").css("display", "block");
     $("#documentuser_type").val(userinfo.user_type)
     $("#errormessage").text("");
     $("#documentuploadform").trigger("reset")
     if (userinfo.category_id != 0) {
         $("#selectcategory").val(userinfo.category_id);
         // $("#selectcategory").attr("disabled", 'disabled');
         $("#selectcategory").select2();
         $("#selectdocuments").empty();

     } else {
         $("#selectcategory").prop("disabled", false);
     }
     $(".form-group").removeClass('has-danger');
     $(".text-danger").text('');
     $("#gallery-image").css('display', 'none');
     $("#gallery-image1").css('display', 'none');
     $("#backimage").attr('src', '');
     $("#frontimage").attr('src', '');
     $(".select2-selection").css('border-color', 'none');
     var usertype = $("#documentuser_type").val();
     var value = $("#selectcategory").val();
     if (value == 1) {
         if (usertype == "C") {
             $("#selectdocuments").append('<option value="" data-select2-id="25">Select</option><option value="1" data-select2-id="40">PAN Card</option>')
             // '<option value="6" style="display: none;" data-select2-id="45">Cancelled Cheque</option>');
         } else {
             $("#selectdocuments").append('<option value="" data-select2-id="25">Select</option><option value="1" data-select2-id="40">PAN Card</option>' +
                 '<option value="2" style="" data-select2-id="41">Aadhar Card</option><option value="3" style="" data-select2-id="42">Voter ID</option>' +
                 '<option value="5" style="" data-select2-id="44">Driving License</option>' +
                 '<option value="6" style="display: none;" data-select2-id="45">Cancelled Cheque</option>');
         }
     } else if (value == 2) {
         $("#selectdocuments").append('<option value="" data-select2-id="25">Select</option><option value="1" data-select2-id="40">PAN Card</option>' +
             '<option value="2" style="" data-select2-id="41">Aadhar Card</option><option value="3" style="" data-select2-id="42">Voter ID</option>' +
             '<option value="4" style="" data-select2-id="43">GST</option><option value="5" style="" data-select2-id="44">Driving License</option>' +
             '<option value="6" style="display: none;" data-select2-id="45">Cancelled Cheque</option>')


     } else if (value == 3) {
         $("#selectdocuments").append('<option value="" data-select2-id="25">Select</option><option value="1" data-select2-id="40">PAN Card</option>' +
             '<option value="4" style="" data-select2-id="43">GST</option>' +
             '<option value="6" style="display: none;" data-select2-id="45">Cancelled Cheque</option>');

     } else if (value == 4 || value == 5) {
         $("#selectdocuments").append('<option value="" data-select2-id="25">Select</option><option value="1" data-select2-id="40">PAN Card</option>' +
             '<option value="4" style="" data-select2-id="43">GST</option>' +
             '<option value="6" style="display: none;" data-select2-id="45">Cancelled Cheque</option><option value="7" style="display: none;" data-select2-id="46">Certificate</option>');
     }

     $("#selectdocuments").select2();

     $("#uploaddocumentmodal").modal({
         backdrop: 'static',
         keyboard: false
     });

 });

 $("#selectdocuments").on('change', function (e) {
     var value = this.value;
     if (value == 6 || value == 7) {
         $("#documentnumberdiv").css("display", "none");
     } else {
         $("#documentnumberdiv").css("display", "block");
     }
 });
 $(document).on('click', ".viewdocument", function (e) {
     var id = this.id;
     var userinfo = CommonArr[CommonArr.map(function (item) {
         return item.id;
     }).indexOf(parseInt(id))];
     $("#username_type").text(userinfo.firstname + ' ' + userinfo.lastname + ' - ' + userinfo.usertype);
     var input = new FormData();
     input.append('id', id);
     input.append('criteriaId', $('meta[name="criteria"]').attr('content'));
     var url = '/kyc/getkycdocument';
     var method = "POST";
     commonajaxcall(url, input, method).then(result => {
         var data = result.data;
         $("#documentbody").empty();
         $("#documentbody").append(data)
         feather.replace();
         $("#exampleModal").modal({
             keyboard: false,
             backdrop: 'static'
         });

     }).catch(error => {
         console.log(error);
     });
 });

 $(document).on('click', '.approve', function (e) {
     var id = this.id;
     var splitid = id.split("_");
     var classvalue = splitid[0] + '_' + splitid[1];
     var input = new FormData();
     input.append('id', id);
     input.append('criteriaId', $('meta[name="criteria"]').attr('content'));
     var url = '/kyc/approverejectkycdoc';
     var method = "post";
     confirmationalert("Do you want to approve the document", "Approve",
         "Cancel", 'Approve', url, input, method, classvalue)
 });

 $(document).on('click', '.reject', function (e) {
     var id = this.id;
     var input = new FormData();
     var splitid = id.split("_");
     var classvalue = splitid[0] + '_' + splitid[1];
     input.append('id', id);
     input.append('criteriaId', $('meta[name="criteria"]').attr('content'));
     var url = '/kyc/approverejectkycdoc';
     var method = "post";
     confirmationalert("Do you want to reject the document", "Reject", "Cancel", 'Reject', url, input, method, classvalue);
 });

 $(document).on('click', '.download', function (e) {
     var id = this.id;
     var input = new FormData();
     var url = '/kyc/docdownload';
     input.append('id', id);
     input.append('criteriaId', $('meta[name="criteria"]').attr('content'));
     var method = "post";
     commonajaxcall(url, input, method).then(function (result) {
         var a = document.createElement('a');
         a.style.display = 'none';
         a.href = result.url;
         a.download = result.filename;
         a.target = "_blank";
         document.body.appendChild(a);
         a.click();
         a.remove();
     }).catch(function (error) {
         console.log(error);
     });
 });


 $(document).on('click', '.view', function (e) {
     var id = this.id;
     var input = new FormData();
     var url = '/kyc/docdownload';
     input.append('id', id);
     input.append('criteriaId', $('meta[name="criteria"]').attr('content'));
     var method = "post";
     commonajaxcall(url, input, method).then(function (result) {
         $("#viewimage").attr("src", result.url);
         $("#documentnodetailview").text(result.documentNo);
         $("#viewdoc").modal({
             keyboard: false,
             backdrop: 'static'
         });
     }).catch(function (error) {
         console.log(error);
     });
 });

 $(document).on('click', '.postdetailview', function (e) {
     var posttype = $("#posttype").val();
     var id = this.id;
     var input = new FormData();
     input.append('id', id);
     input.append('posttype', posttype);
     if (posttype == "PR") {
         input.append('projectcategory', $("#projectcategory").val());
     }
     var url = '/getdetailview';
     var method = "POST";
     commonajaxcall(url, input, method).then(result => {
         var data = result;
         $("#postdetailview").empty()
         $("#postdetailview").append(data)
         imageview();
         $("#postdetailview").modal({
             keyboard: false,
             backdrop: 'static'
         });

     }).catch(error => {
         console.log(error);
     });

 });
