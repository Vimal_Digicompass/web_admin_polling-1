common_load_data('onload');

function common_load_data(type) {
    var fromdate = "";
    var todate = "";
    if (type == "onload") {
        fromdate = moment().subtract(7, 'd').format('YYYY-MM-DD');
        todate = moment().format('YYYY-MM-DD');
    } else {
        fromdate = $('#dashboarddate').data('daterangepicker').startDate.format('YYYY-MM-DD');
        todate = $('#dashboarddate').data('daterangepicker').endDate.format('YYYY-MM-DD');
    }

    load_data(fromdate, todate)
}

function load_data(fromdate, todate) {
 $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
 });
    var input = new FormData();
    input.append('fromdate', fromdate);
    input.append('todate', todate)
    $.ajax({
        type: 'post',
        url: '/getdashboarddata',
        data: input,
        contentType: false,
        processData: false,
        success: function (result) {
            $("#dashboardsearch").text("Search");
            $("#dashboardappend").empty();
            $("#dashboardappend").append(result);
        },
        error: function (error) {
            $("#dashboardsearch").text("Search");
            console.log(error.message)
        }
    });
}
$("#dashboardsearch").on('click', function () {

    $("#dashboardsearch").text("Processing...");
    common_load_data("click");
});

function myFunction() {
    var input, filter, cards, cardContainer, h5, title, i;
    input = document.getElementById("myFilter");
    filter = input.value.toUpperCase();
    cardContainer = document.getElementById("myItems");
    cards = cardContainer.getElementsByClassName("col-space");
    for (i = 0; i < cards.length; i++) {
        title = cards[i].querySelector(".card-top h5.card-title");
        if (title.innerText.toUpperCase().indexOf(filter) > -1) {
            cards[i].style.display = "";
        } else {
            cards[i].style.display = "none";
        }
    }
}
