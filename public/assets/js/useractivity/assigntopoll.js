$(document).on('change', '#teamlead', function () {
    var value = this.value;
    var input = new FormData();
    input.append('user_id', value);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "post",
        url: 'getuser',
        data: input,
        contentType: false,
        processData: false,
        success: function (result) {
            $("#user").empty();
            $("#permission").empty();
            userList = result.data;
            $("#user").append("<option value=''>Select</option>");
             $("#permission").append("<option value=''>Select</option>");
            $.each(userList, function (key, item) {
                $("#user").append("<option value=" + item.user_id + ">" + item.username + "</option>");
            })
        },
        error: function (error) {

            console.log(error.message)
        }
    });
});

$(document).on('change', '#user', function () {
    var value = this.value;
    var input = new FormData();
    input.append('user_id', value);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "post",
        url: 'getpermission',
        data: input,
        contentType: false,
        processData: false,
        success: function (result) {
            $("#permission").empty();
            permissionList = result.data;
            $("#permission").append("<option value=''>Select</option>");
            $.each(permissionList, function (key, item) {
                $("#permission").append("<option value=" + item.criteria_id + ">" + item.criteria_name+ "</option>");
            })
        },
        error: function (error) {

            console.log(error.message)
        }
    });
});
