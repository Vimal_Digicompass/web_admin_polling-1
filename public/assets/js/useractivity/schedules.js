$(function () {

    $('#form_edit_schedule').on('submit', function (event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        postData = new FormData($('#form_edit_schedule')[0]);
        $.ajax({
            method: "POST",
            url: "/users/storeandedit",
            data: postData,
            contentType: false,
            processData: false,
            success: function (response) {
                $('#editSchedule').modal('hide');
                onFilterFormSubmit();
            },
            error: function (response) {

                onGettingSchedulesFailure(data)
            }
        });
    })

    function onGettingSchedulesSuccess(data) {
        if ($.fn.DataTable.isDataTable('#table_criteria_schedule')) {
            $('#table_criteria_schedule').DataTable().destroy();
        }
        var htmlContent = "";
        var criteria_name = "";
        var city = "";
        schedules = data.schedules;

        if (Object.keys(data.schedules).length == 0) {
            htmlContent += (`<tr><td colspan="5">No schedules for the given filters.</td></tr>`);
        } else {
            data.schedules.forEach(element => {
                if (element.criteriamaster != null) {
                    criteria_name = element.criteriamaster.criteria_name;
                }
                if (element.city != null) {
                    city = element.city.title;
                }
                htmlContent += (`<tr>
                <td>${criteria_name}</td>
                <td>${city}</td>
                <td>${element.rule_name}</td>
                <td><button
                type="button" id="${element.poll_timing_id}_edit_schedule_document_id"
                class = "btn btn-sm btn-outline-primary btn-edit-schedule" > <i class = "fa fa-edit"></i>
                Edit
                </button></td>
                </tr>`);
            });
        }

        $('#table_criteria_schedule_body').empty().append(htmlContent);
        $('#table_criteria_schedule').DataTable({
            "aLengthMenu": [
                [10, 30, 50],
                [10, 30, 50]
            ],
            "iDisplayLength": 10,
            "language": {
                search: "",
                "searchPlaceholder": "search",
            },
            dom: 'lBfrtip'
        });

    }

    function onGettingSchedulesFailure(data) {
        Toast.fire({
            icon: 'error',
            title: error.message
        });
    }

    function onFilterFormSubmit() {
        callUrl = "/users/getScheduleDetail",
            postData = new FormData($('#form_schedules_filter')[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: callUrl,
            method: 'POST',
            data: postData,
            contentType: false,
            processData: false,
            success: function (response) {
                onGettingSchedulesSuccess(response);
                Toast.fire({
                    icon: 'success',
                    title: response.message
                });
            },
            error: function (data) {
                onGettingSchedulesFailure(data);
            }
        });
    }

    function onAddFormSubmit() {
        postData = new FormData($('#form_schedules_filter')[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "/users/storeandedit",
            method: 'POST',
            data: postData,
            contentType: false,
            processData: false,
            success: function (response) {

                if (response.code == 201) {
                    return Toast.fire({
                        icon: 'error',
                        title: response.message
                    });
                } else {
                    onFilterFormSubmit();
                    Toast.fire({
                        icon: 'success',
                        title: response.message
                    });

                }

            },
            error: function (data) {
                onGettingSchedulesFailure(data)

            }
        });
    }

    $('#submit-btn').on('click', function (event) {
        event.preventDefault();
        $('#table_criteria_schedule_body').empty()
        onFilterFormSubmit();
    });
    $('#add-btn').on('click', function (event) {
        event.preventDefault();
        $('#table_criteria_schedule_body').empty()
        onAddFormSubmit();
    });
    onFilterFormSubmit();

});

$(document).on('click', '.btn-edit-schedule', function (e) {
    selectedScheduleId = ((this.id).split("_"))[0];
    $("#id").val(selectedScheduleId);
    $('#editSchedule').modal({
        backdrop: 'static',
        keyboard: false
    });
});
