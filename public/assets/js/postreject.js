$("#posttype").on('change', function(e){
    var value = this.value
    if(value == "service"){
        $('.property').hide();
        $('.project').hide();
        $('.service').show();
        $("#type").text('Service Category')
    }else if(value == "project"){
        $('.property').hide();
        $('.project').show();
        $('.service').hide();
        $("#type").text('Project Type')
    }else{
        $('.property').show();
        $('.project').hide();
        $('.service').hide();
         $("#type").text('Property Type')
    }

})
